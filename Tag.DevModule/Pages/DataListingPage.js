var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var searchkeyword = Observable("");
var receivedDataList = Observable();
var dataPage = 0;

function onViewActivate() {
    console.log(receivedDataList.value)

    if (!receivedDataList.value) {
        dataPage = 0;
        getLatestDataList();
    }
}


function pagingLoadMore() {
    if (receivedDataList.length != 0) {
        getLatestDataList();
    }
}

function searchIconClickHandler() {
    dataPage = 0;
    receivedDataList.clear();
    getLatestDataList();
}

function getLatestDataList() {
    busy.activate();

    dataPage ++;

    var apiEndpoint = "https://newsapi.org/v2/top-headlines?apiKey=0c48ce67aeeb4b938c52c5a4bf4c331c&page="+dataPage +"&pageSize=20&sources=bbc-news,crypto-coins-news,cnbc,time";
    if (searchkeyword.value != "") {
        apiEndpoint += "&q=" + searchkeyword.value;
    }

    _WebService.requestExternal(apiEndpoint).then(function (result) {
        gotLatestData(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotLatestData(result) {
    console.log("gotLatestData")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "ok") {
        var resultArr = arr.articles;
        if (resultArr !== null) {
            receivedDataList.addAll(resultArr);
        }
    }

}

function itemClickHandler(args) {
    console.log("itemClickHandler", JSON.stringify(args.data));
    router.push("dataDetailsPage", { item: args.data, datId: Math.random() });
}


function showNetworkErrorMessage() {
    loadingAnimShow.value = false;

    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivate: onViewActivate,
    receivedDataList: receivedDataList,
    searchkeyword: searchkeyword,
    searchIconClickHandler: searchIconClickHandler,
    pagingLoadMore: pagingLoadMore,
    itemClickHandler: itemClickHandler,
}