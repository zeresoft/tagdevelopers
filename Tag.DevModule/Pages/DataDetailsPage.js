var Observable = require('FuseJS/Observable');
var moment = require('Utils/moment');

var title = Observable("");
var author = Observable("");
var description = Observable("");
var imageUrl = Observable("");
var publishedAt = Observable("");

this.Parameter.onValueChanged(module, function (param) {
    var item = param.item;
    title.value = item.title;
    author.value = item.source.name;
    description.value = item.description;
    imageUrl.value = item.urlToImage;
    publishedAt.value = moment(item.publishedAt).format('D MMM YYYY hh:mm a');
});

module.exports = {
    title: title,
    author: author,
    description: description,
    imageUrl: imageUrl,
    publishedAt: publishedAt,
}