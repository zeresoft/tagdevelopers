<MerchantShopPage ux:Template="merchantShopPage" router="router" />
<MerchantShopDetailsPage ux:Template="merchantShopDetailsPage" router="router" />
<InventoryPage ux:Template="inventoryPage" router="router" />
<InventoryCreatePage ux:Template="inventoryCreatePage" router="router" />
<UserShoppingPage ux:Template="userShoppingPage" router="router"/>
<UserShopDetailsPage ux:Template="userShopDetailsPage" router="router"/>
<UserProductDetailsPage ux:Template="userProductDetailsPage" router="router"/>
<FavoriteListPage ux:Template="favoriteListPage" router="router" />
<CheckOutPage ux:Template="checkOutPage" router="router" />
<CheckOutAddressPage ux:Template="checkOutAddressPage" router="router" />
<CheckOutOrderPage ux:Template="checkOutOrderPage" router="router" />
<CheckOutMessagePage ux:Template="checkOutMessagePage" router="router" />