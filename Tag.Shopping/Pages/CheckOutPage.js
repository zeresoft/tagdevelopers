
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var dataLoadingComplete = Observable(false);

var shopId;

var shopTitle = Observable("");

var orderList = Observable();
var currency = Observable("");
var subTotal = Observable(0);
var totalItems = Observable();

this.Parameter.onValueChanged(module, function (param) {
    dataLoadingComplete.value = false;

    console.log("CheckoutPageChanged", JSON.stringify(param));
    console.log("CheckoutPageChanged", JSON.stringify(param.shop_id));

    currency.value = "";
    subTotal.value = 0;
    totalItems.value = 0;

    if (param.shop_id != null) {
        shopId = param.shop_id;
        shopTitle.value = param.shop_title;

        getShoppingCartItems();
    }
});

function getShoppingCartItems() {
    console.log("getShoppingCartItems");
    orderList.clear();

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("shop/Cart", apiBodyObj).then(function (result) {
        gotShoppingCartItems(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotShoppingCartItems(result) {
    console.log("gotShoppingCartItems");
    dataLoadingComplete.value = true;

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr !== null) {
            orderList.addAll(resultArr);
            doCalculation();
        }
    }
}

function doCalculation() {
    currency.value = "";

    subTotal.value = 0;
    totalItems.value = 0;

    orderList.forEach(function (item) {
        currency.value = item.currency_code;
        totalItems.value += item.qty;

        var itemTotal = item.qty * item.price;
        subTotal.value += itemTotal;
    });
}

function deleteFromCartClickHandler(args) {
    console.log("deleteFromCartClickHandler", JSON.stringify(args.data));

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("shop/RemoveCartItem", apiBodyObj).then(function (result) {
        console.log("deleteFromCartResult");

        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr));

        if (arr.status == "success") {
            orderList.remove(args.data);
            doCalculation();
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}


function saveForLaterClickHandler(args) {
    console.log("saveForLaterClickHandler", JSON.stringify(args.data));

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("shop/RemoveCartItem", apiBodyObj).then(function (result) {
        console.log("deleteFromCartResult");

        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr));

        if (arr.status == "success") {
            favoriteAddHandler(args.data.inventory_id);

            orderList.remove(args.data);
            doCalculation();
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function favoriteAddHandler(productId) {
    console.log("favoriteAddHandler");
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.product_id = productId;

    _WebService.request("shop/AddFavorite", apiBodyObj).then(function (result) {
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {

        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function productClickHandler(args) {
    console.log("productClickHandler", JSON.stringify(args));
    var product_details = args.data;
    router.push("userProductDetailsPage", { product_id: product_details.inventory_id, shop_id: shopId, shop_title: shopTitle.value, datId: Math.random() });
}

function orderContinueHandler() {
    router.push("checkOutAddressPage", { shop_id: shopId, shop_title: shopTitle.value, currency: currency.value, subTotal: subTotal.value, totalItems: totalItems.value, datId: Math.random() });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    dataLoadingComplete: dataLoadingComplete,
    shopTitle: shopTitle,
    orderList: orderList,
    currency: currency,
    subTotal: subTotal,
    totalItems: totalItems,

    productClickHandler: productClickHandler,
    deleteFromCartClickHandler: deleteFromCartClickHandler,
    saveForLaterClickHandler: saveForLaterClickHandler,

    orderContinueHandler: orderContinueHandler,
}