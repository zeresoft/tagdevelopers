var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var purchaseHistory = Observable();

function onViewActivatePurchaseHistory() {
    getPurchaseHistory();
}

function getPurchaseHistory() {
    console.log("getPurchaseHistory");
    purchaseHistory.clear();
    busy.activate();

    _WebService.request("shop/UserOrder").then(function (result) {
        gotPurchaseHistory(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotPurchaseHistory(result) {
    console.log("gotPurchaseHistory")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var orders = result.list;
        if (orders !== null) {
            var resultArrGroup = groupBy(orders, 'transaction_id');
            console.log("----------------------------------------")
            console.log(JSON.stringify(resultArrGroup))

            var resultArrProcessed = [];

            for (var key in resultArrGroup) {
                var item = resultArrGroup[key];

                var transactionObj = {};
                transactionObj.transaction_id = item[0].transaction_id;
                transactionObj.orderDate = item[0].orderDate;
                transactionObj.shop_name = item[0].shop_name;

                transactionObj.delivery_status = item[0].delivery_status;
                transactionObj.shipping_address = item[0].shipping_address;
                transactionObj.shipping_option = item[0].shipping_option;
                transactionObj.currency_code = item[0].currency_code;

                transactionObj.shipping_progress = item[0].shipping_progress;

                transactionObj.orderData = item;

                var totalBill = 0;
                for (var i = 0; i < item.length; i++) {
                   totalBill += item[i].amount;
                }
                transactionObj.total_amount = totalBill;

                resultArrProcessed.push(transactionObj);
            }

            var resultArrProcessedReverse = resultArrProcessed.reverse();
            purchaseHistory.addAll(resultArrProcessedReverse);
        }
    }
}

var groupBy = function (xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivatePurchaseHistory: onViewActivatePurchaseHistory,
    purchaseHistory: purchaseHistory

}