
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var dataLoadingComplete = Observable(false);

var shopId;
var orders = [];

var shopTitle = Observable("");

var orderList = Observable();
var currency = Observable("");
var subTotal = Observable(0);
var deliveryCharges = Observable(0);
var total = Observable();
var totalItems = Observable();

var confirmationPopupShow = Observable(false);
var orderConfirmationPopupShow = Observable(false);


this.Parameter.onValueChanged(module, function (param) {
    dataLoadingComplete.value = false;

    console.log("CheckoutPageChanged", JSON.stringify(param));
    console.log("CheckoutPageChanged", JSON.stringify(param.shop_id));

    currency.value = "";
    subTotal.value = 0;
    deliveryCharges.value = 0;
    total.value = 0;
    totalItems.value = 0;

    if (param.shop_id != null) {
        // confirmationPopupShow.value = false;
        // orderConfirmationPopupShow.value = false;

        shopId = param.shop_id;
        shopTitle.value = param.shop_title;

        getShoppingCartItems();

    } else {

    }
});

function getShoppingCartItems() {
    console.log("getShoppingCartItems");
    orderList.clear();

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("shop/Cart", apiBodyObj).then(function (result) {
        gotShoppingCartItems(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotShoppingCartItems(result) {
    console.log("gotShoppingCartItems");
    dataLoadingComplete.value = true;

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr !== null) {
            orderList.addAll(resultArr);

            getAddressList();
            doCalculation();

        }
    }
}

function doCalculation() {
    currency.value = "";

    subTotal.value = 0;
    deliveryCharges.value = 0;
    total.value = 0;
    totalItems.value = 0;

    orderList.forEach(function (item) {
        currency.value = item.currency_code;
        totalItems.value += item.qty;

        var itemTotal = item.qty * item.price;
        // var itemdelivery = item.delivery_charges;

        subTotal.value += itemTotal;
        deliveryCharges.value += itemdelivery;

    });
    total.value = subTotal.value + deliveryCharges.value;
}

function deleteFromCartClickHandler(args) {
    console.log("deleteFromCartClickHandler", JSON.stringify(args.data));

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("shop/RemoveCartItem", apiBodyObj).then(function (result) {
        console.log("deleteFromCartResult");

        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr));

        if (arr.status == "success") {
            orderList.remove(args.data);
            doCalculation();
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function productClickHandler(args) {
    console.log("productClickHandler", JSON.stringify(args));
    var product_details = args.data;
    router.push("userProductDetailsPage", { product_id: product_details.inventory_id, shop_id: shopId, shop_title: shopTitle.value, datId: Math.random() });
}

var editAddAddressPopupShow = Observable(false);
var firstnameInput = Observable("");
var lastnameInput = Observable("");
var phoneInput = Observable("");
var addressInput = Observable("");
var cityInput = Observable("");
var postalCodeInput = Observable("");
var postalCodeInput = Observable("");

var addressDetail = Observable();

var addressList = Observable();
var addressSelection = Observable(-1);
var editAddressId = "";

function getAddressList() {
    console.log("getAddressList");
    addressList.clear();
    addressSelection.value = -1;
    editAddressId = "";

    busy.activate();

    _WebService.request("shop/GetAddress").then(function (result) {
        gotGetAddressResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotGetAddressResult(result) {
    console.log("gotGetAddressResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        var blankAddress = {};
        resultArr.push(blankAddress);

        addressList.addAll(resultArr);
        addressSelection.value = resultArr[0].id;
    }
}

function selectAddressHandler(args) {
    console.log(JSON.stringify(args.data));

    addressSelection.value = args.data.id;
}

function addAddressHandler() {
    editAddressId = "";
    firstnameInput.value = "";
    lastnameInput.value = "";
    phoneInput.value = "";
    addressInput.value = "";
    cityInput.value = "";
    postalCodeInput.value = "";

    editAddAddressPopupShow.value = true;
}

function editAddressHandler(args) {
    console.log("editAddressHandler");
    console.log(JSON.stringify(args.data));

    var addressDataSel = args.data;
    editAddressId = addressDataSel.id;

    firstnameInput.value = addressDataSel.first_name;
    lastnameInput.value = addressDataSel.last_name;
    phoneInput.value = addressDataSel.phone;
    addressInput.value = addressDataSel.address;
    cityInput.value = addressDataSel.city;
    postalCodeInput.value = addressDataSel.postal_code;

    editAddAddressPopupShow.value = true;
}

function saveAddressHandler() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.firstname = firstnameInput.value;
    apiBodyObj.lastname = lastnameInput.value;
    apiBodyObj.phone = phoneInput.value;
    apiBodyObj.address = addressInput.value;
    apiBodyObj.city = cityInput.value;
    apiBodyObj.postal_code = postalCodeInput.value;

    var apiUrl = "";
    if (editAddressId == "") {
        apiUrl = "shop/AddAddress";
    } else {
        apiBodyObj.id = editAddressId;
        apiUrl = "shop/UpdateAddress";
    }

    _WebService.request(apiUrl, apiBodyObj).then(function (result) {
        gotSaveAddressResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotSaveAddressResult(result) {
    console.log("gotSaveAddressResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        getAddressList();
        editAddAddressPopupShow.value = false;
    }

}

function deleteAddressHandler(args) {
    console.log("deleteAddressHandler");
    console.log(JSON.stringify(args.data));

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("shop/DeleteAddress", apiBodyObj).then(function (result) {
        gotDeleteAddressResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotDeleteAddressResult(result) {
    console.log("gotDeleteAddressResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        getAddressList();
    }
}


function orderConfirmationHandler() {
    console.log("orderConfirmationHandler-------------------");
    if (addressSelection.value <= 0) {
        tagEvents.emit("toastShow", { message: 'Please select address' });
        return false;
    }

    confirmationPopupShow.value = true;
}

function cancelPlaceOrderClickHandler() {
    console.log("cancelPlaceOrderClickHandler");
    confirmationPopupShow.value = false;
}

function placeOrderClickHandler() {
    cancelPlaceOrderClickHandler();

    console.log("placeOrderClickHandler");

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;
    apiBodyObj.address_id = addressSelection.value;

    _WebService.request("shop/order", apiBodyObj).then(function (result) {
        gotOrderPlaceResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotOrderPlaceResult(result) {
    console.log("gotOrderPlaceResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        orderList.clear();
        addressDetail.clear();
        orderConfirmationPopupShow.value = true;
    }
}

function orderConfirmationCloseHandler() {
    console.log("orderConfirmationCloseHandler");
    orderConfirmationPopupShow.value = false;
    router.goto("userShoppingPage");

    // router.push("userShopDetailsPage", { shop_id: shopId, datId: Math.random() });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    dataLoadingComplete: dataLoadingComplete,
    orderList: orderList,
    currency: currency,
    subTotal: subTotal,
    deliveryCharges: deliveryCharges,
    total: total,
    totalItems: totalItems,

    productClickHandler: productClickHandler,
    deleteFromCartClickHandler: deleteFromCartClickHandler,

    addressDetail: addressDetail,
    firstnameInput: firstnameInput,
    lastnameInput: lastnameInput,
    phoneInput: phoneInput,
    addressInput: addressInput,
    cityInput: cityInput,
    postalCodeInput: postalCodeInput,
    selectAddressHandler: selectAddressHandler,
    addAddressHandler: addAddressHandler,
    saveAddressHandler: saveAddressHandler,
    editAddressHandler: editAddressHandler,
    deleteAddressHandler: deleteAddressHandler,
    editAddAddressPopupShow: editAddAddressPopupShow,


    addressList: addressList,
    addressSelection: addressSelection,


    placeOrderClickHandler: placeOrderClickHandler,
    cancelPlaceOrderClickHandler: cancelPlaceOrderClickHandler,
    orderConfirmationHandler: orderConfirmationHandler,
    orderConfirmationCloseHandler: orderConfirmationCloseHandler,




    confirmationPopupShow: confirmationPopupShow,
    orderConfirmationPopupShow: orderConfirmationPopupShow,
}