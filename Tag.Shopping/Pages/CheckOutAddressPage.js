
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var shopId;
var shopTitle = Observable("");

var currency = Observable("");
var subTotal = Observable(0);
var totalItems = Observable();


var editAddAddressPopupShow = Observable(false);
var firstnameInput = Observable("");
var lastnameInput = Observable("");
var phoneInput = Observable("");
var addressInput = Observable("");
var cityInput = Observable("");
var postalCodeInput = Observable("");
var postalCodeInput = Observable("");

var addressList = Observable();
var addressSelection = Observable(-1);
var editAddressId = "";

this.Parameter.onValueChanged(module, function (param) {
    console.log("CheckoutPageChanged", JSON.stringify(param));
    console.log("CheckoutPageChanged", JSON.stringify(param.shop_id));

    currency.value = "";
    subTotal.value = 0;
    totalItems.value = 0;

    if (param.shop_id != null) {
        shopId = param.shop_id;
        shopTitle.value = param.shop_title;

        currency.value = param.currency;
        subTotal.value = param.subTotal;
        totalItems.value = param.totalItems;

        getAddressList();
    } 
});

function getAddressList() {
    console.log("getAddressList");
    addressList.clear();
    addressSelection.value = -1;
    editAddressId = "";

    busy.activate();

    _WebService.request("shop/GetAddress").then(function (result) {
        gotGetAddressResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotGetAddressResult(result) {
    console.log("gotGetAddressResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        var blankAddress = {};
        resultArr.push(blankAddress);

        addressList.addAll(resultArr);
        addressSelection.value = resultArr[0].id;
    }
}

function selectAddressHandler(args) {
    console.log(JSON.stringify(args.data));

    addressSelection.value = args.data.id;
}

function addAddressHandler() {
    editAddressId = "";
    firstnameInput.value = "";
    lastnameInput.value = "";
    phoneInput.value = "";
    addressInput.value = "";
    cityInput.value = "";
    postalCodeInput.value = "";

    editAddAddressPopupShow.value = true;
}

function editAddressHandler(args) {
    console.log("editAddressHandler");
    console.log(JSON.stringify(args.data));

    var addressDataSel = args.data;
    editAddressId = addressDataSel.id;

    firstnameInput.value = addressDataSel.first_name;
    lastnameInput.value = addressDataSel.last_name;
    phoneInput.value = addressDataSel.phone;
    addressInput.value = addressDataSel.address;
    cityInput.value = addressDataSel.city;
    postalCodeInput.value = addressDataSel.postal_code;

    editAddAddressPopupShow.value = true;
}

function saveAddressHandler() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.firstname = firstnameInput.value;
    apiBodyObj.lastname = lastnameInput.value;
    apiBodyObj.phone = phoneInput.value;
    apiBodyObj.address = addressInput.value;
    apiBodyObj.city = cityInput.value;
    apiBodyObj.postal_code = postalCodeInput.value;

    var apiUrl = "";
    if (editAddressId == "") {
        apiUrl = "shop/AddAddress";
    } else {
        apiBodyObj.id = editAddressId;
        apiUrl = "shop/UpdateAddress";
    }

    _WebService.request(apiUrl, apiBodyObj).then(function (result) {
        gotSaveAddressResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotSaveAddressResult(result) {
    console.log("gotSaveAddressResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        getAddressList();
        editAddAddressPopupShow.value = false;
    }

}

function deleteAddressHandler(args) {
    console.log("deleteAddressHandler");
    console.log(JSON.stringify(args.data));

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("shop/DeleteAddress", apiBodyObj).then(function (result) {
        gotDeleteAddressResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotDeleteAddressResult(result) {
    console.log("gotDeleteAddressResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));
    
    if (arr.status == "success") {
        getAddressList();
    }
}

function orderBackHandler() {
    router.goBack();
}

function orderContinueHandler() {
    console.log(addressSelection.value);
    if(!addressSelection.value){
        tagEvents.emit("alertEvent", { type: "info", title: "SHIPPING ADDRESS", message: "Please select a shipping address." });
    }else{
        router.push("checkOutOrderPage", { shop_id: shopId, shop_title: shopTitle.value, currency: currency.value, subTotal: subTotal.value, totalItems: totalItems.value, address_id: addressSelection.value, datId: Math.random() });
    }
}



function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    shopTitle: shopTitle,
    currency: currency,
    subTotal: subTotal,
    totalItems: totalItems,

    addressList: addressList,
    addressSelection: addressSelection,

    firstnameInput: firstnameInput,
    lastnameInput: lastnameInput,
    phoneInput: phoneInput,
    addressInput: addressInput,
    cityInput: cityInput,
    postalCodeInput: postalCodeInput,

    selectAddressHandler: selectAddressHandler,
    addAddressHandler: addAddressHandler,
    saveAddressHandler: saveAddressHandler,
    editAddressHandler: editAddressHandler,
    deleteAddressHandler: deleteAddressHandler,
    editAddAddressPopupShow: editAddAddressPopupShow,

    orderBackHandler: orderBackHandler,
    orderContinueHandler: orderContinueHandler,
}