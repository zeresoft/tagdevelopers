var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var searchkeyword = Observable("");
var favoriteList = Observable();

var listView = Observable(true);
var gridView = Observable(false);

function onViewActivateFavoriteList() {
    console.log("onViewActivate");

    getFavoriteProductList();
}

function searchIconClickHandler() {
    getFavoriteProductList();
}

function getFavoriteProductList() {
    console.log("getFavoriteProductList");
    favoriteList.clear();
    busy.activate();

    var apiBodyObj = {};

    if (_model.merchantLinked != "") {
        apiBodyObj.merchant_id = _model.merchantLinked;
    }
    
    if (searchkeyword.value != "") {
        apiBodyObj.name = searchkeyword.value;
    }

    _WebService.request("shop/FavoriteList", apiBodyObj).then(function (result) {
        gotFavoriteProductList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotFavoriteProductList(result) {
    console.log("gotFavoriteProductList")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;
        
        if (resultArr !== null) {
            favoriteList.addAll(resultArr);
        }
    }
}

function favoriteClickHandler(args) {
    console.log("favoriteClickHandler", JSON.stringify(args.data));
    var product_details = args.data;

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.id = product_details.id;

    _WebService.request("shop/RemoveFavourite", apiBodyObj).then(function (result) {
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {
            favoriteList.remove(args.data);
        }

    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function productClickHandler(args) {
    console.log("productClickHandler", JSON.stringify(args));
    var product_details = args.data;

    router.push("userProductDetailsPage", { product_id: product_details.product_id, shop_id: product_details.shop_id, shop_title: product_details.shop_name, datId: Math.random() });
}

function showGridView() {
    console.log("showGridView");
    gridView.value = true;
    listView.value = false;
}

function showListView() {
    console.log("showListView");
    gridView.value = false;
    listView.value = true;
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivateFavoriteList: onViewActivateFavoriteList,
    favoriteClickHandler: favoriteClickHandler,
    productClickHandler: productClickHandler,
    showGridView: showGridView,
    showListView: showListView,
    searchIconClickHandler: searchIconClickHandler,
    favoriteList: favoriteList,
    searchkeyword: searchkeyword,

    gridView: gridView,
    listView: listView,
}