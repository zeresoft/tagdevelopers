var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var searchkeyword = Observable("");
var shopList = Observable();

var loadingAnimShow = Observable(true);
var moreAvailable = Observable(false);

var categoriesList = Observable();
var categoryDropdownIndex = Observable();
var selectedShopType;

var shopLoadCount = 10;

function onViewActivateShopList() {
    console.log("onViewActivateShopList");
    loadingAnimShow.value = true;
    moreAvailable.value = false;
    searchkeyword.value = "";
    selectedShopType = "";

    categoriesList.clear();
    categoryDropdownIndex.value = -1;

    getCategoriesList();
}

function getCategoriesList() {
    console.log("getCategoriesList");
    categoriesList.clear();

    var apiBodyObj = {};

    _WebService.request("Shop/GetShopTypes", apiBodyObj).then(function (result) {
        gotCategoriesList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCategoriesList(result) {
    console.log("gotCategoriesList");

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr.length > 0) {
            categoriesList.add({ id: 0, title: loc.value.all_categories });
            categoriesList.addAll(resultArr);
            categoryDropdownIndex.value = 0;
        }
    }

    shopList.clear();
    getShopList();

}

function categoryDropdownChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log("categoryDropdownChange-------------------", JSON.stringify(selectedItem));

    selectedShopType = selectedItem.id;
    categoryDropdownIndex.value = selectedItem.indexLocal;
    if (selectedShopType == 0) {
        selectedShopType = "";
    }

    shopList.clear();
    getShopList();
}

function searchIconClickHandler() {
    shopList.clear();
    getShopList();
}

function pagingLoadMore() {
    if (shopList.length != 0) {
        console.log("pagingLoadMore")
        getShopList();
    }
}

function getShopList() {
    console.log("getShopList");
    loadingAnimShow.value = true;
    moreAvailable.value = false;

    var apiBodyObj = {};

    if (_model.merchantLinked != "") {
        apiBodyObj.merchant_id = _model.merchantLinked;
    }

    if (searchkeyword.value != "") {
        apiBodyObj.title = searchkeyword.value;
    }

    if (selectedShopType != "") {
        apiBodyObj.type = selectedShopType;
    }

    apiBodyObj.count = shopLoadCount;
    apiBodyObj.offset = shopList.length;

    _WebService.request("shop/", apiBodyObj).then(function (result) {
        gotShopList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotShopList(result) {
    console.log("gotShopList")
    loadingAnimShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;
        if (resultArr !== null) {
            shopList.addAll(resultArr);

            if (resultArr.length >= shopLoadCount) {
                moreAvailable.value = true;
            }

        }
    }
}

function shopClickHandler(args) {
    console.log("shopClickHandler", JSON.stringify(args));
    var shop_details = args.data;
    router.push("userShopDetailsPage", { shop_id: shop_details.id, datId: Math.random() });
}

function showNetworkErrorMessage() {
    loadingAnimShow.value = false;

    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    loadingAnimShow: loadingAnimShow,
    onViewActivateShopList: onViewActivateShopList,
    shopClickHandler: shopClickHandler,
    searchIconClickHandler: searchIconClickHandler,
    pagingLoadMore: pagingLoadMore,
    moreAvailable: moreAvailable,

    searchkeyword: searchkeyword,
    shopList: shopList,

    categoriesList: categoriesList,
    categoryDropdownIndex: categoryDropdownIndex,
    categoryDropdownChange: categoryDropdownChange
}