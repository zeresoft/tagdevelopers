var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var editingMode = Observable(false);
var tabData = [loc.value.shop_details, loc.value.orders];
var selectedPageIndex = Observable(0);

var walletId;
var currencyCode = Observable();

var shopId = "";
var shopName = Observable("");
var shopDescription = Observable("");
var shopDeliveryCharge = Observable("");

var uploadImageData = "";
var displayImage = Observable();
var displayImageLocal = Observable(true);
var imageSelectPopupShow = Observable(false);

var uploadShopImageData = "";
var displayShopImage = Observable();
var displayShopImageLocal = Observable(true);


var orderItems = Observable({ index: 0, name: loc.value.to_deliver, value: "deliver" }, { index: 1, name: loc.value.delivered, value: "delivered" });
var orderList = Observable();
var orderItemsIndex = Observable(0);

var deliveryOptPopUpBo = Observable(false);
var deliveryOptionList = Observable();

var createDeliveryOptBo = Observable(false);
var deliveryOptionTitle = Observable();
var deliveryOptionDes = Observable();
var deliveryOptionId = "";

var categorySelectedText = Observable("Select Category");
var categorySelectPopupShow = Observable(false);
var selectedCategories = Observable();
var categoriesList = Observable();
var oldCategoriesArr = [];


this.Parameter.onValueChanged(module, function (param) {
    console.log("merchantShopDetailsPage", JSON.stringify(param));
    setActiveTab(0);
    editingMode.value = false;
    walletId = "";
    currencyCode.value = loc.value.select;
    categorySelectedText.value = "Select Category";
    categorySelectPopupShow.value = false;
    selectedCategories.clear();
    categoriesList.clear();

    orderItemsIndex.value = 0;

    uploadImageData = ""
    displayImage.value = "";
    displayImageLocal.value = true;

    uploadShopImageData = ""
    displayShopImage.value = "";
    displayShopImageLocal.value = true;

    shopId = "";
    shopName.value = "";
    shopDescription.value = "";


    deliveryOptPopUpBo.value = false;
    createDeliveryOptBo.value = false;
    deliveryOptionTitle.value = "";
    deliveryOptionDes.value = "";
    shopDeliveryCharge.value = "";
    deliveryOptionId = "";
    deliveryOptionList.clear();
    oldCategoriesArr = [];

    if (param.shop_details != null) {
        editingMode.value = true;

        var shopData = param.shop_details;
        if (shopData.shop_type) {
            var itemCategoryData = shopData.shop_type;
            var itemCategoryId = [];
            for (var i = 0; i < itemCategoryData.length; i++) {
                itemCategoryId.push(itemCategoryData[i].type_id);

            }
            oldCategoriesArr = itemCategoryId;
        }

        walletId = shopData.wallet_id;
        currencyCode.value = shopData.currency_code;

        shopId = shopData.id;
        shopName.value = shopData.title;
        shopDescription.value = shopData.description;

        if (shopData.logo != "") {
            displayImage.value = shopData.logo;
            displayImageLocal.value = false;
        }

        if (shopData.header_image != "") {
            displayShopImage.value = shopData.header_image;
            displayShopImageLocal.value = false;
        }
        getDeliveryOptionsList();
    }else{
    defaultWalletLoad();

    }
    getCategoriesList();
});

function getCategoriesList() {
    console.log("getCategoriesList");

    categoriesList.clear();
    busy.activate();

    _WebService.request("Shop/GetShopTypes").then(function (result) {
        gotCategoriesList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCategoriesList(result) {
    console.log("gotCategoriesList");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;
        if (resultArr !== null) {
            categoriesList.addAll(resultArr);

            console.log('selected Cateogries', JSON.stringify(oldCategoriesArr))
            selectedCategories.addAll(oldCategoriesArr);
            categorySelectionShow();
        }
    }
}

function categorySelectionShow() {
    if (selectedCategories.length > 0) {
        var namesArr = [];
        var selectionsArr = selectedCategories.toArray();
        var itemsArr = categoriesList.toArray();
        console.log("rgrgrtgrtgrtg" + JSON.stringify(itemsArr))

        selectionsArr.map(function (item) {
            console.log('item', JSON.stringify(item));

            for (var i = 0; i < itemsArr.length; i++) {
                console.log(" itemsArr[i].id " + itemsArr[i].id)
                if (itemsArr[i].id == item) {
                    namesArr.push(itemsArr[i].title);
                    break;
                }
            }
        });

        categorySelectedText.value = namesArr.join(', ');
    } else {
        categorySelectedText.value = "Select Category";
    }
}

function categoryDropdownChange() {
    categorySelectionShow();
}

function onTabBarChanged(args) {
    console.log("onTabBarChanged")
    setActiveTab(args.index);
}

function setActiveTab(index) {
    console.log("setActiveTab " + index)
    selectedPageIndex.value = index;

    if (index == 1) {
        getShopOrders();
    }
}

function getDeliveryOptionsList() {
    busy.activate();
    deliveryOptionList.clear();

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("Shop/GetShippingOption", apiBodyObj).then(function (result) {
        gotDeliveryOptionsList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotDeliveryOptionsList(result) {
    console.log("gotDeliveryOptionsList")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;
        if (resultArr !== null) {
            deliveryOptionList.addAll(resultArr);
        }
    }
}



function defaultWalletLoad() {
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;

    _WebService.request("user/DefaultWallet", apiBodyObj).then(function (result) {
        gotDefaultWalletResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDefaultWalletResult(result) {
    console.log("gotDefaultWalletResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        if (resultObj.wallet_id) {
            walletId = resultObj.wallet_id;
            currencyCode.value = resultObj.currency_code;
        }
    }
}

function walletOptionChangedEvent(args) {
    console.log(JSON.stringify(args))
    walletId = args.walletId;
    currencyCode.value = args.currencyCode;
}

function categoryDropdownClicked() {
    categorySelectPopupShow.value = true;
}

var selctingLogo = true;
function onImageChangeClick() {
    selctingLogo = true;
    imageSelectPopupShow.value = true;
}

function onShopImageChangeClick() {
    selctingLogo = false;
    imageSelectPopupShow.value = true;
}

function onImageSelectionComplete(args) {
    imageSelectPopupShow.value = false;
    if (selctingLogo) {
        uploadImageData = encodeURIComponent(args.imageData);
        displayImage.value = args.imagePath;
        displayImageLocal.value = true;
    } else {
        uploadShopImageData = encodeURIComponent(args.imageData);
        displayShopImage.value = args.imagePath;
        displayShopImageLocal.value = true;
    }
}


function onShopImageRemoveClick() {
    if (uploadShopImageData != "") {
        uploadShopImageData = ""
        displayShopImage.value = "";
        displayShopImageLocal.value = true;
    } else {
        busy.activate();

        var apiBodyObj = {};
        apiBodyObj.id = shopId;

        _WebService.request("Shop/DeleteHeaderImage", apiBodyObj).then(function (result) {
            gotShopImageRemoveResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }
}

function gotShopImageRemoveResult(result) {
    console.log("gotShopImageRemoveResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        uploadShopImageData = ""
        displayShopImage.value = "";
        displayShopImageLocal.value = true;
    }
}


function createDeliveryChargePopup() {
    createDeliveryOptBo.value = true;
    deliveryOptionId = "";
    deliveryOptionTitle.value = "";
    deliveryOptionDes.value = "";
    shopDeliveryCharge.value = "";
    deliveryOptPopUpBo.value = true;
}

function onDeliveryOptnItemClick(args) {
    console.log("onDeliveryOptnItemClick", JSON.stringify(args));
    createDeliveryOptBo.value = false;
    deliveryOptionTitle.value = args.data.title;
    deliveryOptionDes.value = args.data.description;
    shopDeliveryCharge.value = args.data.shipping_charge;
    deliveryOptionId = args.data.id;

    deliveryOptPopUpBo.value = true;
}

function createDeliveryChargeHandler() {

    if (deliveryOptionTitle.value == "") {
        tagEvents.emit("toastShow", { message: "Title is required" });
        return false;
    }
    if (deliveryOptionDes.value == "") {
        tagEvents.emit("toastShow", { message: "Shipping detail is required" });
        return false;
    }
    if (shopDeliveryCharge.value === "" || shopDeliveryCharge.value < 0) {
        tagEvents.emit("toastShow", { message: "Delivery charge is required" });
        return false;
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.title = deliveryOptionTitle.value;
    apiBodyObj.description = deliveryOptionDes.value;
    apiBodyObj.shipping_charge = shopDeliveryCharge.value;
    apiBodyObj.shop_id = shopId;
    // apiBodyObj.shipping_days = 5;

    if (createDeliveryOptBo.value == false) {
        apiBodyObj.id = deliveryOptionId;
        _WebService.request("Shop/UpdateShippingOption", apiBodyObj).then(function (result) {
            getDeliveryOptionsresult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        _WebService.request("Shop/AddShippingOption", apiBodyObj).then(function (result) {
            getDeliveryOptionsresult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }
}

function getDeliveryOptionsresult(result) {
    console.log("getDeliveryOptionsresult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        deliveryOptPopUpBo.value = false;
        getDeliveryOptionsList();
    } else {
        tagEvents.emit("toastShow", { message: "Unable to save delivery option" });
    }
}

function deliveryOptDeleteHandler(args) {
    console.log("deliveryOptDeleteHandler ", JSON.stringify(args));
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("Shop/DeleteShippingOption", apiBodyObj).then(function (result) {
        busy.deactivate();

        var arr = result;
        console.log("RemoveShippingOption", JSON.stringify(arr));

        if (arr.status == "success") {
            deliveryOptionList.remove(args.data);
        } else {
            tagEvents.emit("toastShow", { message: result.error });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function createShopProcesss() {
    if (shopName.value == "") {
        tagEvents.emit("toastShow", { message: "Shop name is required" });
        return false;
    }

    if (shopDescription.value == "") {
        tagEvents.emit("toastShow", { message: "Shop description is required" });
        return false;
    }

    if (walletId === "") {
        tagEvents.emit("toastShow", { message: "Shop currency is required" });
        return false;
    }
    if (selectedCategories.toArray().length == 0) {
        tagEvents.emit("toastShow", { message: "Category is required" });
        return false;

    }

    var apiBodyObj = {};
    apiBodyObj.title = shopName.value;
    apiBodyObj.description = shopDescription.value;
    apiBodyObj.wallet_id = walletId;

    if (uploadImageData != "") {
        apiBodyObj.logo = uploadImageData;
    }

    if (uploadShopImageData != "") {
        apiBodyObj.header_image = uploadShopImageData;
    }
    apiBodyObj.shop_type = JSON.stringify(selectedCategories.toArray());

    busy.activate();

    if (editingMode.value == false) {
        _WebService.request("shop/create", apiBodyObj).then(function (result) {
            gotCreateShopResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        apiBodyObj.shop_id = shopId;
        console.log('shopId', shopId);
        _WebService.request("shop/update", apiBodyObj).then(function (result) {
            gotCreateShopResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }
}

function gotCreateShopResult(result) {
    console.log("gotCreateShopResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (editingMode.value == false) {
            shopId = arr.id;
            editingMode.value = true;

            //router.goBack();
            tagEvents.emit("toastShow", { message: "Shop created successfully" });
        } else {
            tagEvents.emit("toastShow", { message: "Shop updated successfully" });
        }
    } else {
        if (arr.error && arr.error != "") {
            tagEvents.emit("toastShow", { message: arr.error });
        } else {
            if (editingMode.value == false)
                tagEvents.emit("toastShow", { message: "Unable to Create Shop" });
            else
                tagEvents.emit("toastShow", { message: "Unable to Update Shop" });
        }
    }
}



function onOrderTypeChangeEvent(args) {
    console.log("onOrderTypeChangeEvent change");
    getShopOrders();
}

function getShopOrders() {
    console.log('Orders List with ', orderItemsIndex.value);
    orderList.clear();

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    if (orderItemsIndex.value == 1) {
        apiBodyObj.delivery_status = "yes";
    } else {
        apiBodyObj.delivery_status = "no";
    }

    _WebService.request("shop/OwnerOrder", apiBodyObj).then(function (result) {
        gotShopOrdersResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

var groupBy = function (xs, key) {
    return xs.reduce(function (rv, x) {
        (rv[x[key]] = rv[x[key]] || []).push(x);
        return rv;
    }, {});
};

function gotShopOrdersResult(result) {
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr !== null) {
            var resultArrGroup = groupBy(resultArr, 'transaction_id');

            // console.log(JSON.stringify(resultArrGroup));
            var resultArrProcessed = [];

            for (var key in resultArrGroup) {
                var item = resultArrGroup[key];
                
                var transactionObj = {};
                transactionObj.transaction_id = item[0].transaction_id;
                transactionObj.orderDate = item[0].orderDate;
                transactionObj.delivery_status = item[0].delivery_status;
                transactionObj.shipping_address = item[0].shipping_address;
                transactionObj.shipping_option = item[0].shipping_option;
                transactionObj.currency_code = item[0].currency_code;
                
                var shippingStatus = Observable();
                shippingStatus.value = item[0].shipping_progress;
                transactionObj.shipping_progress = shippingStatus;


                transactionObj.orderData = item;
                var totalBill = 0;
                for (var i = 0; i < item.length; i++) {
                    totalBill += item[i].amount;
                }
                transactionObj.total_amount = totalBill;

                resultArrProcessed.push(transactionObj);
            }

            var resultArrProcessedReverse = resultArrProcessed.reverse();
            orderList.addAll(resultArrProcessedReverse);
        }
    }
}


function onShippedClick(args){
    console.log("onDeliverClick", JSON.stringify(args));
    busy.activate();

    var apiBodyObj = {};
    // apiBodyObj.order_id = args.data.id;
    apiBodyObj.transaction_id = args.data.transaction_id;
    apiBodyObj.delivered_by = "transaction";//order or transaction
    apiBodyObj.delivery_status = "no";
    apiBodyObj.shipping_progress = "shipped";

    _WebService.request("shop/Deliver", apiBodyObj).then(function (result) {
        console.log('gotShoppedResult');
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr));

        if (arr.status == "success") {
            args.data.shipping_progress.value = "shipped";
            // tagEvents.emit("toastShow", { message: "Shipped successfully" });
        } else {
            tagEvents.emit("toastShow", { message: "Unable to change status" });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
    
}

function onDeliverClick(args) {
    console.log("onDeliverClick", JSON.stringify(args));
    busy.activate();
    
    var apiBodyObj = {};
    // apiBodyObj.order_id = args.data.id;
    apiBodyObj.transaction_id = args.data.transaction_id;
    apiBodyObj.delivered_by = "transaction";//order or transaction
    apiBodyObj.delivery_status = "yes";
    apiBodyObj.shipping_progress = "deliverd";
    
    _WebService.request("shop/Deliver", apiBodyObj).then(function (result) {
        console.log('gotShopDeliverResult');
        busy.deactivate();
        
        var arr = result;
        console.log(JSON.stringify(arr));
        
        if (arr.status == "success") {
            orderList.remove(args.data);
            // tagEvents.emit("toastShow", { message: "Marked as delivered successfully" });
        } else {
            tagEvents.emit("toastShow", { message: "Unable to change status" });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function inventoryClickHandler() {
    console.log("inventoryClickHandler", shopId);
    router.push("inventoryPage", { shop_id: shopId, shop_title: shopName.value, datId: Math.random() });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    editingMode: editingMode,
    selectedPageIndex: selectedPageIndex,
    tabData: tabData,
    onTabBarChanged: onTabBarChanged,

    shopName: shopName,
    shopDescription: shopDescription,
    currencyCode: currencyCode,
    walletOptionChangedEvent: walletOptionChangedEvent,
    createShopProcesss: createShopProcesss,
    inventoryClickHandler: inventoryClickHandler,

    onImageChangeClick: onImageChangeClick,
    onShopImageChangeClick: onShopImageChangeClick,
    onShopImageRemoveClick: onShopImageRemoveClick,
    onImageSelectionComplete: onImageSelectionComplete,
    displayImage: displayImage,
    displayImageLocal: displayImageLocal,
    displayShopImage: displayShopImage,
    displayShopImageLocal: displayShopImageLocal,
    imageSelectPopupShow: imageSelectPopupShow,

    createDeliveryChargePopup: createDeliveryChargePopup,
    deliveryOptPopUpBo: deliveryOptPopUpBo,
    deliveryOptionList: deliveryOptionList,
    createDeliveryChargeHandler: createDeliveryChargeHandler,
    deliveryOptionTitle: deliveryOptionTitle,
    deliveryOptionDes: deliveryOptionDes,
    onDeliveryOptnItemClick: onDeliveryOptnItemClick,
    createDeliveryOptBo: createDeliveryOptBo,
    deliveryOptDeleteHandler: deliveryOptDeleteHandler,
    shopDeliveryCharge: shopDeliveryCharge,
    onShippedClick:onShippedClick,


    orderItems: orderItems,
    orderList: orderList,
    orderItemsIndex: orderItemsIndex,
    onOrderTypeChangeEvent: onOrderTypeChangeEvent,
    onDeliverClick: onDeliverClick,
  
    categorySelectedText: categorySelectedText,
    categoryDropdownClicked: categoryDropdownClicked,
    categorySelectPopupShow: categorySelectPopupShow,
    selectedCategories: selectedCategories,
    categoriesList: categoriesList,
    categoryDropdownChange: categoryDropdownChange,

}