var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var showTitle = Observable(false);
var merchantShops = Observable();
var searchkeyword = Observable("");

this.Parameter.onValueChanged(module, function (param) {
    console.log("MerchantShopPage", JSON.stringify(param));

    if (param && param.showTitle == true)
        showTitle.value = true;
    else
        showTitle.value = false;
});

function onViewActivateMerchantShopPage() {
    console.log("onViewActivateMerchantShopPage");
    searchkeyword.value = "";
    getMerchantShops();
}

function searchIconClickHandler() {
    getMerchantShops();
}

function getMerchantShops() {
    console.log("getMerchantShopPage");
    merchantShops.clear();

    busy.activate();
    var apiBodyObj = {};
    if (searchkeyword.value != "") {
        apiBodyObj.name = searchkeyword.value;
    }

    _WebService.request("shop/my", apiBodyObj).then(function (result) {
        gotMerchantShops(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotMerchantShops(result) {
    console.log("gotMerchantShops")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;
        if (resultArr !== null) {
            merchantShops.addAll(resultArr);
        } else {
            // showShopDetailsPage();
        }
    } else {
        // showShopDetailsPage();
    }
}

function shopClickHandler(result) {
    console.log("shopDetailsClickHandler", JSON.stringify(result));
    router.push("merchantShopDetailsPage", { shop_details: result ? result.data : null, datId: Math.random() });
}

function showShopDetailsPage(result) {
    console.log("showShopDetailsPage", JSON.stringify(result));
    router.goto("merchantShopDetailsPage", { shop_details: result ? result.data : null, datId: Math.random() });
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivateMerchantShopPage: onViewActivateMerchantShopPage,
    shopClickHandler: shopClickHandler,
    searchIconClickHandler: searchIconClickHandler,

    showTitle: showTitle,
    searchkeyword: searchkeyword,
    merchantShops: merchantShops
}