
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var editingMode = Observable(false);

var shopId;
var productId;
var uploadImageData = [];
var shopTitle = Observable("");
var productTitle = Observable("");
var productDescription = Observable("");
var productSKUCode = Observable("");
var productPrice = Observable(1);
var productStock = Observable(1);
var productTimeToShip = Observable(1);
var productImages = Observable();

var uploadImages = Observable();

var addCategoryInput = Observable("");
var categorySelectedText = Observable("Select Category");
var addSizeInput = Observable("");
var addColorInput = Observable("");
var addOtherOptionInput = Observable("");

var oldCategoriesArr = [];
var selectedCategories = Observable();

var categoriesList = Observable();
var sizeOptionsList = Observable();
var colorOptionsList = Observable();
var otherOptionsList = Observable();

var sizeCheckBox = Observable(false);
var colorCheckBox = Observable(false);
var otherCheckBox = Observable(false);

var sizePopupShow = Observable(false);
var colorPopupShow = Observable(false);
var otherOptionPopupShow = Observable(false);
var categoryOperationsPopupShow = Observable(false);
var categorySelectPopupShow = Observable(false);
var imageSelectPopupShow = Observable(false);


this.Parameter.onValueChanged(module, function (param) {
    console.log("merchantCreateProductPage", JSON.stringify(param));

    productImages.clear();
    uploadImages.clear();

    selectedCategories.clear();
    categoriesList.clear();

    addCategoryInput.value = "";
    categorySelectedText.value = "Select Category";
    addSizeInput.value = "";
    addColorInput.value = "";
    addOtherOptionInput.value = "";

    sizeOptionsList.clear();
    colorOptionsList.clear();
    otherOptionsList.clear();

    sizeCheckBox.value = false;
    colorCheckBox.value = false;
    otherCheckBox.value = false;

    sizePopupShow.value = false;
    sizePopupShow.value = false;
    otherOptionPopupShow.value = false;

    categoryOperationsPopupShow.value = false;
    categorySelectPopupShow.value = false;
    imageSelectPopupShow.value = false;

    shopId = param.shop_id;
    shopTitle = param.shop_title;

    editingMode.value = false;

    productId = "";
    productTitle.value = "";
    productDescription.value = "";
    productPrice.value = "";
    productStock.value = 1;
    productTimeToShip.value = 1;
    productSKUCode.value = "";
    oldCategoriesArr = [];

    if (param.product_details != null) {
        uploadImageData = [];
        editingMode.value = true;

        var productData = param.product_details;
        productId = productData.id;
        productTitle.value = productData.name;
        productDescription.value = productData.description;
        productPrice.value = productData.price;
        productStock.value = productData.stock;
        productTimeToShip.value = productData.shipment_days;
        productSKUCode.value = productData.sku_code;

        if (productData.item_category) {
            var itemCategoryData = productData.item_category;
            var itemCategoryId = [];
            for (var i = 0; i < itemCategoryData.length; i++) {
                itemCategoryId.push(itemCategoryData[i].category_id);
                
            }
            oldCategoriesArr = itemCategoryId;
        }

        if (productData.size && productData.size.length > 0) {
            sizeCheckBox.value = true;
            sizeOptionsList.addAll(productData.size);
        }

        if (productData.color && productData.color.length > 0) {
            colorCheckBox.value = true;
            colorOptionsList.addAll(productData.color);
        }

        if (productData.other && productData.other.length > 0) {
            otherCheckBox.value = true;
            otherOptionsList.addAll(productData.other);
        }
        getInventoryImages();
        
    }
    getCategoriesList();
});


function getCategoriesList() {
    console.log("getCategoriesList");

    categoriesList.clear();
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("shop/ListCategory", apiBodyObj).then(function (result) {
        gotCategoriesList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCategoriesList(result) {
    console.log("gotCategoriesList");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;
        categoriesList.addAll(resultArr);

        console.log('selected Cateogries', JSON.stringify(oldCategoriesArr))
        selectedCategories.addAll(oldCategoriesArr);
        categorySelectionShow();
    }

}



function getInventoryImages() {
    productImages.clear();
    uploadImages.clear();
    uploadImageData = [];

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.inventory_id = productId;

    _WebService.request("shop/GetInventoryImages", apiBodyObj).then(function (result) {
        gotInventoryImagesList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotInventoryImagesList(result) {
    console.log("gotInventoryImagesList");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr != null) {
            productImages.addAll(resultArr);
        }
    }
}

function categoryEditClickHandler() {
    console.log("categoryEditClickHandler");
    addCategoryInput.value = "";

    categoryOperationsPopupShow.value = true;
    categoryScrollView.gotoRelative(0, 0);
}

function categoryDropdownClicked() {
    categorySelectPopupShow.value = true;
}

function categoryDropdownChange() {
    categorySelectionShow();
}

function categorySelectionShow() {
    if (selectedCategories.length > 0) {
        var namesArr = [];
        var selectionsArr = selectedCategories.toArray();
        var itemsArr = categoriesList.toArray();

        selectionsArr.map(function (item) {
            console.log('item', JSON.stringify(item));

            for (var i = 0; i < itemsArr.length; i++) {
                console.log(" itemsArr[i].id " + itemsArr[i].id)
                if (itemsArr[i].id == item) {
                    namesArr.push(itemsArr[i].name);
                    break;
                }
            }
        });

        categorySelectedText.value = namesArr.join(', ');
    } else {
        categorySelectedText.value = "Select Category";
    }

}

function addCategoryClickHandler() {
    console.log("addCategoryClickHandler");

    if (addCategoryInput.value != "") {

        busy.activate();

        var apiBodyObj = {};
        apiBodyObj.shop_id = shopId;
        apiBodyObj.name = addCategoryInput.value;

        _WebService.request("shop/AddCategory", apiBodyObj).then(function (result) {
            addCategoryResponseHandler(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        tagEvents.emit("toastShow", { message: loc.value.blank_name });
    }
}

function addCategoryResponseHandler(result) {
    console.log("addCategoryResponseHandler");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var newCategory = {};

        newCategory.id = arr.id;
        newCategory.shop_id = shopId;
        newCategory.name = addCategoryInput.value;
        categoriesList.add(newCategory);

        addCategoryInput.value = "";

        // setTimeout is used here since we have to wait for the UI to update before we can scroll to the bottom.
        setTimeout(function () {
            categoryScrollView.gotoRelative(0, 1);
        }, 100);
    } else {
        tagEvents.emit("toastShow", { message: result.error });
    }
}

function categoryDeleteClickHandler(args) {
    console.log("categoryDeleteClickHandler", JSON.stringify(args.data));
    var categoryDetail = args.data;
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = categoryDetail.id;

    _WebService.request("shop/DeleteCategory", apiBodyObj).then(function (result) {
        busy.deactivate();
        console.log("deleteCategoryResponseHandler");

        var arr = result;
        console.log(JSON.stringify(arr));

        if (arr.status == "success") {
            categoriesList.remove(args.data);
            tagEvents.emit("toastShow", { message: "Category deleted successfully" });
        } else {
            tagEvents.emit("toastShow", { message: result.error });

        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function addSizeClickHandler() {
    console.log("addSizeClickHandler");

    if (addSizeInput.value != "") {
        sizeOptionsList.add(addSizeInput.value);
        addSizeInput.value = "";

        setTimeout(function () {
            sizeScrollView.gotoRelative(0, 1);
        }, 100);
    } else {
        tagEvents.emit("toastShow", { message: loc.value.input_blank_error });
    }
}


function addColorClickHandler() {
    console.log("addColorClickHandlery");

    if (addColorInput.value != "") {
        colorOptionsList.add(addColorInput.value);
        addColorInput.value = "";

        setTimeout(function () {
            colorScrollView.gotoRelative(0, 1);
        }, 100);
    } else {
        tagEvents.emit("toastShow", { message: loc.value.input_blank_error });
    }
}


function addOtherOptionClickHandler() {
    console.log("addOtherOptionClickHandler");

    if (addOtherOptionInput.value != "") {
        otherOptionsList.add(addOtherOptionInput.value);
        addOtherOptionInput.value = "";

        setTimeout(function () {
            otherOptionScrollView.gotoRelative(0, 1);
        }, 100);
    } else {
        tagEvents.emit("toastShow", { message: loc.value.input_blank_error });
    }
}

function saveProductClickHandler() {
    console.log("saveProductClickHandler");

    if (productTitle.value == "") {
        tagEvents.emit("toastShow", { message: "Product title is required" });
        return false;
    }

    if (selectedCategories.length == 0) {
        tagEvents.emit("toastShow", { message: "Product category is required" });
        return false;
    }

    if (productDescription.value == "") {
        tagEvents.emit("toastShow", { message: "Product description is required" });
        return false;
    }

    if (productStock.value == "") {
        tagEvents.emit("toastShow", { message: "Product stock is required" });
        return false;
    }

    if (productPrice.value == "") {
        tagEvents.emit("toastShow", { message: "Product price is required" });
        return false;
    }

    if (productTimeToShip.value == "") {
        tagEvents.emit("toastShow", { message: "Shipment days required" });
        return false;
    }

    if (productSKUCode.value == "") {
        tagEvents.emit("toastShow", { message: "SKU code is required" });
        return false;
    }

    saveProduct(false);
}

function duplicateProductClickHandler() {
    saveProduct(true);
}


function saveProduct(duplicateProduct) {
    busy.activate();

    var sizeOptions = sizeCheckBox.value == true ? sizeOptionsList.toArray() : [];
    var colorOptions = colorCheckBox.value == true ? colorOptionsList.toArray() : [];
    var otherOptions = otherCheckBox.value == true ? otherOptionsList.toArray() : [];

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;
    apiBodyObj.name = productTitle.value;
    apiBodyObj.description = productDescription.value;
    apiBodyObj.price = productPrice.value;
    apiBodyObj.stock = productStock.value;
    apiBodyObj.item_category = JSON.stringify(selectedCategories.toArray());
    apiBodyObj.shipment_days = productTimeToShip.value;
    apiBodyObj.sku_code = productSKUCode.value;
    apiBodyObj.size = JSON.stringify(sizeOptions);
    apiBodyObj.color = JSON.stringify(colorOptions);
    apiBodyObj.other = JSON.stringify(otherOptions);

    var apiUrl = "shop/createInventory";

    if (!duplicateProduct || duplicateProduct == false) {

        // if (uploadImageData && uploadImageData.length > 0) {
        //   uploadImageData.forEach(function (image, index) {
        //        apiBodyObj["images[" + index + "]"] = image;
        //  });
        // }

        if (editingMode.value == true) {
            apiBodyObj.invent_id = productId;
            apiUrl = "shop/UpdateInventory";
        }
    }



    _WebService.request(apiUrl, apiBodyObj).then(function (result) {
        console.log("gotCreateProductResult")
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr))
        if (arr.status == "success") {
            var resultArr = arr.result;


            if (duplicateProduct == true) {
                router.goBack();
                tagEvents.emit("toastShow", { message: "Product duplicated successfully" });
            } else if (editingMode.value == false) {
                productId = arr.id;
                console.log("inventoryId" + arr.id)
                editingMode.value = true;
                if (uploadImageData.length == 0) {

                } else {

                    uploadNewImages();

                }

                tagEvents.emit("toastShow", { message: "Product created successfully" });
            } else {
                tagEvents.emit("toastShow", { message: "Product updated successfully" });
                uploadNewImages();
            }
        } else {

            if (arr.error && arr.error != "") {
                tagEvents.emit("toastShow", { message: arr.error });
            } else {
                if (duplicateProduct == true)
                    tagEvents.emit("toastShow", { message: "Unable to Duplicate Product" });
                else if (editingMode.value == false)
                    tagEvents.emit("toastShow", { message: "Unable to Create Product" });
                else
                    tagEvents.emit("toastShow", { message: "Unable to Update Product" });
            }
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}
var indexArr = []
function uploadNewImages() {
    indexArr = [];
    var apiBodyObj = {};
    apiBodyObj.id = productId;
    var apiUrl = "Shop/UpdloadInventoryImage";

    if (uploadImageData && uploadImageData.length > 0) {
        uploadImageData.forEach(function (image, index) {

            console.log("index" + index)
            apiBodyObj.image = image
            _WebService.request(apiUrl, apiBodyObj).then(function (result) {
                console.log("gotProductImageResult")
                //  uploadImageData.splice(index);               
                gotProductImageResult(result, index);

            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });

        });







    }


}
function gotProductImageResult(result, index) {
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {

        indexArr.push(index)
        console.log("imageLengthnumber.." + uploadImageData.length)
        if (uploadImageData.length == indexArr.length) {

            getInventoryImages();

        }

    }
}

function sizePopupHandler() {
    console.log("sizePopupHandler");
    sizePopupShow.value = true;
}

function colorPopupHandler() {
    console.log("colorPopupHandler");
    colorPopupShow.value = true;
}

function optionPopupHandler() {
    console.log("optionPopupHandler");
    otherOptionPopupShow.value = true;
}

function sizeDeleteClickHandler(args) {
    console.log("sizeDeleteClickHandler", JSON.stringify(args));

    var sizeObj = args.data;
    console.log("sizeDetail" + sizeObj);
    //sizeOptionsList.removeAt(sizeDetail.indexLocal);
    sizeOptionsList.remove(sizeObj);
}

function colorDeleteClickHandler(args) {
    console.log("colorDeleteClickHandler", JSON.stringify(args));

    var colorObj = args.data;
    colorOptionsList.remove(colorObj);
}

function otherOptionDeleteClickHandler(args) {
    console.log("otherOptionDeleteClickHandler", JSON.stringify(args));
    var otherObj = args.data;
    otherOptionsList.remove(otherObj);
}

function onImageChangeClick() {
    console.log("onImageChangeClick");
    imageSelectPopupShow.value = true;
}

function onImageSelectionComplete(args) {
    console.log("onImageSelectionComplete");
    imageSelectPopupShow.value = false;

    uploadImageData.push(encodeURIComponent(args.imageData));
    uploadImages.add(args.imagePath);
}

function productImageDeleteHandler(args) {
    console.log("productImageDeleteHandler ", JSON.stringify(args));

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;
    apiBodyObj.image_url = args.data.image;

    _WebService.request("shop/RemoveInventoryImage", apiBodyObj).then(function (result) {
        busy.deactivate();

        var arr = result;
        console.log("RemoveProductImage", JSON.stringify(arr));
        if (arr.status == "success") {
            productImages.remove(args.data);

        } else {
            tagEvents.emit("toastShow", { message: result.error });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });


}

function uploadImageDeleteHandler(args) {
    console.log("uploadImageDeleteHandler ", JSON.stringify(args));

    console.log("uploadImageData ", uploadImageData.length);
    uploadImages.remove(args.data);
    uploadImageData.splice(args.data, 1);
    console.log("uploadImageData ", uploadImageData.length);
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    categoryEditClickHandler: categoryEditClickHandler,
    categoryDropdownClicked: categoryDropdownClicked,

    categoryDropdownChange: categoryDropdownChange,
    saveProductClickHandler: saveProductClickHandler,
    addCategoryClickHandler: addCategoryClickHandler,
    addColorClickHandler: addColorClickHandler,
    addSizeClickHandler: addSizeClickHandler,
    addOtherOptionClickHandler: addOtherOptionClickHandler,
    sizePopupHandler: sizePopupHandler,
    colorPopupHandler: colorPopupHandler,
    optionPopupHandler: optionPopupHandler,
    categoryDeleteClickHandler: categoryDeleteClickHandler,
    sizeDeleteClickHandler: sizeDeleteClickHandler,
    colorDeleteClickHandler: colorDeleteClickHandler,
    otherOptionDeleteClickHandler: otherOptionDeleteClickHandler,
    onImageSelectionComplete: onImageSelectionComplete,
    onImageChangeClick: onImageChangeClick,
    duplicateProductClickHandler: duplicateProductClickHandler,
    productImageDeleteHandler: productImageDeleteHandler,
    uploadImageDeleteHandler: uploadImageDeleteHandler,

    editingMode: editingMode,
    shopTitle: shopTitle,
    productTitle: productTitle,
    productDescription: productDescription,
    productPrice: productPrice,
    productStock: productStock,
    productTimeToShip: productTimeToShip,
    productSKUCode: productSKUCode,
    productImages: productImages,
    uploadImages: uploadImages,
    sizeOptionsList: sizeOptionsList,
    colorOptionsList: colorOptionsList,
    otherOptionsList: otherOptionsList,
    selectedCategories: selectedCategories,

    sizeCheckBox: sizeCheckBox,
    colorCheckBox: colorCheckBox,
    otherCheckBox: otherCheckBox,
    sizePopupShow: sizePopupShow,
    colorPopupShow: colorPopupShow,
    otherOptionPopupShow: otherOptionPopupShow,
    addCategoryInput: addCategoryInput,
    categorySelectedText: categorySelectedText,
    categoriesList: categoriesList,
    categoryOperationsPopupShow: categoryOperationsPopupShow,
    categorySelectPopupShow: categorySelectPopupShow,
    addSizeInput: addSizeInput,
    addColorInput: addColorInput,
    addOtherOptionInput: addOtherOptionInput,
    imageSelectPopupShow: imageSelectPopupShow,

}