var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var dataLoadingComplete = Observable(false);

var productId;
var shopId;
var favoriteId;

var shopTitle = Observable("");
var products = Observable();
var productIndex = Observable();

var productStock = Observable(0);
var favoriteProduct = Observable(false);


var addToCartPopupShow = Observable(false);

var productImageIndex = Observable(0);

var color = "";
var size = "";
var other = "";

var sizeList = Observable();
var colorList = Observable();
var otherList = Observable();

var quantityInput = Observable("");

var colorIndex = Observable();
var sizeIndex = Observable();
var otherIndex = Observable();

var loadingAnimShow = Observable(true);


this.Parameter.onValueChanged(module, function (param) {
    console.log("OnValueChanged", JSON.stringify(param));
    dataLoadingComplete.value = false;
    loadingAnimShow.value = true;

    if (param.product_id && param.product_id != "") {
        productIndex.value = -1;

        shopId = param.shop_id;
        shopTitle.value = param.shop_title;
        productId = param.product_id;

        tagEvents.emit("cartItemsCountEmit", { shop_id: shopId, shop_name: shopTitle.value });

        popupDefaultValues();
        getProductList();
    } else {
        router.goBack();
    }


});

function popupDefaultValues() {
    colorIndex.value = -1;
    sizeIndex.value = -1;
    otherIndex.value = -1;

    color = "";
    size = "";
    other = "";

    quantityInput.value = "1";
}


function getProductList() {
    console.log("getProductList");
    products.clear();
    loadingAnimShow.value = true;

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("shop/ProductList", apiBodyObj).then(function (result) {
        gotProductList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotProductList(result) {
    dataLoadingComplete.value = true;

    console.log("gotProductList")
    loadingAnimShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr !== null) {


            var resultArrProcess = [];

            for (var i = 0; i < resultArr.length; i++) {
                var item = {};
                item = resultArr[i];

                var stockObs = Observable();
                stockObs.value = item.stock;
                item.stockObs = stockObs;

                if (item.favorite == false) {
                    item.favorite_id = "";
                }

                resultArrProcess.push(item);
            }

            products.addAll(resultArrProcess);

            var index = -1;
            for (var i = 0; i < resultArr.length; i++) {
                console.log(" i " + i)
                if (resultArr[i].id === productId) {
                    index = i;
                    break;
                }
            }
            productIndex.value = index;


            // loadProductDetails();
        }
    }
}

productIndex.onValueChanged(module, function (item) {
    console.log("productIndex", JSON.stringify(item));

    if (productIndex.value > -1)
        loadProductDetails();
});

function loadProductDetails() {
    console.log("loadProductDetails");

    sizeList.clear();
    colorList.clear();
    otherList.clear();

    productImageIndex.value = 0;

    var productData = products.getAt(productIndex.value);
    console.log("Selected Product ", JSON.stringify(productData));

    productId = productData.id;
    productStock.value = productData.stockObs.value;

    if (productData.favorite == true) {
        favoriteProduct.value = true;
        favoriteId = productData.favorite_id;
    } else {
        favoriteProduct.value = false;
        favoriteId = 0;
    }

    if (productData.size && productData.size.length > 0) {
        var sizes = productData.size;
        sizes.forEach(function (size) {
            sizeList.add({ 'name': size });
        });
    }

    if (productData.color && productData.color.length > 0) {
        var colors = productData.color;
        colors.forEach(function (color) {
            colorList.add({ 'name': color });
        });
    }

    if (productData.other && productData.other.length > 0) {
        var others = productData.other;
        others.forEach(function (other) {
            otherList.add({ 'name': other });
        });
    }
}

function showPopupClickHandler() {
    console.log("addToCartClickHandler");
    popupDefaultValues();

    if (colorList.length == 1) {
        var colorOption = otherList.getAt(0);
        color = colorOption.name;
        colorIndex.value = 0;
    }

    if (sizeList.length == 1) {
        var sizeOption = otherList.getAt(0);
        size = sizeOption.name;
        sizeIndex.value = 0;
    }

    if (otherList.length == 1) {
        var otherOption = otherList.getAt(0);
        other = otherOption.name;
        otherIndex.value = 0;
    }

    addToCartPopupShow.value = true;
}

function cancelPopupClickHandler() {
    console.log("cancelPopupClickHandler");
    addToCartPopupShow.value = false;
}

function colorDropdownChange(args) {
    console.log("colorDropdownChange-------------------");
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem));
    color = selectedItem.name;
    colorIndex.value = selectedItem.indexLocal;
}

function sizeDropdownChange(args) {
    console.log("sizeDropdownChange-------------------");
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem));
    size = selectedItem.name;
    sizeIndex.value = selectedItem.indexLocal;
}

function otherDropdownChange(args) {
    console.log("otherDropdownChange-------------------");
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem));
    other = selectedItem.name;
    otherIndex.value = selectedItem.indexLocal;
}

function addToCartClickHandler() {
    console.log("addToCartClickHandler");
    var apiBodyObj = {};
    var quantity = parseInt(quantityInput.value);

    if (colorList.toArray().length > 0) {
        if (color == "") {
            tagEvents.emit("toastShow", { message: "Please select color" });
            return false;
        } else {
            apiBodyObj.color = color;
        }
    }

    if (sizeList.toArray().length > 0) {
        if (size == "") {
            tagEvents.emit("toastShow", { message: "Please select size" });
            return false;
        } else {
            apiBodyObj.size = size;
        }
    }

    if (otherList.toArray().length > 0) {
        if (other == "") {
            tagEvents.emit("toastShow", { message: "Please select other" });
            return false;
        } else {
            apiBodyObj.other = other;
        }
    }

    if (quantity <= 0) {
        tagEvents.emit("toastShow", { message: "Please enter quantity" });
        return false;
    }

    if (quantity > productStock.value) {
        tagEvents.emit("toastShow", { message: "Please enter quantity less than or equal to " + productStock.value });
        return false
    }


    apiBodyObj.shop_id = shopId;
    apiBodyObj.inventory_id = productId;
    apiBodyObj.qty = quantity;

    loadingAnimShow.value = true;

    _WebService.request("shop/AddToCart", apiBodyObj).then(function (result) {
        gotAddToCartResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotAddToCartResult(result) {
    console.log("gotAddToCartResult")
    loadingAnimShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var productData = products.getAt(productIndex.value);
        productStock.value = productStock.value - quantityInput.value;
        productData.stockObs.value = productStock.value;

        popupDefaultValues();
        cancelPopupClickHandler();
        tagEvents.emit("cartItemsCountEmit", { shop_id: shopId, shop_name: shopTitle.value });

    } else {
        // if (arr.error == "insuffcient_balance") {
        //     tagEvents.emit("toastShow", { message: loc.value.insufficient_balance });
        // } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        // }
    }
}

function favoriteClickHandler() {
    console.log("favoriteClickHandler");
    
    loadingAnimShow.value = true;
    
    var apiUrl = "";
    var apiBodyObj = {};
    
    if (favoriteProduct.value == true) {
        apiBodyObj.id = favoriteId;
        apiUrl = "shop/RemoveFavourite";
    } else {
        apiBodyObj.product_id = productId;
        apiUrl = "shop/AddFavorite";
    }
    // favoriteProduct.value = !favoriteProduct.value;

    _WebService.request(apiUrl, apiBodyObj).then(function (result) {
        favoriteClickResponseHandler(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function favoriteClickResponseHandler(result) {
    loadingAnimShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        
        if (arr.id) {
            favoriteProduct.value = true;
            favoriteId = arr.id;
            // tagEvents.emit("toastShow", { message: "Added to Wishlist" });
        } else {
            favoriteProduct.value = false;
            favoriteId = 0;
        }
        
        var productData = products.getAt(productIndex.value);
        productData.favorite = favoriteProduct.value;
        productData.favorite_id = favoriteId;

    } else {
        if (arr.error == "already_added") {
            tagEvents.emit("toastShow", { message: "Already added to wishlist" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function showNetworkErrorMessage() {
    loadingAnimShow.value = false;
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    dataLoadingComplete: dataLoadingComplete,
    loadingAnimShow: loadingAnimShow,

    showPopupClickHandler: showPopupClickHandler,
    colorDropdownChange: colorDropdownChange,
    sizeDropdownChange: sizeDropdownChange,
    otherDropdownChange: otherDropdownChange,
    addToCartClickHandler: addToCartClickHandler,
    cancelPopupClickHandler: cancelPopupClickHandler,
    favoriteClickHandler: favoriteClickHandler,
    favoriteProduct: favoriteProduct,

    shopTitle: shopTitle,

    productStock: productStock,
    addToCartPopupShow: addToCartPopupShow,
    colorList: colorList,
    sizeList: sizeList,
    otherList: otherList,
    quantityInput: quantityInput,
    colorIndex: colorIndex,
    sizeIndex: sizeIndex,
    otherIndex: otherIndex,
    products: products,
    productIndex: productIndex,
    productImageIndex: productImageIndex,
}