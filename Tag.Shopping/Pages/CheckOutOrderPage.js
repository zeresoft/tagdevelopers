
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var shopId;
var address_id;
var shopTitle = Observable("");

var currency = Observable("");
var subTotal = Observable(0);
var deliveryCharges = Observable(0);
var total = Observable();
var totalItems = Observable();

var confirmationPopupShow = Observable(false);

var deliveryOptionList = Observable();
var deliveryOptionId = Observable(-1);


this.Parameter.onValueChanged(module, function (param) {
    console.log("CheckoutPageChanged", JSON.stringify(param));
    console.log("CheckoutPageChanged", JSON.stringify(param.shop_id));

    deliveryOptionList.clear();
    deliveryOptionId.value = -1;

    currency.value = "";
    subTotal.value = 0;
    deliveryCharges.value = 0;
    total.value = 0;
    totalItems.value = 0;

    if (param.shop_id != null) {

        shopId = param.shop_id;
        address_id = param.address_id;
        shopTitle.value = param.shop_title;

        currency.value = param.currency;
        subTotal.value = param.subTotal;
        totalItems.value = param.totalItems;

        getDeliveryOptionsList();
    }
});


function getDeliveryOptionsList() {
    busy.activate();
    deliveryOptionList.clear();

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("Shop/GetShippingOption", apiBodyObj).then(function (result) {
        gotDeliveryOptionsList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotDeliveryOptionsList(result) {
    console.log("gotDeliveryOptionsList")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;
        if (resultArr !== null && resultArr.length > 0) {
            deliveryOptionList.addAll(resultArr);
            deliveryOptionId.value = resultArr[0].id;

            doCalculation();
        }
    }
}

function deliveryOptionsChange(args) {
    doCalculation();
}

function doCalculation() {
    var resultArr = deliveryOptionList.toArray();
    var index = -1;
    for (var i = 0; i < resultArr.length; i++) {
        if (resultArr[i].id == deliveryOptionId.value) {
            index = i;
            break;
        }
    }

    if (index != -1) {
        // deliveryCharges.value = resultArr[index].shipping_charge* totalItems.value;
        deliveryCharges.value = resultArr[index].shipping_charge;
    } else {
        deliveryCharges.value = 0;
    }

    total.value = subTotal.value + deliveryCharges.value;
}

function orderBackHandler() {
    router.goBack();
}

function orderConfirmationHandler() {
    if (deliveryOptionId.value == -1) {
        tagEvents.emit("toastShow", { message: "Please specify a shipping method" });
        return;
    }

    console.log("orderConfirmationHandler-------------------");
    confirmationPopupShow.value = true;
}

function cancelPlaceOrderClickHandler() {
    console.log("cancelPlaceOrderClickHandler");
    confirmationPopupShow.value = false;
}

function placeOrderClickHandler() {
    cancelPlaceOrderClickHandler();

    console.log("placeOrderClickHandler");

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;
    apiBodyObj.address_id = address_id;
    apiBodyObj.option_id = deliveryOptionId.value;

    _WebService.request("shop/order", apiBodyObj).then(function (result) {
        gotOrderPlaceResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotOrderPlaceResult(result) {
    console.log("gotOrderPlaceResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        router.goto("checkOutMessagePage");
    } else {
        if (arr.error == "insuffcient_balance") {
            tagEvents.emit("toastShow", { message: "Insufficient balance" });
        } else if (arr.error == "stock_not_available") {
            tagEvents.emit("toastShow", { message: "One or more item is out of stock" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    shopTitle: shopTitle,

    currency: currency,
    subTotal: subTotal,
    deliveryCharges: deliveryCharges,
    total: total,
    totalItems: totalItems,

    deliveryOptionList: deliveryOptionList,
    deliveryOptionId: deliveryOptionId,
    deliveryOptionsChange: deliveryOptionsChange,

    confirmationPopupShow: confirmationPopupShow,

    orderBackHandler: orderBackHandler,
    placeOrderClickHandler: placeOrderClickHandler,
    cancelPlaceOrderClickHandler: cancelPlaceOrderClickHandler,
    orderConfirmationHandler: orderConfirmationHandler,

}