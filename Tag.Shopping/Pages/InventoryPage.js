
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var shopId;
var productId;
var shopTitle = Observable("");
var productList = Observable();
var showDeleteConfirmationPopup = Observable(false);
var searchkeyword = Observable("");

this.Parameter.onValueChanged(module, function (param) {
    console.log("InventoryPageChanged", JSON.stringify(param));

    shopId = param.shop_id;
    shopTitle.value = param.shop_title;
    // getProductList();
});

function onViewActivate() {
    productList.clear();
    searchkeyword.value = "";
    getProductList();
}
function searchIconClickHandler() {
    productList.clear();
    getProductList();
}

function getProductList() {
    console.log("getProductList");

    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.shop_id = shopId;
    if (searchkeyword.value != "") {
        apiBodyObj.name = searchkeyword.value;
    }

    _WebService.request("shop/MyInventory", apiBodyObj).then(function (result) {
        gotProductList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotProductList(result) {
    console.log("gotProductList")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;
        if (resultArr != null) {
            productList.addAll(resultArr);
        }
    }
}


function productConfirmClickHandler(args) {
    console.log("productConfirmClickHandler", JSON.stringify(args));
    var productDetail = args.data
    productId = productDetail.id;
    showDeleteConfirmationPopup.value = true;
}

function productDeleteClickHandler() {
    console.log("productDeleteClickHandler", productId);

    if (!productId) {
        tagEvents.emit("toastShow", { message: "Product id is not valid" });
        return false;
    }

    showDeleteConfirmationPopup.value = false;

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = productId;

    _WebService.request("shop/Deleteinventory", apiBodyObj).then(function (result) {
        productDeleteResponseHandler(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function productDeleteResponseHandler(result) {
    console.log("productDeleteResponseHandler");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        productList.removeWhere(function (product) {
            return product.id == productId;
        });
        tagEvents.emit("toastShow", { message: 'Product deleted successfully' });
    } else {
        tagEvents.emit("toastShow", { message: result.error });
    }
}


function productClickHandler(result) {
    console.log("createProductClickHandler", JSON.stringify(result));
    router.push("inventoryCreatePage", { shop_id: shopId, shop_title: shopTitle.value, product_details: result ? result.data : null, datId: Math.random() });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivate: onViewActivate,
    productClickHandler: productClickHandler,
    productConfirmClickHandler: productConfirmClickHandler,
    productDeleteClickHandler: productDeleteClickHandler,
    searchIconClickHandler: searchIconClickHandler,

    shopTitle: shopTitle,
    productList: productList,
    showDeleteConfirmationPopup: showDeleteConfirmationPopup,
    searchkeyword: searchkeyword

}