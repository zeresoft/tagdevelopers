var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');

var showTitle = Observable(false);
var selectedPageIndex = Observable(0);

var tabData = [loc.value.shops, loc.value.puchase_history];

function onViewActivate() {
    console.log("onViewActivate");

    selectedPageIndex.value = 0;
}

this.Parameter.onValueChanged(module, function (param) {
    console.log("UserShoppingPage", JSON.stringify(param));

    if (param && param.showTitle == true)
        showTitle.value = true;
    else
        showTitle.value = false;
});

selectedPageIndex.onValueChanged(module, function (selIndex) {
    console.log(JSON.stringify(selIndex));

    if (selIndex == 0) {
        router.gotoRelative(inner, "userShopListPage");
    } else if (selIndex == 1) {
        router.gotoRelative(inner, "userHistoryPage");
    } 
});

module.exports = {
    onViewActivate: onViewActivate,

    showTitle: showTitle,
    selectedPageIndex: selectedPageIndex,
    tabData: tabData
}