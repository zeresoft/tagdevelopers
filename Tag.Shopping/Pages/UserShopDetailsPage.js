
var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');

var dataLoadingComplete = Observable(false);

var shopId;
var shopTitle = Observable("");
var shopLogoUrl = Observable("");
var shopImageUrl = Observable("");
var shopDescription = Observable("");

var shopCategoryId = 0;
var searchkeyword = Observable("");
var categoryDropdownIndex = Observable();
var productList = Observable();
var categoriesList = Observable();
var listView = Observable(true);
var gridView = Observable(false);

var loadingAnimShow = Observable(true);
var filterPopupShow = Observable(false);

var shopRatingAvg = Observable("0");
var shopRatingValue = Observable();
var ratingPossible = Observable(false);
var ratingPopupShow = Observable(false);


var totalReview = Observable(0);
var reviewPossible = Observable("no");
var reviewAddPopupShow = Observable(false);
var reviewId = "";
var shopReviewNote = Observable();
var shopReviewListShow = Observable(false);
var shopReviewList = Observable();

this.Parameter.onValueChanged(module, function (param) {
    console.log("ProdctPageChanged", JSON.stringify(param));
    loadingAnimShow.value = true;
    dataLoadingComplete.value = false;
    shopReviewListShow.value = false;

    shopRatingAvg.value = "0";
    shopRatingValue.value = 0;
    ratingPossible.value = false;

    totalReview.value = 0;
    reviewPossible.value = "no";

    productList.clear();
    categoriesList.clear();
    categoryDropdownIndex.value = -1;
    searchkeyword.value = "";

    shopTitle.value = "";
    shopLogoUrl.value = "";
    shopImageUrl.value = "";
    shopDescription.value = "";
    shopCategoryId = 0;

    if (param.shop_id && param.shop_id != null) {
        listView.value = true;
        gridView.value = false;

        shopId = param.shop_id;

        getShopDetails();
    } else {
        router.goBack();
    }

});

function getShopDetails() {
    console.log("getShopDetails");
    loadingAnimShow.value = true;

    var apiBodyObj = {};
    apiBodyObj.id = shopId;

    _WebService.request("shop/Details", apiBodyObj).then(function (result) {
        gotShopDetails(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotShopDetails(result) {
    console.log("gotShopDetails");

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var shopData = arr.list;

        shopTitle.value = shopData.title;
        shopDescription.value = shopData.description;

        shopRatingAvg.value = shopData.rating;
        if (shopData.rating_allowed == "yes") {
            ratingPossible.value = true;
        } else {
            ratingPossible.value = false;
        }


        totalReview.value = shopData.review;
        reviewPossible.value = shopData.review_allowed;

        if (shopData.header_image) {
            console.log("header_image : " + shopData.header_image);
            shopImageUrl.value = shopData.header_image;
        }

        console.log("header_image : " + shopImageUrl.value);
        if (shopData.logo) {
            console.log("Logo : " + shopData.logo);
            shopLogoUrl.value = shopData.logo;

            if (shopImageUrl.value == "") {
                shopImageUrl.value = shopData.logo;
            }
        }
        console.log("Logo : " + shopLogoUrl.value);
        console.log("header_image : " + shopImageUrl.value);

        tagEvents.emit("cartItemsCountEmit", { shop_id: shopId, shop_name: shopData.title });
        getCategoriesList();

    }
}


function getCategoriesList() {
    console.log("getCategoriesList");

    categoriesList.clear();

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("shop/ListCategory", apiBodyObj).then(function (result) {
        gotCategoriesList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCategoriesList(result) {
    console.log("gotCategoriesList", JSON.stringify(result));
    dataLoadingComplete.value = true;
    loadingAnimShow.value = false;

    var arr = result;

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr.length > 0) {
            categoriesList.add({ id: 0, name: loc.value.all_categories });

            categoriesList.addAll(resultArr);
            categoryDropdownIndex.value = 0;
        }

        getProductList();

    }
}

function searchIconClickHandler() {
    console.log("searchIconClickHandler");
    getProductList();
}

function categoryDropdownChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log("categoryDropdownChange-------------------", JSON.stringify(selectedItem));

    shopCategoryId = selectedItem.id;
    categoryDropdownIndex.value = selectedItem.indexLocal;
    getProductList();
}

function getProductList() {
    console.log("getProductList");
    filterPopupShow.value = false;

    productList.clear();
    loadingAnimShow.value = true;

    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    if (shopCategoryId != 0) {
        apiBodyObj.category_id = shopCategoryId;
    }

    if (searchkeyword.value != "") {
        apiBodyObj.name = searchkeyword.value;
    }

    _WebService.request("shop/ProductList", apiBodyObj).then(function (result) {
        gotProductList(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotProductList(result) {
    console.log("gotProductList")
    loadingAnimShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr !== null) {
            var resultArrProcess = [];

            for (var i = 0; i < resultArr.length; i++) {
                var item = {};
                item = resultArr[i];

                item.descriptionDisplay = truncateWods(resultArr[i].description,10);

                var favoriteObs = Observable();
                favoriteObs.value = item.favorite;
                item.favoriteObs = favoriteObs;

                if (item.favorite == true) {
                    item.favoriteIdObs = item.favorite_id;
                } else {
                    item.favoriteIdObs = "";
                }

                resultArrProcess.push(item);
            }

            productList.addAll(resultArrProcess);
        }
    }
}

function truncateWods(str, no_words) {
    return str.split(" ").splice(0,no_words).join(" ");
}

function productClickHandler(args) {
    console.log("productClickHandler", JSON.stringify(args));
    var product_details = args.data;

    router.push("userProductDetailsPage", { product_id: product_details.id, shop_id: shopId, shop_title: shopTitle.value, datId: Math.random() });
}

function showGridView() {
    console.log("showGridView");
    gridView.value = true;
    listView.value = false;
}

function showListView() {
    console.log("showListView");
    gridView.value = false;
    listView.value = true;
}

function favoriteClickHandler(args) {
    console.log("favoriteClickHandler", JSON.stringify(args.data));
    var product_details = args.data;
    var favoriteAddingProcess = false;

    loadingAnimShow.value = true;

    var apiBodyObj = {};

    var apiUrl = "";
    if (product_details.favoriteObs.value == true) {
        console.log("favorite_id");
        apiBodyObj.id = product_details.favoriteIdObs;
        apiUrl = "shop/RemoveFavourite";
        args.data.favoriteObs.value = false;
    } else {
        favoriteAddingProcess = true;
        console.log("productId");
        apiBodyObj.product_id = product_details.id;
        apiUrl = "shop/AddFavorite";
        args.data.favoriteObs.value = true;
    }

    _WebService.request(apiUrl, apiBodyObj).then(function (result) {
        loadingAnimShow.value = false;

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {

            if (favoriteAddingProcess) {
                tagEvents.emit("toastShow", { message: "Added to Wishlist" });
                args.data.favoriteObs.value = true;
                args.data.favoriteIdObs = arr.id;
            } else {
                args.data.favoriteObs.value = false;
                args.data.favoriteIdObs = "";
            }
        } else {
            if (arr.error == "already_added") {
                tagEvents.emit("toastShow", { message: "Already added to wishlist" });
            } else {
                tagEvents.emit("toastShow", { message: loc.value.error_occurred });
            }
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function ratingAddClicked() {
    shopRatingValue.value = 0;
    ratingPopupShow.value = true;
}

function saveShopRating() {
    console.log("shopRatingValue " + shopRatingValue.value)
    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;
    apiBodyObj.rating_value = shopRatingValue.value;
    _WebService.request("Shop/ShopRating", apiBodyObj).then(function (result) {
        gotRatingUpdateResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotRatingUpdateResult(result) {
    console.log("gotRatingUpdateResult")
    ratingPopupShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        shopRatingAvg.value = arr.rating;
        tagEvents.emit("toastShow", { message: "Rating updated" });
    } else {
        tagEvents.emit("toastShow", { message: "Could not update rating. Please try again later" });

    }
}


tagEvents.on("shopFeedbackEvent", function (arg) {
    console.log("shopFeedbackEvent", JSON.stringify(arg));
    openReviewList();
});

function openReviewList() {
    getShopReviewList();
    shopReviewListShow.value = true;
}
function closeReviewList() {
    shopReviewListShow.value = false;
}

function getShopReviewList() {
    loadingAnimShow.value = true;

    shopReviewList.clear();
    var apiBodyObj = {};
    apiBodyObj.shop_id = shopId;

    _WebService.request("Shop/ShopReviewList", apiBodyObj).then(function (result) {
        gotReviewListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotReviewListResult(result) {
    console.log("gotReviewListResult")
    loadingAnimShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.list;

        if (resultArr !== null) {
            var resultArrProcessed = [];
            for (var i = 0; i < resultArr.length; i++) {
                var obj = {};
                obj = resultArr[i];
                obj.imgPath = _model.userImageUrl + resultArr[i].user_id + "?kycImage=0";
                obj.commentDisplay = Observable(resultArr[i].comment);

                resultArrProcessed.push(obj);
            }

            shopReviewList.addAll(resultArrProcessed);
        }
    }
}

function reviewAddClicked() {
    shopReviewNote.value = "";
    reviewId = "";

    reviewAddPopupShow.value = true;
}


function editShopReview(args) {
    console.log(JSON.stringify(args))
    reviewId = args.data.id;
    shopReviewNote.value = args.data.comment;

    reviewAddPopupShow.value = true;
}

function saveUpdateShopReview() {
    var apiBodyObj = {};
    apiBodyObj.comment = shopReviewNote.value;

    if (reviewId != "") {
        apiBodyObj.id = reviewId;

        _WebService.request("Shop/ShopReviewEdit", apiBodyObj).then(function (result) {
            gotReviewEditedResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        apiBodyObj.shop_id = shopId;

        _WebService.request("Shop/ShopReview", apiBodyObj).then(function (result) {
            gotReviewSavedResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }
}


function gotReviewEditedResult(result) {
    console.log("gotReviewEditedResult")

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        reviewAddPopupShow.value = false;

        shopReviewList.forEach(function (item, index) {
            console.log(item.id + "    " + reviewId)
            if (item.id == reviewId) {
                item.commentDisplay.value = shopReviewNote.value;
            }
        });

        tagEvents.emit("toastShow", { message: "Successfully Updated" });
    } else {
        tagEvents.emit("toastShow", { message: "Could not add review. Please try again later" });
    }
}

function gotReviewSavedResult(result) {
    console.log("gotReviewSavedResult")

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        reviewAddPopupShow.value = false;
        totalReview.value = totalReview.value + 1;
        getShopReviewList();

        tagEvents.emit("toastShow", { message: "Successfully Created" });
    } else {
        tagEvents.emit("toastShow", { message: "Could not add review. Please try again later" });
    }

}

function deleteShopReview(args) {
    console.log("delete" + JSON.stringify(args))
    loadingAnimShow.value = true;

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("Shop/ShopReviewDelete", apiBodyObj).then(function (result) {
        loadingAnimShow.value = false;

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {
            shopReviewList.remove(args.data);
        } else {
            tagEvents.emit("toastShow", { message: "Unable to remove this review now, please try again later" });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}




function showNetworkErrorMessage() {
    loadingAnimShow.value = false;

    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    dataLoadingComplete: dataLoadingComplete,
    loadingAnimShow: loadingAnimShow,

    productClickHandler: productClickHandler,
    categoryDropdownChange: categoryDropdownChange,
    showGridView: showGridView,
    showListView: showListView,
    searchIconClickHandler: searchIconClickHandler,

    gridView: gridView,
    listView: listView,
    shopTitle: shopTitle,
    categoryDropdownIndex: categoryDropdownIndex,
    productList: productList,
    categoriesList: categoriesList,
    shopId: shopId,
    shopDescription: shopDescription,
    shopImageUrl: shopImageUrl,
    shopLogoUrl: shopLogoUrl,
    shopCategoryId: shopCategoryId,
    searchkeyword: searchkeyword,

    shopRatingValue: shopRatingValue,
    shopRatingAvg: shopRatingAvg,
    ratingPossible: ratingPossible,
    saveShopRating: saveShopRating,
    ratingPopupShow: ratingPopupShow,
    ratingAddClicked: ratingAddClicked,


    totalReview: totalReview,
    reviewPossible: reviewPossible,
    reviewAddClicked: reviewAddClicked,
    reviewAddPopupShow: reviewAddPopupShow,
    shopReviewNote: shopReviewNote,
    saveUpdateShopReview: saveUpdateShopReview,
    openReviewList: openReviewList,
    closeReviewList: closeReviewList,
    shopReviewListShow: shopReviewListShow,
    shopReviewList: shopReviewList,
    deleteShopReview: deleteShopReview,
    editShopReview: editShopReview,

    favoriteClickHandler: favoriteClickHandler,

    filterPopupShow: filterPopupShow,
}