var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');
var Validator = require('Utils/Validator');
var companyTypes = Observable({ index: 0, name: "Corporation or Partnership", value: "corporation" }, { index: 1, name: "Single Proprietorship", value: "single" });
var registrarionItems = Observable({ index: 0, name: "Yes", value: "yes" }, { index: 1, name: "No", value: "no" });
var amlaRegulationItems = Observable({ index: 0, name: "Yes", value: "yes" }, { index: 1, name: "No", value: "no" });
var imageSelectPopupShow = Observable(false);
var AdditionalDocumentsBoo = Observable(false);
var DirctorsShareHolderBoo = Observable(false);
var SecretaryCertificateBoo = Observable(false);
var CompanyRegsterImageBoo = Observable(false);
var CompanyProfileImageBoo = Observable(false);
var businessNameTxt = Observable("");
var taxVatID = Observable("");
var websiteURL = Observable("");
var officeAdress = Observable("");
var cityTxt = Observable("");
var stateTxt = Observable("");
var countryCode = Observable("");
var postCode = Observable("");
var natureOfBusiness = Observable("");
var fullNameBeneOwners = Observable("");
var dailyMaximValue = Observable("");
var monthlyMinValue = Observable("");
var intendedUseAccount = Observable();
var sourceOfFund = Observable();
var companyRegsterImage = Observable("");
var additionalDocumentsImage = Observable("");
var secretaryCertificate = Observable("");
var dirctorsShareHolderImage = Observable("");
var companyTypesIndex = Observable(-1);
var registrarionTypesIndex = Observable(-1);
var amlaRegulationItemsIndex = Observable(-1);
var companyRegsterDataB64;
var secretaryCertificateDataB64;
var dirctorsShareHolderDataB64;
var aditionalDocumentsDataB64;
var companyProfileImageDataB64;
var companyIndex;
var registerIndex;
var amalaIndex;
var kycVerifyMerchantBoo = Observable(false);
var allImageData = [];
var tabData = ["VERIFY", "EXTRA"];
var kycMerchDataVerifyStatus = Observable("");
var selectedItem = Observable();
var registrarion = Observable();
var amlaRegulation = Observable();
var countryName = Observable("");
var profileHashKey = Observable("");
var companyHashKey = Observable("");
var secretaryHashKey = Observable("");
var directorsHashKey = Observable("");
var kycMerchDataHashKey = Observable("");

function companyTypesChangeEvent(args) {
    selectedItem.value = companyTypes.getAt(args.selectedIndex);
   console.log(JSON.stringify(selectedItem.value))

}
function registrarionTypesChangeEvent(args) {
    registrarion.value = registrarionItems.getAt(args.selectedIndex);
    console.log(JSON.stringify(registrarion.value))
   
}
function amlaRegulationChangeEvent(args) {
    amlaRegulation.value = amlaRegulationItems.getAt(args.selectedIndex);
    console.log(JSON.stringify(amlaRegulation.value))

     
}
function onCountryChangeEvent(args) {
  
    console.log(args.countryID, args.countryName);
    countryCode.value = args.countryID;
    countryName.value = args.countryName;

}

function onViewActivateCreate() {
    kycMerchDataVerifyStatus.value = "";
    getKYCMerchantVerifiedData();
   
}



function getKYCMerchantVerifiedData(){
    busy.activate();
    var apiBodyObj = {};
 
    _WebService.request("community/GetMerchantKycData", apiBodyObj).then(function (result) {
        gotKYCMerchVeriDataResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotKYCMerchVeriDataResult(result){
    console.log("gotKYCMerchVeriDataResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {


                var resultArr = arr.result;        
                if(resultArr.status == "pending"){
                    kycMerchDataVerifyStatus.value = resultArr.status;
                }else if(resultArr.status == "unapproved"){
                    kycMerchDataVerifyStatus.value = resultArr.status;
                }else if(resultArr.status == "approved"){
                    kycMerchDataVerifyStatus.value = resultArr.status;
                    kycMerchDataHashKey.value = resultArr.data_hash;
                }else{
                    kycMerchDataVerifyStatus.value = "new";
                    getCompanyRegsterImage();

                   }

          
 

    }else{
        if(arr.error == "info_missing"){
            kycMerchDataVerifyStatus.value = "new";
            profileImgPendingStatus.value = "new";
            companyImgPendingStatus.value = "new";
            secretaryCertificateStatus.value = "new";
            directorsShareholdersStatus.value = "new";
            extrasKycMerchantStatus.value = "new";
            getCompanyRegsterImage();
          
           

         }
    }

}
function onVerificationClick(){
            kycMerchDataVerifyStatus.value = "new";
            profileImgPendingStatus.value = "new";
            companyImgPendingStatus.value = "new";
            secretaryCertificateStatus.value = "new";
            directorsShareholdersStatus.value = "new";
            extrasKycMerchantStatus.value = "new";
            getCompanyRegsterImage();
}

function getCompanyRegsterImage(){
    busy.activate();
    var apiBodyObj = {};
   // apiBodyObj.type = "register";
   var merchantDatas = ["company_registration","profile_image","secretary_certificate","directors_and_major_shareholders","extras"];
   var merchantDatasJson = JSON.stringify(merchantDatas);
    apiBodyObj.verification_type = merchantDatasJson;
    _WebService.request("verification/status", apiBodyObj).then(function (result) {
        gotCompanyRegsterImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}
var profileImgPendingStatus = Observable("");
var companyImgPendingStatus = Observable("");
var secretaryCertificateStatus = Observable("");
var directorsShareholdersStatus = Observable("");
var extrasKycMerchantStatus = Observable("");
function gotCompanyRegsterImageResult(result){
    console.log("gotCompanyRegsterImageResult")
    busy.deactivate();

    var arr = result;
   
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        allImageData  = resultArr;
        console.log(allImageData.length);
        for(var i in allImageData){
            if(allImageData[i].verification_type == "profile_image"){
              
                profileImgPendingStatus.value = "";
               
               
                if(allImageData[i].status == "pending"){
                    profileImgPendingStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "unapproved"){
                    console.log("ttttttttttttttttttttttttttttt")
                    profileImgPendingStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "approved"){
                    profileImgPendingStatus.value = allImageData[i].status;
                    profileHashKey.value = allImageData[i].hash_proof;
                }else{
                 
                  profileImgPendingStatus.value = "new";
                }
                console.log(profileImgPendingStatus.value);
                
            }else if(allImageData[i].verification_type == "company_registration"){
               // companyImgPendingStatus.value = "";
              
                if(allImageData[i].status == "pending"){
                    companyImgPendingStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "unapproved"){
                    companyImgPendingStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "approved"){
                    companyImgPendingStatus.value = allImageData[i].status;
                    companyHashKey.value = allImageData[i].hash_proof;
                }else{
                    companyImgPendingStatus.value = "new";
                }
              
                             

            }else if(allImageData[i].verification_type == "secretary_certificate"){
              
              //  secretaryCertificateStatus.value = "";
                if(allImageData[i].status == "pending"){
                    secretaryCertificateStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "unapproved"){
                    secretaryCertificateStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "approved"){
                    secretaryCertificateStatus.value = allImageData[i].status;
                    secretaryHashKey.value = allImageData[i].hash_proof;
                }else{
                    secretaryCertificateStatus.value = "new";
                }
              
        

              }else if(allImageData[i].verification_type == "directors_and_major_shareholders"){
               // directorsShareholdersStatus.value = "";
                if(allImageData[i].status == "pending"){
                    directorsShareholdersStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "unapproved"){
                    directorsShareholdersStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "approved"){
                    directorsShareholdersStatus.value = allImageData[i].status;
                    directorsHashKey.value = allImageData[i].hash_proof;;
                }else{
                    directorsShareholdersStatus.value = "new";
                }
              
                
        

              }else if(allImageData[i].verification_type == "extras"){
                //extrasKycMerchantStatus.value = "";
                if(allImageData[i].status == "pending"){
                    extrasKycMerchantStatus.value = allImageData[i].status;
                }else if(allImageData[i].status == "unapproved"){
                    extrasKycMerchantStatus.value = allImageData[i].status;
                }else if(allImageData[i].statuss == "approved"){
                    extrasKycMerchantStatus.value = allImageData[i].status;
                }else{
                    extrasKycMerchantStatus.value = "new";
                }
               
        

              }
        }
      
         //   companyRegsterImage.value = resultArr.imageUrl;
         
          //  getSecretaryCertificateImage();


    }else{
        if(arr.error == "status_fetching_failed"){
          
             profileImgPendingStatus.value = "new";
             companyImgPendingStatus.value = "new";
             secretaryCertificateStatus.value = "new";
             directorsShareholdersStatus.value = "new";
             extrasKycMerchantStatus.value = "new";

         }
       
    }

}
function uploadNewProfile(){
    profileImgPendingStatus.value = "new";

}
function uploadNewRegistration(){
    companyImgPendingStatus.value = "new";
}
function uploadNewCertificat(){
    secretaryCertificateStatus.value = "new";
}
function uploadNewShareholdersDoc(){
    directorsShareholdersStatus.value = "new";
}
function uploadNewAdditionaDoc(){
    extrasKycMerchantStatus.value = "new";
}

function onProfileImageClick(){

    imageSelectPopupShow.value = true;
    CompanyProfileImageBoo.value = true;
    AdditionalDocumentsBoo.value = false;
    DirctorsShareHolderBoo.value = false;
    SecretaryCertificateBoo.value = false;
    CompanyRegsterImageBoo.value = false;

}

function onAdditionalDocImageClick(){

    imageSelectPopupShow.value = true;
    AdditionalDocumentsBoo.value = true;
    DirctorsShareHolderBoo.value = false;
    SecretaryCertificateBoo.value = false;
    CompanyRegsterImageBoo.value = false;
    CompanyProfileImageBoo.value = false;

}
function onShareHolderImageClick(){
    imageSelectPopupShow.value = true;
    AdditionalDocumentsBoo.value = false;
    DirctorsShareHolderBoo.value = true;
    SecretaryCertificateBoo.value = false;
    CompanyRegsterImageBoo.value = false;
    CompanyProfileImageBoo.value = false;

}
function onSecCertificateimageClick()
{
    imageSelectPopupShow.value = true;
    AdditionalDocumentsBoo.value = false;
    DirctorsShareHolderBoo.value = false;
    SecretaryCertificateBoo.value = true;
    CompanyRegsterImageBoo.value = false;
    CompanyProfileImageBoo.value = false;

}
function onCompanyRegsterImgClick(args) {
   
    
    imageSelectPopupShow.value = true;
    AdditionalDocumentsBoo.value = false;
    DirctorsShareHolderBoo.value = false;
    SecretaryCertificateBoo.value = false;
    CompanyRegsterImageBoo.value = true;
    CompanyProfileImageBoo.value = false;
   
    
}


function onUserImagegSelectionComplete(args) {
    imageSelectPopupShow.value = false;
  //  console.log(selectImageCategory)
    busy.activate();
    var apiBodyObj = {};
    if(CompanyRegsterImageBoo.value == true){
      // companyRegsterImage.value = args.imagePath;
       companyRegsterDataB64 =  encodeURIComponent(args.imageData);
       apiBodyObj.upload_type = 'company_registration';
       apiBodyObj.data = companyRegsterDataB64;

    }else if(SecretaryCertificateBoo.value == true){
      //  secretaryCertificate.value = args.imagePath;
        secretaryCertificateDataB64 =  encodeURIComponent(args.imageData);
        apiBodyObj.upload_type = "secretary_certificate";
        apiBodyObj.data = secretaryCertificateDataB64;

    }else if(DirctorsShareHolderBoo.value == true){
        //dirctorsShareHolderImage.value = args.imagePath;
        dirctorsShareHolderDataB64 =  encodeURIComponent(args.imageData);
        apiBodyObj.upload_type = "directors_and_major_shareholders";
        apiBodyObj.data = dirctorsShareHolderDataB64;

    }else if(AdditionalDocumentsBoo.value == true){
       // additionalDocumentsImage.value = args.imagePath;
        aditionalDocumentsDataB64 =  encodeURIComponent(args.imageData);
        apiBodyObj.upload_type = "extras";
        apiBodyObj.data = aditionalDocumentsDataB64;
    }
    else if(CompanyProfileImageBoo.value == true){
  
         companyProfileImageDataB64 =  encodeURIComponent(args.imageData);
         apiBodyObj.upload_type = "profile_image";
         apiBodyObj.data = companyProfileImageDataB64;
     }

    _WebService.request("verification/Upload", apiBodyObj).then(function (result) {
        gotkycMerchaImgUploadResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
   

}


function gotkycMerchaImgUploadResult(result){
    console.log("gotkycMerchaImgUploadResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
         getCompanyRegsterImage();

    
    }else{
        tagEvents.emit("toastShow", { message: arr.error });
    }

}
var business_type;
var business_reg;
var business_regulation;
function createKycMerchantClick(){
    console.log(companyRegsterImage.value)
    if (businessNameTxt.value === "" ) {
        tagEvents.emit("toastShow", { message: "Business Name should not be empty" });
        return;
    }
    if (countryCode.value === "") {
        tagEvents.emit("toastShow", { message: "Country Should  Be Select" });
        return;
    }
    if (taxVatID.value === "" ) {
        tagEvents.emit("toastShow", { message: "TaxVatID Name should not be empty" });
        return;
    }
  
    if (officeAdress.value === "" ) {
        tagEvents.emit("toastShow", { message: "Office Adress should not be empty" });
        return;
    }
    if (cityTxt.value === "" ) {
        tagEvents.emit("toastShow", { message: "City should not be empty" });
        return;
    }
    if (stateTxt.value === "" ) {
        tagEvents.emit("toastShow", { message: "State should not be empty" });
        return;
    }
    if (!Validator.amount(postCode.value)) {
        tagEvents.emit("toastShow", { message: "PostCode  should  be valid" });
        return;
    }
    if (companyTypesIndex.value === -1) {
        tagEvents.emit("toastShow", { message: "Company Type should not be null" });
        return;
    }
     if (registrarionTypesIndex.value === -1) {
         tagEvents.emit("toastShow", { message: "Registrarion Type should not be null" });
         return;
     }
     if (amlaRegulationItemsIndex.value === -1) {
         tagEvents.emit("toastShow", { message: "AmlaRegulation Type should not be null" });
         return;
     }
    if (natureOfBusiness.value === "" ) {
        tagEvents.emit("toastShow", { message: "Nature Of Business  should not be empty" });
        return;
    }
    if (fullNameBeneOwners.value === "" ) {
        tagEvents.emit("toastShow", { message: "FullName BeneOwners  should not be empty" });
        return;
    }
    if (!Validator.amount(dailyMaximValue.value)) {
        tagEvents.emit("toastShow", { message: "Daily MaximValue number  should  be valid" });
        return;
    }
    if (!Validator.amount(monthlyMinValue.value)) {
        tagEvents.emit("toastShow", { message: "Monthly MinValue number  should  be valid" });
        return;
    }
    if (intendedUseAccount.value === "" ) {
        tagEvents.emit("toastShow", { message: "Intended UseAccount  should not be empty" });
        return;
    }
    if (sourceOfFund.value === "" ) {
        tagEvents.emit("toastShow", { message: "Source Of Fund  should not be empty" });
        return;
    }
    if (profileImgPendingStatus.value == "new" ) {
        tagEvents.emit("toastShow", { message: "Profile Imgage  should not be empty" });
       return;
    }
     if (companyImgPendingStatus.value == "new" ) {
         tagEvents.emit("toastShow", { message: "Company RegsterImage should not be empty" });
        return;
     }
     if (secretaryCertificateStatus.value == "new" ) {
        tagEvents.emit("toastShow", { message: "Secretary Certificate Data should not be empty" });
         return;
    }
     if (directorsShareholdersStatus.value == "new" ) {
         tagEvents.emit("toastShow", { message: "Dirctors ShareHolder Data should not be empty" });
         return;
     }
    if(selectedItem.value.index == 0)
    {
     
        business_type = "corporation";
    }else{
     
        business_type = "single";
    }

    if(registrarion.value.index == 0)
    {
        business_reg = "yes";
    }else{
        business_reg = "no";
    }
    if(amlaRegulation.value.index == 0)
    {
        business_regulation = "yes";
    }else{
        business_regulation = "no";
    }
    
    
    console.log(business_reg)
    console.log(business_regulation)
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.community_id = _model.nowCommunityID;
    apiBodyObj.business_name = businessNameTxt.value;
    apiBodyObj.vat_id = taxVatID.value;
    apiBodyObj.company_website = websiteURL.value;
    apiBodyObj.office_address = officeAdress.value;
    apiBodyObj.country = countryCode.value;
    apiBodyObj.city = cityTxt.value;
    apiBodyObj.state = stateTxt.value;
    apiBodyObj.postal = postCode.value;
    apiBodyObj.business_type = business_type;
    apiBodyObj.business_reg = business_reg;
    apiBodyObj.business_regulation = business_regulation;
    apiBodyObj.business_nature = natureOfBusiness.value;
    apiBodyObj.beneficial_owners = fullNameBeneOwners.value;
    apiBodyObj.daily_maximum = dailyMaximValue.value;
    apiBodyObj.monthly_maximum = monthlyMinValue.value;
    apiBodyObj.account_use = intendedUseAccount.value;
    apiBodyObj.fund_source = sourceOfFund.value;
    apiBodyObj.status = 0;
    _WebService.request("community/MerchantKycData/", apiBodyObj).then(function (result) {
        gotKYCMerchantResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });


}
function gotKYCMerchantResult(result){
    console.log("gotKYCMerchantResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            getKYCMerchantVerifiedData();
            tagEvents.emit("toastShow", { message: loc.value.success });

        }else{

        }

    }

}
function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    cityTxt:cityTxt,
    stateTxt:stateTxt,
    postCode:postCode,
    taxVatID:taxVatID,
    websiteURL:websiteURL,
    countryCode:countryCode,
    sourceOfFund:sourceOfFund,
    companyTypes:companyTypes,
    officeAdress:officeAdress,
    dailyMaximValue:dailyMaximValue,
    monthlyMinValue:monthlyMinValue,
    intendedUseAccount:intendedUseAccount,
    natureOfBusiness:natureOfBusiness,
    fullNameBeneOwners:fullNameBeneOwners,
    registrarionItems:registrarionItems,
    amlaRegulationItems:amlaRegulationItems,
    businessNameTxt:businessNameTxt,
    imageSelectPopupShow:imageSelectPopupShow,
    onCompanyRegsterImgClick:onCompanyRegsterImgClick,
    onUserImagegSelectionComplete:onUserImagegSelectionComplete,
    companyRegsterImage:companyRegsterImage,
    onProfileImageClick:onProfileImageClick,
    onAdditionalDocImageClick:onAdditionalDocImageClick,
    dirctorsShareHolderImage:dirctorsShareHolderImage,
    secretaryCertificate:secretaryCertificate,
    companyTypesIndex:companyTypesIndex,
    registrarionTypesIndex:registrarionTypesIndex,
    amlaRegulationItemsIndex:amlaRegulationItemsIndex,
    AdditionalDocumentsBoo:AdditionalDocumentsBoo,
    DirctorsShareHolderBoo:DirctorsShareHolderBoo,
    SecretaryCertificateBoo:SecretaryCertificateBoo,
    CompanyRegsterImageBoo:CompanyRegsterImageBoo,
    companyTypesChangeEvent:companyTypesChangeEvent,
    amlaRegulationChangeEvent:amlaRegulationChangeEvent,
    registrarionTypesChangeEvent:registrarionTypesChangeEvent,
    onSecCertificateimageClick:onSecCertificateimageClick,
    onShareHolderImageClick:onShareHolderImageClick,
    additionalDocumentsImage:additionalDocumentsImage,
    createKycMerchantClick:createKycMerchantClick,
    onViewActivateCreate:onViewActivateCreate,
    kycVerifyMerchantBoo:kycVerifyMerchantBoo,
    CompanyProfileImageBoo:CompanyProfileImageBoo,
    profileImgPendingStatus:profileImgPendingStatus,
    companyImgPendingStatus:companyImgPendingStatus,
    secretaryCertificateStatus:secretaryCertificateStatus,
    directorsShareholdersStatus:directorsShareholdersStatus,
    extrasKycMerchantStatus:extrasKycMerchantStatus,
    tabData:tabData,
    profileHashKey:profileHashKey,
    secretaryHashKey:secretaryHashKey,
    companyHashKey:companyHashKey,
    directorsHashKey:directorsHashKey,
    kycMerchDataHashKey:kycMerchDataHashKey,
    kycMerchDataVerifyStatus:kycMerchDataVerifyStatus,
    onVerificationClick:onVerificationClick,
    countryName:countryName,
    onCountryChangeEvent:onCountryChangeEvent,
    selectedItem:selectedItem,
    registrarion:registrarion,
    amlaRegulation:amlaRegulation,
    uploadNewProfile:uploadNewProfile,
    uploadNewRegistration:uploadNewRegistration,
    uploadNewCertificat:uploadNewCertificat,
    uploadNewShareholdersDoc:uploadNewShareholdersDoc,
    uploadNewAdditionaDoc:uploadNewAdditionaDoc





}