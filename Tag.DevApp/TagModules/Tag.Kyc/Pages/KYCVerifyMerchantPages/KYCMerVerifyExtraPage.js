var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');
var imageSelectPopupShow = Observable(false);
var aditionalDocumentsDataB64;
var allImageData = Observable();
function onViewActivateCreate() {

    getCompanyRegsterImage();

}
function getCompanyRegsterImage() {
    allImageData.clear();
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.verification_type = "extras";
    _WebService.request("verification/status", apiBodyObj).then(function (result) {
        gotCompanyRegsterImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotCompanyRegsterImageResult(result) {
    console.log("gotCompanyRegsterImageResult")
    busy.deactivate();

    var arr = result;

    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;

        allImageData.addAll(resultArr);
        console.log(allImageData.length);


    } else {
        if (arr.error == "status_fetching_failed") {
            tagEvents.emit("toastShow", { message: "Documents Not Found" });

        }

    }

}
function onAdditionalDocImageClick() {

    imageSelectPopupShow.value = true;

}

function onUserImagegSelectionComplete(args) {
    imageSelectPopupShow.value = false;
    busy.activate();
    var apiBodyObj = {};
    aditionalDocumentsDataB64 = encodeURIComponent(args.imageData);
    apiBodyObj.upload_type = "extras";
    apiBodyObj.data = aditionalDocumentsDataB64;
    _WebService.request("verification/Upload", apiBodyObj).then(function (result) {
        gotkycMerchaImgUploadResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });


}


function gotkycMerchaImgUploadResult(result) {
    console.log("gotkycMerchaImgUploadResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        getCompanyRegsterImage();
    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "file_waiting_for_approval") {
            tagEvents.emit("toastShow", { message: "File is already uploaded " });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }

}
module.exports = {
    onViewActivateCreate: onViewActivateCreate,
    onAdditionalDocImageClick: onAdditionalDocImageClick,
    imageSelectPopupShow: imageSelectPopupShow,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,
    allImageData: allImageData

}