var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var selectedPageIndex = Observable(0);
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var kycVerifyLevTwoData = ["DATA","EXTRA"];
var kycVerifyLevThreeData = ["VERIFY", "EXTRA"];
var tabData = Observable();

var kycVerifyMerchantBoo = Observable(false);

function onViewActivateCreateMain() {

    getKYCVerifiedLevel();


}
function getKYCVerifiedLevel() {
    tabData.clear();

    var apiBodyObj = {};
    _WebService.request("verification/GetLevel", apiBodyObj).then(function (result) {
        gotKYCveriMerchantLevel(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}
function gotKYCveriMerchantLevel(result) {
    console.log("gotKYCveriMerchantLevel")

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {

            if (resultArr.verification_level == 1) {
                kycVerifyMerchantBoo.value = true; 
                tabData.addAll(kycVerifyLevThreeData);
              
            } else {
                kycVerifyMerchantBoo.value = false; 
                tabData.addAll(kycVerifyLevTwoData);
                console.log(resultArr.verification_level);

            }


        } else {


        }

    }

}
function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

selectedPageIndex.onValueChanged(module, function (selIndex) {
    console.log(JSON.stringify(selIndex));
 
    if (kycVerifyMerchantBoo.value == true) {


        if (selIndex == 0) {
            router.gotoRelative(inner, "KYCMerchantVerifyPage");
        }
        else if (selIndex == 1) {
            router.gotoRelative(inner, "KYCMerVerifyExtraPage");
        }
    } else {
        if (selIndex == 0) {
            console.log("rferferferfer" );
            router.gotoRelative(inner, "KYCMerchantVerification");
        }
        else if (selIndex == 1) {
            router.gotoRelative(inner, "KYCMerVerifyExtraPage");
        }

    }

})

module.exports = {
    onViewActivateCreateMain: onViewActivateCreateMain,
    selectedPageIndex: selectedPageIndex,
    tabData: tabData,
    kycVerifyLevTwoData: kycVerifyLevTwoData,
    kycVerifyLevThreeData: kycVerifyLevThreeData,
    kycVerifyMerchantBoo:kycVerifyMerchantBoo

}