var Observable = require('FuseJS/Observable');
var moment = require('Utils/moment');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');
var Preferences = require('Utils/Preferences');

var userEmailDisplay = Observable();
var emailorsmsVerifyPopupShow = Observable(false);

var emailOrMobile = Observable({ index: 0, name: "Email", value: "email" }, { index: 1, name: "Mobile", value: "mobile" });
var emailOrMobileIndex = Observable(0);

var radiomenuShowStat = Observable(false);

var loadingCompleteStat = Observable(false);
var email = Observable("");
var newEmailInput = Observable("");

var userMobileDisplay = Observable("");
var profileCountryCode = Observable("");
var countryCode = Observable("");
var newMobileInput = Observable("");

var emailandsmsVerifyPopupShow = Observable(false);

var level2ButtonEnable = Observable(false);
var level3ButtonEnable = Observable(false);
var level4ButtonEnable = Observable(false);
var level5ButtonEnable = Observable(false);

var level1EmailorSmsverified = Observable(false);
var level2EmailorSmsverified = Observable(false);
var validationSmsCodeInput = Observable("");

var level1CashInAmount = Observable();
var level2CashInAmount = Observable();
var level3CashInAmount = Observable();
var level4CashInAmount = Observable();
var level5CashInAmount = Observable();

var level1CashOutAmount = Observable();
var level2CashOutAmount = Observable();
var level3CashOutAmount = Observable();
var level4CashOutAmount = Observable();
var level5CashOutAmount = Observable();

var level4ProoofAddressStatus = Observable("new");
var level5CustomLimitsStatus = Observable("new");
var level3ImageIDSelfienogovIDstatus = Observable("new");
var verifivationlevel;

var levelOneBySms = false;

function onViewActivate() {
    userVerificationStatusLoad();
}

function userVerificationStatusLoad() {
    emailOrMobileIndex.value = 0;
    verifySmsPageControl.seekToPath("emailPage");

    radiomenuShowStat.value = false;

    email.value = _model.userprofileDetails.value.user_email;
    newEmailInput.value = email.value;

    userMobileDisplay.value = _model.userprofileDetails.value.user_mobile;
    profileCountryCode.value = _model.userprofileDetails.value.country_phonecode;

    countryCode.value = profileCountryCode.value;
    newMobileInput.value = userMobileDisplay.value;

    level3ImageIDSelfienogovIDstatus.value = "new";
    level4ProoofAddressStatus.value = "new";
    level5CustomLimitsStatus.value = "new";


    busy.activate();

    _WebService.request("verification/GetLevel").then(function (result) {
        gotUserVerificationStatusResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUserVerificationStatusResult(result) {
    console.log("gotUserVerificationStatusResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        var inRemainingString = "";
        var outRemainingString = "";

        if (resultObj.in_remaining_amount != 0) {
            inRemainingString = " - " + resultObj.in_remaining_amount + " remaining"
        }
        if (resultObj.out_remaining_amount != 0) {
            outRemainingString = " - " + resultObj.out_remaining_amount + " remaining"
        }


        var levelDetails = resultObj.verification_level_details;
        level1CashInAmount.value = levelDetails[0].cash_in_text;
        level2CashInAmount.value = levelDetails[1].cash_in_text;
        level3CashInAmount.value = levelDetails[2].cash_in_text;
        level4CashInAmount.value = levelDetails[3].cash_in_text;
        level5CashInAmount.value = levelDetails[4].cash_in_text;

        level1CashOutAmount.value = levelDetails[0].cash_out_text;
        level2CashOutAmount.value = levelDetails[1].cash_out_text;
        level3CashOutAmount.value = levelDetails[2].cash_out_text;
        level4CashOutAmount.value = levelDetails[3].cash_out_text;
        level5CashOutAmount.value = levelDetails[4].cash_out_text;

        verifivationlevel = resultObj.verification_level;
        switch (verifivationlevel) {
            case 0:
                radiomenuShowStat.value = true;
                emailOrMobileIndex.value = 0;
                verifySmsPageControl.seekToPath("emailPage");

                level1EmailorSmsverified.value = false;
                level2ButtonEnable.value = false;
                level3ButtonEnable.value = false;
                level4ButtonEnable.value = false;
                level5ButtonEnable.value = false;

                break;
            case 1:
                if (resultObj.level_1_by_sms == true) {
                    console.log("level1 sms verified");
                    levelOneBySms = true;
                } else {
                    console.log("level1 email verified");
                    levelOneBySms = false;
                }

                level2ButtonEnable.value = true;
                level1EmailorSmsverified.value = true;

                level1CashInAmount.value = level1CashInAmount.value + inRemainingString;
                level1CashOutAmount.value = level1CashOutAmount.value + outRemainingString;
                break;
            case 2:
                level1EmailorSmsverified.value = true;
                level2EmailorSmsverified.value = true;
                level2ButtonEnable.value = true;
                level3ButtonEnable.value = true;

                level2CashInAmount.value = level2CashInAmount.value + inRemainingString;
                level2CashOutAmount.value = level2CashOutAmount.value + outRemainingString;
                break;
            case 3:
                level1EmailorSmsverified.value = true;
                level2EmailorSmsverified.value = true;
                level4ButtonEnable.value = true;

                level3CashInAmount.value = level3CashInAmount.value + inRemainingString;
                level3CashOutAmount.value = level3CashOutAmount.value + outRemainingString;
                break;
            case 4:
                level1EmailorSmsverified.value = true;
                level2EmailorSmsverified.value = true;
                level5ButtonEnable.value = true;

                level4CashInAmount.value = level4CashInAmount.value + inRemainingString;
                level4CashOutAmount.value = level4CashOutAmount.value + outRemainingString;
                break;
            case 5:
                level1EmailorSmsverified.value = true;
                level2EmailorSmsverified.value = true;

                level5CashInAmount.value = level5CashInAmount.value + inRemainingString;
                level5CashOutAmount.value = level5CashOutAmount.value + outRemainingString;
                break;
        }


    }

    loadingCompleteStat.value = true;

    if (verifivationlevel >= 2) {
        getStatus();
    }
}

function getStatus() {
    busy.activate();

    _WebService.request("verification/status").then(function (result) {
        gotStatusResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotStatusResult(result) {
    console.log("gotSttausResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultObjarray = arr.result;
        for (var i = 0; i < resultObjarray.length; i++) {
            var resultObject = resultObjarray[i];
            var kycleveltype = resultObject.verification_type;
            var status = resultObject.status;
            console.log("KYC Level Type : " + kycleveltype);

            switch (kycleveltype) {
                case "Level_3_overview"://It means Level 3 Kyc Verification
                    if (status == "update_please") {
                        level3ImageIDSelfienogovIDstatus.value = "new";
                        if (verifivationlevel == 2) {
                            level3ButtonEnable.value = true;
                            level4ButtonEnable.value = false;
                            level5ButtonEnable.value = false;
                        }
                    } else if (status == "approved") {
                        level3ImageIDSelfienogovIDstatus.value = "approved";
                    } else if (status == "unapproved") {
                        level3ImageIDSelfienogovIDstatus.value = "unapproved";
                    } else if (status == "pending") {
                        level3ImageIDSelfienogovIDstatus.value = "pending";
                    }
                    break;
                case "Level_4_overview":
                    if (status == "update_please") {
                        level4ProoofAddressStatus.value = "new";
                        if (verifivationlevel == 3) {
                            level4ButtonEnable.value = true;
                            level5ButtonEnable.value = false;
                        }
                    } else if (status == "pending") {
                        level4ProoofAddressStatus.value = "pending";
                        level5ButtonEnable.value = false;
                    } else if (status == "approved") {
                        level4ProoofAddressStatus.value = "approved";
                        level5ButtonEnable.value = true;
                    } else if (status == "unapproved") {
                        level4ProoofAddressStatus.value = "new";
                        level4ButtonEnable.value = true;
                        level5ButtonEnable.value = false;
                    }
                    break;
                case "Level_5_overview":
                    if (status == "update_please") {
                        if (verifivationlevel == 4)//We enable Level5 button only after completed verification level4
                        {
                            level5CustomLimitsStatus.value = "new";
                            level5ButtonEnable.value = true;
                        }
                    } else if (status == "pending") {
                        level5CustomLimitsStatus.value = "pending";
                    } else if (status == "approved") {
                        level5CustomLimitsStatus.value = "approved";
                    } else if (status == "unapproved") {
                        level5CustomLimitsStatus.value = "new";
                        level5ButtonEnable.value = true;
                    }

                    break;
            }


        }

    }
}


function onEmailOrMobileChangeEvent(args) {
    console.log(JSON.stringify(args))
    smsVerifyStateGroup.goto(initalState);

    if (args.selectedIndex == 0) {
        emailOrMobileIndex.value = 0;
        verifySmsPageControl.seekToPath("emailPage");
    } else {
        emailOrMobileIndex.value = 1;
        verifySmsPageControl.seekToPath("smsPage");
    }

}


function emailorsmsVerifyClicked() {
    console.log("email verify clicked");
    smsVerifyStateGroup.goto(initalState);

    emailorsmsVerifyPopupShow.value = true;
}

function emailorsmsVerifyClickedTwo() {
    console.log("email verify clicked two");

    smsVerifyStateGroup.goto(initalState);
    emailorsmsVerifyPopupShow.value = true;

    if (levelOneBySms) {
        emailOrMobileIndex.value = 0;
        verifySmsPageControl.seekToPath("emailPage");
    } else {
        emailOrMobileIndex.value = 1;
        verifySmsPageControl.seekToPath("smsPage");
    }
}

function verifyEmailClicked() {
    busy.activate();

    _WebService.request("user/resendactivationmail").then(function (result) {
        gotSendActivationMailResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotSendActivationMailResult(result) {
    console.log("gotSendActivationMailResult")
    emailorsmsVerifyPopupShow.value = false;

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        tagEvents.emit("toastShow", { message: "Verification Email sent" });
    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}

function modifyEmailClick() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.email = newEmailInput.value;

    _WebService.request("user/updateprofile/", apiBodyObj).then(function (result) {
        gotUpdateprofileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUpdateprofileResult(result) {
    console.log("gotUpdateprofileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;

        emailorsmsVerifyPopupShow.value = false;
        tagEvents.emit("toastShow", { message: "Email Changed" });
        logoutrocessCall();

    } else {
        // if (arr.error == "Mobile_number_already_used by_someone_else") {
        tagEvents.emit("toastShow", { message: "Email is already used" });
        // }
    }
}

function logoutrocessCall() {
    Preferences.write("email", null);
    Preferences.write("access_token", null);
    Preferences.write("refresh_token", null);
    Preferences.write("token_type", null);
    Preferences.write("expires_time", null);
    // Preferences.write("use_pin", null);

    router.goto("loginPage");
}

function verifySmsClicked() {
    busy.activate();

    _WebService.request("User/ResendSMSCode").then(function (result) {
        gotSendActivationSmsResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotSendActivationSmsResult(result) {
    console.log("gotSendActivationSmsResult")

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    console.log(JSON.stringify(level1EmailorSmsverified.value))

    if (arr.status == "success") {
        var resultObj = arr.result;
        validationSmsCodeInput.value = "";

        smsVerifyStateGroup.goto(validateState);
        tagEvents.emit("toastShow", { message: "Verification SMS sent" });
    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}

function validateSmsCode() {
    if (validationSmsCodeInput.value == "") {
        tagEvents.emit("toastShow", { message: "Please enter verification code" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.code = validationSmsCodeInput.value;

    _WebService.request("user/VerifySMSCode", apiBodyObj).then(function (result) {
        gotVerifySMSCodeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotVerifySMSCodeResult(result) {
    busy.deactivate();
    var arr = result;
    console.log("Verify SMS code Result-->" + JSON.stringify(arr))
    if (arr.status == "success") {
        var resultObj = arr.result;
        emailorsmsVerifyPopupShow.value = false;
        tagEvents.emit("toastShow", { message: "Mobile verification completed" });

        userVerificationStatusLoad();

    } else {
        if (arr.error == "code_missmatch") {
            tagEvents.emit("toastShow", { message: "This code is not valid" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function onCountryCodeChangeEvent(args) {
    countryCode.value = args.countryPhoneCode;
}

function modifyMobileNoClicked() {
    if (countryCode.value == "" || newMobileInput.value == "") {
        tagEvents.emit("toastShow", { message: "Please enter a valid country code and mobile number" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.user_mobile = newMobileInput.value;
    apiBodyObj.country_phonecode = countryCode.value;

    _WebService.request("user/updateprofile/", apiBodyObj).then(function (result) {
        gotUpdateMobileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUpdateMobileResult(result) {
    console.log("gotUpdateMobileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;
        _model.userprofileDetails.value.user_mobile = newMobileInput.value;
        _model.userprofileDetails.value.country_phonecode = countryCode.value;

        console.log("User Mobile" + _model.userprofileDetails.value.user_mobile);
        userMobileDisplay.value = _model.userprofileDetails.value.user_mobile;
        profileCountryCode.value = _model.userprofileDetails.value.country_phonecode;

        tagEvents.emit("toastShow", { message: "Mobile number updated successfully" });
    } else {
        if (arr.error == "Mobile_number_already_used by_someone_else") {
            tagEvents.emit("toastShow", { message: "Mobile number is already used" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function level3idImageSelfieHandler() {
    router.goto("kycVerifyIdentityPage", { govIDstatus: "true", datId: Math.random() });
}

function nogovIDClickHandler() {
    router.goto("kycVerifyIdentityPage", { govIDstatus: "false", datId: Math.random() });
}

function proofofAddressClickHandler() {
    router.push("kycProofofAddressPage");
}

function customLimitApplyClickHandler() {
    router.push("kycCustomsLimitsPage", { datId: Math.random() });
}

module.exports = {

    onViewActivate: onViewActivate,
    emailorsmsVerifyPopupShow: emailorsmsVerifyPopupShow,
    userEmailDisplay: userEmailDisplay,

    loadingCompleteStat: loadingCompleteStat,
    emailorsmsVerifyClicked: emailorsmsVerifyClicked,
    emailorsmsVerifyClickedTwo: emailorsmsVerifyClickedTwo,
    verifyEmailClicked: verifyEmailClicked,
    level3idImageSelfieHandler: level3idImageSelfieHandler,
    proofofAddressClickHandler: proofofAddressClickHandler,
    customLimitApplyClickHandler: customLimitApplyClickHandler,
    onEmailOrMobileChangeEvent: onEmailOrMobileChangeEvent,
    emailOrMobileIndex: emailOrMobileIndex,
    emailOrMobile: emailOrMobile,
    email: email,
    userMobileDisplay: userMobileDisplay,
    countryCode: countryCode,
    profileCountryCode: profileCountryCode,

    emailandsmsVerifyPopupShow: emailandsmsVerifyPopupShow,

    verifySmsClicked: verifySmsClicked,
    level2ButtonEnable: level2ButtonEnable,
    nogovIDClickHandler: nogovIDClickHandler,
    level1EmailorSmsverified: level1EmailorSmsverified,
    validateSmsCode: validateSmsCode,
    validationSmsCodeInput: validationSmsCodeInput,
    level2EmailorSmsverified: level2EmailorSmsverified,
    modifyMobileNoClicked: modifyMobileNoClicked,
    newMobileInput: newMobileInput,
    modifyEmailClick: modifyEmailClick,
    newEmailInput: newEmailInput,

    level1CashInAmount: level1CashInAmount,
    level2CashInAmount: level2CashInAmount,
    level3CashInAmount: level3CashInAmount,
    level4CashInAmount: level4CashInAmount,
    level5CashInAmount: level5CashInAmount,

    level1CashOutAmount: level1CashOutAmount,
    level2CashOutAmount: level2CashOutAmount,
    level3CashOutAmount: level3CashOutAmount,
    level4CashOutAmount: level4CashOutAmount,
    level5CashOutAmount: level5CashOutAmount,

    level3ButtonEnable: level3ButtonEnable,
    level4ButtonEnable: level4ButtonEnable,
    level5ButtonEnable: level5ButtonEnable,
    level3ImageIDSelfienogovIDstatus: level3ImageIDSelfienogovIDstatus,
    level4ProoofAddressStatus: level4ProoofAddressStatus,
    level5CustomLimitsStatus: level5CustomLimitsStatus,

    radiomenuShowStat: radiomenuShowStat,
    onCountryCodeChangeEvent: onCountryCodeChangeEvent,


}