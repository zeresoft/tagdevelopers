var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var emailorTagID = Observable("");

function submitForApprovalClickHandler() {
    console.log("UserProfileDetails-->" + JSON.stringify(_model.userprofileDetails.value));
    if (emailorTagID.value === "") {
        tagEvents.emit("toastShow", { message: "Please enter a valid TAG ID or Email" });
        return;
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.kyc_type = "USER";
    apiBodyObj.full_name = _model.userprofileDetails.value.user_firstname + " " + _model.userprofileDetails.value.user_lastname;
    apiBodyObj.dob = _model.userprofileDetails.value.user_dob;
    apiBodyObj.email = _model.userprofileDetails.value.user_email;
    apiBodyObj.mobile_phone = _model.userprofileDetails.value.user_mobile;
    apiBodyObj.approver_email_id = emailorTagID.value;

    _WebService.request("KycChecker/CreateKYCRecord", apiBodyObj).then(function (result) {
        gotnoGovIDApprovalResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotnoGovIDApprovalResult(result) {
    console.log("gotCreateKYCrecordResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: loc.value.nogovid_sendrequestmessage });
    } else {
        if (arr.error == "kyc_record_is_added_by_the_approver") {
            tagEvents.emit("toastShow", { message: loc.value.nogovid_alreadysentmessage });
        } else if (arr.error == "user_kyc_data_is_not_added") {
            tagEvents.emit("toastShow", { message: loc.value.nogovid_checkandtryagainmessage });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.nogovid_requestfailedmessage });
        }
    }
}

module.exports = {
    submitForApprovalClickHandler: submitForApprovalClickHandler,
    emailorTagID: emailorTagID
}
