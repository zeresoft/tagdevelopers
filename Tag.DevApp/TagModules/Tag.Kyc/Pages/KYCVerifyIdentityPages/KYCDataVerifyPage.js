var Observable = require('FuseJS/Observable');
var moment = require('Utils/moment');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');
var Validator = require('Utils/Validator');
var profileGenderIndex = Observable(-1);
var dateDisplay = Observable("0000-00-00");
var dateSelected = "";
var popupShowDatePick = Observable(false);
var firstName = Observable("");
var middleName = Observable("");
var lastName = Observable("");
var presentAddress = Observable("");
var permanentAddress = Observable("");
var country = Observable("");
var countryName = Observable("");
var tinSssNumber = Observable("");
var birthPlace = Observable("");
var sourceOfFund = Observable("");
var WorkType = Observable("");
var kycIdTypes = [{ name: "TIN", value: "1" }, { name: "SSS", value: "2" }, { name: "GSIS", value: "3" }, { name: "OTHER", value: "4" }];
var kycIdTypesIndex = Observable(-1);
var kycIdTypesSelected = -1;
var profileGenderItems = Observable({ index: 0, name: "Male", value: "male" }, { index: 1, name: "Female", value: "female" });
var hashProofTxt = Observable("");
var kycGenderIndex;
var kycDataVerifyStatus = Observable("");
var selectedItem = Observable();

function onViewActivateCreate() {
    kycDataVerifyStatus.value = "";
    getKYCAdressInfo();
}

function getKYCAdressInfo() {
    busy.activate();
    var apiBodyObj = {};
    _WebService.request("user/GetAddressInfo/", apiBodyObj).then(function (result) {
        gotKYCAddressInfoResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}
function gotKYCAddressInfoResult(result) {
    console.log("gotKYCAddressInfoResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {

        var resultArr = arr.result;

        if (resultArr.status == "pending") {
            kycDataVerifyStatus.value = resultArr.status;
        } else if (resultArr.status == "unapproved") {
            kycDataVerifyStatus.value = resultArr.status;
        } else if (resultArr.status == "approved") {
            kycDataVerifyStatus.value = resultArr.status;
            hashProofTxt.value = resultArr.data_hash;
        } else {
            kycDataVerifyStatus.value = "new";
        }

    } else {
        if (arr.error == "info_missing") {
            kycDataVerifyStatus.value = "new";
        }
    }

}

function onVerificationClick() {
    kycDataVerifyStatus.value = "new";
}


function onGenderChangeEvent(args) {
    selectedItem.value = profileGenderItems.getAt(args.selectedIndex);
    console.log(JSON.stringify(selectedItem.value))
}

function dateChangeClickHandler() {
    popupShowDatePick.value = true;
}

function onDateCloseDialog(args) {
    dateDisplay.value = moment(args.date).format('YYYY-MM-DD');
    dateSelected = moment(args.date).format('YYYY-MM-DD');
    console.log(dateDisplay.value);
}

function kycIdTypesChange(args) {
    console.log("kycItemChange-------------------");
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem));
    kycIdTypesSelected = selectedItem.value;
    kycIdTypesIndex.value = selectedItem.indexLocal;
}

function saveKYCDataHandler() {
    if (firstName.value === "") {
        tagEvents.emit("toastShow", { message: loc.value.name_should_not_empty });
        return;
    }
    if (lastName.value === "") {
        tagEvents.emit("toastShow", { message: loc.value.name_should_not_empty });
        return;
    }
    if (presentAddress.value === "") {
        tagEvents.emit("toastShow", { message: "Present adress should not be empty" });
        return;
    }
    if (permanentAddress.value === "") {
        tagEvents.emit("toastShow", { message: "Permanent adress should not be empty" });
        return;
    }
    if (country.value === "") {
        tagEvents.emit("toastShow", { message: "Please select your country" });
        return;
    }
    if (profileGenderIndex.value === -1) {
        tagEvents.emit("toastShow", { message: "Please select your gender" });
        return;
    }
    if (WorkType.value === "") {
        tagEvents.emit("toastShow", { message: "Work/Employer should not be empty" });
        return;
    }
    // if (kycIdTypesSelected === -1) {
    //     tagEvents.emit("toastShow", { message: "Please select GOV ID Type" });
    //     return;
    // }
    // if (tinSssNumber.value === "") {
    //     tagEvents.emit("toastShow", { message: "GOV ID number should not be empty" });
    //     return;
    // }
    if (birthPlace.value === "") {
        tagEvents.emit("toastShow", { message: "Birth place should not be empty" });
        return;
    }
    if (dateSelected == "") {
        tagEvents.emit("toastShow", { message: "Please select date of birth" });
        return;
    }
    if (sourceOfFund.value === "") {
        tagEvents.emit("toastShow", { message: "Please enter the source of fund" });
        return;
    }


    busy.activate();

    if (selectedItem.value.index == 0) {
        kycGenderIndex = "male";
    } else {
        kycGenderIndex = "female";
    }

    var apiBodyObj = {};
    apiBodyObj.firstname = firstName.value;
    apiBodyObj.middlename = middleName.value;
    apiBodyObj.lastname = lastName.value;
    apiBodyObj.gender = kycGenderIndex;
    apiBodyObj.nationality = country.value;
    if (kycIdTypesSelected != -1) {
        apiBodyObj.government_id_type = kycIdTypesSelected;
    }
    if (tinSssNumber.value != "") {
        apiBodyObj.government_id_number = tinSssNumber.value;
    }
    apiBodyObj.nature_of_work = WorkType.value;
    apiBodyObj.source_of_funds = sourceOfFund.value;
    apiBodyObj.dob = dateSelected;
    apiBodyObj.pob = birthPlace.value;
    apiBodyObj.currentAddress = presentAddress.value;
    apiBodyObj.permanentAddress = permanentAddress.value;
    apiBodyObj.user_id = _model.userprofileDetails.value.id;

    _WebService.request("user/UpdateAddressInfo/", apiBodyObj).then(function (result) {
        gotKYCDataResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}
function gotKYCDataResult(result) {
    console.log("gotKYCDataResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        getKYCAdressInfo();
        tagEvents.emit("toastShow", { message: "Information has been submitted" });
    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_data_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "data_waiting_for_approval") {
            tagEvents.emit("toastShow", { message: "Data is already uploaded " });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }

}
function onCountryChangeEvent(args) {
    console.log(args.countryID, args.countryName);
    country.value = args.countryID;
    countryName.value = args.countryName;

}
function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}
module.exports = {
    firstName: firstName,
    middleName: middleName,
    lastName: lastName,
    country: country,
    dateDisplay: dateDisplay,
    WorkType: WorkType,
    birthPlace: birthPlace,
    sourceOfFund: sourceOfFund,
    tinSssNumber: tinSssNumber,
    presentAddress: presentAddress,
    permanentAddress: permanentAddress,
    profileGenderItems: profileGenderItems,
    profileGenderIndex: profileGenderIndex,
    onGenderChangeEvent: onGenderChangeEvent,
    popupShowDatePick: popupShowDatePick,
    dateChangeClickHandler: dateChangeClickHandler,
    onDateCloseDialog: onDateCloseDialog,
    saveKYCDataHandler: saveKYCDataHandler,
    onViewActivateCreate: onViewActivateCreate,
    getKYCAdressInfo: getKYCAdressInfo,
    kycIdTypesIndex: kycIdTypesIndex,
    kycIdTypesChange: kycIdTypesChange,
    kycIdTypes: kycIdTypes,
    onCountryChangeEvent: onCountryChangeEvent,
    selectedItem: selectedItem,
    kycDataVerifyStatus: kycDataVerifyStatus,
    onVerificationClick: onVerificationClick,
    countryName: countryName,
    hashProofTxt: hashProofTxt

}