var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var uploadImageData = "";

var imageSelectPopupShow = Observable(false);
var displayImage = Observable();
var imageSelected = Observable(false);
var kycProfileVerifyStatus = Observable("");

function onViewActivateCreate() {
    kycProfileVerifyStatus.value = "";
    imageSelected.value = false;
    uploadImageData = "";

    getKYCverifiedImage();
}

function getKYCverifiedImage() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.verification_type = "profile_image";
    _WebService.request("verification/status", apiBodyObj).then(function (result) {
        gotKYCverifiedImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotKYCverifiedImageResult(result) {
    console.log("gotKYCverifiedImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;

        if (resultArr[0].status == "pending") {
            kycProfileVerifyStatus.value = resultArr[0].status;
        } else if (resultArr[0].status == "unapproved") {
            kycProfileVerifyStatus.value = resultArr[0].status;
        } else if (resultArr[0].status == "approved") {
            kycProfileVerifyStatus.value = resultArr[0].status;
            // hashProofTxt.value = resultArr[0].hash_proof;
        } else {
            kycProfileVerifyStatus.value = "new";
        }
    } else {
        if (arr.error == "status_fetching_failed") {
            kycProfileVerifyStatus.value = "new";
            tagEvents.emit("toastShow", { message: "Status fetching failed, try again." });
        }
    }

}

function tryAgainClickHandler() {
    console.log("try again");
    kycProfileVerifyStatus.value = "new";
    imageSelected.value = false;
}

function onSelectPhotoClickHandler() {
    console.log("onImageChangeClick");
    imageSelectPopupShow.value = true;
}

function onUserImagegSelectionComplete(args) {
    console.log("Image Selection Completed");
    console.log("ImagePath" + args.imagePath);

    uploadImageData = encodeURIComponent(args.imageData);
    displayImage.value = args.imagePath;
    imageSelected.value = true;
    imageSelectPopupShow.value = false;
}

function onUploadPhotoClickHandler() {
    saveKycProfileImage();
}

function saveKycProfileImage() {
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.upload_type = "profile_image";
    apiBodyObj.data = uploadImageData;

    _WebService.request("verification/Upload", apiBodyObj).then(function (result) {
        gotKYCProfileImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotKYCProfileImageResult(result) {
    console.log("gotKYCProfileImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;

        uploadImageData = "";
        getKYCverifiedImage();
    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "file_waiting_for_approval") {
            tagEvents.emit("toastShow", { message: "File is already uploaded " });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivateCreate: onViewActivateCreate,
    imageSelectPopupShow: imageSelectPopupShow,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,
    onSelectPhotoClickHandler: onSelectPhotoClickHandler,
    imageSelected: imageSelected,
    displayImage: displayImage,
    onUploadPhotoClickHandler: onUploadPhotoClickHandler,
    kycProfileVerifyStatus: kycProfileVerifyStatus,
    tryAgainClickHandler: tryAgainClickHandler
};