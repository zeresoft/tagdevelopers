var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var uploadImageData = "";

var imageSelectPopupShow = Observable(false);
var displayImage = Observable();
var imageSelected = Observable(false);
var kycProofofAddressVerifyStatus = Observable("");

function onViewActivateCreate() {
    kycProofofAddressVerifyStatus.value = "";
    imageSelected.value = false;
    uploadImageData = "";

    getCompanyRegsterImage();
}

function getCompanyRegsterImage() {
    //allImageData.clear();
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.verification_type = "proof_of_address";
    _WebService.request("verification/status", apiBodyObj).then(function (result) {
        gotCompanyRegsterImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotCompanyRegsterImageResult(result) {
    console.log("gotCompanyRegsterImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;

        if (resultArr[0].status == "pending") {
            kycProofofAddressVerifyStatus.value = resultArr[0].status;
        } else if (resultArr[0].status == "unapproved") {
            kycProofofAddressVerifyStatus.value = resultArr[0].status;
        } else if (resultArr[0].status == "approved") {
            kycProofofAddressVerifyStatus.value = resultArr[0].status;
            // hashProofTxt.value = resultArr[0].hash_proof;
        } else if (resultArr[0].status == "update_please") {
            kycProofofAddressVerifyStatus.value = "new";
        } else {
            kycProofofAddressVerifyStatus.value = "new";
        }
    } else {
        if (arr.error == "status_fetching_failed") {
            kycProofofAddressVerifyStatus.value = "new";
            //tagEvents.emit("toastShow", { message: arr.error });
        }
    }
}

function tryAgainClickHandler() {
    console.log("try again");
    kycProofofAddressVerifyStatus.value = "new";
    imageSelected.value = false;
}

function onSelectPhotoClickHandler() {
    console.log("onImageChangeClick");
    imageSelectPopupShow.value = true;
}

function onUserImagegSelectionComplete(args) {
    console.log("Image Selection Completed");
    console.log("ImagePath" + args.imagePath);

    uploadImageData = encodeURIComponent(args.imageData);
    displayImage.value = args.imagePath;
    imageSelected.value = true;
    imageSelectPopupShow.value = false;
}

function onUploadPhotoClickHandler() {
    saveExtraDocuments();
}

function saveExtraDocuments() {
    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.upload_type = "proof_of_address";
    apiBodyObj.data = uploadImageData;

    _WebService.request("verification/Upload", apiBodyObj).then(function (result) {
        gotkycMerchaImgUploadResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotkycMerchaImgUploadResult(result) {
    console.log("gotkycMerchaImgUploadResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        getCompanyRegsterImage();
    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "file_waiting_for_approval") {
            tagEvents.emit("toastShow", { message: "File is already uploaded " });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }

}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    imageSelectPopupShow: imageSelectPopupShow,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,
    onSelectPhotoClickHandler: onSelectPhotoClickHandler,
    imageSelected: imageSelected,
    displayImage: displayImage,
    onViewActivateCreate: onViewActivateCreate,
    kycProofofAddressVerifyStatus: kycProofofAddressVerifyStatus,
    onUploadPhotoClickHandler: onUploadPhotoClickHandler,
    tryAgainClickHandler:tryAgainClickHandler,
};