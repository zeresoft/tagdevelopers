var Observable = require('FuseJS/Observable');
var moment = require('Utils/moment');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');
var FileDialog = require("FileDialog");

var uploadImageData1;
var uploadImageData2;
var upload1Image = true;
var document1Selected = Observable(false);
var document2Selected = Observable(false);
var tagcashuseTypeList = Observable({ index: 0, name: "Daily", value: "daily" }, { index: 1, name: "Weekly", value: "weekly" }, { index: 2, name: "Monthly", value: "monthly" });
var tagcashUseTypeDropdownIndex = Observable(0);

var employmentTypeList = Observable({ index: 0, name: "Employed", value: "employed" }, { index: 1, name: "Registered Business Owner", value: "registered business owner" }, { index: 2, name: "Freelance", value: "freelance" }, { index: 3, name: "Retired", value: "retired" }, { index: 4, name: "Unemployed", value: "Unemployed" });
var employmentTypeDropdownIndex = Observable(0);
var document1 = false;
var document2 = false;
var codeFile1 = "";
var codeFile2 = "";
var amount = Observable("");
var message = Observable("");
var jobType = "employed";
var often_type = "daily";
var tagcashusingTypeIndex = Observable(0);

var isPoliticalPosition = Observable(false);
var isElected = Observable(false);

var document1Uploaded = Observable(false);
var document2Uploaded = Observable(false);

function document1SelectClickHandler() {
    document1 = true;
    document2 = false;
    openFileClicked();
}

function document2SelectClickHandler() {
    document2 = true;
    document1 = false;
    openFileClicked();
}

function openFileClicked() {
    FileDialog.Launch();
    FileDialog.on("PATHRECEIVED", function (message) {
        console.log(JSON.stringify(message));
        console.log(message.data);

        if (document1 == true) {
            codeFile1 = message.data;
            document1Selected.value = true;
        }

        if (document2 == true) {
            codeFile2 = message.data;
            document2Selected.value = true;
        }
    });

}

FileDialog.on("PATHCANCELED", function (message) {
    console.log("PATHCANCELED received " + message);
});

FileDialog.on("PATHERROR", function (message) {
    console.log("PATHERROR received " + message);
});


function saveExtraDocuments() {
    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.upload_type = "extras";
    if (document1 == true) {
        if (codeFile1 == "") {
            tagEvents.emit("toastShow", { message: "Please Select Document" });
            return;
        }
        apiBodyObj.data = codeFile1;
    }
    if (document2 == true) {
        if (codeFile2 == "") {
            tagEvents.emit("toastShow", { message: "Please Select Document" });
            return;
        }
        apiBodyObj.data = codeFile2;
    }
    _WebService.request("verification/Upload", apiBodyObj).then(function (result) {
        gotkycUploadResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotkycUploadResult(result) {
    console.log("gotkycMerchaImgUploadResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        if (document1 == true) {
            document1Uploaded.value = true;

        }
        if (document2 == true) {
            document2Uploaded.value = true;
        }
        tagEvents.emit("toastShow", { message: "File uploaded successfully" });

    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "file_waiting_for_approval") {
            tagEvents.emit("toastShow", { message: "File is already uploaded " });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function oftenTypeChangeHandler(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem));
    console.log(selectedItem.value);
    often_type = selectedItem.value;
}
function employmentTypeChangeHandler(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem));
    console.log(selectedItem.value);
    jobType = selectedItem.value;
}

function submitForVerificationClickHandler() {

    if (amount.value == "") {
        tagEvents.emit("toastShow", { message: "Please enter a valid amount" });
        return;
    }
    if (message.value == "") {
        tagEvents.emit("toastShow", { message: "Please enter the requirement for limit increase" });
        return;
    }
    if (document1Uploaded.value == false) {
        tagEvents.emit("toastShow", { message: "Please upload two documents" });
        return;
    }
    if (document2Uploaded.value == false) {
        tagEvents.emit("toastShow", { message: "Please upload two documents" });
        return;
    }
    createSubmitProcess();
}

function createSubmitProcess() {
    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.amount = amount.value;
    apiBodyObj.extra_note = message.value;
    apiBodyObj.how_often_using = often_type;
    apiBodyObj.job_type = jobType;

    if (isPoliticalPosition.value = true) {
        apiBodyObj.elected_to_political_position = 1;
    } else {
        apiBodyObj.elected_to_political_position = 0;
    }

    if (isElected) {
        apiBodyObj.friend_elected_to_political_position = 1;
    } else {
        apiBodyObj.friend_elected_to_political_position = 0;
    }

    if (tagcashusingTypeIndex.value == 0) {
        apiBodyObj.using_tagcash = "personal";
    } else {
        apiBodyObj.using_tagcash = "business";
    }

    _WebService.request("verification/Level5Data", apiBodyObj).then(function (result) {
        gotSubmitResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotSubmitResult(result) {
    console.log("gotResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Submitted Successfully" });
        router.goBack();
    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "pending_approval") {
            tagEvents.emit("toastShow", { message: "Upload pending approval" });
        } else {
            tagEvents.emit("toastShow", { message: "Failed to submit data" });
            // tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

module.exports = {
    saveExtraDocuments: saveExtraDocuments,
    oftenTypeChangeHandler: oftenTypeChangeHandler,
    tagcashuseTypeList: tagcashuseTypeList,
    tagcashUseTypeDropdownIndex: tagcashUseTypeDropdownIndex,
    employmentTypeList: employmentTypeList,
    employmentTypeDropdownIndex: employmentTypeDropdownIndex,
    employmentTypeChangeHandler: employmentTypeChangeHandler,
    document1SelectClickHandler: document1SelectClickHandler,
    document2SelectClickHandler: document2SelectClickHandler,
    document1Selected: document1Selected,
    document2Selected: document2Selected,
    openFileClicked: openFileClicked,
    submitForVerificationClickHandler: submitForVerificationClickHandler,
    amount: amount,
    message: message,
    tagcashusingTypeIndex: tagcashusingTypeIndex,
    isPoliticalPosition: isPoliticalPosition,
    isElected: isElected,
    document1Uploaded: document1Uploaded,
    document2Uploaded: document2Uploaded,
}