var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var uploadImageData = "";


var imageSelectPopupShow = Observable(false);
var displayImage = Observable();
var imageSelected = Observable(false);
var kycGovIdVerifyStatus = Observable("");
var hashProofTxt = Observable("");

var acceptedIds = Observable();
var acceptablidsPopupShow = Observable(false);

function onViewActivateCreate() {
    kycGovIdVerifyStatus.value = "";
    imageSelected.value = false;
    uploadImageData = "";

    getKYCverifiedImage();
}

function getKYCverifiedImage() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.verification_type = "govt_id";
    _WebService.request("verification/status", apiBodyObj).then(function (result) {
        gotKYCverifiedImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotKYCverifiedImageResult(result) {
    console.log("gotKYCverifiedImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr[0].status == "pending") {
            kycGovIdVerifyStatus.value = resultArr[0].status;
        } else if (resultArr[0].status == "unapproved") {
            kycGovIdVerifyStatus.value = resultArr[0].status;
        } else if (resultArr[0].status == "approved") {
            kycGovIdVerifyStatus.value = resultArr[0].status;
            // hashProofTxt.value = resultArr[0].hash_proof;
        } else {
            kycGovIdVerifyStatus.value = "new";
        }

    } else {
        if (arr.error == "status_fetching_failed") {
            kycGovIdVerifyStatus.value = "new";
            tagEvents.emit("toastShow", { message: "Status fetching failed" });
        }
    }

    if (acceptedIds.length == 0) {
        getAcceptedIdList();
    }
}


function getAcceptedIdList() {
    busy.activate();
    acceptedIds.clear();

    _WebService.request("verification/ValidGovtIds").then(function (result) {
        gotAcceptedIdListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotAcceptedIdListResult(result) {
    console.log("gotAcceptedIdListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;

        acceptedIds.addAll(resultArr);
    }

}

function onAcceptedClickEvent() {
    acceptablidsPopupShow.value = true;
}

function onVerificationClick() {
    kycGovIdVerifyStatus.value = "new";
    imageSelected.value = false;
}

function onSelectPhotoClickHandler() {
    console.log("onImageChangeClick");
    imageSelectPopupShow.value = true;
}

function onUserImagegSelectionComplete(args) {
    console.log("Image Selection Completed");
    console.log("ImagePath" + args.imagePath);

    uploadImageData = encodeURIComponent(args.imageData);
    displayImage.value = args.imagePath;
    imageSelected.value = true;
    imageSelectPopupShow.value = false;
}

function onUploadPhotoClickHandler() {
    saveKycGovidImage();
}

function saveKycGovidImage() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.upload_type = "govt_id";
    apiBodyObj.data = uploadImageData;

    _WebService.request("verification/Upload", apiBodyObj).then(function (result) {
        gotKYCGovidImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotKYCGovidImageResult(result) {
    console.log("gotKYCGovidImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        uploadImageData = "";
        getKYCverifiedImage();
    } else {
        if (arr.error == "email_verification_pending") {
            tagEvents.emit("toastShow", { message: "Email verification is pending" });
        } else if (arr.error == "sms_verification_pending") {
            tagEvents.emit("toastShow", { message: "SMS verification is pending" });
        } else if (arr.error == "already_approved") {
            tagEvents.emit("toastShow", { message: "Already approved" });
        } else if (arr.error == "file_waiting_for_approval") {
            tagEvents.emit("toastShow", { message: "File is already uploaded " });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    imageSelectPopupShow: imageSelectPopupShow,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,
    onSelectPhotoClickHandler: onSelectPhotoClickHandler,
    imageSelected: imageSelected,
    displayImage: displayImage,
    kycGovIdVerifyStatus: kycGovIdVerifyStatus,
    hashProofTxt: hashProofTxt,
    onVerificationClick: onVerificationClick,
    onViewActivateCreate: onViewActivateCreate,
    onUploadPhotoClickHandler: onUploadPhotoClickHandler,
    acceptedIds: acceptedIds,
    acceptablidsPopupShow: acceptablidsPopupShow,
    onAcceptedClickEvent: onAcceptedClickEvent,
};