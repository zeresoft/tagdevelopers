var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var selectedPageIndex = Observable(0);
var kycVerifyLevTwoData = ["DATA", "PHOTO", "GOV ID", "SELFIE+ID"];

var kycVerifyLevthreenogovIDData = ["DATA", "PHOTO", "APPROVAL"];

var tabData = Observable();
var govIDstatus = Observable(false);

this.Parameter.onValueChanged(module, function (param) {
    console.log("DATA" + JSON.stringify(param));
    console.log("GOVID" + govIDstatus.value);

    govIDstatus.value = param.govIDstatus;

    if (govIDstatus.value == "true") {
        tabData.addAll(kycVerifyLevTwoData);
    } else {
        tabData.addAll(kycVerifyLevthreenogovIDData);
    }
    selectedPageIndex.value = 0;
})

selectedPageIndex.onValueChanged(module, function (selIndex) {
    console.log("NMNMNM" + JSON.stringify(selIndex));

    if (govIDstatus.value == "true") {
        if (selIndex == 0) {
            router.gotoRelative(inner, "kycDataVerifyPage");
        } else if (selIndex == 1) {
            router.gotoRelative(inner, "KYCProfileImagePage");
        } else if (selIndex == 2) {
            router.gotoRelative(inner, "kycGovIdVerifyPage");
        } else if (selIndex == 3) {
            router.gotoRelative(inner, "kycSelfieVerifyPage");
        }
    } else {
        if (selIndex == 0) {
            router.gotoRelative(inner, "kycDataVerifyPage");
        } else if (selIndex == 1) {
            router.gotoRelative(inner, "KYCProfileImagePage");
        } else if (selIndex == 2) {
            router.gotoRelative(inner, "kycnogovidApprovalPage");
        }
    }

})

module.exports = {
    selectedPageIndex: selectedPageIndex,
    tabData: tabData,
    kycVerifyLevTwoData: kycVerifyLevTwoData,

    govIDstatus: govIDstatus
}