using Fuse;
using Fuse.Reactive;
using Uno.UX;
using Fuse.Scripting;
using Uno.Permissions;
using Uno.Threading;
using Fuse.Platform;
using Uno;
using Uno.UX;
using Uno.IO;
using Uno.Compiler.ExportTargetInterop;
using Android;


[extern(Android) ForeignInclude(Language.Java, "android.content.Intent")]
[extern(Android) ForeignInclude(Language.Java, "com.fuse.Activity")]

[extern(Android) ForeignInclude(Language.Java, "java.io.File")]
[extern(Android) ForeignInclude(Language.Java, "java.io.FileInputStream")]
[extern(Android) ForeignInclude(Language.Java, "android.util.Base64")]
[extern(Android) ForeignInclude(Language.Java, "java.io.IOException")]
[extern(Android) ForeignInclude(Language.Java, "java.io.FileNotFoundException")]


[UXGlobalModule]
public class FileDialog : NativeEventEmitterModule
{
	static readonly FileDialog _instance;

    Fuse.Scripting.Object _resultHolder;

	public FileDialog() : base(true, "PATHRECEIVED", "PATHCANCELED", "PATHERROR", "PermissionReceived", "PermissionDenied")
	{
		if defined(Android)
		{
			Permissions.Request(Permissions.Android.WRITE_EXTERNAL_STORAGE).Then(OnPermissionsPermitted,OnPermissionsRejected);
			Permissions.Request(Permissions.Android.READ_EXTERNAL_STORAGE).Then(OnPermissionsPermitted,OnPermissionsRejected);
		}

		if (_instance != null) return;

		_instance = this;
		Uno.UX.Resource.SetGlobalKey(_instance, "FileDialog");
	    AddMember(new NativeFunction("Launch", (NativeCallback)Launch));
	}

	extern(Android) void OnPermissionsPermitted(PlatformPermission p)
	{
		Emit("PermissionReceived", "Permissions allowed" + p);
	}

	extern(Android) void OnPermissionsRejected(Exception e)
	{
		Emit("PermissionDenied", "PermissionDenied "+ e);
	}


	extern(Android) object[]  Launch(Fuse.Scripting.Context c, object[] args){
		debug_log "launching FileDialog";

		_resultHolder = c.NewObject();
		
		var intent = GetIntent();
        if (intent!=null)
        {
            ActivityUtils.StartActivity(intent, OnResult);
            debug_log "launching FileDialog";
        } else {
            Emit("PATHERROR", "Unable to launch FileDialog");
        }
        return null;
	}

	extern(!Android) object Launch(Context c, object[] args){
		debug_log "launching error definitely not on android";
		return null;
	}

	[Foreign(Language.Java)]
	extern (Android)  Java.Object GetIntent()
	@{
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("file/*");
		// intent.setType("application/pdf");
		//target.addCategory(Intent.CATEGORY_OPENABLE); 
		return intent;
	@}

	[Foreign(Language.Java)]
	 extern(Android) void OnResult(int resultCode, Java.Object intent, object info)
    @{	 
    	//RESULT_CANCELED = 0
    	if (resultCode == 0) {
    		@{FileDialog:Of(_this).resCanceled(string):Call("FileDialog Result canceled")};
    		return;
    	}
    	//RESULT_OK = -1
    	if(resultCode < 0){
	    	Intent t = (Intent)intent;
			String FilePath = t.getData().getPath();
			FilePath = FilePath.replace("/root_path/", "/");

			File file = new File(FilePath);

			String encodedfile = null;
			try {
				FileInputStream fileInputStreamReader = new FileInputStream(file);
				byte[] bytes = new byte[(int)file.length()];
				fileInputStreamReader.read(bytes);
				// encodedfile = Base64.encodeBase64(bytes).toString();
				encodedfile = Base64.encodeToString(bytes, Base64.DEFAULT);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch <span id="IL_AD5" class="IL_AD">block
				// e</span>.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				// e.printStackTrace();
			}


	        @{FileDialog:Of(_this).resSuccess(string, string):Call(encodedfile,FilePath)};
    	}

    @}

    public void resSuccess(string res, string path)
    {
		//for reading text
		// var fileData = Uno.IO.File.ReadAllText(res);

		//for reading bytes array
		// var imageData = Uno.IO.File.ReadAllBytes(res);

    	_resultHolder["data"] = res;
    	_resultHolder["path"] = path;
    	Emit("PATHRECEIVED", _resultHolder);
    }



	public void resCanceled(string res)
	{
		Emit("PATHCANCELED", "res");
	}
}