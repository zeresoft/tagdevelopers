using Uno;
using Uno.Compiler.ExportTargetInterop;
using Android.Base.Wrappers;
using Fuse.Scripting;
using Fuse.Resources;
using Uno.Platform;
using Uno.Threading;
using Uno.UX;

[Require("AndroidManifest.Permission", "android.permission.NFC")]

[extern(Android) ForeignInclude(Language.Java, "android.content.Context")]
[extern(Android) ForeignInclude(Language.Java, "android.nfc.NfcManager")]
[extern(Android) ForeignInclude(Language.Java, "android.nfc.NfcAdapter")]
[extern(Android) ForeignInclude(Language.Java, "android.nfc.NfcAdapter.ReaderCallback")]
[extern(Android) ForeignInclude(Language.Java, "android.nfc.Tag")]
[extern(Android) ForeignInclude(Language.Java, "android.os.Looper")]
//[ForeignInclude(Language.Java, "com.fuse.Activity")]
[extern(Android) ForeignInclude(Language.Java, "android.app.Activity")]
//[extern(Android) ForeignInclude(Language.Java, "android.content.Intent")]

[extern(Android) ForeignInclude(Language.Java, "java.math.BigInteger")]

public class NfcScanner
{

    static void Log(string message)
    {
        debug_log(message);
    }

    [Foreign(Language.Java)]
    public static extern(ANDROID) string SetNfcScanner(string text)
    @{
        if (Looper.myLooper() == null)
        {
            Looper.prepare();
        }
       // Context context = com.fuse.Activity.getRootActivity();
        Activity activity = com.fuse.Activity.getRootActivity();

        NfcManager manager = (NfcManager)activity.getSystemService(Context.NFC_SERVICE);
        NfcAdapter adapter = manager.getDefaultAdapter();
        if (adapter == null || !adapter.isEnabled()) {
            return "false";
        }


         //Bundle opts = new Bundle();
	    //opts.putInt(NfcAdapter.EXTRA_READER_PRESENCE_CHECK_DELAY, 5000);

        int READER_FLAGS =NfcAdapter.FLAG_READER_NFC_A | NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK;

        adapter.enableReaderMode(activity, new ReaderCallback() {

            @Override
            public void onTagDiscovered(Tag tag) {
                byte[] id = tag.getId();
                String UID = String.format("%0" + (id.length * 2) + "X", new BigInteger(1,id));

                @{Log(string):Call("JAVA!")};
                @{NfcScannerManager.ScannSuccess(string):Call(UID)};
              


                //Intent i = new Intent().putExtra(EXTRA_TAG, tag);
                //tagListener.onNewTagIntent(i);
            }
        }, READER_FLAGS, null);


        return "true";

    @}

    public static extern(!ANDROID) string SetNfcScanner(string text)
    {
        return "no android";
    }
}


[UXGlobalModule]
public class NfcScannerManager : NativeModule
{

    static readonly NfcScannerManager _instance;

    static NativeEvent _nativeEvent;

    public NfcScannerManager()
    {
        debug_log "NFC Init";
        if (_instance != null) return;
        debug_log " NFC New!";
        _instance = this;
        Uno.UX.Resource.SetGlobalKey(this, "NfcScannerManager");
        AddMember(new NativeFunction("nfcScannInit", (NativeCallback)NfcScannInit));

        _nativeEvent = new NativeEvent("onNfcReceived");
        AddMember(_nativeEvent);
    }

    static void ScannSuccess(string message)
    {
        debug_log(message);
       _nativeEvent.RaiseAsync(_nativeEvent.ThreadWorker, message);
    }

    public object NfcScannInit(Context c, object[] args)
    {
        debug_log "Set Text: " + args[0].ToString();
        NfcScanner.SetNfcScanner(args[0].ToString());

        return null;
    }
}