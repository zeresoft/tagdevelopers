var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var friendsListData = Observable();
var idNameInput = Observable("");
//var friendsOnlyInput = Observable(false);
//var onlineNowInput = Observable(false);

function onViewActivate() {
    friendsListData.clear();
    friendsListLoad();
}

function searchHandlerClicked() {
    friendsListData.clear();
    friendsListLoad();
}

function pagingLoadMore() {
    if (friendsListData.length != 0) {
        console.log("pagingLoadMore")
        friendsListLoad();
    }
}

function friendsListLoad() {
    console.log("friendsListLoad");

    if (idNameInput.value == "") {
        busy.activate();
        var apiBodyObj = {};
        apiBodyObj.count = 20;
        apiBodyObj.offset = friendsListData.length;

        _WebService.request("contact/getcontacts", apiBodyObj).then(function (result) {
            gotFriendsListResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    } else {
        searchHandler();
    }
}

function gotFriendsListResult(result) {
    console.log("gotFriendsListResult");
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            
            var resultArrProcessed = [];
            for (var i = 0; i < resultArr.length; i++) {
                var obj = {};
                obj = resultArr[i];
                
                if(resultArr[i].contact_id){
                    obj.imgPath = _model.userImageUrl+resultArr[i].contact_id+"?kycImage=0";
                }else{
                    obj.imgPath = _model.userImageUrl+resultArr[i].id+"?kycImage=0";
                }
                
                resultArrProcessed.push(obj);
            }

            friendsListData.addAll(resultArrProcessed);
        }
    }
}

function friendsClickHandle(args) {
    console.log("friendsClickHandle");
    console.log(JSON.stringify(args.data));
    busy.activate();

    var resultUser = {};
    if (args.data.hasOwnProperty("contact_id")) {
        resultUser.id = args.data.contact_id;
        resultUser.name = args.data.contact_firstname + " " + args.data.contact_lastname;
        resultUser.rating = args.data.rating;
    } else {
        resultUser.id = args.data.id;
        resultUser.name = args.data.name;
        resultUser.rating = args.data.rating;
    }

    router.push("userScanMenuPage", { resultUser: resultUser, datId: Math.random() });


}


function searchHandler() {
    console.log("searchHandler");
    // console.log(JSON.stringify(_model.userprofileDetails.value));

    if (_model.userprofileDetails.value.id == idNameInput.value) {
        tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Its you :)" });
        return;
    }

    var apiBodyObj = {};
    apiBodyObj.count = 20;
    apiBodyObj.offset = friendsListData.length;

    // if (friendsOnlyInput.value) {
    //     apiBodyObj.friends = friendsOnlyInput.value;
    // }
    // if (onlineNowInput.value) {
    //     apiBodyObj.online = onlineNowInput.value;
    // }

    if (validateMobile(idNameInput.value) == true) {
        apiBodyObj.mobile = idNameInput.value;
    } else if (validateEmail(idNameInput.value) == true) {
        apiBodyObj.email = idNameInput.value;
    } else if (validateId(idNameInput.value) == true) {
        apiBodyObj.id = idNameInput.value;
    } else if (validateName(idNameInput.value) == true) {
        apiBodyObj.name = idNameInput.value;
    }

    getUser(apiBodyObj);
}

function getUser(apiBodyObj) {
    console.log("getUser");


    busy.activate();

    _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
        getUserResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function getUserResult(result) {
    console.log("getUserResult");
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;

        if (resultArr !== null) {

            var resultArrProcessed = [];
            for (var i = 0; i < resultArr.length; i++) {
                var obj = {};
                obj = resultArr[i];
                
                if(resultArr[i].contact_id){
                    obj.imgPath = _model.userImageUrl+resultArr[i].contact_id+"?kycImage=0";
                }else{
                    obj.imgPath = _model.userImageUrl+resultArr[i].id+"?kycImage=0";
                }
                
                resultArrProcessed.push(obj);
            }

            friendsListData.addAll(resultArrProcessed);
        }
    }
    // else {
    //     if (arr.hasOwnProperty("error") && arr.error == "no_users")
    //         tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "No users found" });
    // }
}

function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        console.log("Is email");
        return (true);
    }
    console.log("Is not email");
    return (false);
}

function validateMobile(mobile) {
    if (mobile.length > 7 && /^[0-9 -()+]+$/.test(mobile)) {
        console.log("Is mobile");
        return (true);
    }
    console.log("Is not mobile");
    return (false);
}

function validateId(id) {
    if (/^\d+$/.test(id)) {
        console.log("Is id");
        return (true);
    }
    console.log("Is not id");
    return (false);
}

function validateName(name) {
    if (name.length >= 3 && /^[a-zA-Z ]+/.test(name)) {
        console.log("Is Valid name");
        return (true);
    }
    console.log("Is not valid name");
    return (false);
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    onViewActivate: onViewActivate,
    searchHandlerClicked: searchHandlerClicked,
    pagingLoadMore: pagingLoadMore,
    friendsListData: friendsListData,

    idNameInput: idNameInput,
    // friendsOnlyInput: friendsOnlyInput,
    // onlineNowInput: onlineNowInput,

    friendsClickHandle: friendsClickHandle,
}