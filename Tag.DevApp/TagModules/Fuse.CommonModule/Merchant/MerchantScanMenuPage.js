var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var TransferError = require('Utils/TransferError');
var moment = require('Utils/moment');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var idUserScaned = "";

var urlUserImageScaned = Observable();
var nameUserScaned = Observable();
var ratingUserScaned = Observable();
var joinPossibleStat = Observable(false);
var leavePossibleStat = Observable(false);

var nowRating = Observable(0);
var rateuserPopupShow = Observable(false);
var roleNameScaned = Observable();

var creditChargePopupShow = Observable(false);
var walletIdPay;
var currencyCodePay = Observable();
var amountInputPay = Observable("");
var notesTxtPay = Observable("");
var defaultWalletId = "";
var defaultWalletCode = "";

var miniAppMerchantList = Observable();
var billerListMerchantList = Observable();

var identifierPay = false;
var identifierValue = "";

var merchantServices = Observable(false);
var payreceiptPopupShow = Observable(false);
var scanedIdDisplay = Observable(false);
var receiptAmount = Observable("");
var receiptTransactionID = Observable("");
var receiptDate = Observable("");

this.Parameter.onValueChanged(module, function (param) {
  miniAppMerchantList.clear();
  billerListMerchantList.clear();

  if (_model.activePerspectiveType == "user") {
    merchantServices.value = false;
  } else {
    merchantServices.value = true;
  }

  var resultUser = param.resultUser;
  nowRating.value = 0;

  idUserScaned = resultUser.id;
  scanedIdDisplay.value = idUserScaned;

  urlUserImageScaned.value = _model.communityImageUrl + resultUser.id;
  nameUserScaned.value = resultUser.community_name;
  ratingUserScaned.value = resultUser.rating;

  joinPossibleStat.value = false;
  leavePossibleStat.value = false;
  roleNameScaned.value = "";

  console.log(JSON.stringify(resultUser));

  identifierPay = false;
  identifierValue = "";
  if (param.identifier != null) {
    identifierPay = true;
    identifierValue = param.identifier;
  }

  if (param.perspective == "user") {

    if (resultUser.role_type == 0) {
      roleNameScaned.value = "Non Member";

      joinPossibleStat.value = true;
    } else if (resultUser.role_type == "owner") {
      roleNameScaned.value = "Owner";
    } else {
      roleNameScaned.value = "------------";
      leavePossibleStat.value = true;
    }
  }

  defaultWalletLoad();

})


function defaultWalletLoad() {
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.new_call = 1;

  _WebService.request("user/DefaultWallet", apiBodyObj).then(function (result) {
    gotDefaultWalletResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotDefaultWalletResult(result) {
  console.log("gotDefaultWalletResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    var resultObj = arr.result;
    if (resultObj.wallet_id) {
      defaultWalletId = resultObj.wallet_id;
      defaultWalletCode = resultObj.currency_code;
    }
  }

  billerListLoad();

}


function billerListLoad() {
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.merchant_id = idUserScaned;

  _WebService.request("BillerSetup/searchbiller", apiBodyObj).then(function (result) {
    gotBillerSearchResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotBillerSearchResult(result) {
  console.log("gotBillerSearchResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    var resultArr = arr.result;
    billerListMerchantList.addAll(resultArr);
  }

  // miniappMerchantListLoad();
}


function miniappMerchantListLoad() {
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.merchant_id = idUserScaned;

  _WebService.request("MiniappConfiguration/", apiBodyObj).then(function (result) {
    gotMiniappConfigResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotMiniappConfigResult(result) {
  console.log("gotMiniappConfigResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultArr = arr.result;
    miniAppMerchantList.addAll(resultArr);
  }

}

function payCommunity_clickHandler() {
  walletIdPay = "";
  currencyCodePay.value = loc.value.select;
  amountInputPay.value = "";
  notesTxtPay.value = "";

  if (defaultWalletId != "") {
    walletIdPay = defaultWalletId;
    currencyCodePay.value = defaultWalletCode;
  }

  creditChargePopupShow.value = true;
}


function walletOptionChangedPayEvent(args) {
  walletIdPay = args.walletId;
  currencyCodePay.value = args.currencyCode;
}

function creditChargeSendHandler() {
  var amountValue = amountInputPay.value;

  if (!Validator.amount(amountValue)) {
    tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
    return;
  }

  if (walletIdPay === "") {
    tagEvents.emit("toastShow", { message: loc.value.select_wallet });
    return;
  }

  busy.activate();
  var apiBodyObj = {};
  apiBodyObj.amount = amountInputPay.value;
  apiBodyObj.from_wallet_id = walletIdPay;
  apiBodyObj.to_wallet_id = walletIdPay;
  apiBodyObj.narration = notesTxtPay.value;

  if (identifierPay) {
    apiBodyObj.identifier = identifierValue;
  } else {
    apiBodyObj.to_id = idUserScaned;
    apiBodyObj.to_type = "community";
  }

  _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
    gotWalletTransferResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotWalletTransferResult(result) {
  console.log("gotWalletTransferResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    creditChargePopupShow.value = false;
    var resultObj = arr.result;

    receiptAmount.value = resultObj.transfer_currency + " " + resultObj.transfer_amount;
    receiptTransactionID.value = resultObj.transaction_id;
    receiptDate.value = moment(resultObj.transfer_date, "YYYY-MM-DD hh:mm:ss").format('D MMMM YYYY h:mm A');

    payreceiptPopupShow.value = true;
    // tagEvents.emit("alertEvent", { type: "info", title: "PAY", message: resultObj.transfer_amount + " " + resultObj.transfer_currency + " : Credited to user" });
  } else {
    TransferError.errorHandle(arr);
  }

}

function ratingChangeClickHandler() {
  console.log("Rating Click Handler");
  rateuserPopupShow.value = true;
}

function rateCancelClickHandler() {
  rateuserPopupShow.value = false;
}

function rateUserClickHandler() {
  busy.activate();

  var apiBodyObj = {};

  var newRating = nowRating.value;
  newRating = Math.round(newRating);;
  apiBodyObj.rating = newRating;

  apiBodyObj.to_id = idUserScaned;
  apiBodyObj.to_type = "community";

  _WebService.request("ratings/addratings", apiBodyObj).then(function (result) {
    gotAddRatingResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotAddRatingResult(result) {
  console.log("gotAddRatingResult")
  busy.deactivate();
  rateuserPopupShow.value = false;

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    tagEvents.emit("toastShow", { message: loc.value.rate_success_message });
  } else {
    tagEvents.emit("toastShow", { message: loc.value.error_occurred });
  }

}

function communityJoinClickHandler() {
  console.log("communityJoinClickHandler");

  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.id = idUserScaned;

  _WebService.request("community/join", apiBodyObj).then(function (result) {
    gotCommunityJoinResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotCommunityJoinResult(result) {
  console.log("gotCommunityJoinResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    joinPossibleStat.value = false;
    leavePossibleStat.value = true;

    tagEvents.emit("toastShow", { message: loc.value.community_join_success });
  } else {
    if (arr.error == "private_community") {
      tagEvents.emit("toastShow", { message: "Private community" });
    } else {

      tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
  }

}

function communityLeaveClickHandler() {
  console.log("communityLeaveClickHandler");
  busy.activate();

  _WebService.request("community/leave/" + idUserScaned).then(function (result) {
    gotCommunityLeaveResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotCommunityLeaveResult(result) {
  console.log("gotCommunityLeaveResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    joinPossibleStat.value = true;
    leavePossibleStat.value = false;

    tagEvents.emit("toastShow", { message: loc.value.community_leave_success });
  } else {
    tagEvents.emit("toastShow", { message: loc.value.error_occurred });
  }

}

function minippLoadClick(args) {
  console.log(JSON.stringify(args.data.load_url))

  // router.push("miniAppWebView", { data: args.data, datId: Math.random() });
}



var billPayPopupShow = Observable(false);
var billingDataList = Observable();
var billPayAmount = Observable("");
var billCurrencyCode = Observable("");
var billPayTitle = Observable("");
var otherValue = Observable("");
var billerId;

function billerButtonClick(args) {
  console.log(JSON.stringify(args.data))

  billingDataList.clear();
  billPayAmount.value = "";
  otherValue.value = "";
  billerId = args.data.billtype_ID;
  billCurrencyCode.value = args.data.currency;
  billPayTitle.value = args.data.title;

  // billerName.value =  args.data.owner.name;
  // biller = billerName.value;

  var datas = args.data.biller_data;
  for (var i = 0; i < datas.length; i++) {
    var billField = {};

    billField.fieldName = datas[i].display_name;
    billField.slugName = datas[i].slug;
    billField.fieldValue = Observable();
    billingDataList.add(billField);

  }
  billPayPopupShow.value = true;
}



function payBillPayment() {
  // fieldValueBo = false;
  if (billPayAmount.value == "") {
    tagEvents.emit("toastShow", { message: "Amount is Required" });
    return;
  }

  var apiBodyObj = {};
  apiBodyObj.biller_id = billerId;
  apiBodyObj.amount = billPayAmount.value;
  apiBodyObj.merchant_id = idUserScaned;
  apiBodyObj.other_data = otherValue.value;

  var fieldValueNotComplete = false;
  billingDataList.forEach(function (item) {
    console.log("------------");
    console.log(JSON.stringify(item.fieldValue.value));

    apiBodyObj[item.slugName] = item.fieldValue.value;
    if (item.fieldValue.value == null) {
      fieldValueNotComplete = true;
      // return;
    }
  })

  if (fieldValueNotComplete == true) {
    tagEvents.emit("toastShow", { message: "Please Enter Required Field" });
    return;
  }

  busy.activate();

  _WebService.request("BillerSetup/PayBill", apiBodyObj).then(function (result) {
    gotPayBillMerchantResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });


}
function gotPayBillMerchantResult(result) {
  console.log("gotPayBillMerchantResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    tagEvents.emit("alertEvent", { type: "info", title: "", message: "Bill payment completed successfully", callback: confirmSendAlertClosedHandler });
  } else {
    if (arr.error == "insuffcient_balance") {
      tagEvents.emit("toastShow", { message: "Insuffcient Balance" });
    }
  }
}

function confirmSendAlertClosedHandler(args) {
  billPayPopupShow.value = false;
}


function popupCloseHandle() {
  billPayPopupShow.value = false;
  creditChargePopupShow.value = false;
  rateuserPopupShow.value = false;
  payreceiptPopupShow.value = false;
}



function showNetworkErrorMessage() {
  busy.deactivate();
  tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
  urlUserImageScaned: urlUserImageScaned,
  nameUserScaned: nameUserScaned,
  ratingUserScaned: ratingUserScaned,
  joinPossibleStat: joinPossibleStat,
  leavePossibleStat: leavePossibleStat,

  payCommunity_clickHandler: payCommunity_clickHandler,
  ratingChangeClickHandler: ratingChangeClickHandler,
  rateuserPopupShow: rateuserPopupShow,
  nowRating: nowRating,
  rateUserClickHandler: rateUserClickHandler,
  rateCancelClickHandler: rateCancelClickHandler,
  roleNameScaned: roleNameScaned,

  communityJoinClickHandler: communityJoinClickHandler,
  communityLeaveClickHandler: communityLeaveClickHandler,


  creditChargePopupShow: creditChargePopupShow,
  currencyCodePay: currencyCodePay,
  amountInputPay: amountInputPay,
  notesTxtPay: notesTxtPay,
  walletOptionChangedPayEvent: walletOptionChangedPayEvent,
  creditChargeSendHandler: creditChargeSendHandler,

  miniAppMerchantList: miniAppMerchantList,
  minippLoadClick: minippLoadClick,

  billerListMerchantList: billerListMerchantList,
  billerButtonClick: billerButtonClick,
  billPayPopupShow: billPayPopupShow,
  billingDataList: billingDataList,
  billPayAmount: billPayAmount,
  billCurrencyCode: billCurrencyCode,
  billPayTitle: billPayTitle,
  otherValue: otherValue,
  payBillPayment: payBillPayment,

  popupCloseHandle: popupCloseHandle,
  merchantServices: merchantServices,
  payreceiptPopupShow: payreceiptPopupShow,
  scanedIdDisplay: scanedIdDisplay,
  receiptAmount: receiptAmount,
  receiptTransactionID: receiptTransactionID,
  receiptDate: receiptDate,

}