var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var selectedPageIndex = Observable(0);
var invitePopupShow = Observable(false);
var emailInviteInnput = Observable("");

this.Parameter.onValueChanged(module, function (param) {
    console.log(JSON.stringify(param.requestStatus))
    if (param.requestStatus == "yes") {
        selectedPageIndex.value = 1;
        router.gotoRelative(inner, "friendRequestsPage");
    }else{
        selectedPageIndex.value = 0;
        router.gotoRelative(inner, "friendsListPage");
    }
})

function onTabBarChanged(args) {
    if (selectedPageIndex.value == 0) {
       router.gotoRelative(inner, "friendsListPage");
    } else if (selectedPageIndex.value == 1) {
        router.gotoRelative(inner, "friendRequestsPage");
    }
}

function inviteFriendClicked() {
    emailInviteInnput.value = "";
    invitePopupShow.value = true;
}

function inviteCancelClickHandler() {
    invitePopupShow.value = false;
}

function inviteUserClickHandler() {
    console.log("inviteUserClickHandler");

    if (Validator.email(emailInviteInnput.value)) {
        busy.activate();

        var apiBodyObj = {};
        apiBodyObj.email = emailInviteInnput.value;
        _WebService.request("contact/invite", apiBodyObj).then(function (result) {
            inviteUserResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    }
}

function inviteUserResult(result) {
    console.log("inviteUserResult");
    busy.deactivate();
    invitePopupShow.value = false;

    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Successfully send contact invitation" });
    }
    else {
        if (arr.hasOwnProperty("error") && arr.error == "contact_already_exists")
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Contact already exists" });
        else if (arr.hasOwnProperty("error") && arr.error == "user_blocked")
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "User blocked" });
        else if (arr.hasOwnProperty("error") && arr.error == "request_pending")
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Contact request pending" });
        else if (arr.hasOwnProperty("error") && arr.error == "already_in_the_list")
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Contact already in the list" });
        else if (arr.hasOwnProperty("error") && arr.error == "invalid_email")
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Invalid Email" });
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    selectedPageIndex: selectedPageIndex,
    onTabBarChanged: onTabBarChanged,
    inviteFriendClicked: inviteFriendClicked,
    invitePopupShow: invitePopupShow,
    emailInviteInnput: emailInviteInnput,
    inviteCancelClickHandler: inviteCancelClickHandler,
    inviteUserClickHandler: inviteUserClickHandler,
}