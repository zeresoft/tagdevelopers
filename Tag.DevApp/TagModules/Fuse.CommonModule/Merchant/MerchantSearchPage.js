var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var membersListData = Observable();
var idNameInput = Observable("");

var firstNameInput = Observable("");
var lastNameInput = Observable("");
var emailSearchedShow = Observable("");

var addInviteOptions = Observable({ index: 0, name: "Send email invite", value: "invite" }, { index: 1, name: "Add and send email", value: "add" });
var addInviteOptionsIndex = Observable(0);

var addInvitePopupShow = Observable(false);

var searchLoaded = false;

function onViewActivate() {
    membersLoadHandle();
}

function membersLoadHandle() {
    membersListData.clear();

    if (idNameInput.value != "") {
        searchLoaded = true;
        searchListLoad();
    } else {
        searchLoaded = false;
        membersListLoad();
    }
}

function pagingLoadMore() {
    if (membersListData.length != 0) {
        console.log("pagingLoadMore")
        if (!searchLoaded) {
            membersListLoad();
        }
    }
}


function membersListLoad() {
    busy.activate();

    var role_type = _model.communityprofileDetails.value.role.role_type;

    var apiBodyObj = {};
    apiBodyObj.count = 20;
    apiBodyObj.offset = membersListData.length;

    // call when user is owner
    if (role_type == "owner") {
        _WebService.request("community/AllMembers", apiBodyObj).then(function (result) {
            gotMembersListResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    } else {
        _WebService.request("community/members", apiBodyObj).then(function (result) {
            gotMembersListResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    }
}

function gotMembersListResult(result) {
    console.log("gotmembersListResult");
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        var resultArr = arr.result;

        var resultArrProcessed = [];
        for (var i = 0; i < resultArr.length; i++) {
            var obj = {};
            obj = resultArr[i];
            obj.imgPath = _model.userImageUrl + resultArr[i].id + "?kycImage=0";

            var roleObj = {};
            roleObj.role_name = resultArr[i].role_name;
            roleObj.role_type = resultArr[i].role_type;
            roleObj.role_status = "approved";
            obj.role = roleObj;

            obj.rating = 0;

            resultArrProcessed.push(obj);
        }

        membersListData.addAll(resultArrProcessed);

    }
}

function searchHandler() {
    membersLoadHandle();
}

function searchListLoad() {
    console.log("searchHandler");
    var apiBodyObj = {};
    if (validateMobile(idNameInput.value) == true) {
        console.log("Is mobile");
        apiBodyObj.mobile = idNameInput.value;
        getUserProfile(apiBodyObj);
    } else if (validateEmail(idNameInput.value) == true) {
        console.log("Is email");
        emailSearchedShow.value = idNameInput.value;

        apiBodyObj.email = idNameInput.value;
        getUserProfile(apiBodyObj);
    } else if (validateId(idNameInput.value) == true) {
        console.log("Is id");
        apiBodyObj.id = idNameInput.value;
        getUserProfile(apiBodyObj);
    } else if (validateName(idNameInput.value) == true) {
        console.log("Is Valid name " + idNameInput.value);

        busy.activate();
        apiBodyObj.name = idNameInput.value;
        _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
            gotUsersListResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        tagEvents.emit("toastShow", { message: "Plesae enter a valid input" });
    }
}

function gotUsersListResult(result) {
    console.log("gotUsersListResult");

    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;

        if (resultArr != null) {
            var resultArrProcessed = [];
            for (var i = 0; i < resultArr.length; i++) {
                var obj = {};
                obj = resultArr[i];
                obj.imgPath = _model.userImageUrl + resultArr[i].id + "?kycImage=0";

                resultArrProcessed.push(obj);
            }

            membersListData.addAll(resultArrProcessed);
        }

    } else {
        tagEvents.emit("toastShow", { message: "No results found" });
    }
}

function usersClickHandle(args) {
    console.log(JSON.stringify(args.data));
    userSearchSelectedHandler(args.data);
}

function getUserProfile(apiBodyObj) {
    busy.activate();
    _WebService.request("user/profile", apiBodyObj).then(function (result) {
        gotUserProfileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUserProfileResult(result) {
    console.log("gotUserProfileResult");
    busy.deactivate();
    console.log(JSON.stringify(result));

    var arr = result;
    idNameInput.value = "";

    if (arr.status == "success") {
        userSearchSelectedHandler(arr.result);
    } else {
        if (arr.error == "invalid_id") {
            tagEvents.emit("toastShow", { message: loc.value.invalid_user_id });
        } else if (arr.error == "invalid_email") {
            tagEvents.emit("toastShow", { message: loc.value.invalid_user_email });
            showNewUserPopup();
        } else if (arr.error == "invalid_mobile") {
            tagEvents.emit("toastShow", { message: loc.value.invalid_mobile_no });
        } else {
            tagEvents.emit("toastShow", { message: "No results found" });
        }
    }
}


function userSearchSelectedHandler(resultObj) {
    console.log("userSearchSelectedHandler-------------");
    console.log(JSON.stringify(resultObj));
    var username = resultObj.user_firstname + " " + resultObj.user_lastname;

    var role_name = "Non Member";
    var role_status = "notapproved";
    var role_type = "notowner";
    if (resultObj.role.role_status == "approved") {
        role_name = resultObj.role.role_name;
        role_status = "approved";
        role_type = resultObj.role.role_type;
    }

    router.push("merchantSearchResultPage", { username: username, rating: resultObj.rating, role: role_name, role_status: role_status, role_type: role_type, userID: resultObj.id, datId: Math.random() });
}


function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        console.log("Is email");
        return (true);
    }
    console.log("Is not email");
    return (false);
}

function validateMobile(mobile) {
    if (mobile.length > 7 && /^[0-9 -()+]+$/.test(mobile)) {
        console.log("Is mobile");
        return (true);
    }
    console.log("Is not mobile");
    return (false);
}

function validateId(id) {
    if (/^\d+$/.test(id)) {
        console.log("Is id");
        return (true);
    }
    console.log("Is not id");
    return (false);
}

function validateName(name) {
    if (name.length >= 3 && /^[a-zA-Z ]+/.test(name)) {
        console.log("Is Valid name");
        return (true);
    }
    console.log("Is not valid name");
    return (false);
}



function showNewUserPopup() {
    addInviteOptionsIndex.value = 1;
    firstNameInput.value = "";
    lastNameInput.value = "";
    addInvitePopupShow.value = true;
}

function addNewUserConfirmClickHandler() {
    console.log("addNewUserConfirmClickHandler");

    var first_name = firstNameInput.value;
    var last_name = lastNameInput.value;

    if (!first_name || first_name == '') {
        tagEvents.emit("toastShow", { message: "First name is required" });
        return false;
    }

    if (!last_name || last_name == '') {
        tagEvents.emit("toastShow", { message: "Last name is required" });
        return false;
    }


    busy.activate();
    var apiBodyObj = {};

    if (addInviteOptionsIndex.value == 1) {
        apiBodyObj.user_firstname = first_name;
        apiBodyObj.user_lastname = last_name;
        apiBodyObj.user_email = emailSearchedShow.value;
        // apiBodyObj.role_id = role_ID;

        _WebService.request("user/createUser", apiBodyObj).then(function (result) {
            gotAddUserResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        apiBodyObj.email = emailSearchedShow.value;
        _WebService.request("contact/invite", apiBodyObj).then(function (result) {
            inviteUserResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    }
}

function gotAddUserResult(result) {
    console.log("gotAddUserResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        addInvitePopupShow.value = false;

        emailSearchedShow.value = "";
        tagEvents.emit("toastShow", { message: "User added" });

        membersLoadHandle();
    } else {
        console.log(arr.error);
        tagEvents.emit("toastShow", { message: "Adding user failed" });
    }

}

function inviteUserResult(result) {
    console.log("inviteUserResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        addInvitePopupShow.value = false;

        emailSearchedShow.value = "";
        tagEvents.emit("toastShow", { message: "invitations has been sent" });
        membersLoadHandle();
    } else {
        console.log(arr.error);
        tagEvents.emit("toastShow", { message: "Sending invitation failed" });
    }
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivate: onViewActivate,
    membersListData: membersListData,
    idNameInput: idNameInput,
    searchHandler: searchHandler,
    usersClickHandle: usersClickHandle,

    addInvitePopupShow: addInvitePopupShow,
    firstNameInput: firstNameInput,
    lastNameInput: lastNameInput,
    emailSearchedShow: emailSearchedShow,
    addInviteOptions: addInviteOptions,
    addInviteOptionsIndex: addInviteOptionsIndex,

    addNewUserConfirmClickHandler: addNewUserConfirmClickHandler,
    pagingLoadMore: pagingLoadMore,
}