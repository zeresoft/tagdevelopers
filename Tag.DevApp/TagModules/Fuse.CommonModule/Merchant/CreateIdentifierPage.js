var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var createIdentifierPopupShow = Observable(false);

var createIdenifierbuttonText = Observable(loc.value.save);
var createidentifiervalue = Observable(false);
var identifierName = Observable("");
var identifier = Observable("");
var identifiersListData = Observable();
var identifier_ID = Observable();
var searchKeyInput = Observable("");
var usertobeConnectedItems = Observable({ index: 0, name: "My Self", value: "myself" }, { index: 1, name: "Another User", value: "anotheruser" }, { index: 2, name: "Another Merchant", value: "anothermerchant" });
var userConnectedItemsIndex = Observable(0);
var myselfConnectionFlag = Observable(true);
var idEmailInput = Observable();
var identifierUpdateDeleteValue = Observable("");
var identifierTypeHintText = Observable("ID,Email");
var linked_totype = Observable("");
var merchant_perspectve = Observable(false);
var mobilenumberInputPaymentConfirmation = Observable("");
var countryCodeInput = Observable("");

function onViewActivate() {
    identifiersListData.clear();

    if (_model.activePerspectiveType == "community") {
        merchant_perspectve.value = true;
    } else {
        merchant_perspectve.value = false;
    }

    getIdentifierList();
}

function search_clickHandler() {
    identifiersListData.clear();
    getIdentifierList();
}

function pagingLoadMore() {
    if (identifiersListData.length != 0) {
        console.log("pagingLoadMore")
        getIdentifierList();
    }
}

function getIdentifierList() {
    busy.activate();

    var apiBodyObj = {};
    if (searchKeyInput.value != "") {
        apiBodyObj.search = searchKeyInput.value;
    }
    apiBodyObj.count = 20;
    apiBodyObj.offset = identifiersListData.length;

    _WebService.request("identifiers/myIdentifiers", apiBodyObj).then(function (result) {
        gotIdentifiersListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotIdentifiersListResult(result) {
    console.log("gotIdentifiersList")
    busy.deactivate();

    var arr = result;
    console.log("IdentifiersListResult" + JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            identifiersListData.addAll(resultArr);
        }
    }
}

function identifieritemClickHandler(args) {
    idEmailInput.clear();
    identifierName.value = args.data.identifier_name;
    identifier.value = args.data.identifier;
    identifier_ID.value = args.data.id;

    _model.nfcReturnTarget = "add";

    createIdentifierPopupShow.value = true;
    createIdenifierbuttonText.value = loc.value.update;
    createidentifiervalue.value = false;

    if ((args.data.linked_by_type == 1) || (args.data.linked_by_type == 2)) {
        myselfConnectionFlag.value = false;
        identifierUpdateDeleteValue.value = "deleteonly";
        idEmailInput.value = args.data.user_id;
    } else if ((args.data.linked_by_type == null) || (args.data.linked_by_type == 0)) {
        myselfConnectionFlag.value = true;

        identifierUpdateDeleteValue.value = "updateanddelete"
        userConnectedItemsIndex.value = 0;
        idEmailInput.value = "";
        linked_totype.value = "self";
    } else {
        identifierUpdateDeleteValue.value = "deleteonly"
    }

    if (args.data.linked_by_type == 1) {
        userConnectedItemsIndex.value = 1;
        if (merchant_perspectve.value == true) {
            linked_totype.value = "Identifier Created For Another User";
        } else {
            linked_totype.value = "Identifier Created By Another Merchant";
        }

    } else if (args.data.linked_by_type == 2) {

        userConnectedItemsIndex.value = 2;


        if (_model.nowCommunityID == args.data.merchant_id) {

            linked_totype.value = "Identifier Created for Another Merchant";
        } else {

            linked_totype.value = "Identifier Created By Another Merchant";
            idEmailInput.value = args.data.merchant_id;

        }
    } else if (args.data.linked_by_type == "") {

        if (merchant_perspectve.value == false) {
            linked_totype.value = "Identifier Created By User Self";
            identifierUpdateDeleteValue.value = "updateanddelete";//User can only delete the identifier created by another merchant
        } else {
            linked_totype.value = "Identifier Created By Merchant Self";
            identifierUpdateDeleteValue.value = "updateanddelete"
        }

    }

    if (args.data.mobile_no != null) {
        mobilenumberInputPaymentConfirmation.value = args.data.mobile_no.toString();
    }

    if (args.data.country_code != null) {
        countryCodeInput.value = args.data.country_code;
    }
}

function createIdentifierClickHandler() {
    _model.nfcReturnTarget = "add";

    createIdentifierPopupShow.value = true;
    createidentifiervalue.value = true;
    createIdenifierbuttonText.value = loc.value.save;
    identifierName.value = "";
    identifier.value = "";
    idEmailInput.value = "";
    userConnectedItemsIndex.value = 0;
    myselfConnectionFlag.value = true;
    mobilenumberInputPaymentConfirmation.value = "";
    countryCodeInput.value = "";
}

function qrscan_clickHandler() {
    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandler });
}

function qrReceivedHandler(args) {
    var scanData = args;
    identifier.value = scanData;
    console.log("scanData" + scanData)
}

tagEvents.on("nfcAddScanedEvent", function (arg) {
    identifier.value = arg.scanData;
});

function onCountryCodeChangeEvent(args) {
    countryCodeInput.value = args.countryPhoneCode;
}

function popupCloseHandle() {
    console.log("-------------popupCloseHandle------- ");
    _model.nfcReturnTarget = "search";
    createIdentifierPopupShow.value = false;
}

function saveClickHandler() {
    console.log("save clicked");

    if (identifierName.value === "") {
        tagEvents.emit("toastShow", { message: loc.value.identifier_valid_name });
        return;
    }

    if (identifier.value === "") {
        tagEvents.emit("toastShow", { message: loc.value.identifier_valid_identifier });
        return;
    }

    if ((userConnectedItemsIndex.value == 1) || (userConnectedItemsIndex.value == 2)) {
        if (idEmailInput.value === "") {
            tagEvents.emit("toastShow", { message: loc.value.identifier_validinput });
            return;
        }
    }

    var mobilenumber_flag = false;


    if (mobilenumberInputPaymentConfirmation.value != "") {
        if (validateMobile(mobilenumberInputPaymentConfirmation.value)) {
            if (countryCodeInput.value != "") {
                mobilenumber_flag = true;
            } else {
                tagEvents.emit("toastShow", { message: "Please select country code" });
                return;
            }
        } else {
            tagEvents.emit("toastShow", { message: loc.value.identifier_validmobilenumber });
            return;
        }
    } 



    if (merchant_perspectve.value) {
        var apiBodyObj = {};
        apiBodyObj.identifier_name = identifierName.value;
        apiBodyObj.identifier = identifier.value;

        if (createidentifiervalue.value) {
            if ((userConnectedItemsIndex.value == 1)) {
                apiBodyObj.user_type = userConnectedItemsIndex.value;
                if (mobilenumber_flag) {
                    apiBodyObj.country_code = countryCodeInput.value;
                    apiBodyObj.mobile_no = mobilenumberInputPaymentConfirmation.value;
                }

                if (validateEmail(idEmailInput.value)) {
                    getUserIdfromEmail(idEmailInput.value, apiBodyObj);
                } else if (validateId(idEmailInput.value)) {
                    apiBodyObj.user_id = idEmailInput.value;
                    doIdentifierCreateProcess(apiBodyObj);
                } else {
                    tagEvents.emit("toastShow", { message: loc.value.dentifier_valididemail });
                }

            } else if ((userConnectedItemsIndex.value == 2)) {
                if (validateId(idEmailInput.value)) {
                    apiBodyObj.user_type = userConnectedItemsIndex.value;
                    if (mobilenumber_flag) {
                        apiBodyObj.country_code = countryCodeInput.value;
                        apiBodyObj.mobile_no = mobilenumberInputPaymentConfirmation.value;
                    }

                    apiBodyObj.user_id = idEmailInput.value;
                    doIdentifierCreateProcess(apiBodyObj);
                } else {
                    tagEvents.emit("toastShow", { message: loc.value.identifier_validid });
                }
            } else if (userConnectedItemsIndex.value == 0) {
                //This is for option MYSELF
                if (mobilenumber_flag == true) {
                    apiBodyObj.country_code = countryCodeInput.value;
                    apiBodyObj.mobile_no = mobilenumberInputPaymentConfirmation.value;
                }
                doIdentifierCreateProcess(apiBodyObj);
            }
        } else {//Updation Process is only for MY SELF option
            apiBodyObj.id = identifier_ID.value;
            if (mobilenumber_flag == true) {
                apiBodyObj.country_code = countryCodeInput.value;
                apiBodyObj.mobile_no = mobilenumberInputPaymentConfirmation.value;
            }

            doIdentifierUpdateProcess(apiBodyObj);
        }
    } else {
        doCreateUpdateIdentifierforUser();
    }
}

function doCreateUpdateIdentifierforUser() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.identifier_name = identifierName.value;
    apiBodyObj.identifier = identifier.value;

    if (createidentifiervalue.value) {
        _WebService.request("Identifiers/create", apiBodyObj).then(function (result) {
            gotCreateIdentifierResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        //this part is updation click handling
        apiBodyObj.id = identifier_ID.value;

        _WebService.request("Identifiers/update", apiBodyObj).then(function (result) {
            gotUpdateIdentifierResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    }
}

function getUserIdfromEmail(emailInput, identifierapiBodyObject) {
    var apiBodyObject = {};
    apiBodyObject.email = emailInput;

    busy.activate();
    _WebService.request("user/searchuser", apiBodyObject).then(function (result) {
        gotUserResult(result, identifierapiBodyObject);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUserResult(result, identifierapiBodyObject) {
    console.log("getUserResult");
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;
        var user_id = resultArr[0].id;
        console.log("USERID-->" + user_id);
        identifierapiBodyObject.user_id = user_id;
        doIdentifierCreateProcess(identifierapiBodyObject);
    }
    else {
        tagEvents.emit("toastShow", { message: loc.value.identifier_validusernotfound });
    }
}

function doIdentifierCreateProcess(apiBodyObj) {
    busy.activate();

    _WebService.request("Identifiers/create", apiBodyObj).then(function (result) {
        gotCreateIdentifierResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function doIdentifierUpdateProcess(apiBodyObj) {
    busy.activate();

    _WebService.request("Identifiers/update", apiBodyObj).then(function (result) {
        gotUpdateIdentifierResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCreateIdentifierResult(result) {
    busy.deactivate();

    console.log("gotCreateIdentifierResult");
    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        _model.nfcReturnTarget = "search";
        createIdentifierPopupShow.value = false;

        tagEvents.emit("toastShow", { message: loc.value.identifier_created_success_message });

        identifiersListData.clear();
        getIdentifierList();
    } else {
        if (arr.error == "identifiers already exist") {
            tagEvents.emit("alertEvent", { type: "info", title: "Identifier", message: "Identifier already exist, please use another QR or NFC tag." });
        } else {
            _model.nfcReturnTarget = "search";
            createIdentifierPopupShow.value = false;
            tagEvents.emit("toastShow", { message: loc.value.identifier_created_failed_message });
        }
    }
}

function gotUpdateIdentifierResult(result) {
    busy.deactivate();
    _model.nfcReturnTarget = "search";
    createIdentifierPopupShow.value = false;
    console.log("gotUpdatingIdentifierResult");
    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: loc.value.identifier_update_success_message });
        identifiersListData.clear();
        getIdentifierList();
    } else {
        tagEvents.emit("toastShow", { message: loc.value.identifier_update_failed_message });
    }
}

function deleteClickHandler() {
    busy.activate();
    console.log("delete clicked");
    var apiBodyObj = {};
    apiBodyObj._id = identifier_ID.value;
    _WebService.request("Identifiers/delete", apiBodyObj).then(function (result) {
        gotDeleteIdentifierResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDeleteIdentifierResult(result) {
    busy.deactivate();
    createIdentifierPopupShow.value = false;
    console.log("gotDeletingIdentifierResult");
    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: loc.value.identifier_delete_success_message });

        identifiersListData.clear();
        getIdentifierList();
    } else {
        tagEvents.emit("toastShow", { message: loc.value.identifier_delete_failed_message });
    }
}

function connectionTypeSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))
    userConnectedItemsIndex.value = selectedItem.index;
    if (userConnectedItemsIndex.value == 0) {
        myselfConnectionFlag.value = true;
    } else if (userConnectedItemsIndex.value == 1) {
        myselfConnectionFlag.value = false;
        identifierTypeHintText.value = "ID,Email";
    } else if (userConnectedItemsIndex.value == 2) {
        myselfConnectionFlag.value = false;
        identifierTypeHintText.value = "ID";
    } else {
        myselfConnectionFlag.value = true;
    }
}

function validateId(id) {
    if (/^\d+$/.test(id)) {
        console.log("Is id");
        return (true);
    }
    console.log("Is not id");
    return (false);
}
function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        console.log("Is email");
        return (true);
    }
    console.log("Is not email");
    return (false);
}
function validateMobile(mobile) {

    if (mobile.length > 7 && /^[0-9 -()+]+$/.test(mobile)) {
        console.log("Is mobile");
        return (true);
    }
    console.log("Is not mobile");
    return (false);
}

module.exports = {
    pagingLoadMore: pagingLoadMore,
    createIdentifierClickHandler: createIdentifierClickHandler,
    createIdentifierPopupShow: createIdentifierPopupShow,
    saveClickHandler: saveClickHandler,
    deleteClickHandler: deleteClickHandler,
    identifierName: identifierName,
    identifier: identifier,
    identifiersListData: identifiersListData,
    createidentifiervalue: createidentifiervalue,
    qrscan_clickHandler: qrscan_clickHandler,
    onViewActivate: onViewActivate,
    identifieritemClickHandler: identifieritemClickHandler,
    createIdenifierbuttonText: createIdenifierbuttonText,
    search_clickHandler: search_clickHandler,
    searchKeyInput: searchKeyInput,

    popupCloseHandle: popupCloseHandle,
    usertobeConnectedItems: usertobeConnectedItems,
    connectionTypeSelectionChange: connectionTypeSelectionChange,
    userConnectedItemsIndex: userConnectedItemsIndex,
    myselfConnectionFlag: myselfConnectionFlag,
    idEmailInput: idEmailInput,
    identifierUpdateDeleteValue: identifierUpdateDeleteValue,
    identifierTypeHintText: identifierTypeHintText,
    linked_totype: linked_totype,
    merchant_perspectve: merchant_perspectve,
    mobilenumberInputPaymentConfirmation: mobilenumberInputPaymentConfirmation,
    countryCodeInput: countryCodeInput,
    onCountryCodeChangeEvent: onCountryCodeChangeEvent,


}