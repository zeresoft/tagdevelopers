var Observable = require("FuseJS/Observable");
var _model = require("Models/GlobalModel");
var _WebService = require("Models/WebService");

var merchant_id; //It is using for updation process

var createMerchantPopupShow = Observable(false);
var merchantNameInput = Observable("");
var countryId = Observable();
var countryName = Observable();
var resellerIDInput = Observable("");

var merchantsDataList = Observable();
var merchantSearchInput = Observable("");
var showLeaveConfirmationPopup = Observable(false);


function onViewActivate() {
  merchantListLoad();
}

function merchantListLoad() {
  merchantsDataList.clear();
  busy.activate();
  var apiBodyObj = {};

  if (merchantSearchInput.value != "") {
    apiBodyObj.name = merchantSearchInput.value;
  } else {
    apiBodyObj.role_type = "member";
  }

  _WebService.request("community/search", apiBodyObj).then(function (result) {
    gotMerchantsListResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function gotMerchantsListResult(result) {
  console.log("gotmerchantsListResult");
  busy.deactivate();
  var arr = result;
  console.log(JSON.stringify(arr));
  if (arr && arr.status == "success") {
    var resultArr = arr.result;
    if (resultArr !== null && resultArr.length > 0) {
      merchantsDataList.addAll(resultArr);
    } else {
      tagEvents.emit("toastShow", { message: loc.value.data_not_found });
    }
  }
}

function searchHandler() {
  merchantListLoad();
}

function leaveClickHandler(args) {
  console.log("leaveClickHandler");
  merchant_id = args.data.id;
  showLeaveConfirmationPopup.value = true;
}

function leaveCommunityHandler() {
  console.log("leaveCommunityHandler");

  showLeaveConfirmationPopup.value = false;

  busy.activate();

  _WebService.request("community/leave/" + merchant_id).then(function (result) {
    leaveCommnuintyResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function leaveCommnuintyResult(result) {
  console.log("leaveCommnuintyResult");

  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));
  if (arr.status == "success") {
    merchantListLoad();
    tagEvents.emit("toastShow", { message: loc.value.community_leave_success });
  } else {
    tagEvents.emit("toastShow", { message: loc.value.error });
  }
}

function joinClickHandler(args) {
  console.log("joinClickHandler");

  busy.activate();

  merchant_id = args.data.id;

  _WebService.request("community/join/" + merchant_id).then(function (result) {
    joinCommnuintyResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function joinCommnuintyResult(result) {
  console.log("joinCommnuintyResult");
  busy.deactivate();
  var arr = result;
  console.log(JSON.stringify(arr));
  if (arr.status == "success") {
    merchantListLoad();
    tagEvents.emit("toastShow", { message: loc.value.community_join_success });
  } else {
    if (result.error == "private_community")
      tagEvents.emit("toastShow", {
        message: loc.value.private_community
      });
    else tagEvents.emit("toastShow", { message: loc.value.error });
  }
}


function createMerchantClickHandler() {
  merchantNameInput.value = "";
  countryId.value = _model.userprofileDetails.value.user_country.country_id;
  countryName.value = _model.userprofileDetails.value.user_country.country_name;
  createMerchantPopupShow.value = true;
}

function onCountrySelectMerchantEvent(args) {
  countryId.value = args.countryID;
  countryName.value = args.countryName;
}

function merchantCreate_clickHandler() {
  if (merchantNameInput.value !== "") {
    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.community_name = merchantNameInput.value;
    apiBodyObj.country_id = countryId.value;
    apiBodyObj.resller_id = resellerIDInput.value;

    _WebService.request("community/create", apiBodyObj).then(function (result) {
      gotCommunityCreateResult(result);
    }).catch(function (error) {
      console.log("Couldn't get data: " + error);
      showNetworkErrorMessage();
    });
  }
}


function gotCommunityCreateResult(result) {
  console.log("gotCommunityCreateResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    createMerchantPopupShow.value = false;

    tagEvents.emit("toastShow", {
      message: loc.value.merchant_creation_success
    });
    return false;
  } else {
    if (arr.error == "community_name_already_used") {
      tagEvents.emit("alertEvent", {
        type: "info",
        title: loc.value.error,
        message: loc.value.merchant_name_inuse
      });
    }
  }
}


function redirectToMerchantDetailPage(args) {
  console.log("redirectToMerchantDetailPage", JSON.stringify(args));
  busy.activate();
  var apiBodyObj = {};

  apiBodyObj.name = args.data.id;
  _WebService.request("community/searchNew", apiBodyObj).then(function (result) {
    gotSearchCommunityResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function gotSearchCommunityResult(result) {
  console.log("gotSearchCommunityResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr && arr.status == "success") {
    var resultArr = arr.result;
    if (resultArr.length !== 0) {
      var resultUser = resultArr[0];
      router.push("merchantScanMenuPage", { resultUser: resultUser, perspective: "user", datId: Math.random() });
    } else {
      tagEvents.emit("toastShow", { message: loc.value.data_not_found });
    }
  } else {
    tagEvents.emit("toastShow", { message: loc.value.data_not_found });
  }
}

module.exports = {
  onViewActivate: onViewActivate,

  merchantSearchInput: merchantSearchInput,
  showLeaveConfirmationPopup: showLeaveConfirmationPopup,
  merchantsDataList: merchantsDataList,
  createMerchantPopupShow: createMerchantPopupShow,
  merchantNameInput: merchantNameInput,
  countryId: countryId,
  countryName: countryName,
  resellerIDInput: resellerIDInput,

  searchHandler: searchHandler,
  leaveClickHandler: leaveClickHandler,
  joinClickHandler: joinClickHandler,
  leaveCommunityHandler: leaveCommunityHandler,
  createMerchantClickHandler: createMerchantClickHandler,
  onCountrySelectMerchantEvent: onCountrySelectMerchantEvent,
  merchantCreate_clickHandler: merchantCreate_clickHandler,
  redirectToMerchantDetailPage: redirectToMerchantDetailPage
};
