var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var rolesListData = Observable();
var createRolePopupShow = Observable(false);
var changeOwnerPopupShow = Observable(false);
var ownerID = Observable("");
var roleTitle = Observable("");
var roleTypeItems = Observable({ index: 0, name: loc.value.staff, value: "staff" }, { index: 1, name: loc.value.member, value: "member" });
var roleTypeIndex = Observable(0);
var roleType = "staff";
var roleId = 0;
var nameInput = Observable("");
var ownerIdInput = Observable("");
var pinInput = Observable("");
var cancelOrDelete = Observable("");

function onViewActivate() {
    clearChangeOwnerVals();
    rolesListLoad();
}

function rolesListLoad() {
    rolesListData.clear();
    busy.activate();

    _WebService.request("role/list").then(function (result) {
        gotRolesListResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotRolesListResult(result) {
    console.log("gotRolesListResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            rolesListData.addAll(resultArr);
        }
    }
}

function rolesClickHandle(args) {
    var type = args.data.role_type;
    console.log("rolesClickHandle" + type);

    if (type == "owner") {
        ownerID.value = _model.communityprofileDetails.value.community_owner.id;
        changeOwnerPopupShow.value = true;
    } else {
        roleTitle.value = "UPDATE ROLE";
        if (type == "staff") {
            roleTypeIndex.value = 0;
        }
        else if (type == "member") {
            roleTypeIndex.value = 1;
        }
        roleType = args.data.role_type;
        roleId = args.data.id;
        nameInput.value = args.data.role_name;
        cancelOrDelete.value = "DELETE";
        createRolePopupShow.value = true;
    }
}

function roleAddClickHandle() {
    clearVals();
    roleTitle.value = "CREATE ROLE";
    roleTypeIndex.value = 0;
    cancelOrDelete.value = "CANCEL";
    createRolePopupShow.value = true;
}

function onRoleTypeChangeEvent(args) {
    var selectedItem = roleTypeItems.getAt(args.selectedIndex);
    roleType = selectedItem.value;
}

function submitButton_clickHandler() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.role_type = roleType;
    apiBodyObj.role_name = nameInput.value;
    apiBodyObj.role_default = "0";
    var path;
    if (roleId == 0)
        path = "role/create";
    else
        path = "role/edit/" + roleId;
    _WebService.request(path, apiBodyObj).then(function (result) {
        if (result.status == "success") {
            console.log("submitSuccess");
            rolesListLoad();
            clearVals();
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        busy.deactivate();
        showNetworkErrorMessage();
        clearVals();
    });
}

function cancelHandler() {
    if (roleId == 0)
        clearVals();
    else
        deleteRole();
}

function deleteRole() {
    busy.activate();

    _WebService.request("role/delete/" + roleId).then(function (result) {
        deleteRoleResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function deleteRoleResult(result) {
    console.log("deleteRoleResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        console.log("deleteRoleSuccess");
        rolesListLoad();
        clearVals();
    }
}

function clearVals() {
    roleTypeIndex.value = 0;
    nameInput.value = "";
    createRolePopupShow.value = false;
    roleId = 0;
}




function changeOwnerButton_clickHandler() {
    if (pinInput.length < 4) {
        tagEvents.emit("toastShow", { message: loc.value.enter_pin });
        return;
    }
    if (ownerIdInput.value == "") {
        tagEvents.emit("toastShow", { message: loc.value.enter_new_owner_id });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.user_id = ownerIdInput.value;
    apiBodyObj.user_pin = pinInput.value;

    _WebService.request("community/transferowner", apiBodyObj).then(function (result) {
        changeOwnerResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        busy.deactivate();
        showNetworkErrorMessage();
    });
}

function changeOwnerResult(result) {
    console.log("changeOwnerResult");
    busy.deactivate();

    var res = result;
    console.log(JSON.stringify(res));
    if (res.status == "success") {
        console.log("changeOwnerResultSuccess");
        tagEvents.emit("alertEvent", { type: "info", title: loc.value.change_owner, message: loc.value.owner_changed });
    } else {
        if (res.error == "permission_denied") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.permission_denied_change_owner });
        }
        else if (res.error == "invalid_user") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.invalid_user });
        }
        else if (res.error == "Wrong_User_Pin_Entered") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.Wrong_User_Pin_Entered });
        }
        else if (res.error == "already_community_owner") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.already_community_owner });
        }
    }
    clearChangeOwnerVals();
}

function cancelChangeOwnerHandler() {
    clearChangeOwnerVals();
}

function clearChangeOwnerVals() {
    changeOwnerPopupShow.value = false;
    ownerIdInput.value = "";
    pinInput.value = "";
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivate: onViewActivate,
    rolesListData: rolesListData,
    createRolePopupShow: createRolePopupShow,
    changeOwnerPopupShow: changeOwnerPopupShow,
    ownerID: ownerID,
    roleTitle: roleTitle,
    roleTypeItems: roleTypeItems,
    roleTypeIndex: roleTypeIndex,
    roleAddClickHandle: roleAddClickHandle,
    onRoleTypeChangeEvent: onRoleTypeChangeEvent,
    submitButton_clickHandler: submitButton_clickHandler,
    cancelHandler: cancelHandler,
    changeOwnerButton_clickHandler: changeOwnerButton_clickHandler,
    cancelChangeOwnerHandler: cancelChangeOwnerHandler,
    nameInput: nameInput,
    rolesClickHandle: rolesClickHandle,

    ownerIdInput: ownerIdInput,
    pinInput: pinInput,
    cancelOrDelete: cancelOrDelete,
}