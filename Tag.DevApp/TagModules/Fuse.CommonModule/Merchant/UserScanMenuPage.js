var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var TransferError = require('Utils/TransferError');
var moment = require('Utils/moment');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var idUserScaned = "";

var urlUserImageScaned = Observable();
var nameUserScaned = Observable();
var ratingUserScaned = Observable();

var addContactPossibleStat = Observable(false);
var removeContactPossibleStat = Observable(false);

var nowRating = Observable(0);
var rateuserPopupShow = Observable(false);
var requestwalletId;
var requestCurrencyCode = Observable();
var requestAmountInput = Observable("");
var requestNotesTxt = Observable("");
var requestsCreatePopupShow = Observable(false);

var creditChargePopupShow = Observable(false);
var walletIdPay;
var currencyCodePay = Observable();
var amountInputPay = Observable("");
var notesTxtPay = Observable("");
var defaultWalletId = "";
var defaultWalletCode = "";
var resultUserdetails;

var identifierPay = false;
var identifierValue = "";

var merchantServices = Observable(false);
var payreceiptPopupShow = Observable(false);
var scanedIdDisplay = Observable(false);
var receiptAmount = Observable("");
var receiptTransactionID = Observable("");
var receiptDate = Observable("");

this.Parameter.onValueChanged(module, function (param) {
  if (_model.activePerspectiveType == "user") {
    merchantServices.value = false;
  } else {
    merchantServices.value = true;
  }
  
  var resultUser = param.resultUser;
  resultUserdetails = resultUser;
  nowRating.value = 0;

  idUserScaned = resultUser.id;
  scanedIdDisplay.value = idUserScaned;

  urlUserImageScaned.value = _model.userImageUrl + resultUser.id + "?kycImage=0";

  nameUserScaned.value = resultUser.name;
  ratingUserScaned.value = resultUser.rating;

  addContactPossibleStat.value = false;
  removeContactPossibleStat.value = false;

  identifierPay = false;
  identifierValue = "";
  if (param.identifier != null) {
    identifierPay = true;
    identifierValue = param.identifier;
  }

  if (_model.activePerspectiveType == "user") {
    loadUserProfile(resultUser.id);
  }

  defaultWalletLoad();

})


function defaultWalletLoad() {
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.new_call = 1;

  _WebService.request("user/DefaultWallet", apiBodyObj).then(function (result) {
    gotDefaultWalletResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotDefaultWalletResult(result) {
  console.log("gotDefaultWalletResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    var resultObj = arr.result;
    if (resultObj.wallet_id) {
      defaultWalletId = resultObj.wallet_id;
      defaultWalletCode = resultObj.currency_code;
    }
  }

}


function loadUserProfile(useridCheck) {
  console.log("loadUserProfile");
  busy.activate();

  _WebService.request("user/profile/" + useridCheck).then(function (result) {
    loadUserProfileResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function loadUserProfileResult(result) {
  console.log("loadUserProfileResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultObj = arr.result;

    addContactPossibleStat.value = false;
    removeContactPossibleStat.value = false;

    if (resultObj.contact_status != "self") {
      if (resultObj.contact_status == "contact") {
        removeContactPossibleStat.value = true;
      } else {
        addContactPossibleStat.value = true;
      }
    }

  }
}



function payUser_clickHandler() {
  walletIdPay = "";
  currencyCodePay.value = loc.value.select;
  amountInputPay.value = "";
  notesTxtPay.value = "";

  if (defaultWalletId != "") {
    walletIdPay = defaultWalletId;
    currencyCodePay.value = defaultWalletCode;
  }

  creditChargePopupShow.value = true;

}

function walletOptionChangedPayEvent(args) {
  walletIdPay = args.walletId;
  currencyCodePay.value = args.currencyCode;
}

function creditChargeSendHandler() {
  var amountValue = amountInputPay.value;

  if (!Validator.amount(amountValue)) {
    tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
    return;
  }

  if (walletIdPay === "") {
    tagEvents.emit("toastShow", { message: loc.value.select_wallet });
    return;
  }

  busy.activate();
  var apiBodyObj = {};
  apiBodyObj.amount = amountInputPay.value;
  apiBodyObj.from_wallet_id = walletIdPay;
  apiBodyObj.to_wallet_id = walletIdPay;
  apiBodyObj.narration = notesTxtPay.value;


  if (identifierPay) {
    apiBodyObj.identifier = identifierValue;
  } else {
    apiBodyObj.to_id = idUserScaned;
    apiBodyObj.to_type = "user";
  }

  _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
    gotWalletTransferResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotWalletTransferResult(result) {
  console.log("gotWalletTransferResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    creditChargePopupShow.value = false;
    var resultObj = arr.result;
    
    receiptAmount.value = resultObj.transfer_currency + " " + resultObj.transfer_amount;
    receiptTransactionID.value = resultObj.transaction_id;
    receiptDate.value = moment(resultObj.transfer_date, "YYYY-MM-DD hh:mm:ss").format('D MMMM YYYY h:mm A');

    payreceiptPopupShow.value = true;
    // tagEvents.emit("alertEvent", { type: "info", title: "PAY", message: resultObj.transfer_amount + " " + resultObj.transfer_currency + " : Credited to user" });
  } else {
    TransferError.errorHandle(arr);
  }

}

function requestSendClickHandler() {
  console.log("requestsCreateClickHandler ")
  requestCurrencyCode.value = "PHP";
  requestwalletId = 1;
  requestAmountInput.value = "";
  requestNotesTxt.value = "";

  requestsCreatePopupShow.value = true;
}

function walletOptionChangedEvent(args) {
  requestwalletId = args.walletId;
  requestCurrencyCode.value = args.currencyCode;
}

function requestsCreateHandler() {
  var amountValue = requestAmountInput.value;

  if (!Validator.amount(amountValue)) {
    tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
    return;
  }

  if (requestwalletId === "") {
    tagEvents.emit("toastShow", { message: loc.value.select_wallet });
    return;
  }

  busy.activate();
  var apiBodyObj = {};
  apiBodyObj.to_user = idUserScaned;
  apiBodyObj.to_type = "user";

  apiBodyObj.amount = amountValue;
  apiBodyObj.wallet = requestwalletId;
  apiBodyObj.remarks = requestNotesTxt.value;
  // can_accept_bitcoin:1

  _WebService.request("Credit/Requestfunds", apiBodyObj).then(function (result) {
    gotRequestfundsCreateResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotRequestfundsCreateResult(result) {
  console.log("gotMyRequestsResult");
  busy.deactivate();
  requestsCreatePopupShow.value = false;

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    tagEvents.emit("toastShow", { message: "Fund request send" });
  } else {
    if (arr.error == "invalid_user_can_not_lend_from_yourself") {
      tagEvents.emit("alertEvent", { type: "info", title: "Request failed", message: "Can not lend from yourself." });
    } else if (arr.error == "invalid_user") {
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "The ID or email that you entered is not valid." });
    } else {
      tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
  }
}


function ratingChangeClickHandler() {
  console.log("Rating Click Handler");
  rateuserPopupShow.value = true;
}

function rateCancelClickHandler() {
  rateuserPopupShow.value = false;
}

function rateUserClickHandler() {
  busy.activate();

  var apiBodyObj = {};

  var newRating = nowRating.value;
  newRating = Math.round(newRating);;
  apiBodyObj.rating = newRating;

  apiBodyObj.to_id = idUserScaned;
  apiBodyObj.to_type = "user";

  _WebService.request("ratings/addratings", apiBodyObj).then(function (result) {
    gotAddRatingResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotAddRatingResult(result) {
  console.log("gotAddRatingResult")
  busy.deactivate();
  rateuserPopupShow.value = false;

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    tagEvents.emit("toastShow", { message: loc.value.rate_success_message });
  } else {
    tagEvents.emit("toastShow", { message: loc.value.error_occurred });
  }

}

function contactAddClickHandler() {
  console.log("contactAddClickHandler");
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.userid = idUserScaned;

  _WebService.request("contact/add", apiBodyObj).then(function (result) {
    gotContactAddResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function gotContactAddResult(result) {
  console.log("gotContactAddResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    addContactPossibleStat.value = false;
    removeContactPossibleStat.value = true;

    tagEvents.emit("toastShow", { message: loc.value.contact_add_success });
  } else {
    if (arr.error == "contact_already_exists") {
      tagEvents.emit("toastShow", { message: "Already in friends list" });
    } else {
      tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
  }

}

function contactRemoveClickHandler() {
  console.log("contactRemoveClickHandler");
  busy.activate();

  _WebService.request("contact/delete/" + idUserScaned).then(function (result) {
    gotContactRemoveResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function gotContactRemoveResult(result) {
  console.log("gotContactRemoveResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    addContactPossibleStat.value = true;
    removeContactPossibleStat.value = false;

    tagEvents.emit("toastShow", { message: loc.value.contact_remove_success });
  } else {
    tagEvents.emit("toastShow", { message: loc.value.error_occurred });
  }

}

function chatClickHandler() {
  console.log("chat click handler");
  router.push("userFriendsChatPage", { resultUser: resultUserdetails, source: "userdetails", datId: Math.random() });
}

function popupCloseHandle() {
  billPayPopupShow.value = false;
  creditChargePopupShow.value = false;
  rateuserPopupShow.value = false;
  payreceiptPopupShow.value = false;
}

function showNetworkErrorMessage() {
  busy.deactivate();
  tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
  urlUserImageScaned: urlUserImageScaned,
  nameUserScaned: nameUserScaned,
  ratingUserScaned: ratingUserScaned,
  addContactPossibleStat: addContactPossibleStat,
  removeContactPossibleStat: removeContactPossibleStat,

  payUser_clickHandler: payUser_clickHandler,
  requestSendClickHandler: requestSendClickHandler,
  requestsCreatePopupShow: requestsCreatePopupShow,
  walletOptionChangedEvent: walletOptionChangedEvent,
  requestCurrencyCode: requestCurrencyCode,
  requestAmountInput: requestAmountInput,
  requestNotesTxt: requestNotesTxt,
  requestsCreateHandler: requestsCreateHandler,

  ratingChangeClickHandler: ratingChangeClickHandler,
  rateuserPopupShow: rateuserPopupShow,
  nowRating: nowRating,
  rateUserClickHandler: rateUserClickHandler,
  rateCancelClickHandler: rateCancelClickHandler,

  contactAddClickHandler: contactAddClickHandler,
  contactRemoveClickHandler: contactRemoveClickHandler,

  creditChargePopupShow: creditChargePopupShow,
  currencyCodePay: currencyCodePay,
  amountInputPay: amountInputPay,
  notesTxtPay: notesTxtPay,
  walletOptionChangedPayEvent: walletOptionChangedPayEvent,
  creditChargeSendHandler: creditChargeSendHandler,
  chatClickHandler: chatClickHandler,

  popupCloseHandle: popupCloseHandle,
  merchantServices: merchantServices,
  payreceiptPopupShow: payreceiptPopupShow,
  scanedIdDisplay: scanedIdDisplay,
  receiptAmount: receiptAmount,
  receiptTransactionID: receiptTransactionID,
  receiptDate: receiptDate,

}