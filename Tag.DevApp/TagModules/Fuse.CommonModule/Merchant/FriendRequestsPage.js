var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var requestsListData = Observable();
var inviteDetailPopupShow = Observable(false);

function onViewActivate() {
    console.log("requestsListLoad");
    requestsListLoad();
}

function requestsListLoad() {
    requestsListData.clear();
    busy.activate();
    _WebService.request("contact/getcontactrequests").then(function (result) {
        gotRequestsListResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotRequestsListResult(result) {
    console.log("gotRequestsListResult");
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;


        if (resultArr !== null) {
            var resultArrProcessed = [];
            for (var i = 0; i < resultArr.length; i++) {
                var obj = {};
                obj = resultArr[i];
                obj.imgPath = _model.userImageUrl + resultArr[i].contact_id + "?kycImage=0";

                resultArrProcessed.push(obj);
            }

            requestsListData.addAll(resultArrProcessed);
            tagEvents.emit("contactNoRefresh", { number: resultArr.length });
        } else {
            tagEvents.emit("contactNoRefresh", { number: 0 });
        }
    }
}



var contactIdSelected;
var requestsSelectName = Observable();
var requestsSelectID = Observable();
var requestsSelectImage = Observable();
var requestsSelectRating = Observable();
var requestsSelectDate = Observable();

function friendRequestsClickHandle(args) {
    console.log("friendRequestsClickHandle");
    console.log(JSON.stringify(args.data));

    contactIdSelected = args.data.contact_id;
    requestsSelectName.value = args.data.contact_firstname + " " + args.data.contact_lastname;
    requestsSelectID.value = args.data.contact_id;
    requestsSelectImage.value = _model.userImageUrl + args.data.contact_id + "?kycImage=0";
    requestsSelectRating.value = args.data.rating;
    requestsSelectDate.value = args.data.contact_date;

    inviteDetailPopupShow.value = true;
}

function acceptContactRequestClickHandler() {
    console.log("acceptContactRequestClickHandler");
    busy.activate();

    _WebService.request("contact/add/" + contactIdSelected).then(function (result) {
        gotAcceptAddContactResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotAcceptAddContactResult(result) {
    console.log("gotAcceptAddContactResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        inviteDetailPopupShow.value = false;

        requestsListLoad();
    } else {
        if (arr.error == "contact_already_exists") {
            tagEvents.emit("toastShow", { message: "Contact already exists" });
            // } else if (arr.error == "user_blocked") {
            // tagEvents.emit("toastShow", { message: "User blocked" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function deleteContactRequestClickHandler() {
    console.log("deleteContactRequestClickHandler");
    busy.activate();

    _WebService.request("contact/delete/" + contactIdSelected).then(function (result) {
        gotAcceptDeleteContactResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotAcceptDeleteContactResult(result) {
    console.log("gotAcceptDeleteContactResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {
        inviteDetailPopupShow.value = false;

        requestsListLoad();
    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivate: onViewActivate,
    requestsListData: requestsListData,
    acceptContactRequestClickHandler: acceptContactRequestClickHandler,
    deleteContactRequestClickHandler: deleteContactRequestClickHandler,
    friendRequestsClickHandle: friendRequestsClickHandle,
    inviteDetailPopupShow: inviteDetailPopupShow,
    requestsSelectName: requestsSelectName,
    requestsSelectID: requestsSelectID,
    requestsSelectImage: requestsSelectImage,
    requestsSelectRating: requestsSelectRating,
    requestsSelectDate: requestsSelectDate,
}