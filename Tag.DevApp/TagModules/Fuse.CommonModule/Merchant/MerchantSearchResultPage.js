var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var TransferError = require('Utils/TransferError');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var dataLoadingComplete = Observable(false);
var chargingPossible = Observable(false);

var fullname = Observable();
var imageurl = Observable();
var rating = Observable();
var userid = Observable();
var canChat = Observable();

var roleDisplay = Observable();
var rolesPopupShow = Observable(false);
var roleListItems = Observable();
var roleListItemsArr = [];
var roleEditableStatus = Observable(false);

var rateuserPopupShow = Observable(false);
var nowRating = Observable(0);
var defaultWalletId = "";
var defaultWalletCode = "";

var addMemberPossibleStat = Observable(false);
var removeMemberPossibleStat = Observable(false);
var requestwalletId;
var requestCurrencyCode = Observable();
var requestAmountInput = Observable("");
var requestNotesTxt = Observable("");
var requestsCreatePopupShow = Observable(false);
var stampListItems = Observable();
var stampStatus = Observable(false);
var stampPopupShow = Observable(false);
var isRedeemable = Observable(false);

var identifierPay = false;
var identifierValue = "";

this.Parameter.onValueChanged(module, function (param) {
  // console.log(JSON.stringify(param));
  dataLoadingComplete.value = false;
  chargingPossible.value = false;
  defaultWalletId = "";
  defaultWalletCode = "";

  fullname.value = param.username;
  imageurl.value = _model.userImageUrl + param.userID;
  userid.value = param.userID;
  canChat.value = param.canChat
  roleDisplay.value = param.role;

  addMemberPossibleStat.value = false;
  removeMemberPossibleStat.value = false;

  identifierPay = false;
  identifierValue = "";
  if (param.identifier != null) {
    identifierPay = true;
    identifierValue = param.identifier;
  }

  roleEditableStatus.value = false;
  if (param.role_status == "approved") {
    if (param.role_type != "owner") {
      roleEditableStatus.value = true;
      removeMemberPossibleStat.value = true;
    }
  } else {
    addMemberPossibleStat.value = true;
  }

  rating.value = param.rating;
  defaultWalletLoad();

})

function defaultWalletLoad() {
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.new_call = 1;

  _WebService.request("user/DefaultWallet", apiBodyObj).then(function (result) {
    gotDefaultWalletResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotDefaultWalletResult(result) {
  console.log("gotDefaultWalletResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    var resultObj = arr.result;
    if (resultObj.wallet_id) {
      defaultWalletId = resultObj.wallet_id;
      defaultWalletCode = resultObj.currency_code;
    }
  }

  checkCommunityisVerified();
}

function checkCommunityisVerified() {
  busy.activate();

  _WebService.request("verification/GetLevel").then(function (result) {
    gotMerchantVerificationResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotMerchantVerificationResult(result) {
  console.log("gotMerchantVerificationResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultObj = arr.result;
    var verifivationlevel = resultObj.verification_level;

    if (verifivationlevel == 0) {
      chargingPossible.value = false;
    } else {
      chargingPossible.value = true;
    }
  }

  getRoles();
}

function getRoles() {
  busy.activate();
  roleListItemsArr = [];
  roleListItems.clear();

  _WebService.request("role/list").then(function (result) {
    getRolesResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
}

function getRolesResult(result) {
  console.log("getRolesResult");
  busy.deactivate();
  dataLoadingComplete.value = true;

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultArr = arr.result;
    if (resultArr !== null) {
      roleListItemsArr = resultArr;
      roleListItems.addAll(resultArr);
    }
  }

  ownedStampsLoad();

}

function roleClickHandler() {
  rolesPopupShow.value = true;
}

function onRolesItemSelected(args) {
  console.log("onRolesItemSelected");
  busy.activate();

  var apiBodyObj = {};
  apiBodyObj.role_id = args.data.id;

  _WebService.request("community/adduser/" + userid.value, apiBodyObj).then(function (result) {
    onRoleChangeResult(result, args.data.role_name);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
  rolesPopupShow.value = false;
}

function onRoleChangeResult(result, name) {
  console.log("onRoleChangeResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultArr = arr.result;

    if (resultArr.hasOwnProperty("status") && resultArr.status == "approved") {
      roleDisplay.value = name;
    }
  } else {
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Failed to Change the Role" });
  }
}

function ratingChangeClickHandler() {
  console.log("Rating Click Handler");
  rateuserPopupShow.value = true;
}

function rateCancelClickHandler() {
  rateuserPopupShow.value = false;
}

function rateUserClickHandler() {
  busy.activate();

  var apiBodyObj = {};

  var newRating = nowRating.value;
  newRating = Math.round(newRating);;
  apiBodyObj.rating = newRating;

  apiBodyObj.to_id = userid.value;
  apiBodyObj.to_type = "user";

  _WebService.request("ratings/addratings", apiBodyObj).then(function (result) {
    gotAddRatingResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotAddRatingResult(result) {
  console.log("gotAddRatingResult")
  busy.deactivate();
  rateuserPopupShow.value = false;

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    tagEvents.emit("toastShow", { message: loc.value.rate_success_message });
  } else {
    tagEvents.emit("toastShow", { message: loc.value.error_occurred });
  }

}




var creditChargePopupShow = Observable(false);
var pinInputPopupShow = Observable(false);
var creditChargeTitle = Observable();

var walletId;
var currencyCode = Observable();
var amountInput = Observable("");
var notesTxt = Observable("");

var nowSendingCredit = true;

function creditPayShowHandler() {
  nowSendingCredit = true;
  creditChargeTitle.value = "CREDIT";
  resetCreditChargeInput();
}

function chargePayShowHandler() {
  nowSendingCredit = false;
  creditChargeTitle.value = "CHARGE";
  resetCreditChargeInput();
}

function resetCreditChargeInput() {
  walletId = "";
  currencyCode.value = loc.value.select;
  amountInput.value = "";
  notesTxt.value = "";

  if (defaultWalletId != "") {
    walletId = defaultWalletId;
    currencyCode.value = defaultWalletCode;
  }

  creditChargePopupShow.value = true;
}

function walletOptionChangedEvent(args) {
  walletId = args.walletId;
  currencyCode.value = args.currencyCode;
}

function creditChargeSendHandler() {
  var amountValue = amountInput.value;

  if (!Validator.amount(amountValue)) {
    tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
    return;
  }

  if (walletId === "") {
    tagEvents.emit("toastShow", { message: loc.value.select_wallet });
    return;
  }

  if (nowSendingCredit) {
    creditChargeSendProcess();
  } else {
    pinInputPopupShow.value = true;
  }

}

function onPinEnteredEvent(args) {
  console.log(args.pin)

  pinInputPopupShow.value = false;
  creditChargeSendProcess(args.pin);
}

function creditChargeSendProcess(pin) {
  busy.activate();
  var apiBodyObj = {};
  apiBodyObj.amount = amountInput.value;
  apiBodyObj.to_wallet_id = walletId;
  apiBodyObj.from_wallet_id = walletId;
  apiBodyObj.narration = notesTxt.value;

  if (nowSendingCredit) {
    if (identifierPay) {
      apiBodyObj.identifier = identifierValue;
    } else {
      apiBodyObj.to_id = userid.value;
      apiBodyObj.to_type = "user";
    }
  } else {
    //This is the case for charging from a user
    apiBodyObj.charging = "true";
    apiBodyObj.pin = pin;//PiN Number is using only for charging a user
    apiBodyObj.from_id = userid.value;
    apiBodyObj.from_type = "user";
  }

  _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
    gotWalletTransferResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotWalletTransferResult(result) {
  console.log("gotWalletTransferResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    creditChargePopupShow.value = false;
    var resultObj = arr.result;

    if (nowSendingCredit) {
      tagEvents.emit("alertEvent", { type: "info", title: "CREDIT", message: resultObj.transfer_amount + " " + resultObj.transfer_currency + " : Credited to user" });
    } else {
      tagEvents.emit("alertEvent", { type: "info", title: "CHARGE", message: resultObj.transfer_amount + " " + resultObj.transfer_currency + " : Charged from user" });
    }
  } else {
    TransferError.errorHandle(arr);
  }

}

function openChatHandler() {
  console.log("openChatHandler clicked")
  router.push("chatTabDiscover");
}

function memberAddClickHandler() {
  console.log("memberAddClickHandler");
  busy.activate();

  _WebService.request("community/adduser/" + userid.value).then(function (result) {
    gotAddmemberResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotAddmemberResult(result) {
  busy.deactivate();
  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var result = arr.result;

    if (result.status == "pending") {
      tagEvents.emit("toastShow", { message: "Request is pending" });
    } else if (result.status == "approved") {
      addMemberPossibleStat.value = false;
      removeMemberPossibleStat.value = true;
      roleEditableStatus.value = true;
      setRoleNameDisplay(result.role_id);

      tagEvents.emit("toastShow", { message: "User added" });
    }
  } else if (arr.status == "failed") {
    var error_response = arr.error;
    if (error_response == "only_one_owner_per_community") {
      tagEvents.emit("toastShow", { message: loc.value.oneownerper_community_message });
    } else if (error_response == "same_role_id") {
      tagEvents.emit("toastShow", { message: "This role is already assigned" });
    } else if (error_response == "request_pending") {
      tagEvents.emit("toastShow", { message: "Request is pending" });
    } else if (error_response == "blocked_by_user") {
      tagEvents.emit("toastShow", { message: "Blocked by user" });
    } else {
      tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
    //{"error":"","status":"failed"}

  }
}

function setRoleNameDisplay(role_id) {
  for (var i = 0; i < roleListItemsArr.length; i++) {
    if (roleListItemsArr[i].id == role_id) {
      roleDisplay.value = roleListItemsArr[i].role_name;
    }
  }
}

function memberRemoveClickHandler() {
  console.log("memberRemoveClickHandler");
  busy.activate();

  _WebService.request("community/removeuser/" + userid.value).then(function (result) {
    gotRemovememberResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotRemovememberResult(result) {
  busy.deactivate();
  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    addMemberPossibleStat.value = true;
    removeMemberPossibleStat.value = false;
    roleEditableStatus.value = false;
    roleDisplay.value = "Non Member";

    tagEvents.emit("toastShow", { message: "User removed" });
  }
}

function requestSendClickHandler() {
  console.log("requestsCreateClickHandler ")

  if (defaultWalletId != "") {
    requestwalletId = defaultWalletId;
    requestCurrencyCode.value = defaultWalletCode;
  } else {
    requestwalletId = "";
    requestCurrencyCode.value = loc.value.select;
  }

  requestAmountInput.value = "";
  requestNotesTxt.value = "";

  requestsCreatePopupShow.value = true;
}

function walletOptionRequestChangedEvent(args) {
  requestwalletId = args.walletId;
  requestCurrencyCode.value = args.currencyCode;
}

function requestsCreateHandler() {
  var amountValue = requestAmountInput.value;

  if (!Validator.amount(amountValue)) {
    tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
    return;
  }

  if (requestwalletId === "") {
    tagEvents.emit("toastShow", { message: loc.value.select_wallet });
    return;
  }


  busy.activate();
  var apiBodyObj = {};
  apiBodyObj.to_user = userid.value;
  apiBodyObj.to_type = "user";

  apiBodyObj.amount = amountValue;
  apiBodyObj.wallet = requestwalletId;
  apiBodyObj.remarks = requestNotesTxt.value;

  _WebService.request("Credit/Requestfunds", apiBodyObj).then(function (result) {
    gotRequestfundsCreateResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotRequestfundsCreateResult(result) {
  console.log("gotMyRequestsResult");
  busy.deactivate();
  requestsCreatePopupShow.value = false;

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    tagEvents.emit("toastShow", { message: "Fund request send" });
  } else {
    if (arr.error == "invalid_user_can_not_lend_from_yourself") {
      tagEvents.emit("alertEvent", { type: "info", title: "Request failed", message: "Can not lend from yourself." });
    } else if (arr.error == "invalid_user") {
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "The ID or email that you entered is not valid." });
    } else {
      tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
  }
}



//stamps-----------

function ownedStampsLoad() {
  busy.activate();
  stampListItems.clear();
  var apiBodyObj = {};
  apiBodyObj.user_id = userid.value;

  _WebService.request("stamp/GetuserStamp", apiBodyObj).then(function (result) {
    gotOwnedStampsLoadResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function stampClickHandler() {
  console.log("stampClickHandler");
  stampPopupShow.value = true;
}

function gotOwnedStampsLoadResult(result) {
  console.log("gotOwnedStampsLoadResult")
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr))

  if (arr.status == "success") {
    var resultArr = arr.result.list;
    if (resultArr.length > 0) {
      stampStatus.value = true;
      stampListItems.addAll(resultArr);
    } else {
      stampStatus.value = false;
    }
  }
}


function addStampClickHandler(args) {
  //console.log("addStampClickHandler"+args.data.stamp._id.$id)

  busy.activate();

  var apiBodyObj = {};
  //apiBodyObj.stamp_id = args.data.stamp._id.$id;
  apiBodyObj.stamp_id = args.data.id.$id;

  _WebService.request("stamp/addStamp", apiBodyObj).then(function (result) {
    onAddStampResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
  stampPopupShow.value = false;
}

function onAddStampResult(result) {
  console.log("onAddStampResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultArr = arr.result;
    tagEvents.emit("toastShow", { message: "Successfully added Stamp to Stamp Book" });
    // if (resultArr.hasOwnProperty("status") && resultArr.status == "approved") {
    //   roleDisplay.value = name;
    // }
  } else {
    console.log(arr.error);
    if (arr.error == "Add stamp after some time")
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "You can only add one stamp per day. Try again later" });
    else
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Failed to Add Stamp" });
  }
}

function redeemClickHandler(args) {
  busy.activate();

  var apiBodyObj = {};
  //apiBodyObj.stamp_id = args.data.stamp._id.$id;
  apiBodyObj.stamp_id = args.data.id.$id;

  _WebService.request("stamp/Redeem", apiBodyObj).then(function (result) {
    redeemStampResult(result);
  }).catch(function (error) {
    showNetworkErrorMessage();
  });
  stampPopupShow.value = false;
}

function redeemStampResult(result) {
  console.log("redeemStampResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));
  router.goBack();
  if (arr.status == "success") {
    var resultArr = arr.result;
    tagEvents.emit("toastShow", { message: "Successfully redeemed the Stamp" });
    // if (resultArr.hasOwnProperty("status") && resultArr.status == "approved") {
    //   roleDisplay.value = name;
    // }
  } else {
    console.log(arr.error);
    if (arr.error == "Stamp already expired.")
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Stamp already expired." });
    else
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Failed to Redeem Stamp" });
  }
}

function showNetworkErrorMessage() {
  busy.deactivate();
  tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
  dataLoadingComplete: dataLoadingComplete,
  chargingPossible: chargingPossible,

  fullname: fullname,
  imageurl: imageurl,
  rating: rating,
  userid: userid,
  canChat: canChat,
  openChatHandler: openChatHandler,
  roleDisplay: roleDisplay,
  roleEditableStatus: roleEditableStatus,

  roleClickHandler: roleClickHandler,
  roleListItems: roleListItems,
  rolesPopupShow: rolesPopupShow,
  onRolesItemSelected: onRolesItemSelected,

  rateuserPopupShow: rateuserPopupShow,
  rateCancelClickHandler: rateCancelClickHandler,
  nowRating: nowRating,
  ratingChangeClickHandler: ratingChangeClickHandler,
  rateUserClickHandler: rateUserClickHandler,

  creditPayShowHandler: creditPayShowHandler,
  chargePayShowHandler: chargePayShowHandler,
  creditChargePopupShow: creditChargePopupShow,
  creditChargeTitle: creditChargeTitle,
  creditChargeSendHandler: creditChargeSendHandler,
  walletOptionChangedEvent: walletOptionChangedEvent,

  pinInputPopupShow: pinInputPopupShow,
  onPinEnteredEvent: onPinEnteredEvent,
  currencyCode: currencyCode,
  amountInput: amountInput,
  notesTxt: notesTxt,

  addMemberPossibleStat: addMemberPossibleStat,
  removeMemberPossibleStat: removeMemberPossibleStat,
  memberAddClickHandler: memberAddClickHandler,
  memberRemoveClickHandler: memberRemoveClickHandler,

  requestSendClickHandler: requestSendClickHandler,
  requestsCreatePopupShow: requestsCreatePopupShow,
  walletOptionRequestChangedEvent: walletOptionRequestChangedEvent,
  requestCurrencyCode: requestCurrencyCode,
  requestAmountInput: requestAmountInput,
  requestNotesTxt: requestNotesTxt,
  requestsCreateHandler: requestsCreateHandler,

  stampStatus: stampStatus,
  stampClickHandler: stampClickHandler,
  stampPopupShow: stampPopupShow,
  stampListItems: stampListItems,
  addStampClickHandler: addStampClickHandler,
  redeemClickHandler: redeemClickHandler,
}