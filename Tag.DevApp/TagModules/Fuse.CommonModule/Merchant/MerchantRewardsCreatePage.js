var Observable = require('FuseJS/Observable');
var Preferences = require('Utils/Preferences');


var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var rewardDetailsPopupShow = Observable(false);
var moneyreceiveCurrencyCode = Observable("Select Wallet");
var rewardOutCurrencyCode = Observable("Select Wallet");

var rewardGooutValue = Observable("");
var moneyreceiveInputValue = Observable("");
var rewardsDataList = Observable();
var editingMode = Observable(false);
var reward_id;//It is using for updation process
var rewardout_walletid;

var moneyreceive_walletid;

function onViewActivate() {
    console.log("onViewActivate")
    loadCreatedRewards();

}
function loadCreatedRewards() {
    rewardsDataList.clear();
    busy.activate();
    _WebService.request("reward/ListRule").then(function (result) {
        gotRewardsListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}
function gotRewardsListResult(result) {
    busy.deactivate();

    console.log("gotrewardsListResult");

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            rewardsDataList.addAll(resultArr);
        }

    }
}

function rewardsAddClickHandler() {
    console.log("rewards add clck handler");
    rewardDetailsPopupShow.value = true;
    editingMode.value = false;
}

function moneyreceivewalletOptionChangedEvent(args) {
    moneyreceive_walletid = args.walletId;
    moneyreceiveCurrencyCode.value = args.currencyCode;
    console.log("money wallet id" + moneyreceive_walletid);
    console.log("money currrencycode" + moneyreceiveCurrencyCode.value);
}

function saveRewardClickHandler() {
    console.log("save reward click handler");
    console.log("save reward-money receive" + moneyreceive_walletid);

    if (moneyreceiveInputValue.value === "") {
        tagEvents.emit("toastShow", { message: "Please Enter Valid Receive Input Value" });
        return;
    }
    if (rewardGooutValue.value === "") {
        tagEvents.emit("toastShow", { message: "Please Enter Valid Give Input Value" });
        return;
    }
    if (moneyreceive_walletid === undefined) {
        tagEvents.emit("toastShow", { message: "Please Select Valid Money Receive Wallet" });
        return;
    }
    if (rewardout_walletid === undefined) {
        tagEvents.emit("toastShow", { message: "Please Select Valid Give Wallet" });
        return;
    }
    if (moneyreceive_walletid == rewardout_walletid) {
        tagEvents.emit("toastShow", { message: "Please Select Different GIve and Receive Wallet!" });
    }
    console.log("Success");
    saveRewardProcess();
}

function saveRewardProcess() {

    busy.activate();

    var apiBodyObj = {};


    apiBodyObj.receive_wallet_id = moneyreceive_walletid;
    apiBodyObj.receive_amount = moneyreceiveInputValue.value
    apiBodyObj.reward_wallet_id = rewardout_walletid;
    apiBodyObj.reward_amount = rewardGooutValue.value;
    if (editingMode.value == false) {
        _WebService.request("reward/CreateRule", apiBodyObj).then(function (result) {
            gotCreateRewardResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        apiBodyObj.reward_rule_id = reward_id;
        _WebService.request("reward/UpdateRule", apiBodyObj).then(function (result) {
            gotUpdateRewardResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }
}

function gotCreateRewardResult(result) {
    console.log("gotCreaterewardResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));
    if (arr.status == "success") {

        tagEvents.emit("toastShow", { message: "Reward Created Successfully" });
        rewardDetailsPopupShow.value = false;
        loadCreatedRewards();

    } else {
        tagEvents.emit("toastShow", { message: "Reward Creation Failed !" });
    }

}

function gotUpdateRewardResult(result) {
    busy.deactivate();
    console.log("gotUpdateRewardResult")
    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        tagEvents.emit("toastShow", { message: "Reward updated successfully" });
    } else {
        tagEvents.emit("toastShow", { message: "Reward updating failed" });
    }
    rewardDetailsPopupShow.value = false;
    loadCreatedRewards();
}

function deleteRewardClickHandler() {
    console.log("delete reward click handler");
    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.reward_rule_id = reward_id;
    _WebService.request("reward/DeleteRule", apiBodyObj).then(function (result) {
        gotDeleteRewardResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotDeleteRewardResult(result) {
    busy.deactivate();
    console.log("gotDeleteRewardResult")
    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultArr = arr.result;
        tagEvents.emit("toastShow", { message: "Reward Deleted successfully" });
    } else {
        tagEvents.emit("toastShow", { message: "Reward deleting  failed" });
    }
    rewardDetailsPopupShow.value = false;
    loadCreatedRewards();
}

function rewardoutTokenCreateEvent() {
    rewardDetailsPopupShow.value = false;
    router.push("tokenCreatePage");

}

function rewardoutwalletOptionChangedEvent(args) {
    rewardOutCurrencyCode.value = args.currencyCode;
    rewardout_walletid = args.walletId;
    console.log("reward wallet id" + rewardout_walletid);
    console.log("reward currrencycode" + rewardOutCurrencyCode.value);

}

function rewardListItemClickHandler(args) {
    rewardDetailsPopupShow.value = true;
    editingMode.value = true;
    console.log("Reward Item" + JSON.stringify(args.data));
    rewardOutCurrencyCode.value = args.data.reward_wallet_currency_code;

    rewardout_walletid = args.data.reward_wallet_id;
    moneyreceiveInputValue.value = args.data.receive_amount;
    rewardGooutValue.value = args.data.reward_amount;
    moneyreceiveCurrencyCode.value = args.data.receive_wallet_currency_code;
    moneyreceive_walletid = args.data.receive_wallet_id;
    reward_id = args.data.id;
    console.log("Editing Mode Value" + editingMode.value);
}

module.exports = {
    onViewActivate: onViewActivate,
    rewardDetailsPopupShow: rewardDetailsPopupShow,
    rewardsAddClickHandler: rewardsAddClickHandler,

    moneyreceiveCurrencyCode: moneyreceiveCurrencyCode,
    rewardGooutValue: rewardGooutValue,
    moneyreceiveInputValue: moneyreceiveInputValue,
    saveRewardClickHandler: saveRewardClickHandler,
    moneyreceivewalletOptionChangedEvent: moneyreceivewalletOptionChangedEvent,
    rewardoutwalletOptionChangedEvent: rewardoutwalletOptionChangedEvent,
    rewardOutCurrencyCode: rewardOutCurrencyCode,
    rewardsDataList: rewardsDataList,
    rewardListItemClickHandler: rewardListItemClickHandler,
    editingMode: editingMode,
    deleteRewardClickHandler: deleteRewardClickHandler,

    rewardoutTokenCreateEvent:rewardoutTokenCreateEvent,

}