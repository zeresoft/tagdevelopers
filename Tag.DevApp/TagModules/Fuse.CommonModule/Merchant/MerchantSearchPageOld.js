var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');
var Validator = require('Utils/Validator');

var roleListItems = Observable();
var roleselectIndex = Observable();
var emailAddress = Observable("");
var merchantIDorName = Observable("");
var userSearchPopupShow = Observable();
var usersearchList = Observable();
var addusertocommunityPopupShow = Observable();
var addtocommunity_fullname = Observable();
var addtocommunity_userID = Observable();
var firstName = Observable("");
var lastName = Observable("");
var addUserToTagAndCommunityPopupShow = Observable(false);

var role_ID;
var role_name;


function loadRoles() {
    loadCategoryListCall();
}

function loadCategoryListCall() {
    console.log("loadRoles")
    roleListItems.clear();
    busy.activate();

    _WebService.request("role/list").then(function (result) {
        gotroleListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotroleListResult(result) {
    console.log("gotroleListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        roleListItems.addAll(resultArr);


        console.log("RoleIndexValue" + roleselectIndex.value);
        if (resultArr !== null) {

            for (var i = 0; i < resultArr.length; i++) {
                var obj = resultArr[i];

                role_name = obj.role_name;
                if (role_name == "Member") {
                    roleselectIndex.value = -1;//We need to set value to -1 before setting member index in dropdown.
                    roleselectIndex.value = i;

                    role_ID = obj.id;
                    console.log("RoleName" + role_name);
                }

            }
        }

    }
}

function onroleSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))
    role_ID = selectedItem.id;
    role_name = selectedItem.role_name;
    console.log("SELEction" + role_ID);
}

function searchClickHandler() {

    console.log("search");
    if (merchantIDorName.value == "") {
        tagEvents.emit("toastShow", { message: "Please enter a valid input" });
        return;
    }
    busy.activate();
    usersearchList.clear();

    var apiBodyObj = {};
    var searchID = merchantIDorName.value;
    var result = checkIDorNameOrEmailorMobile(searchID);

    console.log("Result" + result);
    if (result == "valid_email") {
        apiBodyObj.email = searchID;
        _WebService.request("user/profile", apiBodyObj).then(function (result) {
            gotUserProfileResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else if (result == "valid_mobilenumber") {
        apiBodyObj.mobile = searchID;
        _WebService.request("user/profile", apiBodyObj).then(function (result) {
            gotUserProfileResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    } else if (result == "valid_searchid") {
        apiBodyObj.id = searchID;
        _WebService.request("user/profile", apiBodyObj).then(function (result) {
            gotUserProfileResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else if (result == "valid_searchname") {
        apiBodyObj.name = searchID;
        _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
            gotSearchUserResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else if (result == "invalid_input") {
        tagEvents.emit("toastShow", { message: "Invalid input" });
    }
    //Initially We check is it a valid Number(valid number means user entered ID)


}
function checkIDorNameOrEmailorMobile(searchID) {
    var id_name_email_mobile = "";

    if (Validator.email(searchID)) {
        id_name_email_mobile = "valid_email";
        console.log("valid email");
    } else {
        var m = searchID.charAt(0)
        if (m == "0") {
            console.log("use entered mobile number");
            id_name_email_mobile = "valid_mobilenumber";
        } else {
            console.log("NNN" + searchID);
            if (isNaN(searchID)) {
                console.log("checking name");
                var length = searchID.length;
                if (searchID.length >= 3) {
                    console.log("valid search name");
                    id_name_email_mobile = "valid_searchname";
                } else {
                    console.log("please enter a valid input");
                    id_name_email_mobile = "invalid_input";
                }


            } else {
                console.log("valid search ID");
                id_name_email_mobile = "valid_searchid";

            }
        }
    }
    return id_name_email_mobile;
}

function gotUserProfileResult(result) {
    console.log("gotUserProfileResult");
    busy.deactivate();
    console.log(JSON.stringify(result));

    var arr = result;

    if (arr.status == "success") {
        userSearchSelectedHandler(arr.result);
    } else {
        tagEvents.emit("toastShow", { message: "No results found" });
    }
}

function userClickHandler(args) {
    userSearchPopupShow.value = false;
    console.log(JSON.stringify(args.data));

    userSearchSelectedHandler(args.data);
}

function userSearchSelectedHandler(resultObj) {
    console.log("userSearchSelectedHandler-------------");
    console.log(JSON.stringify(resultObj));
    var username = resultObj.user_firstname + " " + resultObj.user_lastname;

    var role_name = "Non Member";
    var role_status = "notapproved";
    var role_type = "notowner";
    if (resultObj.role.role_status == "approved") {
        role_name = resultObj.role.role_name;
        role_status = "approved";
        role_type = resultObj.role.role_type;
    }

    router.push("merchantSearchResultPage", { username: username, rating: resultObj.rating, role: role_name, role_status: role_status, role_type: role_type, userID: resultObj.id, datId: Math.random() });
}

function gotSearchUserResult(result) {
    console.log("ZZZ-usersearchresult" + JSON.stringify(result) + "NNN");
    busy.deactivate();
    var arr = result;
    if (arr.status == "success") {
        userSearchPopupShow.value = true;

        var resultArr = arr.result;
        var resultArrProcessed = [];
        for (var i = 0; i < resultArr.length; i++) {
            var obj = {};
            obj = resultArr[i];
            obj.imgPath = _model.userImageUrl + resultArr[i].id + "?kycImage=0";

            resultArrProcessed.push(obj);
        }

        usersearchList.addAll(resultArrProcessed);
    }
}



function addnewmemberClickHandler() {
    console.log("add new member click handler");
    if (!Validator.email(emailAddress.value)) {
        console.log("invalid email address");
        tagEvents.emit("toastShow", { message: loc.value.enter_valid_email });
        return;

    }
    if (role_ID == undefined) {
        console.log("invalid Role ID");
        tagEvents.emit("toastShow", { message: "Please select a role" });
        return;
    }
    console.log("RoleID" + role_ID);
    console.log("RoleName" + role_name);
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.email = emailAddress.value;
    _WebService.request("user/profile/", apiBodyObj).then(function (result) {
        gotAddmemberUserProfile(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotAddmemberUserProfile(result) {
    busy.deactivate();
    console.log("ZZZ-Addmembersearchresult" + JSON.stringify(result) + "NNN");

    var arr = result;
    if (arr.status == "success") {
        var resultArr = arr.result;
        addtocommunity_fullname.value = resultArr.user_firstname + " " + resultArr.user_lastname;
        addtocommunity_userID.value = resultArr.id;
        addusertocommunityPopupShow.value = true;
    } else {
        if(arr.error == "invalid_email") {
            showNewUserPopup();
        }
    }
}

function cancelnewmemberHandler() {
    addusertocommunityPopupShow.value = false;
}

function addnewmemberconfirmClickHandler() {
    busy.activate();
    addusertocommunityPopupShow.value = false;
    console.log("Add new member confirm click handler");
    var apiBodyObj = {};
    apiBodyObj.role_id = role_ID;
    console.log("Role ID" + role_ID);
    _WebService.request("community/adduser/" + addtocommunity_userID.value, apiBodyObj).then(function (result) {
        gotAddmemberResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotAddmemberResult(result) {
    busy.deactivate();
    console.log("ZZZ-AddmemberResponse" + JSON.stringify(result) + "NNN");
    var arr = result;
    if (arr.status == "success") {
        console.log("success");
        emailAddress.value = "";

        var result = arr.result;
        if (result.status == "pending") {
            tagEvents.emit("toastShow", { message: "Request is in pending" });
        } else if (result.status == "approved") {
            tagEvents.emit("toastShow", { message: "User added" });
        }
    } else if (arr.status == "failed") {
        var error_response = arr.error;
        if (error_response == "only_one_owner_per_community") {

            tagEvents.emit("toastShow", { message: loc.value.oneownerper_community_message });
        }
        else if (error_response == "same_role_id") {
            tagEvents.emit("toastShow", { message: "This role is already assigned!" });
        }

    }
}

function showNewUserPopup() {
    firstName.value = "";
    lastName.value = "";
    addUserToTagAndCommunityPopupShow.value = true;
}
function cancelNewUserHandler() {
    console.log("cancelNewUserHandler");
    addUserToTagAndCommunityPopupShow.value = false;
}
function addNewUserConfirmClickHandler() {
    console.log("addNewUserConfirmClickHandler");
    
    var first_name = firstName.value;
    var last_name = lastName.value;

    if(!first_name || first_name == '') {
        tagEvents.emit("toastShow", { message: "First name is required" });
        return false;
    }

    if(!last_name || last_name == '') {
        tagEvents.emit("toastShow", { message: "Last name is required" });
        return false;
    }
    
    var apiBodyObj = {};
    apiBodyObj.user_firstname = first_name;
    apiBodyObj.user_lastname = last_name;
    apiBodyObj.user_email = emailAddress.value;
    apiBodyObj.role_id = role_ID;

    console.log("Role ID" + role_ID);
    
    busy.activate();

    _WebService.request("user/createUser", apiBodyObj).then(function (result) {
        gotAddUserResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotAddUserResult(result) {
    busy.deactivate();

    var arr = result;
    if (arr.status == "success") {
        console.log("success");
        emailAddress.value = "";
        cancelNewUserHandler();
        tagEvents.emit("toastShow", { message: "User added" });
    } else if (arr.status == "failed") {
        console.log(arr.error);
        tagEvents.emit("toastShow", { message: arr.error });

    }
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {

    roleselectIndex: roleselectIndex,
    roleListItems: roleListItems,
    loadRoles: loadRoles,
    onroleSelectionChange: onroleSelectionChange,
    emailAddress: emailAddress,
    merchantIDorName: merchantIDorName,
    searchClickHandler: searchClickHandler,
    usersearchList: usersearchList,
    userSearchPopupShow: userSearchPopupShow,
    addUserToTagAndCommunityPopupShow: addUserToTagAndCommunityPopupShow,
    firstName: firstName, 
    lastName: lastName,

    userClickHandler: userClickHandler,
    addnewmemberClickHandler: addnewmemberClickHandler,
    addusertocommunityPopupShow: addusertocommunityPopupShow,
    addtocommunity_fullname: addtocommunity_fullname,
    addtocommunity_userID: addtocommunity_userID,
    cancelnewmemberHandler: cancelnewmemberHandler,
    addnewmemberconfirmClickHandler: addnewmemberconfirmClickHandler,
    cancelNewUserHandler: cancelNewUserHandler,
    addNewUserConfirmClickHandler: addNewUserConfirmClickHandler
}
