var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletsListData = Observable();
var createWalletPopupShow = Observable(false);

var walletCreateType = "";
var walletTypeItems = Observable();
var selectedWalletTypeIndex = Observable();
var searchWalletkeyword = Observable("");
var merchantServices = Observable(false);

var tabData = Observable("MONEY", "CRYPTO");
// NFT,SHARES
var selectedPageIndex = Observable(0);
var walletTypeSelected = "[0,1,3]";

var tabShowStatus = Observable(false);
var walletFiatData = [];
var walletOtherData = [];


function onViewActivate() {
    tabShowStatus.value = false;
    walletsListData.clear();

    if (_model.activePerspectiveType == "user") {
        merchantServices.value = false;
    } else {
        merchantServices.value = true;
    }

    walletTypeItems.clear();
    selectedWalletTypeIndex.value = -1;

    walletsListLoad();

}

function onTabBarChanged(args) {
    console.log("onTabBarChanged")
    setActiveTab(args.index);
}

function setActiveTab(index) {
    console.log("setActiveTab " + index)
    selectedPageIndex.value = index;

    walletsListData.clear();
    if (index == 0) {
        // walletTypeSelected = "fiat";
        // walletTypeSelected = "[0]";
        walletsListData.addAll(walletFiatData);

    } else if (index == 1) {
        // walletTypeSelected = "crypto,token";
        // walletTypeSelected = "[1,3]";
        walletsListData.addAll(walletOtherData);
    }

}

function walletsListLoad() {
    walletsListData.clear();
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;
    apiBodyObj.wallet_type = walletTypeSelected;

    _WebService.request("wallet/list", apiBodyObj).then(function (result) {
        gotWalletListResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotWalletListResult(result) {
    console.log("gotWalletListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    walletsListData.clear();
    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {

            if (resultArr.length > 5) {
                walletFiatData = [];
                walletOtherData = [];

                for (var i = 0; i < resultArr.length; i++) {
                    if (resultArr[i].wallet_type_numeric == 0) {
                        walletFiatData.push(resultArr[i]);
                    }else{
                        walletOtherData.push(resultArr[i]);
                    }
                }

                walletsListData.addAll(walletFiatData);
                tabShowStatus.value = true;

                setActiveTab(0);
            } else {
                tabShowStatus.value = false;
                walletsListData.addAll(resultArr);
            }

        }

    }
}


function walletAddClickHandle() {
    console.log("walletAddClickHandle")
    walletCreateType = "";
    selectedWalletTypeIndex.value = -1;

    createWalletPopupShow.value = true;
}

function searchWalletClickHandler() {
    if (searchWalletkeyword.value != "") {

        walletTypeItems.clear();
        walletCreateType = "";
        selectedWalletTypeIndex.value = -1;

        walletTypeLoad(searchWalletkeyword.value);
    }
}

function walletTypeLoad(keySerach) {
    console.log("typeValue " + keySerach)
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;
    apiBodyObj.currency_code = keySerach;

    _WebService.request("wallet/types", apiBodyObj).then(function (result) {
        gotWalletTypesListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotWalletTypesListResult(result) {
    console.log("gotWalletTypesListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null && resultArr.length > 0) {
            walletTypeItems.addAll(resultArr);
        }
    }
}


function onWalletTypeSelectionChange(args) {
    console.log(JSON.stringify(args.data))

    walletCreateType = args.data.wallet_id;
    selectedWalletTypeIndex.value = args.data.wallet_id;
}

function createNewWalletHandle() {
    console.log("walletAddClickHandle")
    busy.activate();
    console.log(walletCreateType)

    var apiBodyObj = {};
    apiBodyObj.wallet_type_id = walletCreateType;

    _WebService.request("wallet/create", apiBodyObj).then(function (result) {
        gotCreateWalletResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });

}

function gotCreateWalletResult(result) {
    console.log("gotCreateWalletResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        createWalletPopupShow.value = false;
        tagEvents.emit("toastShow", { message: "Currency type added to wallet" });

        walletsListLoad();
    } else {
        if (arr.error == "duplicate_wallet") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "This currency type is already available in wallet" });
        }
    }

}


function walletDeleteClickHandle(args) {
    console.log(JSON.stringify(args.data))

    busy.activate();

    _WebService.request("wallet/delete/" + args.data.wallet_id).then(function (result) {
        gotDeleteWalletResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDeleteWalletResult(result) {
    console.log("gotDeleteWalletResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        // var resultArr = arr.result;
        // if (resultArr !== null) {

        // }
        walletsListLoad();
    } else {
        //{"error":"duplicate_wallet","status":"failed"}

    }

}



function walletClickHandle(args) {
    console.log(JSON.stringify(args.data))
    router.push("walletNormalPage", { walletId: args.data.wallet_id, walletName: args.data.wallet_name, currencyCode: args.data.currency_code, walletType: args.data.wallet_type, walletTypeNumeric: args.data.wallet_type_numeric, canReceive: args.data.can_receive_via_multichain, token_type_id: args.data.token_type_id, walletDescription: args.data.wallet_description, contractAddress: args.data.contract_address, subTokenTypeId: args.data.sub_set_token_type_id, datId: Math.random() });
    walletsListData.clear();
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    tabShowStatus: tabShowStatus,
    onViewActivate: onViewActivate,
    merchantServices: merchantServices,
    tabData: tabData,
    selectedPageIndex: selectedPageIndex,
    onTabBarChanged: onTabBarChanged,
    walletsListData: walletsListData,
    walletAddClickHandle: walletAddClickHandle,


    walletTypeItems: walletTypeItems,
    selectedWalletTypeIndex: selectedWalletTypeIndex,
    onWalletTypeSelectionChange: onWalletTypeSelectionChange,

    createNewWalletHandle: createNewWalletHandle,



    walletDeleteClickHandle: walletDeleteClickHandle,
    createWalletPopupShow: createWalletPopupShow,
    walletClickHandle: walletClickHandle,

    searchWalletkeyword: searchWalletkeyword,
    searchWalletClickHandler: searchWalletClickHandler,


}


