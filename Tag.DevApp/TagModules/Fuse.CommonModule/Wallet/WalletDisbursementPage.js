var Observable = require('FuseJS/Observable');
var StringFormat = require('Utils/StringFormat');
var clip = require("ClipboardManager");

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var address = Observable("");
var amnt = Observable("");
var addresses = Observable();
var ShowIt = Observable("Hidden");
var walletId;
var userBalance;
var unique_id = Observable("");
var disbursementServices = Observable(false);
var tabData = Observable("DISBURSEMENT", "RETRY");
var selectedPageIndex = Observable(0);
var disbu = Observable("Visible");
var retry = Observable("Hidden");

this.Parameter.onValueChanged(module, function (param) {
    walletId = param.walletId;
    userBalance = param.balanceAmount;
});

function addMore(args) {
    if(address.value.trim()==''){
        tagEvents.emit("alertEvent", { type: "info", title: "Error", message: "Please enter an address." });
        return false;
    }
    if(amnt.value.trim()==''){
        tagEvents.emit("alertEvent", { type: "info", title: "Error", message: "Please enter an amount." });
        return false;
    }
    addresses.add({Address:address.value,Amount:amnt.value});
    address.value='';
    amnt.value='';
    if(addresses.length>0){
        ShowIt.value = "Visible";
    }
    //console.log(_model.accessToken, walletId);
}

function disburse(){
    // checking addresses length
    if(addresses.length==0){
        tagEvents.emit("alertEvent", { type: "info", title: "Error", message: "There are no disbursement values." });
        return false;
    }

    // checking if disbursement amount is higher than the balance.
    var totalamount = 0;
    addresses.forEach(function(elem, index) {
        totalamount += parseFloat(addresses._values[index].Amount);
    });

    if(parseFloat(userBalance) < parseFloat(totalamount)){
        tagEvents.emit("alertEvent", { type: "info", title: "Error", message: "Disbursement total is higher than your current balance." });
        return false;
    }

    // preparing data to post
    var data='';
    data='&from_wallet_id='+walletId;
    Object.keys(addresses._values).forEach(function (key,i ){
        data += "&" + 'data['+i+'][Address]'+ "=" + addresses._values[i].Address;
        data += "&" + 'data['+i+'][Amount]'+ "=" + addresses._values[i].Amount;
    });

    // making disbursement call
    the_call("wallet/disbursement", data);
}

function the_call(url, postdata) {

    busy.activate();
    // preparing data for the call
    url = _model.serverurl + url;
    if (postdata === undefined){ postdata = ''; }
    var method = 'POST';
    var data = "access_token=" + _model.accessToken;
    data += postdata;
    
    
    return fetch(url, {
        method: method,
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        //console.log("response : " + JSON.stringify(response));
        console.log('success result ',JSON.stringify(response._bodyText));
        addresses.clear();
        ShowIt.value = "Hidden";
        tagEvents.emit("alertEvent", { type: "info", title: "Success", message: "Disbursement started successfully, Please save the disbursement id for retry. ID : "+JSON.parse(response._bodyText).disbursement_id });
        busy.deactivate();
        //return response.json();
    }).catch(function (err) {
        console.log("err : " + err);
        console.log(JSON.stringify(err));
        busy.deactivate();
    });
}

function removeIt(arg){
    addresses.forEach(function(elem, index) {
        if(addresses._values[index].Address==arg.data.Address){
            addresses.removeAt(index);
        }
    });
    if(addresses.length==0){ ShowIt.value = "Hidden"; }
}

function onTabBarChanged(arg){
    disbu.value = "Hidden";
    retry.value = "Hidden";
    if(arg.index=='0'){ disbu.value = "Visible"; }
    if(arg.index=='1'){ retry.value = "Visible"; }
}

function redisburse(){
    if(!unique_id.value){
        tagEvents.emit("alertEvent", { type: "info", title: "Error", message: "Unique ID is required for retrying disbursement." });
        return false;
    }

    busy.activate();
    // preparing data for the call
    url = _model.serverurl + "redisburse";
    var method = 'POST';
    var data = "access_token=" + _model.accessToken;
    data += "&unique_key="+unique_id.value;
    
    
    return fetch(url, {
        method: method,
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        console.log("response : " + JSON.stringify(response));
        unique_id.value="";
        busy.deactivate();
        //return response.json();
    }).catch(function (err) {
        console.log("err : " + err);
        console.log(JSON.stringify(err));
        busy.deactivate();
    });
}

module.exports = {
    addMore: addMore,
    address: address,
    amnt:amnt,
    ShowIt: ShowIt,
    removeIt:removeIt,
    addresses:addresses,
    disburse: disburse,
    disbursementServices: disbursementServices,
    tabData: tabData,
    selectedPageIndex: selectedPageIndex,
    onTabBarChanged:onTabBarChanged,
    disbu: disbu,
    retry: retry,
    unique_id: unique_id,
    redisburse: redisburse
};