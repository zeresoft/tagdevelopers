var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var Timer = require("FuseJS/Timer");

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var amountInput = Observable();
var qrDataString = Observable();

var giftCloseTimer;
var totalTime = 60;
var remainingTime = Observable();

var walletId = "";
var currencyCode = Observable();
var activeWalletISdefault = false;

this.Parameter.onValueChanged(module, function (param) {
    amountInput.value = "";
    qrDataString.value = "";
    remainingTime.value = "";

    walletId = param.walletId;
    currencyCode.value = param.currencyCode;

    defaultWalletLoad();
})


function defaultWalletLoad() {
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;

    _WebService.request("user/DefaultWallet", apiBodyObj).then(function (result) {
        gotDefaultWalletResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDefaultWalletResult(result) {
    console.log("gotDefaultWalletResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        if (resultObj.wallet_id) {
           if( walletId == resultObj.wallet_id){
               activeWalletISdefault = true;
               generateVoucherHandler();
           }else{
               activeWalletISdefault = false;
           }
        }else{
            activeWalletISdefault = false;
        }
    }
}


function generateVoucherHandler() {
    console.log("generateVoucherHandler ");
    qrDataString.value = "";
    remainingTime.value = "";
    if (giftCloseTimer != undefined) {
        Timer.delete(giftCloseTimer);
    }

    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");
    
    if(!activeWalletISdefault){
        if (!Validator.amount(amountValue)) {
            tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
            return;
        }
    }

    busy.activate();

    var apiBodyObj = {};

    if (amountValue != "") {
        apiBodyObj.amount = amountValue;
    }
    // 1, hour , 2 day ,  3 seconds
    apiBodyObj.expiration_type = 3;
    apiBodyObj.expires_at = 60;
    apiBodyObj.open = 1;
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.voucher_count = 1;
    apiBodyObj.quick_pay = 1;
    console.log(JSON.stringify(apiBodyObj));

    _WebService.request("voucher/generate", apiBodyObj).then(function (result) {
        gotGenerateVoucherResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotGenerateVoucherResult(result) {
    busy.deactivate();

    var arr = result;

    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultObj = arr.result;

        var qrDataObj = {};
        var amountValue = amountInput.value;
        if (amountValue == "") {
            qrDataObj.action = "CHARGE";
        } else {
            qrDataObj.action = "QUICKPAY";
        }

        qrDataObj.currency = walletId;

        qrDataObj.id = resultObj.codes[0];
        console.log(JSON.stringify(qrDataObj))

        qrDataString.value = JSON.stringify(qrDataObj);

        totalTime = 60;
        remainingTime.value = totalTime;
        giftCloseTimer = Timer.create(timerFunction, 1000, true);

    } else {
        if (arr.error == "insuffcient_balance") {
            tagEvents.emit("toastShow", { message: loc.value.insufficient_balance });
        }
    }
}


function timerFunction() {

    router.getRoute(function (route) {
        // console.log("**** route" + JSON.stringify(route))
        if (route[0] === "walletQuickpayPage") {
            totalTime--;
            remainingTime.value = totalTime;

            if (totalTime <= 0) {
                // amountInput.value = "";
                qrDataString.value = "";
                remainingTime.value = "";

                Timer.delete(giftCloseTimer);
                generateVoucherHandler();
            }
        } else {
            qrDataString.value = "";
            remainingTime.value = "";
            if (giftCloseTimer != undefined) {
                Timer.delete(giftCloseTimer);
            }
        }
    })

}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    amountInput: amountInput,
    generateVoucherHandler: generateVoucherHandler,
    qrDataString: qrDataString,
    remainingTime: remainingTime,
    currencyCode: currencyCode,

}