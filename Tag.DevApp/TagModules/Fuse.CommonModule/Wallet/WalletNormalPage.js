var Observable = require('FuseJS/Observable');
var StringFormat = require('Utils/StringFormat');
var clip = require("ClipboardManager");
var moment = require('Utils/moment');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var transactionListData = Observable();
var totalEnergy = Observable(0);
var totalEnergyDecimal = Observable(0);
var availableEnergy = Observable("");

var walletId;
var balanceValue;
var walletIdDisplay = Observable();
var balanceAmount = Observable();
var walletName = Observable();
var currencyCode = Observable();
var walletType = Observable();
var walletTypeNumeric = Observable();
var walletNameTitle = Observable("");
var walletDescription = Observable("");
var showDisbursement = Observable(false);
var canReceive = Observable(false);
var nfcOfflineAvailable = Observable(false);

var showMenuStatus = Observable(false);
var infoPopupShow = Observable(false);

var tokenTypeId = 0;
var tokenTypeDisplay = Observable("");
var contractAddressDisplay = Observable("");


var isLoading = Observable(false);

var ethWallet = false;
var sellarWallet = false;
var nemWallet = false;
var bankPossible = false;

var merchantServices = Observable(false);
var quickPayPossible = Observable(true);
var loadbillPossible = Observable(false);
var iconBalenceCol = Observable("#e44933");
var menuColumnCount = Observable(3);
var munuDisplayStage = Observable(false);

var filterPopupShow = Observable(false);
var filterTransferId = Observable("");
var popupShowDatePick = Observable(false);
var filterStartDateDisplay = Observable();
var filterEndDateDisplay = Observable();
var filterStartDateSeleced = "";
var filterEndDateSeleced = "";
var startDateFlag;
var creditsShowStatus = Observable(true);
var debitsShowStatus = Observable(true);

this.Parameter.onValueChanged(module, function (param) {
    quickPayPossible.value = true;
    loadbillPossible.value = false;
    menuColumnCount.value = 3;

    walletId = param.walletId;
    walletIdDisplay.value = param.walletId;
    walletName.value = param.walletName;
    currencyCode.value = param.currencyCode;

    walletType.value = param.walletType;
    walletTypeNumeric.value = param.walletTypeNumeric;

    // walletNameTitle.value = param.walletName + " (" + param.currencyCode + ")"
    walletNameTitle.value = param.walletName;
    canReceive.value = param.canReceive;

    walletDescription.value = param.walletDescription;

    nfcOfflineAvailable.value = false;

    // if (_model.activePerspectiveType == "community") {
    //     if (param.allow_nfc_transfer == "y") {
    //         nfcOfflineAvailable.value = true;
    //     }
    // }

    tokenTypeId = 0;
    tokenTypeDisplay.value = "";
    if (param.token_type_id) {
        tokenTypeId = param.token_type_id;

        if (param.token_type_id == 2) {
            tokenTypeDisplay.value = "ERC20 Token";
        }
    }

    contractAddressDisplay.value = "";
    if (param.contractAddress != " ") {
        contractAddressDisplay.value = param.contractAddress;
    }

    ethWallet = false;
    sellarWallet = false;
    nemWallet = false;

    for (var i = 0; i < param.subTokenTypeId.length; i++) {
        if (param.subTokenTypeId[i] == 2) {
            ethWallet = true;
        }
        if (param.subTokenTypeId[i] == 16) {
            sellarWallet = true;
        }
        if (param.subTokenTypeId[i] == 32) {
            nemWallet = true;
        }

    }


    if (param.walletId == 1) {
        loadbillPossible.value = true;
        menuColumnCount.value = 5;
    } else {
        if (param.currencyCode == "BTC" || param.walletTypeNumeric == 2) {
            quickPayPossible.value = false;
            menuColumnCount.value = 2;
        }
    }

})

function endLoading() {
    isLoading.value = false;
}

function reloadHandler() {
    console.log("reloadHandler-----------------")
    transactionListData.clear();

    isLoading.value = true;
    loadUserWalletTotalCall();
}


function onViewActivate() {
    munuDisplayStage.value = false;

    showMenuStatus.value = false;
    console.log("view activated ............................", JSON.stringify(_model.activePerspectiveType));
    if (_model.activePerspectiveType == "community") {
        showDisbursement.value = true;
        merchantServices.value = true;
        iconBalenceCol.value = "#FFF";
    } else {
        showDisbursement.value = false;
        merchantServices.value = false;
        iconBalenceCol.value = "#e44933";
    }

    balanceAmount.value = "";
    balanceValue = 0;
    totalEnergy.value = 0;
    totalEnergyDecimal.value = 0;
    transactionListData.clear();
    availableEnergy.value = "";

    filterTransferId.value = "";
    filterStartDateDisplay.value = "";
    filterEndDateDisplay.value = "";
    filterStartDateSeleced = "";
    filterEndDateSeleced = "";

    creditsShowStatus.value = true;
    debitsShowStatus.value = true;

    bankPossible = false;
    loadUserWalletTotalCall();
}

function loadUserWalletTotalCall() {
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_type_id = walletId;

    _WebService.request("wallet/list", apiBodyObj).then(function (result) {
        gotLoadEnergyTotalResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotLoadEnergyTotalResult(result) {
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        if (resultObj) {
            var balance_amount = resultObj[0].balance_amount;
            var totalEnergyStr = balance_amount.toString();
            totalEnergyArr = totalEnergyStr.split(".");

            totalEnergy.value = StringFormat.currency(totalEnergyArr[0]);
            totalEnergyDecimal.value = totalEnergyArr[1];

            balanceAmount.value = StringFormat.currency(balance_amount);
            balanceValue = balance_amount;

            if (resultObj[0].promised_amount != 0) {
                var available_amount = balance_amount - resultObj[0].promised_amount;
                availableEnergy.value = StringFormat.currency(available_amount.toFixed(2));
            }

            bankPossible = resultObj[0].bank_deposit_withdraw;
        } else {
            balanceAmount.value = 0;
            balanceValue = 0;

            totalEnergy.value = 0;
            totalEnergyDecimal.value = 0;
            showMenuStatus.value = true;
            availableEnergy.value = "";
        }
        loadUserEnergyListCall();

    }
}

function pagingLoadMore() {
    if (transactionListData.length != 0) {
        loadUserEnergyListCall();
    }
}

function qrscan_clickHandler() {
    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandler });
}

function qrReceivedHandler(args) {
    var resultData = args;
    console.log("scanData SEND" + scanData)

    if (resultData.indexOf("https://tagcash.com/u/") >= 0) {
        var scanUserId = resultData.replace("https://tagcash.com/u/", '');
        console.log(" scanUserId " + scanUserId);
        filterTransferId.value = "U" + scanUserId;
    } else if (resultData.indexOf("https://tagcash.com/c/") >= 0) {
        var scanCommunityId = resultData.replace("https://tagcash.com/c/", '');
        console.log(" scanCommunityId " + scanCommunityId);
        filterTransferId.value = scanCommunityId;
    }

}


tagEvents.on("transactionFiletrEvent", function (arg) {
    console.log("transactionFiletrEvent", JSON.stringify(arg));
    filter_clickHandler();
});

function filter_clickHandler() {
    filterPopupShow.value = true;
}

function filterClose_clickHandler() {
    filterTransferId.value = "";
    filterStartDateDisplay.value = "";
    filterEndDateDisplay.value = "";
    filterStartDateSeleced = "";
    filterEndDateSeleced = "";

    creditsShowStatus.value = true;
    debitsShowStatus.value = true;

    filterTransferList();
}

function startdateChange_clickHandler() {
    popupShowDatePick.value = true;
    startDateFlag = true;
}

function enddateChange_clickHandler() {
    popupShowDatePick.value = true;
    startDateFlag = false;
}

function clearStartDate_clickHandler() {
    filterStartDateDisplay.value = "";
    filterStartDateSeleced = "";
}

function clearEndDate_clickHandler() {
    filterEndDateDisplay.value = "";
    filterEndDateSeleced = "";
}

function onDateCloseDialog(args) {
    if (startDateFlag == true) {
        filterStartDateSeleced = moment(args.date).format('YYYY-MM-DD');
        filterStartDateDisplay.value = moment(args.date).format('DD-MM-YYYY');
    } else {
        filterEndDateSeleced = moment(args.date).format('YYYY-MM-DD');
        filterEndDateDisplay.value = moment(args.date).format('DD-MM-YYYY');
    }
}

function filterTransferList() {
    filterPopupShow.value = false;

    transactionListData.clear();
    loadUserEnergyListCall();
}

function loadUserEnergyListCall() {
    console.log("loadUserEnergyListCall")
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.from_wallet_id = walletId;

    if (filterTransferId.value != "") {
        var checkIdInt = filterTransferId.value;
        var serUserid;
        var serUserType;

        if (checkIdInt.charAt(0) === "U" || checkIdInt.charAt(0) === "u") {
            serUserid = checkIdInt.slice(1);
            serUserType = "user";
        } else {
            serUserid = checkIdInt;
            serUserType = "community";
        }

        apiBodyObj.to_or_from_type = serUserType;
        apiBodyObj.to_or_from_id = serUserid;
    }

    //2019-04-16
    if (filterStartDateSeleced != "") {
        apiBodyObj.from_date = filterStartDateSeleced;
    }
    if (filterEndDateSeleced != "") {
        apiBodyObj.to_date = filterEndDateSeleced;
    }

    if (creditsShowStatus.value != debitsShowStatus.value) {
        if (creditsShowStatus.value == true) {
            apiBodyObj.direction = "in";
        }
        if (debitsShowStatus.value == true) {
            apiBodyObj.direction = "out";
        }
    }

    apiBodyObj.count = 20;
    apiBodyObj.offset = transactionListData.length;

    _WebService.request("wallet/transactions", apiBodyObj).then(function (result) {
        gotLoadEnergyListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotLoadEnergyListResult(result) {
    console.log("gotLoadEnergyListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    endLoading();

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            if (resultArr.length == 0 && transactionListData.length == 0) {
                showMenuStatus.value = true;
            }

            transactionListData.addAll(resultArr);
        }

    }
}

function information_clickHandler() {
    infoPopupShow.value = true;
}

function clearView() {
    showMenuStatus.value = false;
    transactionListData.clear();
}

function send_clickHandler() {
    clearView();
    router.push("walletSendPage", { walletId: walletId, walletName: walletName.value, currencyCode: currencyCode.value, walletType: walletType.value, walletTypeNumeric: walletTypeNumeric.value, tokenTypeId: tokenTypeId, ethWallet: ethWallet, sellarWallet: sellarWallet, nemWallet: nemWallet, bankPossible: bankPossible, datId: Math.random() });
}

function sendCrypto_clickHandler() {
    clearView();
    // router.push("walletCryptoSendPage", { walletId: walletId, walletName: walletName.value, currencyCode: currencyCode.value, walletType: walletType.value, datId: Math.random() });
    router.push("walletSendPage", { walletId: walletId, walletName: walletName.value, currencyCode: currencyCode.value, walletType: walletType.value, walletTypeNumeric: walletTypeNumeric.value, tokenTypeId: tokenTypeId, ethWallet: ethWallet, sellarWallet: sellarWallet, nemWallet: nemWallet, datId: Math.random() });
}

function deposit_clickHandler() {
    clearView();
    router.push("walletDepositPage", { walletId: walletId, walletName: walletName.value, currencyCode: currencyCode.value, walletType: walletType.value, walletTypeNumeric: walletTypeNumeric.value, tokenTypeId: tokenTypeId, ethWallet: ethWallet, sellarWallet: sellarWallet, nemWallet: nemWallet, bankPossible: bankPossible, datId: Math.random() });
}

function disbursement_clickHandler() {
    clearView();
    router.push("walletDisbursementPage", { walletId: walletId, walletName: walletName.value, currencyCode: currencyCode.value, walletType: walletType.value, balanceAmount: balanceValue, datId: Math.random() });
}

function quickpay_clickHandler() {
    clearView();
    router.push("walletQuickpayPage", { walletId: walletId, walletName: walletName.value, currencyCode: currencyCode.value, walletType: walletType.value, datId: Math.random() });
}

function walletTransItemClickHandle(args) {
    console.log(JSON.stringify(args.data.narration));

    clip.setText(args.data.narration);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}


function buyLoadClick() {
    router.push("buyLoadPage");
}

function payBillsClick() {
    router.push("paybillOptionsPage");
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    onViewActivate: onViewActivate,

    walletNameTitle: walletNameTitle,
    walletDescription: walletDescription,
    currencyCode: currencyCode,
    totalEnergy: totalEnergy,
    totalEnergyDecimal: totalEnergyDecimal,
    availableEnergy: availableEnergy,

    walletType: walletType,
    walletTypeNumeric: walletTypeNumeric,
    canReceive: canReceive,
    nfcOfflineAvailable: nfcOfflineAvailable,

    send_clickHandler: send_clickHandler,
    sendCrypto_clickHandler: sendCrypto_clickHandler,
    quickpay_clickHandler: quickpay_clickHandler,
    disbursement_clickHandler: disbursement_clickHandler,
    showDisbursement: showDisbursement,

    transactionListData: transactionListData,
    deposit_clickHandler: deposit_clickHandler,

    showMenuStatus: showMenuStatus,
    pagingLoadMore: pagingLoadMore,

    filterTransferList: filterTransferList,
    filterTransferId: filterTransferId,

    infoPopupShow: infoPopupShow,
    information_clickHandler: information_clickHandler,
    walletIdDisplay: walletIdDisplay,
    balanceAmount: balanceAmount,

    tokenTypeDisplay: tokenTypeDisplay,
    contractAddressDisplay: contractAddressDisplay,

    isLoading: isLoading,
    reloadHandler: reloadHandler,
    walletTransItemClickHandle: walletTransItemClickHandle,

    merchantServices: merchantServices,
    quickPayPossible: quickPayPossible,
    loadbillPossible: loadbillPossible,
    iconBalenceCol: iconBalenceCol,
    menuColumnCount: menuColumnCount,
    munuDisplayStage: munuDisplayStage,

    buyLoadClick: buyLoadClick,
    payBillsClick: payBillsClick,

    filterPopupShow: filterPopupShow,
    filter_clickHandler: filter_clickHandler,
    filterClose_clickHandler: filterClose_clickHandler,
    creditsShowStatus: creditsShowStatus,
    debitsShowStatus: debitsShowStatus,
    popupShowDatePick: popupShowDatePick,
    filterStartDateDisplay: filterStartDateDisplay,
    filterEndDateDisplay: filterEndDateDisplay,
    startdateChange_clickHandler: startdateChange_clickHandler,
    enddateChange_clickHandler: enddateChange_clickHandler,
    clearStartDate_clickHandler: clearStartDate_clickHandler,
    clearEndDate_clickHandler: clearEndDate_clickHandler,
    onDateCloseDialog: onDateCloseDialog,
    qrscan_clickHandler: qrscan_clickHandler,
}