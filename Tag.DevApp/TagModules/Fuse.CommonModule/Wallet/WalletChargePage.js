var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var TransferError = require('Utils/TransferError');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var amountInput = Observable();

var walletId = "";
var currencyCode = Observable();

var pinInputPopupShow = Observable(false);
var numberarray = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'clear', '0', 'back'];
var pinEntered = "";
var pinDisplayArr = Observable();
var scanUserId = "";

var nfcViewIdLocal;
var nfcChargeStatus = Observable(false);
var nfcChargeCurrency = Observable("");
var nfcChargeAmount = Observable("");

this.Parameter.onValueChanged(module, function (param) {
    nfcViewIdLocal = Math.random();
    _model.nfcViewId = nfcViewIdLocal;

    nfcChargeStatus.value = false;
    nfcChargeCurrency.value = "";
    nfcChargeAmount.value = "";

    amountInput.value = "";
    walletId = param.walletId;
    currencyCode.value = param.currencyCode;
})

function scanVoucherClicked() {
    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandler });
}

function qrReceivedHandler(args) {
    processQRScan(args);
}

function processQRScan(scanData) {
    console.log("scanData" + scanData)
    if (Validator.isJson(scanData)) {
        console.log("JSON")
        var resultJson = JSON.parse(scanData);

        if (resultJson.action) {
            var actionInput = resultJson.action;
            actionInput = actionInput.toUpperCase();

            if (actionInput == "QUICKPAY") {
                // {"action":"QUICKPAY","id":"yVWuFCcMTh"}
                tagEvents.emit("toastShow", { message: "Not a Charge Voucher" });
            } else if (actionInput == "CHARGE") {
                // {"action":"CHARGE","currency":1,"id":"j9ga7uQTDr"}
                if (walletId == resultJson.currency) {
                    quickpayRedume(resultJson.id)
                } else {
                    tagEvents.emit("alertEvent", { type: "info", title: "Default Wallet", message: "Default wallets does not match. Please scan a Charge QR with same wallet." });
                }
            } else {
                //not handled now
                tagEvents.emit("toastShow", { message: loc.value.not_valid_qr });
            }
        } else {
            //not valid
            tagEvents.emit("toastShow", { message: loc.value.not_valid_qr });
        }

    } else {
        var resultData = scanData;
        if (resultData.indexOf("https://tagcash.com/u/") >= 0) {
            scanUserId = resultData.replace("https://tagcash.com/u/", '');
            console.log(" scanUserId " + scanUserId);

            searchUsersHandler(scanUserId);
        } else {
            //not valid
            tagEvents.emit("toastShow", { message: loc.value.not_valid_qr });
        }
    }
}

function quickpayRedume(code) {
    busy.activate();

    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    var apiBodyObj = {};
    apiBodyObj.voucher = code;
    apiBodyObj.amount = amountValue;

    _WebService.request("voucher/redeem", apiBodyObj).then(function (result) {
        gotVouchersRedeemResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotVouchersRedeemResult(result) {
    console.log("gotVouchersRedeemResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        var resultMessage = resultObj.voucher_amount + " " + resultObj.currency_code;
        tagEvents.emit("alertEvent", { type: "info", title: loc.value.transaction_confirmed, message: resultMessage });
    } else {
        if (arr.error == "invalid_or_expired_voucher") {
            tagEvents.emit("toastShow", { message: loc.value.invalid_or_expired_voucher });
        } else if (arr.error == "expired_voucher") {
            tagEvents.emit("toastShow", { message: loc.value.expired_voucher });
        } else if (arr.error == "Insufficient funds") {
            tagEvents.emit("toastShow", { message: loc.value.insufficient_funds });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function searchUsersHandler(id) {
    busy.activate();

    console.log("searchUsePopup_clickHandler");
    var apiBodyObj = {};

    apiBodyObj.id = id;

    _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
        gotSearchUserResult(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotSearchUserResult(result) {
    console.log("gotSearchUserResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            pinInputPopupShow.value = true;
        }
    } else {
        tagEvents.emit("toastShow", { message: loc.value.not_valid_qr });
    }
}

function pinnumberClickHandler(args) {
    if (args.data == "clear") {
        pinEntered = "";
    } else if (args.data == "back") {
        console.log(pinEntered);
        pinEntered = pinEntered.slice(0, pinEntered.length - 1);
        console.log(pinEntered);
    } else {
        if (pinEntered.length < 4) {
            pinEntered = pinEntered + args.data;

            if (pinEntered.length == 4) {
                pinEnteredChargeCall();
            }
        }
    }
    pinDisplayArr.clear();
    var pinEnteredArr = pinEntered.split("");
    pinDisplayArr.addAll(pinEnteredArr);
}


function pinEnteredChargeCall() {
    pinInputPopupShow.value = false;

    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    busy.activate();

    var api_obj = {};
    api_obj.amount = amountValue;
    api_obj.to_wallet_id = walletId;
    api_obj.from_wallet_id = walletId;
    // api_obj.narration = notes.value;

    api_obj.charging = "true";
    api_obj.pin = pinEntered;
    api_obj.from_id = scanUserId;
    api_obj.from_type = "user";

    _WebService.request("wallet/transfer", api_obj).then(function (result) {
        gotWalletTransferResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotWalletTransferResult(result) {
    console.log("gotWalletTransferResult")

    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        var resultMessage = resultObj.transfer_amount + " " + resultObj.transfer_currency;
        tagEvents.emit("alertEvent", { type: "info", title: loc.value.transaction_confirmed, message: resultMessage });
    } else {
        TransferError.errorHandle(arr);
    }
}

function nfcChargeClicked() {
    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    nfcChargeStatus.value = true;
    nfcChargeCurrency.value = currencyCode.value;
    nfcChargeAmount.value = amountInput.value;
}

function nfcChargeCancel() {
    nfcChargeStatus.value = false;
}

tagEvents.on("nfcChargeScanedEvent", function (arg) {
    console.log(" Charge  **** nfcChargeScanedEvent")
    console.log(nfcViewIdLocal +" == "+ _model.nfcViewId)

    if (nfcViewIdLocal == _model.nfcViewId) {
        console.log(" nfcChargeStatus  **** " + nfcChargeStatus.value)
        if (nfcChargeStatus.value == true) {
            nfcChargeCall(arg.scanData);
        }
    }
});


function nfcChargeCall(nfcIdData) {

    busy.activate();

    var api_obj = {};
    api_obj.amount = nfcChargeAmount.value;
    api_obj.wallet_id = walletId;
    api_obj.nfc_id = nfcIdData;

    _WebService.request("wallet/charge", api_obj).then(function (result) {
        gotNfcChargeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotNfcChargeResult(result) {
    console.log("gotNfcChargeResult")

    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        
        nfcChargeStatus.value = false;
        var resultMessage = resultObj.transfer_amount + " " + resultObj.transfer_currency;
        tagEvents.emit("alertEvent", { type: "info", title: loc.value.transaction_confirmed, message: resultMessage });
    } else {
        if (arr.error == "nfc_not_linked_to_user") {
            tagEvents.emit("toastShow", { message: "This card is not linked to any user" });
        } else if (arr.error == "nfc_linked_user_not_found") {
            tagEvents.emit("toastShow", { message: "This card is not linked to any user" });
        } else if (arr.error == "nfc_max_amount_error") {
            tagEvents.emit("toastShow", { message: "Amount is more than maximum allowed using NFC" });
        } else if (arr.error == "duplicate_nfc_id_found") {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        } else {
            TransferError.errorHandle(arr);
        }
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    amountInput: amountInput,
    scanVoucherClicked: scanVoucherClicked,
    currencyCode: currencyCode,

    pinInputPopupShow: pinInputPopupShow,
    numberarray: numberarray,
    pinDisplayArr: pinDisplayArr,
    pinnumberClickHandler: pinnumberClickHandler,

    nfcChargeStatus: nfcChargeStatus,
    nfcChargeCurrency: nfcChargeCurrency,
    nfcChargeAmount: nfcChargeAmount,
    nfcChargeClicked: nfcChargeClicked,
    nfcChargeCancel: nfcChargeCancel,
}