var Observable = require('FuseJS/Observable');
var GeoLocation = require("FuseJS/GeoLocation");
// var Maps = require("FuseJS/Maps");

var Validator = require('Utils/Validator');
var moment = require('Utils/moment');
var TransferError = require('Utils/TransferError');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');


var walletId;
var walletName = Observable();
var currencyCode = Observable();


var bankItems = Observable();
var bankSelectIndex = Observable(0);
var bankFeeSelect = Observable("");

var bankCodeName;
var bankFullName;

var remittanceCenterItems = Observable();
var zoneListShow = Observable(false);
var centerSelectIndex = Observable(0);

var remittanceZonesItems = Observable();
var zonesSelectIndex = Observable(0);

var withdrawAmountInput = Observable();
var withdrawAccountInput = Observable();
var withdrawBranchInput = Observable();
var withdrawBankNameInput = Observable();
var withdrawBeneficiaryNameInput = Observable();
var withdrawSwiftCodeInput = Observable();

var withdrawRemittanceAmountInput = Observable();
var withdrawBitcoinAmountInput = Observable();
var bitcoinAmountCalculated = Observable();
var withdrawBitcoinAddressInput = Observable();

var transactionBankListData = Observable();
var transactionRemittanceListData = Observable();
var transactionBitcoinListData = Observable();


var calculatedFeeAmount = Observable(0);
var amountDeductTotal = Observable();
var namePickupInput = Observable();
var numberPickupInput = Observable();
var remittanceSelectId;
var zoneSelectValue;

var withdrawDropdownSelect = Observable(true);


var useridInput = Observable("");
var amountInput = Observable();
var notesTxt = Observable("");

var resultAmount = Observable("");
var resultUser = Observable("");
var wallectChangable = Observable(false);
var toTransferId;
var toTransferType;
var toTransferName;
var toTransferEmail;
var toTransferAddress;
var searchUserPopupShow = Observable(false);

var withdrawTypeItems = Observable();
var withdrawTypeIndex = Observable(0);

var withdrawType = Observable("tagcash");


var stellarAddressInput = Observable("");
var memoTextEditable = Observable("false");
var memoInputTxt = Observable("");
var stellarAmountInput = Observable("");
var toStellerTransferAddress;
var resultAmountSteller = Observable("");
var resultUserSteller = Observable("");

var internationalCashoutSelected = Observable(false);

var agentCenterList = Observable();
var timeoutMs = 5000;
var locationAvaialble = Observable(false);
var latitude = 0;
var longitude = 0;
var nearbyAgentLoad = Observable(false);

var transactionGoferListData = Observable();
var withdrawGoferAmountInput = Observable("");
var calculatedGoferFeeAmount = Observable("");
var amountGoferDeductTotal = Observable("");
var addressGoferInput = Observable("");
var notesGoferInput = Observable("");

var stellarAddressResolvedShow = Observable("");
var stellarAuthoriseAddressInput = Observable("");
var stellarAuthorisePossible = Observable(false);

this.Parameter.onValueChanged(module, function (param) {
    console.log(JSON.stringify(param));

    sendAgainHandler();
    useridInput.value = "";
    withdrawAmountInput.value = "";
    withdrawAccountInput.value = "";
    withdrawBranchInput.value = "";
    withdrawBankNameInput.value = "";
    withdrawBeneficiaryNameInput.value = "";
    withdrawSwiftCodeInput.value = "";

    withdrawRemittanceAmountInput.value = "";
    calculatedFeeAmount.value = "";
    amountDeductTotal.value = "";
    namePickupInput.value = "";
    numberPickupInput.value = "";

    withdrawBitcoinAmountInput.value = "";
    bitcoinAmountCalculated.value = "";
    withdrawBitcoinAddressInput.value = "";

    withdrawGoferAmountInput.value = "";
    calculatedGoferFeeAmount.value = "";
    amountGoferDeductTotal.value = "";
    addressGoferInput.value = "";
    notesGoferInput.value = "";

    withdrawTypeIndex.value = -1;
    withdrawTypeItems.clear();

    stellarAuthorisePossible.value = false;

    if (param.walletId) {
        walletId = param.walletId;
        walletName.value = param.walletName;
        currencyCode.value = param.currencyCode;
        wallectChangable.value = false;
    } else {
        walletId = "";
        currencyCode.value = loc.value.select;
        wallectChangable.value = true;

        defaultWalletLoad();
    }


    if (param.fromScan) {
        console.log("************** from scan");
        withdrawDropdownSelect.value = false;

        withdrawType.value = "tagcash";
        withdrawTypeIndex.value = 0;

        if (param.recipientType == 3) {
            //Address
            useridInput.value = param.user_id;
        } else if (param.recipientType == 2) {
            //community
            useridInput.value = param.user_id;
        } else {
            useridInput.value = "U" + param.user_id;
        }

        amountInput.value = param.amount;
    } else {

        withdrawDropdownSelect.value = false;
        var depositTypeArr = []

        if (param.walletTypeNumeric == 2) {
            //trading_account
            withdrawType.value = "bank";
        } else if (param.walletTypeNumeric == 1 || param.walletTypeNumeric == 3) {
            withdrawType.value = "tagcash";

            if (param.sellarWallet == true) {
                withdrawDropdownSelect.value = true;

                depositTypeArr.push({ name: "Tagcash", value: "tagcash" });
                depositTypeArr.push({ name: "Stellar", value: "stellar" });

                withdrawTypeItems.addAll(depositTypeArr);
                withdrawTypeIndex.value = 0;
            }
        } else if (param.walletTypeNumeric == 0) {
            withdrawType.value = "tagcash";
            withdrawDropdownSelect.value = true;

            depositTypeArr.push({ name: "Tagcash", value: "tagcash" });

            if (param.sellarWallet == true) {
                depositTypeArr.push({ name: "Stellar", value: "stellar" });
            }

            if (param.bankPossible == true) {
                depositTypeArr.push({ name: "Bank", value: "bank" });
            }

            if (walletId == "1") {
                depositTypeArr.push({ name: "Remittance Center", value: "remittance" });
                depositTypeArr.push({ name: "Dragonpay", value: "dragonpay" });
                depositTypeArr.push({ name: "Cash out via agent", value: "agentcashout" });
                depositTypeArr.push({ name: "Gofer Cash out", value: "gofer" });

                locationAvaialble.value = false;
                nearbyAgentLoad.value = false;
                startImmediateLocation()
            }
            // depositTypeArr.push({ name: "Bitcoin", value: "bitcoin" });

            withdrawTypeItems.addAll(depositTypeArr);
            withdrawTypeIndex.value = 0;
        }

        withdrawTypeChangeProcess();

    }


})

function onWithdrawTypeSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    withdrawType.value = selectedItem.value;
    withdrawTypeChangeProcess();
}


function withdrawTypeChangeProcess() {
    if (withdrawType.value === "bank") {
        loadBanksListCall();
    } else if (withdrawType.value === "stellar") {
        stellarAuthorisePossible.value = false;

        if (_model.activePerspectiveType == "community") {
            checkCommunityisVerified();
        }
    } else if (withdrawType.value === "remittance") {
        loadCentersListCall();
    } else if (withdrawType.value === "dragonpay") {
        loadDragonBanksListCall();
    } else if (withdrawType.value === "agentcashout") {
        loadAgentsListCall();
    } else if (withdrawType.value === "gofer") {
        loadGoferHistoryListCall();
    } else if (withdrawType.value === "bitcoin") {
        //bitcoinDepositQRCall();
    }
}


function checkCommunityisVerified() {
    busy.activate();

    _WebService.request("verification/GetLevel").then(function (result) {
        gotMerchantVerificationResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotMerchantVerificationResult(result) {
    console.log("gotMerchantVerificationResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultObj = arr.result;
        var verifivationlevel = resultObj.verification_level;

        if (verifivationlevel == 0) {
            stellarAuthorisePossible.value = false;
        } else {
            stellarAuthorisePossible.value = true;
        }
    }

}


function defaultWalletLoad() {
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;

    _WebService.request("user/DefaultWallet", apiBodyObj).then(function (result) {
        gotDefaultWalletResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDefaultWalletResult(result) {
    console.log("gotDefaultWalletResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        if (resultObj.wallet_id) {
            walletId = resultObj.wallet_id;
            currencyCode.value = resultObj.currency_code;
        }
    }
}

function walletOptionChangedEvent(args) {
    walletId = args.walletId;
    currencyCode.value = args.currencyCode;
}

function searchUser_clickHandler() {
    searchUserPopupShow.value = true;
}

function onUserSelectedEvent(args) {
    console.log(args.selectedType)
    console.log(args.selectedId)
    console.log(args.selectedName)

    if (args.selectedType === "user") {
        useridInput.value = "U" + args.selectedId;
    } else {
        useridInput.value = args.selectedId;
    }
    searchUserPopupShow.value = false;
}

function transferButton_clickHandler() {
    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    var useridInputValue = useridInput.value;

    if (useridInputValue === "") {
        return;
    }

    if (walletId === "") {
        tagEvents.emit("toastShow", { message: loc.value.select_wallet });
        return;
    }

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }


    var apiBodyObj = {};
    var checkIdInt = useridInputValue;

    if (!validateId(checkIdInt)) {
        if (Validator.email(checkIdInt)) {
            toTransferEmail = checkIdInt;
            confirmEmailAlertShow();
        } else if (checkIdInt.indexOf("*") !== -1) {
            toTransferAddress = checkIdInt;
            confirmAddressAlertShow();
        } else if (checkIdInt.charAt(0) === "U" || checkIdInt.charAt(0) === "u") {

            var serUserid = checkIdInt.slice(1)
            apiBodyObj.id = serUserid;
            console.log("serUserid --------" + serUserid)

            _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
                gotValidateUserResult(result);
            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });
        } else if (checkIfAddress(checkIdInt)) {
            toTransferAddress = checkIdInt;
            confirmAddressAlertShow();

        } else {
            return;
        }
    } else {
        busy.activate();

        apiBodyObj.name = useridInputValue;

        _WebService.request("community/searchNew", apiBodyObj).then(function (result) {
            gotValidateCommunityResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }


}

function gotValidateUserResult(result) {
    console.log("gotValidateUserResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        toTransferType = "user";
        toTransferId = resultArr[0].id;
        toTransferName = resultArr[0].name;
        confirmAlertShow();
    } else {
        if (arr.result == "no_users") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "The ID you entered is not valid." });
        }
    }
}

function gotValidateCommunityResult(result) {
    console.log("gotValidateCommunityResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            toTransferType = "community";
            toTransferId = resultArr[0].id;
            toTransferName = resultArr[0].community_name;
            confirmAlertShow();
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "The ID you entered is not valid." });
        }
    }
}

function confirmAlertShow() {
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", amountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", toTransferName);
    tagEvents.emit("alertEvent", { type: "confirm", title: loc.value.transfer, message: messageText, callback: confirmSendAlertClosedHandler });
}

function confirmSendAlertClosedHandler(args) {
    if (args === "yes") {
        transferAamoundToId();
    }
}

function transferAamoundToId() {
    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.to_type = toTransferType;
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.to_wallet_id = walletId;
    apiBodyObj.narration = notesTxt.value;
    apiBodyObj.to_id = toTransferId;

    _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
        gotTransferTagcoinResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransferTagcoinResult(result) {
    console.log("gotTransferTagcoinResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmount.value = amountInput.value + " " + currencyCode.value;
        resultUser.value = toTransferName;
        sendStateGroup.goto(resultState);

        if (resultObj.scratchcard_game_id) {
            if (_model.activePerspectiveType == "user" && resultObj.scratchcard_game_id != 0) {
                if (resultObj.win_combination_id == 0) {
                    tagEvents.emit("scratchPlayEvent", { game_id: resultObj.scratchcard_game_id });
                } else {
                    tagEvents.emit("scratchPlayEvent", { game_id: resultObj.scratchcard_game_id, win_comb: resultObj.win_combination_id });
                }
            }
        }
    } else {
        TransferError.errorHandle(arr);
    }
}


function confirmEmailAlertShow() {
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", amountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", toTransferEmail);
    tagEvents.emit("alertEvent", { type: "confirm", title: loc.value.transfer, message: messageText, callback: confirmSendEmailClosedHandler });
}

function confirmSendEmailClosedHandler(args) {
    if (args === "yes") {
        transferAamoundToEmail();
    }
}

function transferAamoundToEmail() {
    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.to_wallet_id = walletId;
    apiBodyObj.narration = notesTxt.value;
    apiBodyObj.to_email = toTransferEmail;

    _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
        gotTransferEmailResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransferEmailResult(result) {
    console.log("gotTransferEmailResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmount.value = amountInput.value + " " + currencyCode.value;
        resultUser.value = toTransferEmail;
        sendStateGroup.goto(resultState);

        if (resultObj.to_unreg_email) {
            tagEvents.emit("alertEvent", { type: "info", title: "", message: loc.value.not_registred_send_message });
        }
    } else {
        TransferError.errorHandle(arr);
    }
}




function confirmAddressAlertShow() {
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", amountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", toTransferAddress);
    tagEvents.emit("alertEvent", { type: "confirm", title: loc.value.transfer, message: messageText, callback: confirmSendAddressClosedHandler });
}

function confirmSendAddressClosedHandler(args) {
    if (args === "yes") {
        transferAamoundToAddress();
    }
}

function transferAamoundToAddress() {
    var amountValue = amountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.to_wallet_id = walletId;
    apiBodyObj.narration = notesTxt.value;
    apiBodyObj.to_crypto_address = toTransferAddress;

    _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
        gotTransferAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransferAddressResult(result) {
    console.log("gotTransferAddressResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmount.value = amountInput.value + " " + currencyCode.value;
        resultUser.value = toTransferAddress;
        sendStateGroup.goto(resultState);
    } else {
        if (arr.error == "invalid_crypto_address") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Invalid address" });
        } else {
            TransferError.errorHandle(arr);
        }
    }
}

function reloadTransactionsHandler() {
    router.goBack();
}

function sendAgainHandler() {
    // useridInput.value = "";
    amountInput.value = "";
    notesTxt.value = "";

    sendStateGroup.goto(initalState);


    stellarAmountInput.value = "";
    memoInputTxt.value = "";
    sendStateStellarGroup.goto(initalStellarState);
}


function qrscan_clickHandler() {
    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandler });
}

function qrReceivedHandler(args) {
    var scanData = args;
    console.log("scanData SEND" + scanData)

    if (Validator.isJson(scanData)) {
        console.log("JSON")
        var resultJson = JSON.parse(scanData);

        if (resultJson.action) {
            console.log(resultJson.action)
            var actionInput = resultJson.action;
            actionInput = actionInput.toUpperCase();
            if (actionInput == "PAY") {
                useridInput.value = resultJson.address;
            }
        }

    } else {
        var resultData = scanData;
        // if (resultData.contains("https://tagcash.com/u/")) {

        if (resultData.indexOf("https://tagcash.com/u/") >= 0) {
            var scanUserId = resultData.replace("https://tagcash.com/u/", '');
            console.log(" scanUserId " + scanUserId);
            useridInput.value = "U" + scanUserId;
        } else if (resultData.indexOf("https://tagcash.com/c/") >= 0) {
            var scanCommunityId = resultData.replace("https://tagcash.com/c/", '');
            console.log(" scanCommunityId " + scanCommunityId);
            useridInput.value = scanCommunityId;
        } else if (checkIfAddress(resultData)) {
            //address
            useridInput.value = resultData;
        }
    }

}


function checkIfAddress(addressInput) {

    console.log("checkIfAddress ")
    if (addressInput.length > 20) {
        console.log(">20")
        return isAlphaNumeric(addressInput);
    } else {
        return false;
    }
}

function isAlphaNumeric(str) {
    var code, i, len;

    for (i = 0, len = str.length; i < len; i++) {
        code = str.charCodeAt(i);
        if (!(code > 47 && code < 58) && // numeric (0-9)
            !(code > 64 && code < 91) && // upper alpha (A-Z)
            !(code > 96 && code < 123)) { // lower alpha (a-z)
            return false;
        }
    }
    return true;
};


function validateId(id) {
    if (/^\d+$/.test(id)) {
        console.log("Is id");
        return (true);
    }
    console.log("Is not id");
    return (false);
}





function loadBanksListCall() {
    console.log("loadUserEnergyListCall")
    bankItems.clear();
    bankSelectIndex.value = -1;
    bankFeeSelect.value = "";

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.method = "cash_out";

    _WebService.request("bank/list", apiBodyObj).then(function (result) {
        gotBanksListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBanksListResult(result) {
    console.log("gotBanksListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {

            if (walletId != "1") {
                resultArr.push({ bank_name: "other", "bank_full_name": "Other", });
            }

            bankItems.addAll(resultArr);

            bankSelectIndex.value = 0;
            bankFeeSelect.value = resultArr[0].fee;
            bankCodeName = resultArr[0].bank_name;
            bankFullName = resultArr[0].bank_full_name;

            loadBanksWithdrawCall();
        }

    }
}

function onBankSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    bankCodeName = selectedItem.bank_name;
    bankFullName = selectedItem.bank_full_name;

    if (bankCodeName == "other") {
        internationalCashoutSelected.value = true;
    } else {
        bankFeeSelect.value = selectedItem.fee;
        internationalCashoutSelected.value = false;
    }

    loadBanksWithdrawCall();

}

function loadBanksWithdrawCall() {
    console.log("loadBanksDepositCall")
    transactionBankListData.clear();
    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.bank_code = bankCodeName;
    apiBodyObj.wallet_id = walletId;

    _WebService.request("bank/payments", apiBodyObj).then(function (result) {
        gotBanksDepositListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

    // @Query("offset") int offset,
    // @Query("count") int count);-

}

function gotBanksDepositListResult(result) {
    console.log("gotBanksListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {

            transactionBankListData.addAll(resultArr);

        }

    }
}



function transactionBankCancel(args) {
    console.log("transactionBankCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("bank/DeleteRequest", apiBodyObj).then(function (result) {
        console.log("gotTransactionBankCancelResult")
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {
            transactionBankListData.remove(args.data);
            tagEvents.emit("toastShow", { message: "Deleted successfully" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}


function sendCashOut_clickHandler() {
    console.log("sendCashOut_clickHandler")

    var amountValue = withdrawAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    if (!withdrawAccountInput.value) {
        tagEvents.emit("toastShow", { message: "Account Number should not be empty" });
        return;
    }

    if (withdrawBeneficiaryNameInput.value == "") {
        tagEvents.emit("toastShow", { message: "Beneficiary Name should not be empty" });
        return;
    }

    if (bankCodeName == "other" && withdrawBankNameInput.value == "") {
        tagEvents.emit("toastShow", { message: "Please enter bank name" });
        return;
    }
    if (bankCodeName == "other" && withdrawBranchInput.value == "") {
        tagEvents.emit("toastShow", { message: "Branch should not be empty" });
        return;
    }
    if (bankCodeName == "other" && withdrawSwiftCodeInput.value == "") {
        tagEvents.emit("toastShow", { message: "Swift code should not be empty" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};

    apiBodyObj.amount = amountValue;
    apiBodyObj.bank_code = bankCodeName;
    apiBodyObj.banking_method = "otc";
    apiBodyObj.bank_account_number = withdrawAccountInput.value;
    apiBodyObj.beneficiary_name = withdrawBeneficiaryNameInput.value;
    apiBodyObj.wallet_id = walletId;

    if (bankCodeName == "other") {
        apiBodyObj.bank_name = withdrawBankNameInput.value;
        apiBodyObj.bank_branch = withdrawBranchInput.value;
        apiBodyObj.swift_code = withdrawSwiftCodeInput.value;
    } else {
        apiBodyObj.bank_name = bankFullName;
    }

    var currentdate = new Date();
    var secondsTime = currentdate.getTime()
    apiBodyObj.date = moment(secondsTime).format('YYYY-MM-DD');
    apiBodyObj.time = moment(secondsTime).format('h:m:s');


    _WebService.request("payment/bank", apiBodyObj).then(function (result) {
        gotBankCashOutResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBankCashOutResult(result) {
    console.log("gotBankCashOutResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("alertEvent", { type: "info", title: "Withdrawal", message: "Successfully submitted a withdrawal request. We have sent you a validation email. Simply click on the link in the email you receive from us.", callback: confirmWithdrawalClosedHandler });
    } else {
        if (arr.error == "verification_failed") {
            //kyc failed
            tagEvents.emit("alertEvent", { type: "info", title: "KYC status", message: "Must be KYC verified to carry out this transaction." });
        } else if (arr.error == "insufficent_balance") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else if (arr.error == "insufficient_amount") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.unable_process_message });
        }
    }
}

function confirmWithdrawalClosedHandler(args) {
    router.goBack();
}

function loadCentersListCall() {
    console.log("loadCentersListCall")
    remittanceCenterItems.clear();
    busy.activate();

    _WebService.request("remittanceCenters/list").then(function (result) {
        gotCentersListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCentersListResult(result) {
    console.log("gotCentersListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {

            remittanceCenterItems.addAll(resultArr);

            centerSelectIndex.value = -1;
            centerSelectIndex.value = 0;
            remittanceSelectId = resultArr[0].id;

            loadZonesListCall();
        }

    }
}

function onCentersSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    remittanceSelectId = selectedItem.id;
    loadZonesListCall();
}

function loadZonesListCall() {
    console.log("loadZonesListCall")
    if (remittanceSelectId == "1") {
        zoneListShow.value = true;
        remittanceZonesItems.clear();
        busy.activate();

        var apiBodyObj = {};

        apiBodyObj.rid = remittanceSelectId;

        _WebService.request("remittanceCenters/getZones", apiBodyObj).then(function (result) {
            gotZonesListResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        zoneListShow.value = false;
        loadRemittanceWithdrawCall();
    }
}

function gotZonesListResult(result) {
    console.log("gotZonesListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;

        remittanceZonesItems.addAll(resultArr);

        zonesSelectIndex.value = -1;
        zonesSelectIndex.value = 0;
        zoneSelectValue = resultArr[0].value;

        loadRemittanceWithdrawCall();

    }
}

function onZonesSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    zoneSelectValue = selectedItem.value;
}

function loadRemittanceWithdrawCall() {
    console.log("loadRemittanceWithdrawCall")
    transactionRemittanceListData.clear();
    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.remittance_id = remittanceSelectId;
    // apiBodyObj.depositor_id = _model.userprofileDetails.value.id;

    _WebService.request("remittanceCenters/payments", apiBodyObj).then(function (result) {
        gotRemittanceWithdrawListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

    // @Query("offset") int offset,
    // @Query("count") int count);-

}

function gotRemittanceWithdrawListResult(result) {
    console.log("gotRemittanceWithdrawListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {

            transactionRemittanceListData.addAll(resultArr);

        }

    }
}

function transactionRemittanceCancel(args) {
    console.log("transactionRemittanceCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("bank/DeleteRequest", apiBodyObj).then(function (result) {
        console.log("got RemittanceCancel Result")
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {
            transactionRemittanceListData.remove(args.data);
            tagEvents.emit("toastShow", { message: "Deleted successfully" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function remittanceAmountChanged() {
    calculatedFeeAmount.value = 0;
}

function calculateFeeHandler() {
    var amountValue = withdrawRemittanceAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();
    var apiBodyObj = {};

    if (remittanceSelectId == "1") {
        apiBodyObj.zone = zoneSelectValue;
    }
    apiBodyObj.rid = remittanceSelectId;
    apiBodyObj.amount = amountValue;

    _WebService.request("remittanceCenters/getFee", apiBodyObj).then(function (result) {
        gotCalculateFeeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCalculateFeeResult(result) {
    console.log("gotCalculateFeeResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        calculatedFeeAmount.value = resultObj.fee;

        var amountValue = withdrawRemittanceAmountInput.value;
        amountValue = amountValue.replace(/,/g, "");

        amountDeductTotal.value = Number(amountValue) + Number(resultObj.fee);

    } else {
        if (arr.error == "cashout_limit_exceeded") {
            tagEvents.emit("alertEvent", { type: "info", title: "Transaction limit", message: "Requested amount is more than one time transaction limit for this remittance center. Possible amount is "+arr.cashout_limit+" PHP" });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.unable_process_message });
        }
    }
}


function sendCashOutRemittance_clickHandler() {
    console.log("sendCashOutRemittance_clickHandler")
    var amountValue = withdrawRemittanceAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }
    if (!Validator.amount(calculatedFeeAmount.value)) {
        tagEvents.emit("toastShow", { message: "Fee should not be empty" });
        return;
    }
    if (!namePickupInput.value) {
        tagEvents.emit("toastShow", { message: loc.value.name_should_not_empty });
        return;
    }
    if (!numberPickupInput.value) {
        tagEvents.emit("toastShow", { message: loc.value.mobileno_should_not_empty });
        return;
    }

    busy.activate();

    var apiBodyObj = {};

    var currentdate = new Date();
    var secondsTime = currentdate.getTime();

    apiBodyObj.date = moment(secondsTime).format('YYYY-MM-DD');
    apiBodyObj.time = moment(secondsTime).format('h:m:s');
    apiBodyObj.amount = amountValue;
    apiBodyObj.fee = calculatedFeeAmount.value;
    apiBodyObj.remittance_id = remittanceSelectId;
    apiBodyObj.pickup_name = namePickupInput.value;
    apiBodyObj.pickup_mobile = numberPickupInput.value;
    apiBodyObj.type = "deposit";

    _WebService.request("payment/remittanceCenter", apiBodyObj).then(function (result) {
        gotRemittanceCashOutResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotRemittanceCashOutResult(result) {
    console.log("gotRemittanceCashOutResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Successfully submitted a withdrawal request." });
        router.goBack();
    } else {
        if (arr.error == "verification_failed") {
            //kyc failed
            tagEvents.emit("alertEvent", { type: "info", title: "KYC status", message: "Must be KYC verified to carry out this transaction." });
        } else if (arr.error == "insufficent_balance") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else if (arr.error == "insufficient_amount") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.unable_process_message });
        }
    }
}

var conversionInputAmount = 1;


function calculateBTCHandler() {
    console.log("calculateBTCHandler")

    var amountValue = withdrawBitcoinAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }
    conversionInputAmount = amountValue;

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.from = "php";
    apiBodyObj.to = "btc";

    _WebService.request("wallet/converter", apiBodyObj).then(function (result) {
        gotBtcCalculateResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBtcCalculateResult(result) {
    console.log("gotBtcCalculateResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    if (arr.status == "success") {
        var resultObj = arr.result;

        bitcoinAmountCalculated.value = (conversionInputAmount * resultObj.price).toString();
    } else {
        bitcoinAmountCalculated.value = "";
    }

}

function qrscan_clickHandler_btc() {
    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandlerBtc });
}

function qrReceivedHandlerBtc(args) {
    withdrawBitcoinAddressInput.value = args;
}

function sendCashOutBitcoin_clickHandler() {
    console.log("sendCashOutBitcoin_clickHandler")

    busy.activate();

    var amountValue = withdrawBitcoinAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    var apiBodyObj = {};

    apiBodyObj.bitcoin_address = withdrawBitcoinAddressInput.value;
    apiBodyObj.amount = amountValue;
    apiBodyObj.bitcoin_rate = bitcoinAmountCalculated.value;
    apiBodyObj.wallet_id = 0;

    _WebService.request("payment/bitcoin", apiBodyObj).then(function (result) {
        gotBitcoinCashOutResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBitcoinCashOutResult(result) {
    console.log("gotRemittanceCashOutResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        router.goBack();
    }
}



//Stellar

function stellerAddressInputChange(args) {
    console.log(JSON.stringify(args))
    console.log(stellarAmountInput.value)
    var addressInputValue = stellarAddressInput.value;

    if (addressInputValue.indexOf("*test.tagcash.com") !== -1 || addressInputValue.indexOf("*tagcash.com") !== -1) {
        console.log("Check address")
        busy.activate();

        var apiBodyObj = {};
        apiBodyObj.federated_address = addressInputValue;

        _WebService.request("stellar/ResolveFederatedAddress", apiBodyObj).then(function (result) {
            gotResolveFederatedAddressResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        memoTextEditable.value = true;
    }
}

function gotResolveFederatedAddressResult(result) {
    console.log("gotResolveFederatedAddressResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        stellarAddressResolvedShow.value = resultObj.address;
        memoInputTxt.value = resultObj.memo;
        memoTextEditable.value = false;
    } else {
        stellarAddressResolvedShow.value = "";
        memoInputTxt.value = "";
        memoTextEditable.value = true;
        // if (arr.error == "not_found") {
        //     tagEvents.emit("toastShow", { message: "Invalid address" });
        // } 
    }
}



function stellarAuthoriseClickHandler() {

    if (stellarAuthoriseAddressInput.value == "") {
        tagEvents.emit("toastShow", { message: "Address should not be empty" });
        return;
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.trustor = stellarAuthoriseAddressInput.value;
    // apiBodyObj.trustor = "GBYGH2Y4JWWH3RCSYR33YDELHQIMDYTT4QO4LX2LM5HXRQKA65VYJBSP";
    apiBodyObj.assetCode = currencyCode.value;
    apiBodyObj.authorize = "true";


    _WebService.request("stellar/Authorize", apiBodyObj).then(function (result) {
        gotStellarAuthoriseAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotStellarAuthoriseAddressResult(result) {
    console.log("gotStellarAuthoriseAddressResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        tagEvents.emit("toastShow", { message: "Address authorised successfully" });
        stellarAuthoriseAddressInput.value = "";
    } else {
        // if (arr.error == "not_found") {
        // } 
    }
}






function transferStellar_clickHandler() {
    var amountValue = stellarAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    var addressInputValue = stellarAddressInput.value;

    if (addressInputValue === "") {
        tagEvents.emit("toastShow", { message: "Address should not be empty" });
        return;
    }

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    if (addressInputValue.indexOf("*") !== -1) {
        toStellerTransferAddress = addressInputValue;
        confirmStellerAddressAlertShow();
    } else if (checkIfAddress(addressInputValue)) {
        toStellerTransferAddress = addressInputValue;
        confirmStellerAddressAlertShow();
    } else {
        return;
    }

}

function confirmStellerAddressAlertShow() {
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", stellarAmountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", toStellerTransferAddress);
    tagEvents.emit("alertEvent", { type: "confirm", title: loc.value.transfer, message: messageText, callback: confirmSendStellerAddressClosedHandler });
}

function confirmSendStellerAddressClosedHandler(args) {
    if (args === "yes") {
        transferAamoundToStellerAddress();
    }
}

function transferAamoundToStellerAddress() {
    var amountValue = stellarAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.to_wallet_id = walletId;
    apiBodyObj.narration = memoInputTxt.value;
    apiBodyObj.to_crypto_address = toStellerTransferAddress;

    _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
        gotTransferStellerAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransferStellerAddressResult(result) {
    console.log("gotTransferStellerAddressResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmountSteller.value = stellarAmountInput.value + " " + currencyCode.value;
        resultUserSteller.value = toStellerTransferAddress;
        sendStateStellarGroup.goto(resultStellarState);
    } else {
        if (arr.error == "invalid_crypto_address") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Invalid address" });
        } else {
            TransferError.errorHandle(arr);
        }
    }
}





function startImmediateLocation() {
    GeoLocation.getLocation(timeoutMs).then(function (location) {
        console.log("location : " + JSON.stringify(location));
        locationAvaialble.value = true;
        latitude = location.latitude;
        longitude = location.longitude;
    }).catch(function (fail) {
        console.log("getLocation fail " + fail);
        locationAvaialble.value = false;
        nearbyAgentLoad.value = false;
    });
}

nearbyAgentLoad.onValueChanged(module, function (selectStatus) {
    console.log("nearbyAgentLoad " + JSON.stringify(selectStatus));

    if (withdrawType.value === "agentcashout") {
        loadAgentsListCall();
    }

})

function loadAgentsListCall() {
    console.log("loadAgentsListCall")
    agentCenterList.clear();
    busy.activate();

    var apiBodyObj = {};
    if (nearbyAgentLoad.value == true) {
        apiBodyObj.lat = latitude;
        apiBodyObj.lng = longitude;

        _WebService.request("Agent/GetNearestAgents", apiBodyObj).then(function (result) {
            gotAgentListNearResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        // apiBodyObj.page_count = 40;
        // apiBodyObj.page_offset = 0;

        _WebService.request("Agent/GetAllAgentLocations", apiBodyObj).then(function (result) {
            gotAgentListAllResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    }
}

function gotAgentListNearResult(result) {
    console.log("gotAgentListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            agentCenterList.addAll(resultArr);

        }

    }
}

function gotAgentListAllResult(result) {
    console.log("gotAgentListAllResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            agentCenterList.addAll(resultArr);

        }

    }
}


var agentDetailPopupShow = Observable(false);

var selectAgentName = Observable("");
var selectAgentAddress = Observable("");
var selectAgentWorkDetails = Observable();
var selectAgentMaxCash = Observable("");
var selectAgentLatitude = Observable("");
var selectAgentLongitude = Observable("");

function agentDetailClickHandle(args) {
    console.log(JSON.stringify(args.data))
    var agentData = args.data;

    selectAgentName.value = agentData.location_name;
    selectAgentAddress.value = agentData.address;
    selectAgentWorkDetails.clear();
    selectAgentWorkDetails.addAll(agentData.work_details);

    selectAgentMaxCash.value = agentData.max_cashout;

    selectAgentLatitude.value = agentData.latitude;
    selectAgentLongitude.value = agentData.longitude;


    agentDetailPopupShow.value = true;
}

function direction_clickHandler() {
    // Maps.openAt(selectAgentLatitude.value, selectAgentLongitude.value);
}

//Gofer

function loadGoferHistoryListCall() {
    console.log("loadGoferHistoryListCall")
    transactionGoferListData.clear();
    busy.activate();

    var apiBodyObj = {};
    // apiBodyObj.page_count = 40;
    // apiBodyObj.page_offset = 0;

    _WebService.request("GoferDelivery/ListRequests", apiBodyObj).then(function (result) {
        gotGoferHistoryListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotGoferHistoryListResult(result) {
    console.log("gotAgentListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            transactionGoferListData.addAll(resultArr);
        }

    }
}

function goferAmountChanged() {
    calculatedGoferFeeAmount.value = 0;
}

function calculateGoferFeeHandler() {
    var amountValue = withdrawGoferAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    // apiBodyObj.amount = amountValue;

    _WebService.request("GoferDelivery/GetMoneyDeliveryFee", apiBodyObj).then(function (result) {
        gotCalculateGoferFeeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCalculateGoferFeeResult(result) {
    console.log("gotCalculateGoferFeeResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        calculatedGoferFeeAmount.value = resultObj;

        var amountValue = withdrawGoferAmountInput.value;
        amountValue = amountValue.replace(/,/g, "");

        amountGoferDeductTotal.value = Number(amountValue) + Number(calculatedGoferFeeAmount.value);
    }
}

function sendCashOutGofer_clickHandler() {
    console.log("sendCashOutRemittance_clickHandler")
    var amountValue = withdrawGoferAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }
    if (addressGoferInput.value == "") {
        tagEvents.emit("toastShow", { message: "Address should not be empty" });
        return;
    }

    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.amount = amountValue;
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.delivery_address = addressGoferInput.value;
    apiBodyObj.note = notesGoferInput.value;

    _WebService.request("GoferDelivery/OrderMoney", apiBodyObj).then(function (result) {
        gotGoferCashOutResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotGoferCashOutResult(result) {
    console.log("gotGoferCashOutResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Successfully submitted a withdrawal request." });
        router.goBack();
    } else {
        if (arr.error == "verification_failed") {
            //kyc failed
            tagEvents.emit("alertEvent", { type: "info", title: "KYC status", message: "Must be KYC verified to carry out this transaction." });
        } else if (arr.error == "insufficient_balance_to_pay_the_amount") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.unable_process_message });
        }
    }
}

function transactionGoferCancel(args) {
    console.log("transactionGoferCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.request_id = args.data.id;

    _WebService.request("GoferDelivery/DeleteGoferMoneyRequest", apiBodyObj).then(function (result) {
        gotGoferTransactionCancelResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotGoferTransactionCancelResult(result) {
    console.log("gotGoferTransactionCancelResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        tagEvents.emit("toastShow", { message: "Deleted successfully" });
        loadGoferHistoryListCall();
    } else {
        if (arr.error == "only_pending_request_can_be_delete") {
            tagEvents.emit("toastShow", { message: "Only pending requests can be delete" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}



var dragonbankItems = Observable();
var dragonbankSelectIndex = Observable(0);
var dragonbankFeeSelect = Observable("");

var dragonbankCodeName;
var dragonbankFullName;
var transactionDragonBankListData = Observable();

var withdrawDragonAmountInput = Observable("");
var withdrawDragonAccountInput = Observable("");
var withdrawDragonReceiveEmailInput = Observable("");
var withdrawDragonReceiveMobileInput = Observable("");


function loadDragonBanksListCall() {
    console.log("loadDragonBanksListCall")
    dragonbankItems.clear();
    dragonbankSelectIndex.value = -1;
    dragonbankFeeSelect.value = "";

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.method = "cash_out";

    _WebService.request("bank/DragonpayBankList", apiBodyObj).then(function (result) {
        gotDragonBanksListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDragonBanksListResult(result) {
    console.log("gotDragonBanksListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {
            dragonbankItems.addAll(resultArr);

            dragonbankSelectIndex.value = 0;
            dragonbankFeeSelect.value = resultArr[0].transaction_fee;
            dragonbankCodeName = resultArr[0].bank_name;
            dragonbankFullName = resultArr[0].bank_full_name;

            loadDragonBanksWithdrawCall();
        }

    }
}

function onDragonBankSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    dragonbankCodeName = selectedItem.bank_name;
    dragonbankFullName = selectedItem.bank_full_name;

    dragonbankFeeSelect.value = selectedItem.transaction_fee;
}

function loadDragonBanksWithdrawCall() {
    console.log("loadDragonBanksWithdrawCall")
    transactionDragonBankListData.clear();
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;

    _WebService.request("payment/DragonpayCashOutList", apiBodyObj).then(function (result) {
        gotBanksDragonDepositListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

    // @Query("offset") int offset,
    // @Query("count") int count);-

}

function gotBanksDragonDepositListResult(result) {
    console.log("gotBanksDragonDepositListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data.requests;

        if (resultArr !== null) {
            transactionDragonBankListData.addAll(resultArr);
        }
    }
}


function transactionDragonBankCancel(args) {
    console.log("transactionDragonBankCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("payment/DeleteDragonPayCashoutRequest", apiBodyObj).then(function (result) {
        console.log("gotTransaction Dragon BankCancelResult")
        busy.deactivate();

        var arr = result;
        console.log(JSON.stringify(arr))

        if (arr.status == "success") {
            transactionDragonBankListData.remove(args.data);
            tagEvents.emit("toastShow", { message: "Deleted successfully" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}


function sendCashOutDragonHandler() {
    console.log("sendCashOutDragonHandler")

    var amountValue = withdrawDragonAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    if (!withdrawDragonAccountInput.value) {
        tagEvents.emit("toastShow", { message: "Account Number should not be empty" });
        return;
    }

    if (withdrawDragonReceiveEmailInput.value == "") {
        tagEvents.emit("toastShow", { message: "Email should not be empty" });
        return;
    }
    if (withdrawDragonReceiveMobileInput.value == "") {
        tagEvents.emit("toastShow", { message: "Mobile number should not be empty" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.cashout_recepient_bank_code = dragonbankCodeName;
    apiBodyObj.cashout_recepient_bank_account = withdrawDragonAccountInput.value;

    apiBodyObj.cashout_recepient_email = withdrawDragonReceiveEmailInput.value;
    apiBodyObj.cashout_recepient_mobile_no = withdrawDragonReceiveMobileInput.value;
    apiBodyObj.fee = 0;

    _WebService.request("payment/DragonPayCashoutRequest", apiBodyObj).then(function (result) {
        gotDragonBankCashOutResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotDragonBankCashOutResult(result) {
    console.log("gotDragonBankCashOutResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("alertEvent", { type: "info", title: "Withdrawal", message: "Successfully submitted a withdrawal request.", callback: confirmDragonWithdrawalClosedHandler });
    } else {
        if (arr.error == "verification_failed") {
            //kyc failed
            tagEvents.emit("alertEvent", { type: "info", title: "KYC status", message: "Must be KYC verified to carry out this transaction." });
        } else if (arr.error == "insufficent_balance") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else if (arr.error == "insufficient_amount") {
            tagEvents.emit("toastShow", { message: "Insufficent balance" });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.unable_process_message });
        }
    }
}

function confirmDragonWithdrawalClosedHandler(args) {
    router.goBack();
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    useridInput: useridInput,
    amountInput: amountInput,
    notesTxt: notesTxt,
    transferButton_clickHandler: transferButton_clickHandler,
    reloadTransactionsHandler: reloadTransactionsHandler,
    sendAgainHandler: sendAgainHandler,
    resultAmount: resultAmount,
    resultUser: resultUser,
    currencyCode: currencyCode,

    searchUser_clickHandler: searchUser_clickHandler,
    searchUserPopupShow: searchUserPopupShow,
    onUserSelectedEvent: onUserSelectedEvent,

    qrscan_clickHandler: qrscan_clickHandler,
    walletOptionChangedEvent: walletOptionChangedEvent,

    wallectChangable: wallectChangable,

    withdrawDropdownSelect: withdrawDropdownSelect,

    withdrawTypeItems: withdrawTypeItems,
    withdrawTypeIndex: withdrawTypeIndex,
    withdrawType: withdrawType,
    onWithdrawTypeSelectionChange: onWithdrawTypeSelectionChange,

    bankItems: bankItems,
    bankSelectIndex: bankSelectIndex,
    bankFeeSelect: bankFeeSelect,
    onBankSelectionChange: onBankSelectionChange,

    remittanceCenterItems: remittanceCenterItems,
    zoneListShow: zoneListShow,
    centerSelectIndex: centerSelectIndex,
    onCentersSelectionChange: onCentersSelectionChange,

    remittanceZonesItems: remittanceZonesItems,
    zonesSelectIndex: zonesSelectIndex,
    onZonesSelectionChange: onZonesSelectionChange,

    transactionBankListData: transactionBankListData,
    transactionRemittanceListData: transactionRemittanceListData,
    transactionBitcoinListData: transactionBitcoinListData,

    transactionBankCancel: transactionBankCancel,
    transactionRemittanceCancel: transactionRemittanceCancel,

    withdrawAmountInput: withdrawAmountInput,
    withdrawAccountInput: withdrawAccountInput,
    withdrawBranchInput: withdrawBranchInput,
    withdrawBankNameInput: withdrawBankNameInput,
    withdrawBeneficiaryNameInput: withdrawBeneficiaryNameInput,
    withdrawSwiftCodeInput: withdrawSwiftCodeInput,
    sendCashOut_clickHandler: sendCashOut_clickHandler,


    withdrawRemittanceAmountInput: withdrawRemittanceAmountInput,
    remittanceAmountChanged: remittanceAmountChanged,
    calculateFeeHandler: calculateFeeHandler,
    calculatedFeeAmount: calculatedFeeAmount,
    amountDeductTotal: amountDeductTotal,
    namePickupInput: namePickupInput,
    numberPickupInput: numberPickupInput,
    sendCashOutRemittance_clickHandler: sendCashOutRemittance_clickHandler,



    withdrawBitcoinAmountInput: withdrawBitcoinAmountInput,
    withdrawBitcoinAddressInput: withdrawBitcoinAddressInput,
    bitcoinAmountCalculated: bitcoinAmountCalculated,
    calculateBTCHandler: calculateBTCHandler,


    qrscan_clickHandler_btc: qrscan_clickHandler_btc,


    transferStellar_clickHandler: transferStellar_clickHandler,
    stellarAddressInput: stellarAddressInput,
    memoTextEditable: memoTextEditable,
    memoInputTxt: memoInputTxt,
    stellarAmountInput: stellarAmountInput,

    resultAmountSteller: resultAmountSteller,
    resultUserSteller: resultUserSteller,
    stellarAddressResolvedShow: stellarAddressResolvedShow,
    stellerAddressInputChange: stellerAddressInputChange,

    stellarAuthorisePossible: stellarAuthorisePossible,
    stellarAuthoriseAddressInput: stellarAuthoriseAddressInput,
    stellarAuthoriseClickHandler: stellarAuthoriseClickHandler,

    internationalCashoutSelected: internationalCashoutSelected,

    agentCenterList: agentCenterList,
    locationAvaialble: locationAvaialble,
    nearbyAgentLoad: nearbyAgentLoad,
    agentDetailClickHandle: agentDetailClickHandle,

    selectAgentName: selectAgentName,
    selectAgentAddress: selectAgentAddress,
    selectAgentWorkDetails: selectAgentWorkDetails,
    selectAgentMaxCash: selectAgentMaxCash,
    selectAgentLatitude: selectAgentLatitude,
    selectAgentLongitude: selectAgentLongitude,
    agentDetailPopupShow: agentDetailPopupShow,
    direction_clickHandler: direction_clickHandler,


    transactionGoferListData: transactionGoferListData,
    goferAmountChanged: goferAmountChanged,
    calculateGoferFeeHandler: calculateGoferFeeHandler,
    sendCashOutGofer_clickHandler: sendCashOutGofer_clickHandler,
    withdrawGoferAmountInput: withdrawGoferAmountInput,
    calculatedGoferFeeAmount: calculatedGoferFeeAmount,
    amountGoferDeductTotal: amountGoferDeductTotal,
    addressGoferInput: addressGoferInput,
    notesGoferInput: notesGoferInput,
    transactionGoferCancel: transactionGoferCancel,


    dragonbankItems: dragonbankItems,
    dragonbankSelectIndex: dragonbankSelectIndex,
    dragonbankFeeSelect: dragonbankFeeSelect,
    onDragonBankSelectionChange: onDragonBankSelectionChange,
    transactionDragonBankListData: transactionDragonBankListData,
    transactionDragonBankCancel: transactionDragonBankCancel,
    sendCashOutDragonHandler: sendCashOutDragonHandler,
    withdrawDragonAmountInput: withdrawDragonAmountInput,
    withdrawDragonAccountInput: withdrawDragonAccountInput,
    withdrawDragonReceiveEmailInput: withdrawDragonReceiveEmailInput,
    withdrawDragonReceiveMobileInput: withdrawDragonReceiveMobileInput,
}
