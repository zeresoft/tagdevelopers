var Observable = require('FuseJS/Observable');
var StringFormat = require('Utils/StringFormat');
var clip = require("ClipboardManager");

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletId;
var transactionListData = Observable();
var currencyCode = Observable();
var walletNameTitle = Observable("");
var isLoading = Observable(false);

this.Parameter.onValueChanged(module, function (param) {
    walletId = param.walletId;
    currencyCode.value = param.currencyCode;
    // walletNameTitle.value = param.walletName + " (" + param.currencyCode + ")"
    walletNameTitle.value = param.walletName;

    transactionListData.clear();
    loadUserEnergyListCall();
})


function endLoading() {
    isLoading.value = false;
}

function reloadHandler() {
    console.log("reloadHandler-----------------")
    transactionListData.clear();

    isLoading.value = true;
    loadUserEnergyListCall();
}

function pagingLoadMore() {
    if (transactionListData.length != 0) {
        loadUserEnergyListCall();
    }
}

function loadUserEnergyListCall() {
    console.log("loadUserEnergyListCall")
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.count = 20;
    apiBodyObj.offset = transactionListData.length;

    _WebService.request("wallet/transactions", apiBodyObj).then(function (result) {
        gotLoadEnergyListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotLoadEnergyListResult(result) {
    console.log("gotLoadEnergyListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))
    endLoading();

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            transactionListData.addAll(resultArr);
        }

    }
}

function walletTransItemClickHandle(args) {
    console.log(JSON.stringify(args.data.narration));

    clip.setText(args.data.narration);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    currencyCode: currencyCode,
    walletNameTitle: walletNameTitle,

    transactionListData: transactionListData,
    pagingLoadMore: pagingLoadMore,
    walletTransItemClickHandle: walletTransItemClickHandle,
    isLoading: isLoading,
    reloadHandler: reloadHandler,
}