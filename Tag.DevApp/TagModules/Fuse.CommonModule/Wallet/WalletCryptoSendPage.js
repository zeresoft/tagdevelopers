var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var TransferError = require('Utils/TransferError');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletId;
var walletName = Observable();
var currencyCode = Observable();
var walletType = Observable();

var sendTypeItems = Observable({ index: 0, name: "Tagcash User", value: "tagcash" }, { index: 1, name: "External Address", value: "external" });
var sendTypeIndex = Observable(0);

var recipientTypeItems = Observable({ name: loc.value.user, value: "user" }, { name: loc.value.merchant, value: "merchant" });
var recipientTypeIndex = Observable(0);



var useridInput = Observable("");
var amountInput = Observable();
var notesTxt = Observable("");
var addressInput = Observable();

var resultAmount = Observable("");
var resultUser = Observable("");

var toTransferId;
var toTransferName;
var toTransferEmail;

this.Parameter.onValueChanged(module, function (param) {

    sendAgainHandler();

    console.log(JSON.stringify(param));
    walletId = param.walletId;
    walletName.value = param.walletName;
    currencyCode.value = param.currencyCode;
    walletType.value = param.walletType;

    sendTypeIndex.value = 0;
    recipientTypeIndex.value = 0;
    if (param.fromScan) {
        console.log("************** from scan");
        if (param.recipientType == 2) {
            //community
            recipientTypeIndex.value = 1;
        } else {
            recipientTypeIndex.value = 0;
        }

        useridInput.value = param.user_id;
        amountInput.value = param.amount;
    }

})

function onSendTypeChangeEvent(args) {
    console.log(JSON.stringify(args));

}

function onRecipientTypeSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))
    recipientTypeIndex.value = selectedItem.indexLocal;

}

var searchUserPopupShow = Observable(false);
var searchMerchantStatus = Observable(false);

function searchUser_clickHandler() {
    console.log("recipientTypeIndex.value - " + recipientTypeIndex.value)
    if (recipientTypeIndex.value == 0) {
        searchMerchantStatus.value = false;
    } else {
        searchMerchantStatus.value = true;
    }
    searchUserPopupShow.value = true;
}

function onUserSelectedEvent(args) {
    console.log(args.selectedId)
    console.log(args.selectedName)

    useridInput.value = args.selectedId;
    searchUserPopupShow.value = false;
}


function transferButton_clickHandler() {
    var amountValue = amountInput.value;
    var useridInputValue = useridInput.value;

    if (useridInputValue === "") {
        return;
    }

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    var apiBodyObj = {};

    if (recipientTypeIndex.value == 0) {
        var checkIdInt = useridInputValue;
        var sendToMail = false;

        if (!validateId(checkIdInt)) {
            if (Validator.email(checkIdInt)) {
                sendToMail = true;
                toTransferEmail = checkIdInt;
            } else {
                return;
            }
        } else {
            apiBodyObj.id = checkIdInt;
        }

        if (sendToMail) {
            confirmEmailAlertShow();
        } else {
            busy.activate();
            _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
                gotValidateUserResult(result);
            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });
        }

    } else {
        if (!isNaN(checkIdInt)) {
            return;
        }

        busy.activate();

        apiBodyObj.name = useridInputValue;

        _WebService.request("community/searchNew", apiBodyObj).then(function (result) {
            gotValidateCommunityResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    }
}

function gotValidateUserResult(result) {
    console.log("gotValidateUserResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        toTransferId = resultArr[0].id;
        toTransferName = resultArr[0].name;
        confirmAlertShow();
    } else {
        if (arr.result == "no_users") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "The ID or email that you entered is not valid." });
        }
    }
}

function gotValidateCommunityResult(result) {
    console.log("gotValidateCommunityResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            toTransferId = resultArr[0].id;
            toTransferName = resultArr[0].community_name;
            confirmAlertShow();
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "The ID you entered is not valid." });
        }
    }
}

function confirmAlertShow() {
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", amountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", toTransferName);
    tagEvents.emit("alertEvent", { type: "confirm", title: loc.value.transfer, message: messageText, callback: confirmSendAlertClosedHandler });
}

function confirmSendAlertClosedHandler(args) {
    if (args === "yes") {
        transferAamoundToId();
    }
}

function transferAamoundToId() {
    var amountValue = amountInput.value;

    var toType = "user";
    if (recipientTypeIndex.value != 0) {
        toType = "community";
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.to_type = toType;
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.to_wallet_id = walletId;
    apiBodyObj.narration = notesTxt.value;
    apiBodyObj.to_id = toTransferId;

    _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
        gotTransferTagcoinResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransferTagcoinResult(result) {
    console.log("gotTransferTagcoinResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmount.value = amountInput.value + " " + currencyCode.value;
        resultUser.value = toTransferName;
        sendStateGroup.goto(resultState);
    } else {
        TransferError.errorHandle(arr);
    }
}



function confirmEmailAlertShow() {
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", amountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", toTransferEmail);
    tagEvents.emit("alertEvent", { type: "confirm", title: loc.value.transfer, message: messageText, callback: confirmSendEmailClosedHandler });
}

function confirmSendEmailClosedHandler(args) {
    if (args === "yes") {
        transferAamoundToEmail();
    }
}

function transferAamoundToEmail() {
    var amountValue = amountInput.value;

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.from_wallet_id = walletId;
    apiBodyObj.to_wallet_id = walletId;
    apiBodyObj.narration = notesTxt.value;
    apiBodyObj.to_email = toTransferEmail;

    _WebService.request("wallet/transfer", apiBodyObj).then(function (result) {
        gotTransferEmailResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransferEmailResult(result) {
    console.log("gotTransferEmailResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmount.value = amountInput.value + " " + currencyCode.value;
        resultUser.value = toTransferEmail;
        sendStateGroup.goto(resultState);
    } else {
        TransferError.errorHandle(arr);
    }
}


function reloadTransactionsHandler() {
    router.goBack();
}

function sendAgainHandler() {
    recipientTypeIndex.value = 0;
    // useridInput.value = "";
    amountInput.value = "";
    notesTxt.value = "";
    // addressInput.value = "";

    sendStateGroup.goto(initalState);
}

function sendButton_clickHandler() {
    if (!Validator.amount(amountInput.value)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }
    var messageText = loc.value.send_confirm_message;

    messageText = messageText.replace("%@1", amountInput.value + " " + currencyCode.value);
    messageText = messageText.replace("%@2", addressInput.value);
    tagEvents.emit("alertEvent", { type: "confirm", title: "SEND", message: messageText, callback: confirmExternalSendClosedHandler });
}

function confirmExternalSendClosedHandler(args) {
    if (args === "yes") {
        sendCashOutBitcoin();
    }
}

function sendCashOutBitcoin() {
    console.log("sendCashOutBitcoin_clickHandler")

    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.bitcoin_address = addressInput.value;
    apiBodyObj.amount = amountInput.value;
    apiBodyObj.bitcoin_rate = amountInput.value;
    apiBodyObj.wallet_id = 2;

    _WebService.request("payment/bitcoin", apiBodyObj).then(function (result) {
        gotBitcoinCashOutResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBitcoinCashOutResult(result) {
    console.log("gotRemittanceCashOutResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        resultAmount.value = amountInput.value + " " + currencyCode.value;
        resultUser.value = addressInput.value;
        sendStateGroup.goto(resultState);
    } else {
        if (arr.error == "verification_failed") {
            //kyc failed
            tagEvents.emit("alertEvent", { type: "info", title: "KYC status", message: "Must be KYC verified to carry out this transaction." });
        } else if (arr.error == "balance low") {
            tagEvents.emit("toastShow", { message: loc.value.insufficient_funds });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.unable_process_message });
        }
    }
}


function qrscan_clickHandler() {
    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandler });

}

function qrReceivedHandler(args) {
    var scanData = args;
    if (sendTypeIndex.value == 0) {

        if (isNaN(scanData)) {
            if (scanData.indexOf("https://tagcash.com/u/") >= 0) {
                var scanUserId = scanData.replace("https://tagcash.com/u/", '');
                console.log(" scanUserId " + scanUserId);

                useridInput.value = scanUserId;
            } else if (scanData.indexOf("https://tagcash.com/c/") >= 0) {
                var scanCommunityId = scanData.replace("https://tagcash.com/c/", '');
                console.log(" scanCommunityId " + scanCommunityId);
                useridInput.value = scanCommunityId;
            }
        } else {
            useridInput.value = scanData;
        }
    } else {
        addressInput.value = scanData;
    }
}


function validateId(id) {
    if (/^\d+$/.test(id)) {
        console.log("Is id");
        return (true);
    }
    console.log("Is not id");
    return (false);
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    currencyCode: currencyCode,

    sendTypeItems: sendTypeItems,
    sendTypeIndex: sendTypeIndex,
    onSendTypeChangeEvent: onSendTypeChangeEvent,

    recipientTypeItems: recipientTypeItems,
    recipientTypeIndex: recipientTypeIndex,
    onRecipientTypeSelectionChange: onRecipientTypeSelectionChange,


    useridInput: useridInput,
    amountInput: amountInput,
    notesTxt: notesTxt,
    addressInput: addressInput,

    transferButton_clickHandler: transferButton_clickHandler,
    sendButton_clickHandler: sendButton_clickHandler,


    reloadTransactionsHandler: reloadTransactionsHandler,
    sendAgainHandler: sendAgainHandler,
    resultAmount: resultAmount,
    resultUser: resultUser,

    searchUser_clickHandler: searchUser_clickHandler,
    searchUserPopupShow: searchUserPopupShow,
    searchMerchantStatus: searchMerchantStatus,
    onUserSelectedEvent: onUserSelectedEvent,

    qrscan_clickHandler: qrscan_clickHandler,
}
