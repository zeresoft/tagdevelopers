var Observable = require('FuseJS/Observable');
var moment = require('Utils/moment');
var Validator = require('Utils/Validator');
var clip = require("ClipboardManager");

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletId;
var walletIdCheck = Observable("");
var walletName;
var currencyCode = Observable("");
var walletType = Observable();

var depositTypeItems = Observable();
var depositTypeIndex = Observable(0);
var depositType = Observable("");

var bankItems = Observable();
var bankSelectIndex = Observable(0);
var bankAccountNumber = Observable();
var bankAccountSwift = Observable();
var bankAccountName = Observable();
var bankAccountAddress = Observable();
var bankCodeName;

var depositMethodItems = Observable({ name: "Over the Counter", value: "otc" }, { name: "Online", value: "ot" });
var depositMethodIndex = Observable(0);
var depositMethodSelectable = Observable(true);
var depositMethodSelected = Observable("otc");

var depBitcoinItems = Observable({ index: 0, name: "RAW", value: "row" }, { index: 1, name: "TAGCASH", value: "tagcash" }, { index: 2, name: "BLOCKCHAIN.INFO", value: "blockchain_info" });
var depBitcoinItemsIndex = Observable(0);
var qrDataString = Observable();


var depositAmountInput = Observable();
var depositDragonpayAmountInput = Observable();
var depositSevenAmountInput = Observable();
var depositBitcoinAmountInput = Observable();
var depositTransactionidInput = Observable();
var localCurrencyValueDisplay = Observable();
var localBitcoinInputDisplay = Observable();

var transactionBankListData = Observable();
var transactionSevenListData = Observable();
var transactionBitcoinListData = Observable();

var depositDateSelect;
var depositTimeSelect;
var depositDateDisplay = Observable();

var depositTypeSelect = Observable(true);
var btcWalletAddress = "";
var slipUploadImageUrl = Observable();
var uploadImageData = "";
var imageSelectPopupShow = Observable(false);
var slipImageSelected = Observable(false);

var localValuePopupShow = Observable(false);
var selectNewTime;
var selectNewDispaly = "";

var popupShowDatePick = Observable(false);
var popupShowTimePick = Observable(false);


var amountInputTagcash = Observable();
var notesTxtTagcash = Observable();
var qrDataStringTagcash = Observable();
var dataObj = {};

var externalStepOne = Observable(true);
var externalStepTwo = Observable(false);
var registerAmountInput = Observable();
var txidInput = Observable();
var externalAddressQR = Observable("a1b458f88CA51100891db70FF95cd58D359D5072");
var registerTransactionid = "";

var historyDetailPopupShow = Observable(false);
var historyDetailListData = Observable();

var amountInputCrypto = Observable();
var externalAddressCryptoQR = Observable();
var cryptoQRVisible = Observable(false);


var addressEditPopupShow = Observable(false);
var addressAddedList = Observable();
var stellerAddressInput = Observable("");
var stellerAddressButton = Observable("SAVE");
var stellerAddressDomain = Observable("");
var stellerNameEditMode = false;
var stellerNameEditId = "";
var stellerAddressDefault = Observable("");
var memoTextDefault = Observable("");
var nemAddressDefault = Observable("");

var depositBankPossible = Observable(false);
var transactionDragonpayListData = Observable();

this.Parameter.onValueChanged(module, function (param) {
    console.log(JSON.stringify(param))
    stellerDefaultAddressLoad();

    stellerNameEditMode = false;
    stellerNameEditId = "";
    if (_model.activePerspectiveType == "community") {
        memoTextDefault.value = "C" + _model.nowCommunityID;
    } else {
        memoTextDefault.value = "U" + _model.userprofileDetails.value.id;
    }

    depositTypeIndex.value = -1;

    depositAmountInput.value = "";
    uploadImageData = "";
    slipImageSelected.value = false;
    depositTransactionidInput.value = "";
    depositSevenAmountInput.value = "";
    depositDragonpayAmountInput.value = "";

    depBitcoinItemsIndex.value = 0;
    depositBitcoinAmountInput.value = "";
    localValuePopupShow.value = false;
    qrDataString.value = "";
    btcWalletAddress = "";

    console.log(JSON.stringify(param));
    walletId = param.walletId;
    walletIdCheck.value = param.walletId;
    walletName = param.walletName;
    currencyCode.value = param.currencyCode;
    walletType.value = param.walletType;

    depositTypeSelect.value = true;
    depositTypeItems.clear();

    amountInputTagcash.value = "";
    notesTxtTagcash.value = "";
    qrDataStringTagcash.value = _model.singleCryptoAddresses;

    amountInputCrypto.value = "";
    cryptoQRVisible.value = false;

    if (walletType.value == "trading_account") {
        depositTypeSelect.value = false;
        depositType.value = "bank";
    } else {
        var ethereumPossible = false;

        var depositTypeArr = []
        depositTypeArr.push({ name: "Tagcash", value: "tagcash" });

        if (param.ethWallet == true) {
            ethereumPossible = true;
        }

        if (param.sellarWallet == true) {
            depositTypeArr.push({ name: "Stellar", value: "stellar" });
        }
        if (param.nemWallet == true) {
            depositTypeArr.push({ name: "NEM", value: "nem" });
        }


        if (param.walletTypeNumeric == 1) {
            //BTC,ETH,DASH,DOGE,LTC
            if (param.walletId == 2 || param.walletId == 675 || param.walletId == 681 || param.walletId == 822 || param.walletId == 834) {
                depositTypeArr.push({ name: "External Deposit", value: "externalcrypto" });
            }

            if (param.tokenTypeId == 2) {
                //ERC 20
                // depositTypeArr.push({ name: "From Ethereum Network", value: "external" });
                ethereumPossible = true;
            }
        }

        if (ethereumPossible) {
            //ERC 20
            depositTypeArr.push({ name: "From Ethereum Network", value: "external" });
        }

        if (param.walletTypeNumeric == 0) {
            if (param.bankPossible == true) {
                depositTypeArr.push({ name: "Deposit via Bank", value: "bank" });
            }

            if (walletId == "1") {
                depositMethodSelectable.value = true;
                depositMethodSelected.value = "otc";
                depositTypeArr.push({ name: "Deposit via 7-Eleven", value: "seven" });
                depositTypeArr.push({ name: "Deposit via Dragonpay", value: "dragonpay" });
            } else {
                depositMethodSelectable.value = false;
                depositMethodSelected.value = "ot";
            }
        }

        //PHP - Philippine Pesos type:fiat
        // if (walletId == "1") {
        //     depositTypeArr.push({ name: "Bitcoin", value: "bitcoin" });
        // }

        depositTypeItems.addAll(depositTypeArr);


        depositTypeIndex.value = 0;
        depositType.value = "tagcash";


        dataObj.action = "PAY";
        dataObj.currency = walletId;
        dataObj.address = _model.singleCryptoAddresses;

        if (_model.activePerspectiveType == "user") {
            dataObj.type = "1";
            dataObj.user = _model.userprofileDetails.value.id;
            dataObj.full_name = _model.userprofileDetails.value.user_firstname + " " + _model.userprofileDetails.value.user_lastname;
        } else {
            dataObj.type = "2";
            dataObj.user = _model.nowCommunityID;
            dataObj.full_name = _model.communityprofileDetails.value.community_name;;
        }



        newDepositHandler();
    }


    depositTypeChangeProcess();
})


function stellerDefaultAddressLoad() {
    // if (_model.stellerAddressDefault) {
    //     stellerAddressDefault.value = _model.stellerAddressDefault;
    //     nemAddressDefault.value = _model.nemAddressDefault;

    // } else {

    console.log("stellerDefaultAddressLoad")
    busy.activate();

    _WebService.request("wallet/ReceivingAddresses").then(function (result) {
        gotstellerDefaultAddressLoadResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
    // }
}

function gotstellerDefaultAddressLoadResult(result) {
    console.log("gotstellerDefaultAddressLoadResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;

        for (var i = 0; i < resultArr.length; i++) {
            if (resultArr[i].id == 16) {
                stellerAddressDefault.value = resultArr[i].receiving_address;
                _model.stellerAddressDefault = resultArr[i].receiving_address;;
            }

            if (resultArr[i].id == 32) {
                nemAddressDefault.value = resultArr[i].receiving_address;
                _model.nemAddressDefault = resultArr[i].receiving_address;;
            }

        }

    }
}

//From External
function newDepositHandler() {
    externalStepOne.value = true;
    externalStepTwo.value = false;

    registerAmountInput.value = "";
    txidInput.value = "";
    registerTransactionid = "";
}

function onDepositTypeSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))
    // depositTypeIndex.value = selectedItem.indexLocal;

    depositType.value = selectedItem.value;
    depositTypeChangeProcess();
}

function depositTypeChangeProcess() {
    console.log("depositTypeChangeProcess ");
    console.log(depositType.value);

    if (depositType.value === "bank") {
        //to api format Date "2017-08-16" time "11:44:43";
        var currentdate = new Date();
        var secondsTime = currentdate.getTime();
        depositDateSelect = moment(secondsTime).format('YYYY-MM-DD');
        depositTimeSelect = moment(secondsTime).format('h:m:s');
        // "16-08-2017 11:44 AM";
        depositDateDisplay.value = moment(secondsTime).format('DD-MM-YYYY h:m a');
        depositBankPossible.value = false;

        loadBanksDepositCall();
    } else if (depositType.value === "seven") {
        loadSevenDepositCall();
    } else if (depositType.value === "dragonpay") {
        loadDragonpayDepositCall();
    } else if (depositType.value === "bitcoin") {
        depBitcoinItemsIndex.value = 0;
        depositBitcoinAmountInput.value = "";
        qrDataString.value = "";
        btcWalletAddress = "";
    } else if (depositType.value === "stellar") {
        stellerAddressListLoad();
    }
}




function loadBanksListCall() {
    console.log("loadBanksListCall")
    bankItems.clear();
    bankSelectIndex.value = -1;

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.method = "cash_in";

    _WebService.request("bank/list", apiBodyObj).then(function (result) {
        gotBanksListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotBanksListResult(result) {
    console.log("gotBanksListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {

            // var resultArrEdit = [];
            // resultArrEdit.push(resultArr);
            // bankItems.addAll(resultArrEdit);
            bankItems.addAll(resultArr);

            bankSelectIndex.value = 0;
            bankAccountName.value = resultArr[0].account_name;
            bankAccountAddress.value = resultArr[0].address;
            bankAccountNumber.value = resultArr[0].account_number;
            bankAccountSwift.value = resultArr[0].switf_code;
            bankCodeName = resultArr[0].bank_name;

            loadBanksDepositCall();
        }

    }
}

function onBankSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    bankAccountName.value = selectedItem.account_name;
    bankAccountAddress.value = selectedItem.address;
    bankAccountNumber.value = selectedItem.account_number;
    bankAccountSwift.value = selectedItem.switf_code;
    bankCodeName = selectedItem.bank_name;

    loadBanksDepositCall();

}

function onDepositMethodSelectionChange(args) {
    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))

    depositMethodSelected.value = selectedItem.value;
}

function loadBanksDepositCall() {
    console.log("loadBanksDepositCall")
    transactionBankListData.clear();
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;

    if (depositBankPossible.value == true) {
        apiBodyObj.bank_code = bankCodeName;
    }

    _WebService.request("bank/Deposits", apiBodyObj).then(function (result) {
        gotBanksDepositListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

    // @Query("offset") int offset,
    // @Query("count") int count);-

}

function gotBanksDepositListResult(result) {
    console.log("gotBanksListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        if (resultArr !== null) {

            transactionBankListData.addAll(resultArr);

        }

    }
}


function transactionBankCancel(args) {
    console.log("transactionBankCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("bank/DeleteRequest", apiBodyObj).then(function (result) {
        gotTransactionBankCancelResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransactionBankCancelResult(result) {
    console.log("gotTransactionBankCancelResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        tagEvents.emit("toastShow", { message: "Deleted successfully" });
        loadBanksDepositCall();
    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}



function loadSevenDepositCall() {
    console.log("loadSevenDepositCall")
    transactionSevenListData.clear();
    busy.activate();
    var urlDdeposits = "deposit/requests?id=" + _model.userprofileDetails.value.id + "&deposit=7connect&type=1";

    _WebService.request(urlDdeposits).then(function (result) {
        gotSevenDepositListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotSevenDepositListResult(result) {
    console.log("gotSevenDepositListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;

        transactionSevenListData.addAll(resultObj.requests);

    }
}


function transactionSevenCancel(args) {
    console.log("transactionSevenCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("deposit/DeleteRequest", apiBodyObj).then(function (result) {
        gotTransactionSevenCancelResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransactionSevenCancelResult(result) {
    console.log("gotTransactionSevenCancelResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        tagEvents.emit("toastShow", { message: "Deleted successfully" });
        loadSevenDepositCall();
    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}



function depositBankCheck_clickHandler() {
    console.log("depositBankCheck_clickHandler")

    var amountValue = depositAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.wallet_id = walletId;

    _WebService.request("wallet/AmountCanReceive", apiBodyObj).then(function (result) {
        gotBanksDepositLimitCheckResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBanksDepositLimitCheckResult(result) {
    console.log("gotBanksDepositLimitCheckResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.success == true) {
            depositBankPossible.value = true;
            loadBanksListCall();
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: "Transaction Limit", message: "This amount will exceed the maximum transaction limit. Maximum amount possible now is PHP " + resultArr.in_remaining_amount });
        }

    }
}

function depositBankCancel_clickHandler() {
    depositBankPossible.value = false;
    loadBanksListCall();
}

function uploadSlip_clickHandler() {
    if (uploadImageData != "") {
        slipImageSelected.value = false;
        uploadImageData = "";
        slipUploadImageUrl.clear();
    } else {
        imageSelectPopupShow.value = true;
    }
}

function onImagegSelectionComplete(args) {
    imageSelectPopupShow.value = false;

    slipImageSelected.value = true;
    // uploadImageData = args.imageData;
    uploadImageData = encodeURIComponent(args.imageData);

    slipUploadImageUrl.value = args.imagePath;
}



function dateChange_clickHandler() {
    popupShowDatePick.value = true;

}


function onDateCloseDialog(args) {
    depositDateSelect = moment(args.date).format('YYYY-MM-DD');
    selectNewDispaly = moment(args.date).format('DD-MM-YYYY');

    // popupShowDatePick.value = false;
    timeChangeHandler();
}

function timeChangeHandler() {
    popupShowTimePick.value = true;
}

function onTimeCloseDialog(args) {
    console.log(JSON.stringify(args.time))
    depositTimeSelect = moment(args.time, "HH:mm").format('h:m:s');

    selectNewDispaly += " " + moment(args.time, "HH:mm").format('h:m a');
    depositDateDisplay.value = selectNewDispaly;

    // datePopupCloseClicked();
}

// function datePopupCloseClicked() {
//     popupShowDatePick.value = false;
//     popupShowTimePick.value = false;
// }

function sendNowButton_clickHandler() {
    console.log("loadUserEnergyListCall")

    var amountValue = depositAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    var apiBodyObj = {};

    if (depositMethodSelected.value === "otc") {
        if (uploadImageData == "") {
            tagEvents.emit("toastShow", { message: "Please add a deposit slip" });
            return;
        } else {
            apiBodyObj.image = uploadImageData;
        }

    } else if (depositMethodSelected.value === "ot") {
        if (depositTransactionidInput.value) {
            apiBodyObj.transaction_id = depositTransactionidInput.value;
        } else {
            tagEvents.emit("toastShow", { message: "Transaction Id should not be empty" });
            return;
        }
        apiBodyObj.date = depositDateSelect;
        apiBodyObj.time = depositTimeSelect;
    }

    busy.activate();
    apiBodyObj.amount = amountValue;
    apiBodyObj.bank_code = bankCodeName;
    apiBodyObj.banking_method = depositMethodSelected.value;
    apiBodyObj.wallet_id = walletId;

    _WebService.request("deposit/bank", apiBodyObj).then(function (result) {
        gotSendConfirmationResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotSendConfirmationResult(result) {
    console.log("gotSendConfirmationResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        router.goBack();
        tagEvents.emit("toastShow", { message: "Deposit details submitted successfully" });
    } else {
        if (arr.error == "pending_request_found") {
            tagEvents.emit("alertEvent", { type: "info", title: "Pending Request", message: "You have a pending request. Please cancel or complete it before you can make another." });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}



function depositSevenCheck_clickHandler() {
    console.log("depositSevenCheck_clickHandler")

    var amountValue = depositSevenAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.wallet_id = walletId;

    _WebService.request("wallet/AmountCanReceive", apiBodyObj).then(function (result) {
        gotSevenDepositLimitCheckResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotSevenDepositLimitCheckResult(result) {
    console.log("gotSevenDepositLimitCheckResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.success == true) {
            createBarcodeButton_clickHandler();
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: "Transaction Limit", message: "This amount will exceed the maximum transaction limit. Maximum amount possible now is PHP " + resultArr.in_remaining_amount });
        }

    }
}


function createBarcodeButton_clickHandler() {
    console.log("loadUserEnergyListCall")

    var amountValue = depositSevenAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.amount = amountValue;
    apiBodyObj.type = "deposit";

    _WebService.request("deposit/7connect", apiBodyObj).then(function (result) {
        gotCreaeSevenBarcodeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotCreaeSevenBarcodeResult(result) {
    console.log("gotCreaeSevenBarcodeResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;
        router.push("walletSevenBarcodePage", { pageLink: resultObj.link });
    } else {
        if (arr.error == "pending_request_found") {
            tagEvents.emit("alertEvent", { type: "info", title: "Pending Request", message: "You have a pending request. Please cancel or complete it before you can make another." });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}

function loadDragonpayDepositCall() {
    console.log("loadDragonpayDepositCall")
    transactionDragonpayListData.clear();
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;

    _WebService.request("deposit/DragonpayCashInList", apiBodyObj).then(function (result) {
        gotDragonpayDepositListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

    // @Query("offset") int offset,
    // @Query("count") int count);-

}

function gotDragonpayDepositListResult(result) {
    console.log("gotDragonpayDepositListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;
        transactionDragonpayListData.addAll(resultObj.requests);
    }
}

function transactionDragonpayCancel(args) {
    console.log("transactionSevenCancel")
    console.log(JSON.stringify(args.data))

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = args.data.id;

    _WebService.request("deposit/DeleteDragonPayRequest", apiBodyObj).then(function (result) {
        gotTransactionDragonpayCancelResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotTransactionDragonpayCancelResult(result) {
    console.log("gotTransactionDragonpayCancelResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.data;
        tagEvents.emit("toastShow", { message: "Deleted successfully" });
        loadDragonpayDepositCall();
    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}

function depositDragonpayCheck_clickHandler() {
    console.log("depositDragonpayCheck_clickHandler")

    var amountValue = depositDragonpayAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.amount = amountValue;
    apiBodyObj.wallet_id = walletId;

    _WebService.request("wallet/AmountCanReceive", apiBodyObj).then(function (result) {
        gotDragonDepositLimitCheckResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDragonDepositLimitCheckResult(result) {
    console.log("gotDragonDepositLimitCheckResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.success == true) {
            depositDragonProcessHandler();
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: "Transaction Limit", message: "This amount will exceed the maximum transaction limit. Maximum amount possible now is PHP " + resultArr.in_remaining_amount });
        }

    }
}


function depositDragonProcessHandler() {
    console.log("depositDragonProcessHandler")

    var amountValue = depositDragonpayAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.amount = amountValue;
    apiBodyObj.wallet_id = walletId;

    _WebService.request("deposit/DragonPayCashIn", apiBodyObj).then(function (result) {
        gotdragonDepositProcessResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotdragonDepositProcessResult(result) {
    console.log("gotdragonDepositProcessResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;
        router.goto("walletDragonDepositPage", { pageLink: resultObj.dragonpay_transact_url });
    } else {
        if (arr.error == "pending_request_found") {
            tagEvents.emit("alertEvent", { type: "info", title: "Pending Request", message: "You have a pending request. Please cancel or complete it before you can make another." });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}





function onDepBitcoinChangeEvent(args) {
    console.log(JSON.stringify(args));

    btcWalletAddress = "";
    qrDataString.value = "";
}

function btcRowCopyHandler() {
    console.log("qrClipboardCopyHandler-----------  ");
    clip.setText(qrDataString.value);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}

function btcAmountInputChange(args) {
    // console.log("btcAmountInputChange  " + JSON.stringify(args.value));
    qrDataString.value = "";

    // if(qrDataString.value != ""){
    //     if (depBitcoinItemsIndex.value == 1) {
    //         if (Validator.amount(args.value)) {
    //             generateTagcashReceiveQR();
    //         }
    //     } else if (depBitcoinItemsIndex.value == 2) {
    //         generateBlockchaininfoQR();
    //     }
    // }

}

function generateAddressClickHandler() {
    if (depBitcoinItemsIndex.value == 0) {
        if (walletId == "1") {
            calculateBTCLocalHandler(1);
        } else if (walletId == "2") {
            bitcoinDepositAddressCall();
        }
    } else {
        var amountValue = depositBitcoinAmountInput.value;
        amountValue = amountValue.replace(/,/g, "");

        if (!Validator.amount(amountValue)) {
            tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
            return;
        }

        if (depBitcoinItemsIndex.value == 1) {
            generateTagcashReceiveQR();
        } else if (depBitcoinItemsIndex.value == 2) {
            if (walletId == "1") {
                calculateBTCLocalHandler(amountValue);
            } else if (walletId == "2") {
                bitcoinDepositAddressCall();
            }
        }
    }
}


var conversionInputAmount = 1;

function calculateBTCLocalHandler(amountValue) {
    btcWalletAddress = "";
    qrDataString.value = "";
    conversionInputAmount = amountValue;

    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.from = "php";
    apiBodyObj.to = "btc";

    _WebService.request("wallet/converter", apiBodyObj).then(function (result) {
        gotBtcCalculateResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotBtcCalculateResult(result) {
    console.log("gotBtcCalculateResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        localBitcoinInputDisplay.value = conversionInputAmount;
        localCurrencyValueDisplay.value = (conversionInputAmount * resultObj.price).toString();

        localValuePopupShow.value = true;
    }
}


function localValueOkContinue() {
    localValuePopupShow.value = false;
    bitcoinDepositAddressNewGet();
}

function localValuePopupClose() {
    localValuePopupShow.value = false;
}

function bitcoinDepositAddressCall() {
    // if (btcWalletAddress != "") {
    //     if (depBitcoinItemsIndex.value == 0) {
    //         qrDataString.value = btcWalletAddress;
    //     } else if (depBitcoinItemsIndex.value == 2) {
    //         generateBlockchaininfoQR();
    //     }
    // } else {
    //     bitcoinDepositAddressNewGet();
    // }

    bitcoinDepositAddressNewGet();
}

function bitcoinDepositAddressNewGet() {
    console.log("bitcoinDepositDetailCall")
    busy.activate();

    //1(default) for depositing into PHP or 2 for BTC wallet
    var apiBodyObj = {};

    if (walletId == "1") {
        apiBodyObj.wallet_type_id = 1;
    } else if (walletId == "2") {
        apiBodyObj.wallet_type_id = 2;
    }

    _WebService.request("deposit/bitcoin", apiBodyObj).then(function (result) {
        gotBitcoinDepositAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotBitcoinDepositAddressResult(result) {
    console.log("gotBitcoinDepositQRResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        if (depBitcoinItemsIndex.value == 0) {
            qrDataString.value = resultObj.walletAddress;
        } else if (depBitcoinItemsIndex.value == 2) {
            btcWalletAddress = resultObj.walletAddress;

            generateBlockchaininfoQR();
        }

    } else {
        // {"error":"pending_deposit_found_kindly_deposit","ref_no":null,"address":null,"_error":"old_request_no_address_or_ref_no_available","status":"failed"}
        //{"error":"pending_deposit_found_kindly_deposit","ref_no":"1845a7d8af7e88ca","address":"1Bwxa63o9UJXrSz6MsALPzQbbV9Ph5kRJ1","_error":"pending_deposit_found_kindly_deposit","status":"failed"}
    }
}

function generateBlockchaininfoQR() {
    qrDataString.value = "bitcoin:" + btcWalletAddress + "?amount=" + depositBitcoinAmountInput.value;
}

function generateTagcashReceiveQR() {
    var dataObj = {};
    dataObj.action = "PAY";
    dataObj.amount = depositBitcoinAmountInput.value;
    dataObj.currency = walletId;
    dataObj.address = _model.singleCryptoAddresses;
    dataObj.remarks = "";

    //type : user 1, community 2
    if (_model.activePerspectiveType == "user") {
        dataObj.type = "1";
        dataObj.user = _model.userprofileDetails.value.id;
        dataObj.full_name = _model.userprofileDetails.value.user_firstname + " " + _model.userprofileDetails.value.user_lastname;
    } else {
        dataObj.type = "2";
        dataObj.user = _model.nowCommunityID;
        dataObj.full_name = _model.communityprofileDetails.value.community_name;;
    }

    qrDataString.value = JSON.stringify(dataObj);
}


function generateAddressHandler() {
    qrDataStringTagcash.value = "";

    var amountValue = amountInputTagcash.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        qrDataStringTagcash.value = _model.singleCryptoAddresses;
    } else {
        dataObj.amount = amountValue;
        dataObj.remarks = notesTxtTagcash.value;
        qrDataStringTagcash.value = JSON.stringify(dataObj);
    }

}

function registerAmountHandler() {
    var amountValue = registerAmountInput.value;
    amountValue = amountValue.replace(/,/g, "");

    if (!Validator.amount(amountValue)) {
        tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.wallet_id = walletId;
    apiBodyObj.amount = amountValue;

    _WebService.request("deposit/cryptosubdeposit", apiBodyObj).then(function (result) {
        gotRegisterAmountResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotRegisterAmountResult(result) {
    console.log("gotRegisterAmountResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        registerTransactionid = arr.result;

        externalStepOne.value = false;
        externalStepTwo.value = true;

        console.log("registerTransactionid " + registerTransactionid);
    } else {
    }

}

function externalTransactionHistory() {
    historyDetailPopupShow.value = true;
    historyDetailListData.clear();

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.currency = currencyCode.value;

    _WebService.request("deposit/cryptosubhistory", apiBodyObj).then(function (result) {
        gotExternalTransactionHistoryResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotExternalTransactionHistoryResult(result) {
    console.log("gotExternalTransactionHistoryResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultObj = arr.result;
        historyDetailListData.addAll(resultObj.history);
    } else {
        // tagEvents.emit("toastShow", { message: "Fund request send" });
    }

}

function confirmDepositHandler() {
    if (txidInput.value === "") {
        tagEvents.emit("toastShow", { message: "Please enter TX ID" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    console.log("registerTransactionid " + registerTransactionid);

    apiBodyObj.transaction_id = registerTransactionid;
    apiBodyObj.crypto_transaction_id = txidInput.value;

    _WebService.request("deposit/cryptosubconfirmation", apiBodyObj).then(function (result) {
        gotConfirmDepositResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotConfirmDepositResult(result) {
    console.log("gotConfirmDepositResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultObj = arr.result;

        newDepositHandler();
        tagEvents.emit("toastShow", { message: "Transaction confirmed" });

    } else {
        if (arr.error == "amount_is_not_matching") {
            tagEvents.emit("toastShow", { message: "Amount is not matching" });
        } else if (arr.error == "crypto_transaction_id_already_used") {
            tagEvents.emit("toastShow", { message: "TX ID already used" });
        } else {
            tagEvents.emit("toastShow", { message: "Confirmation failed" });
        }
    }

}

function externalAddressCopyHandler() {
    console.log("externalAddressQR -----------  ");
    clip.setText(externalAddressQR.value);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}



function generateAddressCryptoHandler() {
    // var amountValue = amountInputCrypto.value;

    // if (!Validator.amount(amountValue)) {
    //     tagEvents.emit("toastShow", { message: loc.value.amount_should_not_empty });
    //     return;
    // }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.wallet_type_id = walletId;
    // apiBodyObj.amount = amountValue;

    _WebService.request("deposit/crypto", apiBodyObj).then(function (result) {
        gotGenerateAddressCryptoResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotGenerateAddressCryptoResult(result) {
    console.log("gotGenerateAddressCryptoResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultObj = arr.result;

        externalAddressCryptoQR.value = resultObj.address;
        cryptoQRVisible.value = true;
    }

}

function externalAddressCryptoCopyHandler() {
    console.log("externalAddressQR -----------  ");
    clip.setText(externalAddressCryptoQR.value);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}




function stellerAddressListLoad() {
    addressAddedList.clear();

    busy.activate();
    var apiBodyObj = {};

    _WebService.request("stellar/me", apiBodyObj).then(function (result) {
        gotStellerAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotStellerAddressResult(result) {
    console.log("gotStellerAddressResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;

        addressAddedList.addAll(resultArr);
    }

}


function addressAddClicked() {
    stellerNameEditMode = false;
    stellerAddressInput.value = "";

    if (_model.logedinToDemoMode.value == true) {
        stellerAddressDomain.value = "test.tagcash.com";
    } else {
        stellerAddressDomain.value = "tagcash.com";
    }

    stellerAddressButton.value = "SAVE";

    addressEditPopupShow.value = true;
}

function addressEditClicked(args) {
    console.log(args.data);
    stellerNameEditId = args.data.record_id;
    stellerNameEditMode = true;
    stellerAddressInput.value = args.data.user_nickname;
    stellerAddressDomain.value = args.data.domain;

    stellerAddressButton.value = "UPDATE";

    addressEditPopupShow.value = true;
}

function addressAddClose() {
    addressEditPopupShow.value = false;
}

function addressAddSave() {
    busy.activate();

    var apiUrl = "";
    var apiBodyObj = {};
    if (stellerNameEditMode) {
        apiUrl = "stellar/updateaddress";
        apiBodyObj.record_id = stellerNameEditId;
    } else {
        apiUrl = "stellar/createaddress";

        if (_model.activePerspectiveType == "community") {
            apiBodyObj.user_id = _model.nowCommunityID;
            apiBodyObj.user_type = "C";
        } else {
            apiBodyObj.user_id = _model.userprofileDetails.value.id;
            apiBodyObj.user_type = "U";
        }

        if (_model.logedinToDemoMode.value == true) {
            apiBodyObj.domain = "test.tagcash.com";
        } else {
            apiBodyObj.domain = "tagcash.com";
        }
    }

    apiBodyObj.user_nickname = stellerAddressInput.value;

    _WebService.request(apiUrl, apiBodyObj).then(function (result) {
        gotStellerNicknameAddResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotStellerNicknameAddResult(result) {
    console.log("gotStellerAddressAddResult");
    addressEditPopupShow.value = false;
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        // var resultArr = arr.result;
        stellerAddressListLoad();
        if (stellerNameEditMode) {
            tagEvents.emit("toastShow", { message: "Nickname updated successfully" });
        } else {
            tagEvents.emit("toastShow", { message: "Nickname added successfully" });
        }

    } else {
        if (arr.error.user_nickname) {
            if (arr.error.user_nickname == "username_already_exist") {
                tagEvents.emit("toastShow", { message: "Nickname already exist" });
            }
        }

    }
}


function addressDeleteClicked(args) {
    console.log(JSON.stringify(args.data));
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.record_id = args.data.record_id;

    _WebService.request("stellar/deleteaddress", apiBodyObj).then(function (result) {
        gotDeleteNicknameResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDeleteNicknameResult(result) {
    console.log("gotDeleteNicknameResult");
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        stellerAddressListLoad();
        tagEvents.emit("toastShow", { message: "Nickname deleted successfully" });
    }

}


function addressNEMCopyClicked(args) {
    console.log(JSON.stringify(nemAddressDefault.value));

    clip.setText(nemAddressDefault.value);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}

function addressCopyClicked(args) {
    console.log(JSON.stringify(stellerAddressDefault.value));

    clip.setText(stellerAddressDefault.value);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}

function memoCopyClicked(args) {
    console.log(JSON.stringify(memoTextDefault.value));

    clip.setText(memoTextDefault.value);
    tagEvents.emit("toastShow", { message: loc.value.copiedto_clipboard });
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}




module.exports = {
    depositTypeSelect: depositTypeSelect,
    depositTypeItems: depositTypeItems,
    depositTypeIndex: depositTypeIndex,
    depositType: depositType,
    onDepositTypeSelectionChange: onDepositTypeSelectionChange,
    walletType: walletType,
    currencyCode: currencyCode,
    walletIdCheck: walletIdCheck,

    bankItems: bankItems,
    bankSelectIndex: bankSelectIndex,
    onBankSelectionChange: onBankSelectionChange,
    transactionBankListData: transactionBankListData,
    transactionSevenListData: transactionSevenListData,

    depositMethodItems: depositMethodItems,
    depositMethodIndex: depositMethodIndex,
    depositMethodSelected: depositMethodSelected,
    depositMethodSelectable: depositMethodSelectable,
    onDepositMethodSelectionChange: onDepositMethodSelectionChange,

    bankAccountName: bankAccountName,
    bankAccountAddress: bankAccountAddress,
    bankAccountNumber: bankAccountNumber,
    bankAccountSwift: bankAccountSwift,
    depositAmountInput: depositAmountInput,
    depositTransactionidInput: depositTransactionidInput,
    depositDateDisplay: depositDateDisplay,

    uploadSlip_clickHandler: uploadSlip_clickHandler,
    sendNowButton_clickHandler: sendNowButton_clickHandler,

    depositSevenAmountInput: depositSevenAmountInput,
    depositDragonpayAmountInput: depositDragonpayAmountInput,
    createBarcodeButton_clickHandler: createBarcodeButton_clickHandler,
    transactionBitcoinListData: transactionBitcoinListData,
    depositBitcoinAmountInput: depositBitcoinAmountInput,
    depBitcoinItems: depBitcoinItems,
    depBitcoinItemsIndex: depBitcoinItemsIndex,
    onDepBitcoinChangeEvent: onDepBitcoinChangeEvent,
    generateAddressClickHandler: generateAddressClickHandler,
    btcAmountInputChange: btcAmountInputChange,
    btcRowCopyHandler: btcRowCopyHandler,

    localValuePopupShow: localValuePopupShow,
    localValueOkContinue: localValueOkContinue,
    localValuePopupClose: localValuePopupClose,
    localBitcoinInputDisplay: localBitcoinInputDisplay,
    localCurrencyValueDisplay: localCurrencyValueDisplay,
    qrDataString: qrDataString,
    slipUploadImageUrl: slipUploadImageUrl,
    imageSelectPopupShow: imageSelectPopupShow,
    onImagegSelectionComplete: onImagegSelectionComplete,
    dateChange_clickHandler: dateChange_clickHandler,
    popupShowDatePick: popupShowDatePick,
    popupShowTimePick: popupShowTimePick,
    // datePopupCloseClicked: datePopupCloseClicked,
    onDateCloseDialog: onDateCloseDialog,
    onTimeCloseDialog: onTimeCloseDialog,
    slipImageSelected: slipImageSelected,


    amountInputTagcash: amountInputTagcash,
    notesTxtTagcash: notesTxtTagcash,
    generateAddressHandler: generateAddressHandler,
    qrDataStringTagcash: qrDataStringTagcash,

    newDepositHandler: newDepositHandler,
    externalStepOne: externalStepOne,
    externalStepTwo: externalStepTwo,
    registerAmountInput: registerAmountInput,
    txidInput: txidInput,
    amountInputCrypto: amountInputCrypto,
    cryptoQRVisible: cryptoQRVisible,
    externalAddressQR: externalAddressQR,

    historyDetailPopupShow: historyDetailPopupShow,
    historyDetailListData: historyDetailListData,

    registerAmountHandler: registerAmountHandler,
    externalTransactionHistory: externalTransactionHistory,
    confirmDepositHandler: confirmDepositHandler,
    externalAddressCopyHandler: externalAddressCopyHandler,
    externalAddressCryptoQR: externalAddressCryptoQR,
    generateAddressCryptoHandler: generateAddressCryptoHandler,
    externalAddressCryptoCopyHandler: externalAddressCryptoCopyHandler,


    addressEditPopupShow: addressEditPopupShow,
    addressAddClicked: addressAddClicked,
    addressAddedList: addressAddedList,
    addressAddClose: addressAddClose,
    addressAddSave: addressAddSave,
    stellerAddressInput: stellerAddressInput,
    addressDeleteClicked: addressDeleteClicked,
    addressEditClicked: addressEditClicked,
    stellerAddressButton: stellerAddressButton,
    stellerAddressDomain: stellerAddressDomain,

    stellerAddressDefault: stellerAddressDefault,
    nemAddressDefault: nemAddressDefault,
    memoTextDefault: memoTextDefault,
    addressCopyClicked: addressCopyClicked,
    addressNEMCopyClicked: addressNEMCopyClicked,
    memoCopyClicked: memoCopyClicked,

    depositBankPossible: depositBankPossible,
    depositBankCheck_clickHandler: depositBankCheck_clickHandler,
    depositBankCancel_clickHandler: depositBankCancel_clickHandler,
    depositSevenCheck_clickHandler: depositSevenCheck_clickHandler,

    transactionBankCancel: transactionBankCancel,
    transactionSevenCancel: transactionSevenCancel,

    transactionDragonpayListData: transactionDragonpayListData,
    transactionDragonpayCancel: transactionDragonpayCancel,
    depositDragonpayCheck_clickHandler: depositDragonpayCheck_clickHandler,

}