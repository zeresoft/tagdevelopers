
function currency(number) {
    // thousand_separator = ',',
    //     decimal_separator = '.';

    // var number_string = number.toString(),
    //     split = number_string.split(decimal_separator),
    //     rest = split[0].length % 3,
    //     result = split[0].substr(0, rest),
    //     thousands = split[0].substr(rest).match(/\d{3}/g);

    // if (thousands) {
    //     separator = rest ? thousand_separator : '';
    //     result += separator + thousands.join(separator);
    // }
    // result = split[1] != undefined ? result + decimal_separator + split[1] : result;

    var num_parts = number.toString().split(".");
    num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    var result = num_parts.join(".");

    return result;
}




module.exports = {
    currency: currency,
};