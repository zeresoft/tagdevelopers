



function errorHandle(arr) {
    var errorString = arr.error;
    console.log("errorString  :  " + errorString);
    var message = "";

    var showAlert = false;
    switch (errorString) {
        case "insufficient_amount":
            message = "Insufficient amount";
            break;
        case "crypto_wallets_only_allowded":
            message = "Only crypto wallets allowed";
            break;
        case "to_email_is_not_verified":
            message = "Email is not verified";
            break;
        case "limit_exceeded":
            showAlert = true;
            message = "The user has exceeded their inward allowance.";
            break;
        case "from_level1_failed":
            showAlert = true;
            message = "You are not verified enough transfer this amount.";
            break;
        case "to_level1_failed":
            showAlert = true;
            message = "The person you are sending to has not yet verified their account.";
            // message = "The person you are transferring to, is not verified enough to receive this amount.";
            break;
        case "from_limit_exceeded":
            showAlert = true;
            message = "Your transaction limit exceeded. Please change verification level to increase limit.";
            break;
        case "to_limit_exceeded":
            showAlert = true;
            message = "Receiver transaction limit exceeded.";
            break;
        case "pin_incorrect":
            message = "PIN incorrect";
            break;
        case "Transaction Failed":
            message = "Transaction Failed";
            break;
        case "invalid_to_email":
            message = "Invalid email";
            break;
        case "asset_not_found":
            message = "Asset not found";
            break;
        case "invalid_parameters_to_and_from_matching":
            // message = "Invalid parameters";
            message = "You cannot send to your own wallet";
            break;
        case "stellar_transfer_failed":
            message = "Stellar transfer failed";
            break;
        case "verification_failed":
            message = "KYC verification is required";
            break;
        case "invalid_identifier":
            message = "Invalid identifier";
            break;
        default:
            message = loc.value.unable_process_message;
            break;
    }

    if (showAlert) {
        tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: message });
    } else {
        tagEvents.emit("toastShow", { message: message });
    }

}


module.exports = {
    errorHandle: errorHandle,
};