
/**
* read
* @param value, data to validate
* return true/false
*/
function amount(value) {
    var status = true;

    if (isNaN(value)) {
        status = false;
    } else {
        if (value <= 0) {
            status = false;
        }
    }

    return status;
}


function email(email) {
    email = email.toLowerCase();
    var emailExpression = /([a-z0-9._-]+?)@([a-z0-9.-]+)\.([a-z]{2,4})/;
    return emailExpression.test(email);
}


function isJson(str) {
    var firstCha = str.charAt(0);

    if (firstCha === "{" || firstCha === "[") {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    } else {
        return false;
    }
}

function isString(value) {
    return typeof value === 'string';
}

// A URL validator that is used to validate URLs with the ability to
// restrict schemes and some domains.

function url(value, options) {
    var schemes = ['http', 'https'], allowLocal = false;

    if (!isString(value)) {
        return false;
    }

    // https://gist.github.com/dperini/729294
    var regex =
        "^" +
        // protocol identifier
        "(?:(?:" + schemes.join("|") + ")://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:";

    var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";

    if (allowLocal) {
        tld += "?";
    } else {
        regex +=
            // IP address exclusion
            // private & local networks
            "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
            "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
            "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})";
    }

    regex +=
        // IP address dotted notation octets
        // excludes loopback network 0.0.0.0
        // excludes reserved space >= 224.0.0.0
        // excludes network & broacast addresses
        // (first & last IP address of each class)
        "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
        "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
        "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
        // host name
        "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
        // domain name
        "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
        tld +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:[/?#]\\S*)?" +
        "$";

    var PATTERN = new RegExp(regex, 'i');
    return PATTERN.exec(value);
}

module.exports = {
    amount: amount,
    email: email,
    isJson: isJson,
    url: url,
};