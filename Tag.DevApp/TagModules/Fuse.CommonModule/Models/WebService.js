var _model = require('Models/GlobalModel');

function apiAuthenticate(email, password) {
    var data = "client_id=" + _model.client_id + "&client_secret=" + _model.client_secret + "&grant_type=password&user_email=" + encodeURIComponent(email) + "&user_password=" + encodeURIComponent(password);

    return fetch(_model.serverurl + "oauth/accesstoken", {
        method: 'post',
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        return response.json();    // This returns a promise
    }).then(function (responseObject) {
        return new Promise(function (resolve, reject) {
            resolve(responseObject);
        });
    }).catch(function (err) {
        console.log(err);
    });
}

function apiRefreshToken(refreshToken) {
    var data = "client_id=" + _model.client_id + "&client_secret=" + _model.client_secret + "&grant_type=refresh_token&refresh_token=" + refreshToken;

    return fetch(_model.serverurl + "oauth/accesstoken", {
        method: 'post',
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        return response.json();    // This returns a promise
    }).then(function (responseObject) {
        return new Promise(function (resolve, reject) {
            resolve(responseObject);
        });
    }).catch(function (err) {
        console.log(err);
    });
}


function apiUserRegistration(email, firstName, lastName, password) {
    var data = "client_id=" + _model.client_id + "&client_secret=" + _model.client_secret + "&user_email=" + encodeURIComponent(email) + "&user_firstname=" + firstName + "&user_lastname=" + lastName + "&user_password=" + encodeURIComponent(password);

    return fetch(_model.serverurl + "registration", {
        method: 'post',
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        return response.json();
    }).then(function (responseObject) {
        return new Promise(function (resolve, reject) {
            resolve(responseObject);
        });
    }).catch(function (err) {
        console.log(err);
    });
}

function apiForgotPassword(email, password) {
    var data = "client_id=" + _model.client_id + "&client_secret=" + _model.client_secret + "&user_email=" + encodeURIComponent(email);

    return fetch(_model.serverurl + "registration/resetpassword", {
        method: 'post',
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        return response.json();
    }).then(function (responseObject) {
        return new Promise(function (resolve, reject) {
            resolve(responseObject);
        });
    }).catch(function (err) {
        console.log(err);
    });
}

function request(url, options, n) {
    if (n === undefined) {
        n = 3;
    }
    var method = 'POST';
    if (options === undefined) {
        options = {};
    }

    var data = "access_token=" + _model.accessToken;

    Object.keys(options).forEach(function (key) {
        data += "&" + key + "=" + options[key];
    });

    console.log(_model.serverurl + url);
    console.log(data);

    return fetch(_model.serverurl + url, {
        method: method,
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" },
        body: data
    }).then(function (response) {
        // console.log("response : " + JSON.stringify(response));
        return response.json();
    }).then(function (responseObject) {
        return new Promise(function (resolve, reject) {
            resolve(responseObject);
        });
    }).catch(function (err) {
        console.log("***************************************");
        console.log("err : " + err);
        console.log("n : " + n);
        console.log(JSON.stringify(err));
        console.log("***************************************");
        if (n === 1) throw err;
        return request(url, options, n - 1);
    });

}

function requestExternal(url,method) {
    if (method === undefined) {
        method = 'GET';
    }

    return fetch(url, {
        method: method,
        headers: { "Content-type": "application/x-www-form-urlencoded; charset=UTF-8" }
    }).then(function (response) {
        // console.log("response : " + JSON.stringify(response));
        return response.json();
    }).then(function (responseObject) {
        return new Promise(function (resolve, reject) {
            resolve(responseObject);
        });
    }).catch(function (err) {
        console.log(err);
    });

}

module.exports =
    {
        apiAuthenticate: apiAuthenticate,
        apiRefreshToken: apiRefreshToken,
        apiUserRegistration: apiUserRegistration,
        apiForgotPassword: apiForgotPassword,

        request: request,
        requestExternal: requestExternal,
    };