var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var editingMode = Observable(false);
var profileImage = Observable();
var profileName = Observable();
var profileNickName = Observable();
var profileID = Observable();
var profileEmail = Observable();
var profileMobile = Observable();
var profileCity = Observable();
var profileDetails = Observable();
var profileCountry = Observable();
var profileCountryId = Observable();
var profileCountryCode = Observable();
var profileDateBirth = Observable();
var profileGender = Observable();

var emailVerified = Observable(false);
var smsVerified = Observable(false);

var imageSelectUserPopupShow = Observable(false);

var profileGenderItems = Observable({ index: 0, name: loc.value.male, value: "male" }, { index: 1, name: loc.value.female, value: "female" }, { index: 2, name: loc.value.other, value: "other" });
var profileGenderIndex = Observable(-1);
var inputValueObj = {};
var saveButtonVisible = Observable(false);

function onViewActivate() {

    console.log("onViewActivate")
    console.log(JSON.stringify(_model.userprofileDetails.value));
    
    profileImage.value = _model.userImageUrl + _model.userprofileDetails.value.id + "?kycImage=0&" + Math.random();
    profileName.value = _model.userprofileDetails.value.user_firstname + " " + _model.userprofileDetails.value.user_lastname;
    profileNickName.value = _model.userprofileDetails.value.user_nickname;
    profileID.value = _model.userprofileDetails.value.id;
    
    profileEmail.value = _model.userprofileDetails.value.user_email;
    profileMobile.value = _model.userprofileDetails.value.user_mobile;
    profileCountryId.value = _model.userprofileDetails.value.user_country.country_id;
    profileCountry.value = _model.userprofileDetails.value.user_country.country_name;
    profileCountryCode.value = _model.userprofileDetails.value.country_phonecode;
    profileCity.value = _model.userprofileDetails.value.user_city;
    profileDetails.value = _model.userprofileDetails.value.profile_bio;
    
    console.log("profileCountryCode.value");
    console.log(profileCountryCode.value);

    if (_model.userprofileDetails.value.user_gender) {
        profileGenderItems.forEach(function (item) {
            if (_model.userprofileDetails.value.user_gender == item.value) {
                profileGender.value = item.name;
                profileGenderIndex.value = item.index;
            }
        });
    } else {
        profileGender.value = loc.value.not_set;
        profileGenderIndex.value = -1;
    }

    if (_model.userprofileDetails.value.user_verificationdetails.email_verified) {
        emailVerified.value = true;
    }
    if (_model.userprofileDetails.value.user_verificationdetails.sms_verified) {
        smsVerified.value = true;
    }


    profileDateBirth.value = _model.userprofileDetails.value.user_dob;

    // "user_dob":"1978-06-08",


}


function editModeToggleClicked() {
    editingMode.value = !editingMode.value;

    // inputValueObj = {};
    // saveButtonVisible.value = false;
}


function onEditTextChangeEvent(args) {
    //console.log(JSON.stringify(args))
    // console.log(JSON.stringify(args.value))

    if (args.field == "userName") {
        var userNameInput = args.text;
        var userNameArr = userNameInput.split(" ");;
        saveFieldDataChanges("user_firstname", userNameArr[0]);
        saveFieldDataChanges("user_lastname", userNameArr[1]);
    } else {
        saveFieldDataChanges(args.field, args.text);
    }

}

function onCountryCodeChangeEvent(args) {
    profileCountryCode.value = args.countryPhoneCode;

    saveFieldDataChanges("country_phonecode", args.countryPhoneCode);
}

function onCountryChangeEvent(args) {
    profileCountryId.value = args.countryID;
    profileCountry.value = args.countryName;

    saveFieldDataChanges("country_id", args.countryID);
}

function onGenderChangeEvent(args) {
    var selectedItem = profileGenderItems.getAt(args.selectedIndex);
    saveFieldDataChanges("user_gender", selectedItem.value);
}


function saveFieldDataChanges(editedField, editedValue) {
    inputValueObj[editedField] = editedValue;

    saveButtonVisible.value = true;
    console.log(JSON.stringify(inputValueObj))
}

function editSaveClickHandle() {

    var dataPassError = false;
    Object.keys(inputValueObj).forEach(function (key) {
        console.log(key);
        console.log(JSON.stringify(inputValueObj[key]));

        if (key == "user_mobile") {
            if (inputValueObj[key] === "") {
                dataPassError = true;
                tagEvents.emit("toastShow", { message: "Please enter a valid phone number" });
            } else {
                if (inputValueObj[key].charAt(0) === "+") {
                    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Please enter phone number without country code." });
                    dataPassError = true;
                }
            }
        } else if (key == "email") {
            if (inputValueObj[key] === "") {
                dataPassError = true;
                tagEvents.emit("toastShow", { message: loc.value.email_not_empty });
            }
        }
    });

    if (dataPassError) {
        return;
    }

    busy.activate();

    _WebService.request("user/updateprofile/", inputValueObj).then(function (result) {
        gotUpdateprofileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotUpdateprofileResult(result) {
    console.log("gotUpdateprofileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;

        if (inputValueObj.user_firstname) {
            console.log("Name change")
            _model.userprofileDetails.value.user_firstname = inputValueObj.user_firstname;
            _model.userprofileDetails.value.user_lastname = inputValueObj.user_lastname;
        }

        if (inputValueObj.user_mobile) {
            console.log("user_mobile change")
            _model.userprofileDetails.value.user_mobile = inputValueObj.user_mobile;
        }

        if (inputValueObj.country_phonecode) {
            console.log("country_phonecode change")
            _model.userprofileDetails.value.country_phonecode = inputValueObj.country_phonecode;
        }


        inputValueObj = {};
        saveButtonVisible.value = false;

        tagEvents.emit("toastShow", { message: loc.value.profile_updated_successfully });

        tagEvents.emit("profileRefresh");

        profileUserDetailsLoad();

    } else {
        if (arr.error == "Mobile_number_already_used by_someone_else") {
            tagEvents.emit("toastShow", { message: "Mobile number is already used" });
        } else {
            tagEvents.emit("toastShow", { message: loc.value.error_occurred });
        }
    }
}



function onUserImagegChangeClick() {
    imageSelectUserPopupShow.value = true;
}

function onUserImagegSelectionComplete(args) {
    imageSelectUserPopupShow.value = false;

    uploadImageData = args.imageData;
    // slipUploadImageUrl.value = args.imagePath;

    busy.activate();


    var imageEncoded = encodeURIComponent(args.imageData);

    var apiBodyObj = {};
    apiBodyObj.image = imageEncoded;

    _WebService.request("user/uploadavatar", apiBodyObj).then(function (result) {
        gotUploadProfileImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUploadProfileImageResult(result) {
    console.log("gotUploadProfileImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;

        // inputValueObj = {};
        // saveButtonVisible.value = true;

        // tagEvents.emit("toastShow", { message: "Profile Updated Successfully" });
        tagEvents.emit("profileRefresh");

        profileImage.value = _model.userImageUrl + _model.userprofileDetails.value.id + "?kycImage=0" + Math.random();
    }
}

function profileUserDetailsLoad() {
    busy.activate();

    _WebService.request("user/profile").then(function (result) {
        gotProfileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
    });
}

function gotProfileResult(result) {
    console.log("gotProfileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        _model.userprofileDetails.value = resultObj;
    }

}


function applyAgentClickHandler() {
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    onViewActivate: onViewActivate,
    editingMode: editingMode,
    editModeToggleClicked: editModeToggleClicked,
    editSaveClickHandle: editSaveClickHandle,
    profileImage: profileImage,
    profileName: profileName,
    profileNickName: profileNickName,
    profileID: profileID,
    profileEmail: profileEmail,
    profileMobile: profileMobile,
    profileCity: profileCity,
    profileDetails: profileDetails,
    profileCountry: profileCountry,
    profileCountryId: profileCountryId,
    profileDateBirth: profileDateBirth,
    profileGender: profileGender,
    profileGenderItems: profileGenderItems,
    profileGenderIndex: profileGenderIndex,
    onCountryChangeEvent: onCountryChangeEvent,
    onGenderChangeEvent: onGenderChangeEvent,
    onEditTextChangeEvent: onEditTextChangeEvent,

    emailVerified: emailVerified,
    smsVerified: smsVerified,
    onUserImagegChangeClick: onUserImagegChangeClick,
    imageSelectUserPopupShow: imageSelectUserPopupShow,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,

    saveButtonVisible: saveButtonVisible,
    applyAgentClickHandler: applyAgentClickHandler,

    onCountryCodeChangeEvent: onCountryCodeChangeEvent,
    profileCountryCode: profileCountryCode,
}