var Observable = require('FuseJS/Observable');
var Preferences = require('Utils/Preferences');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var emailValue = Observable("");
var passwordValue = Observable("");

var numberarray = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'clear', '0', 'back'];
var pinEntered = "";
var pinDisplayArr = Observable();

function onViewActivate() {
    tagEvents.emit("menuCloseEvent");

    loginStateGroup.goto(blankState);
    pinDisplayArr.clear();
    pinEntered = "";
    _model.phpWalletStatus = null;
    _model.nfcReturnTarget = "search";

    var accesstokenAvailable = Preferences.read("access_token", false);
    console.log("accesstokenAvailable : " + accesstokenAvailable)

    if (accesstokenAvailable) {
        _model.accessToken = Preferences.read("access_token", "");
        _model.refreshToken = Preferences.read("refresh_token", "");
        _model.tokenType = Preferences.read("token_type", "");

        var nowTimeSec = Date.now();
        var expires_time = Preferences.read("expires_time", 0);

        if (nowTimeSec > expires_time) {
            loginStateGroup.goto(passwordState);
        } else {
            var emailStored = Preferences.read("email", false);
            console.log("emailStored " + emailStored);
            if (emailStored) {
                emailValue.value = emailStored;
            }

            _model.logedinToDemoMode.value = true;
            var usePinAvailable = Preferences.read("use_pin", false);
            console.log("use_pin : " + usePinAvailable)

            if (usePinAvailable) {
                loginStateGroup.goto(pinState);
            } else {
                profileUserDetailsLoad();
            }

        }

    } else {
        console.log("No Token Available");
        loginStateGroup.goto(passwordState);
    }
}

function passwordLoginShowClicked() {
    loginStateGroup.goto(passwordState);
}

function login_clicked() {
    busy.activate();
    _model.logedinToDemoMode.value = true;
    var loginEmailData = emailValue.value;

    _WebService.apiAuthenticate(loginEmailData, passwordValue.value).then(function (loginResult) {
        gotLoginResult(loginResult);
    }).catch(function (error) {
        busy.deactivate();
        console.log("Couldn't get data: " + error);
    });
}

function gotLoginResult(loginResult) {
    busy.deactivate();
    var arr = loginResult;

    Preferences.write("buyload_networks", null);

    if (arr.status == "success") {
        Preferences.write("email", emailValue.value);

        var resultObj = arr.result;
        logedInDataProcess(resultObj);
    } else {
        if (arr.error == "noNetwok") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.invalid_username_password });
        }
    }

}

function logedInDataProcess(resultObj) {
    console.log("logedInDataProcess")
    console.log(JSON.stringify(resultObj))

    _model.accessToken = resultObj.access_token;
    Preferences.write("access_token", resultObj.access_token);
    _model.refreshToken = resultObj.refresh_token;
    Preferences.write("refresh_token", resultObj.refresh_token);
    _model.tokenType = resultObj.token_type;
    Preferences.write("token_type", resultObj.token_type);

    var nowTimeSec = Date.now();
    var expires_time = nowTimeSec + (resultObj.expires_in * 1000);
    Preferences.write("expires_time", expires_time);


    profileUserDetailsLoad();
}

function profileUserDetailsLoad() {
    busy.activate();

    _WebService.request("user/profile").then(function (result) {
        gotProfileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
    });
}

function gotProfileResult(result) {
    console.log("gotProfileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        _model.userprofileDetails.value = resultObj;

        if (resultObj.crypto_addresses) {
            if (resultObj.crypto_addresses.single_address) {
                console.log("address available ----------------")
                _model.singleCryptoAddresses = resultObj.crypto_addresses.single_address;

                router.goto("loginCompletePage");
            } else {
                getMyAddressCall();
            }
        } else {
            getMyAddressCall();
        }

    } else {
        if (arr.action == "tokenverification") {
            callRefreshToken();
        }
    }
}

function callRefreshToken() {

    var refreshTokenAvailable = Preferences.read("refresh_token", false);
    console.log("refreshTokenAvailable : " + refreshTokenAvailable)

    if (refreshTokenAvailable) {
        busy.activate();
        _WebService.apiRefreshToken(refreshTokenAvailable).then(function (loginResult) {
            gotRefreshTokenResult(loginResult);
        }).catch(function (error) {
            busy.deactivate();
            console.log("Couldn't get data: " + error);
            loginStateGroup.goto(passwordState);
        });
    } else {
        loginStateGroup.goto(passwordState);
    }
}

function gotRefreshTokenResult(loginResult) {
    console.log("gotRefreshTokenResult")

    busy.deactivate();
    var arr = loginResult;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        logedInDataProcess(resultObj);
    } else {
        loginStateGroup.goto(passwordState);
    }

}

function getMyAddressCall() {
    _model.singleCryptoAddresses = "";

    _WebService.request("wallet/GetMyAddress").then(function (result) {
        gotGetMyAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotGetMyAddressResult(result) {
    console.log("gotGetMyAddressResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        _model.singleCryptoAddresses = resultObj.address;

        router.goto("loginCompletePage");
    }
}

function forgotPassword_clickHandler() {
    router.push("forgotPasswordPage");
}

function registerButton_clickHandler() {
    router.push("registerUserPage");
}

function pinnumberClickHandler(args) {
    if (args.data == "clear") {
        pinEntered = "";
    } else if (args.data == "back") {
        console.log(pinEntered);
        pinEntered = pinEntered.slice(0, pinEntered.length - 1);
        console.log(pinEntered);
    } else {
        if (pinEntered.length < 4) {
            pinEntered = pinEntered + args.data;

            if (pinEntered.length == 4) {
                pinLoginCall();
            }
        }
    }
    pinDisplayArr.clear();
    var pinEnteredArr = pinEntered.split("");
    pinDisplayArr.addAll(pinEnteredArr);
}

function pinLoginCall() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.pin = pinEntered;

    _WebService.request("perspective/pinLogin", apiBodyObj).then(function (result) {
        gotPinLoginResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
    });
}

function gotPinLoginResult(result) {
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultObj = arr.result;
        logedInDataProcess(resultObj);
    } else {
        if (arr.error == "pin_not_set_yet") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "PIN not set, please login using ID or email." });
            loginStateGroup.goto(passwordState);
        } else if (arr.error == "invalid_pin") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Invalid PIN" });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: "Unable to login, please login using ID or email." });
            loginStateGroup.goto(passwordState);
        }
    }

}

module.exports = {
    onViewActivate: onViewActivate,
    login_clicked: login_clicked,
    emailValue: emailValue,
    passwordValue: passwordValue,
    forgotPassword_clickHandler: forgotPassword_clickHandler,
    registerButton_clickHandler: registerButton_clickHandler,
    passwordLoginShowClicked: passwordLoginShowClicked,

    numberarray: numberarray,
    pinDisplayArr: pinDisplayArr,
    pinnumberClickHandler: pinnumberClickHandler,
}