var Observable = require('FuseJS/Observable');
var Preferences = require('Utils/Preferences');
var Validator = require('Utils/Validator');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var emailValue = Observable("");
var firstNameValue = Observable("");
var lastNameValue = Observable("");
var passwordValue = Observable("");

function registerButton_clickHandler() {
    callRegister();
}

function callRegister() {
    if (emailValue.value !== "" && firstNameValue.value !== "" && lastNameValue.value !== "" && passwordValue.value !== "") {

        if (passwordValue.value.length < 6) {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.password_too_short });
        } else {
            _model.logedinToDemoMode.value = true;
            var loginEmailData= emailValue.value;

            if (Validator.email(loginEmailData)) {
                busy.activate();

                _WebService.apiUserRegistration(loginEmailData, firstNameValue.value, lastNameValue.value, passwordValue.value).then(function (result) {
                    gotRegisterResult(result);
                }).catch(function (error) {
                    busy.deactivate();
                    showNetworkErrorMessage();
                });

            }

        }
    }
}



function gotRegisterResult(rawData) {
    console.log("gotRegisterResult")
    busy.deactivate();

    var arr = rawData;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        if (resultObj.user_email == "d@gmail.net") {
            router.goBack();
        } else {
            Preferences.write("email", resultObj.user_email);

            _model.accessToken = resultObj.access_token;
            Preferences.write("access_token", resultObj.access_token);
            _model.refreshToken = resultObj.refresh_token;
            Preferences.write("refresh_token", resultObj.refresh_token);
            _model.tokenType = resultObj.token_type;
            Preferences.write("token_type", resultObj.token_type);

            var nowTimeSec = Date.now();
            var expires_time = nowTimeSec + (resultObj.expires_in * 1000);
            Preferences.write("expires_time", expires_time);

            if (resultObj.crypto_addresses) {
                if (resultObj.crypto_addresses.single_address) {
                    console.log("address available ----------------")
                    _model.singleCryptoAddresses = resultObj.crypto_addresses.single_address;
                } else {
                    getMyAddressCall();
                }
            } else {
                getMyAddressCall();
            }

            profileUserDetailsLoad();
        }
    } else {
        if (arr.error[0] == "email_exists") {
            emailValue.value = "";
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.email_already_taken });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
        }
    }

}


function profileUserDetailsLoad() {
    busy.activate();

    _WebService.request("user/profile").then(function (result) {
        gotProfileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
    });
}

function gotProfileResult(result) {
    console.log("gotProfileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        _model.userprofileDetails.value = resultObj;

        router.goto("loginCompletePage");
    }

}

function getMyAddressCall() {
    _model.singleCryptoAddresses = "";

    _WebService.request("wallet/GetMyAddress").then(function (result) {
        gotGetMyAddressResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotGetMyAddressResult(result) {
    console.log("gotGetMyAddressResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        _model.singleCryptoAddresses = resultObj.address;
    }
}




function cancel_clickHandler() {
    router.goBack();
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    emailValue: emailValue,
    firstNameValue: firstNameValue,
    lastNameValue: lastNameValue,
    passwordValue: passwordValue,
    registerButton_clickHandler: registerButton_clickHandler,
    cancel_clickHandler: cancel_clickHandler
}