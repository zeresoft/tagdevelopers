var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');
var Preferences = require('Utils/Preferences');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var emailValue = Observable("");

var busyAnimShow = Observable(false);

function resetButton_clickHandler() {
    callReset();
}

function callReset() {
    if (emailValue.value !== "") {

        _model.logedinToDemoMode.value = true;
        var loginEmailData  = emailValue.value;

        if (Validator.email(loginEmailData)) {
            busy.activate();

            _WebService.apiForgotPassword(loginEmailData).then(function (result) {
                gotForgotPasswordResult(result);
            }).catch(function (error) {
                busy.deactivate();
                showNetworkErrorMessage();
            });

        }

    }
}


function gotForgotPasswordResult(rawData) {
    console.log("gotForgotPasswordResult")
    busy.deactivate();

    var arr = rawData;
    console.log(JSON.stringify(arr));
    
    if (arr.status == "success") {
        Preferences.write("email", emailValue.value);

        tagEvents.emit("alertEvent", { type: "info", title: loc.value.reset_password, message: loc.value.forgot_link_sent_success, callback: confirmResetmailClosedHandler });
    } else {
        if (arr.result == "noNetwok") {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
        } else {
            tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.forgot_not_registered_user });
        }
    }

}



function confirmResetmailClosedHandler(args) {
    router.goBack();
}


function cancel_clickHandler() {
    router.goBack();
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    emailValue: emailValue,
    resetButton_clickHandler: resetButton_clickHandler,
    cancel_clickHandler: cancel_clickHandler
}