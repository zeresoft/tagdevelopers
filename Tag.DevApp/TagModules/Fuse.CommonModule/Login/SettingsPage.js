var Observable = require('FuseJS/Observable');
var Preferences = require('Utils/Preferences');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var currencyCode = Observable();
var pinchangePopupShow = Observable(false);
var passwordchangePopupShow = Observable(false);
var newPinInput = Observable();
var confirmPinInput = Observable();
var merchantServices = Observable(false);
var loadingCompleteStat = Observable(false);
var oldPassInput = Observable();
var newPassInput = Observable();
var confirmPassInput = Observable();

var pinLoginStat = Observable(false);

function onViewActivate() {
    console.log("onViewActivate setting ---")
    currencyCode.value = loc.value.select;
    loadingCompleteStat.value = false;
    if (_model.activePerspectiveType == "user") {
        merchantServices.value = false;

        var usePinAvailable = Preferences.read("use_pin", false);
        console.log("use_pin : " + usePinAvailable)
        pinLoginStat.value = usePinAvailable;
    } else {
        merchantServices.value = true;
    }
    setttingsLoad();
}

function setttingsLoad() {
    busy.activate();

    var apiBodyObj = {};

    if (_model.activePerspectiveType == "user") {
        apiBodyObj.community_id = 0;
    } else {
        apiBodyObj.community_id = _model.nowCommunityID;
    }

    _WebService.request("settings", apiBodyObj).then(function (result) {
        gotSetttingsResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotSetttingsResult(result) {
    console.log("gotUpdateprofileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result.defaults;

        if (resultObj.wallet_id == null) {
            currencyCode.value = loc.value.select;
        } else {
            if (resultObj.wallet == null) {
                currencyCode.value = loc.value.select;
            } else {
                currencyCode.value = resultObj.wallet.currency_code;
            }
        }
}

    loadingCompleteStat.value = true;
}


function walletOptionChangedEvent(args) {
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.wallet_id = args.walletId;

    _WebService.request("settings/setdefaults", apiBodyObj).then(function (result) {
        gotSetdefaultsResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotSetdefaultsResult(result) {
    console.log("gotSetdefaultsResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        currencyCode.value = resultObj.wallet.currency_code;
    }
}

function pinchangeButtonClicked() {
    pinchangePopupShow.value = true;

    newPinInput.value = "";
    confirmPinInput.value = "";
}

function pinchangeClicked() {
    if (newPinInput.value != confirmPinInput.value) {
        tagEvents.emit("toastShow", { message: "PIN and confirm PIN does not match" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.pin = newPinInput.value;

    _WebService.request("user/setrestrictions", apiBodyObj).then(function (result) {
        gotPinChangeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotPinChangeResult(result) {
    console.log("gotPinChangeResult")

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        pinchangePopupShow.value = false;
        tagEvents.emit("toastShow", { message: "PIN Changed" });
    }
}



function pinLoginStatChange() {
    console.log("pinLoginStat  " + JSON.stringify(pinLoginStat.value));

    if (pinLoginStat.value == true) {
        Preferences.write("use_pin", true);
    } else {
        Preferences.write("use_pin", null);
    }
}


function passchangeButtonClicked() {
    oldPassInput.value = "";
    newPassInput.value = "";
    confirmPassInput.value = "";

    passwordchangePopupShow.value = true;
}

function passwordchangeClicked() {
    if (newPassInput.value != confirmPassInput.value) {
        tagEvents.emit("toastShow", { message: "Password and confirm password does not match" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.current_password = oldPassInput.value;
    apiBodyObj.user_password = newPassInput.value;

    _WebService.request("user/changepassword", apiBodyObj).then(function (result) {
        gotPasswordChangeResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotPasswordChangeResult(result) {
    console.log("gotPasswordChangeResult")

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        passwordchangePopupShow.value = false;
        tagEvents.emit("toastShow", { message: "Password Changed" });
        logoutrocessCall();
    }
}

function logoutrocessCall() {
    Preferences.write("email", null);
    Preferences.write("access_token", null);
    Preferences.write("refresh_token", null);
    Preferences.write("token_type", null);
    Preferences.write("expires_time", null);
    // Preferences.write("use_pin", null);

    router.goto("loginPage");
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    onViewActivate: onViewActivate,
    merchantServices: merchantServices,
    loadingCompleteStat: loadingCompleteStat,

    currencyCode: currencyCode,
    walletOptionChangedEvent: walletOptionChangedEvent,

    pinchangeButtonClicked: pinchangeButtonClicked,
    pinchangePopupShow: pinchangePopupShow,
    newPinInput: newPinInput,
    confirmPinInput: confirmPinInput,
    pinchangeClicked: pinchangeClicked,

    passwordchangePopupShow: passwordchangePopupShow,
    passchangeButtonClicked: passchangeButtonClicked,
    passwordchangeClicked: passwordchangeClicked,
    oldPassInput: oldPassInput,
    newPassInput: newPassInput,
    confirmPassInput: confirmPassInput,

    pinLoginStat: pinLoginStat,
    pinLoginStatChange: pinLoginStatChange,

}
