var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var editingMode = Observable(false);
var profileImage = Observable();
var profileMemberCount = Observable();
var profileStaffCount = Observable();
var profileName = Observable();
var profileID = Observable();
var profileEmail = Observable();
var profileMobile = Observable();
var profileCountryCode = Observable();
var profileCity = Observable();
var profileDetails = Observable();
var profileCountry = Observable();
var profileCountryId = Observable();

var imageSelectUserPopupShow = Observable(false);
var inputValueObj = {};
var saveButtonVisible = Observable(false);

function onViewActivate() {
    console.log("onViewActivate")
    console.log(JSON.stringify(_model.communityprofileDetails.value));

    profileImage.value = _model.communityImageUrl + _model.communityprofileDetails.value.id+"?"+Math.random();;
    profileMemberCount.value = _model.communityprofileDetails.value.member_count;
    profileStaffCount.value = _model.communityprofileDetails.value.staff_count;
    profileName.value = _model.communityprofileDetails.value.community_name;
    profileID.value = _model.communityprofileDetails.value.id;
    profileEmail.value = _model.communityprofileDetails.value.community_email;
    profileMobile.value = _model.communityprofileDetails.value.community_mobile;
    profileCountryCode.value = _model.communityprofileDetails.value.country_phonecode;
    profileCountryId.value = _model.communityprofileDetails.value.community_country.id;
    profileCountry.value = _model.communityprofileDetails.value.community_country.country_name;
    profileCity.value = _model.communityprofileDetails.value.community_city;
    profileDetails.value = _model.communityprofileDetails.value.community_description;

    console.log("profileCountryCode.value");
    console.log(profileCountryCode.value);
}


function editModeToggleClicked() {
    editingMode.value = !editingMode.value;

    // inputValueObj = {};
    // saveButtonVisible.value = false;
}


function onEditTextChangeEvent(args) {
    //console.log(JSON.stringify(args))
    // console.log(JSON.stringify(args.value))

    saveFieldDataChanges(args.field, args.text);

}

function onCountryCodeChangeEvent(args) {
    profileCountryCode.value = args.countryPhoneCode;

    saveFieldDataChanges("country_phonecode", args.countryPhoneCode);
}

function onCountryChangeEvent(args) {
    profileCountryId.value = args.countryID;
    profileCountry.value = args.countryName;

    saveFieldDataChanges("country_id", args.countryID);
}



function saveFieldDataChanges(editedField, editedValue) {
    inputValueObj[editedField] = editedValue;

    saveButtonVisible.value = true;
    console.log(JSON.stringify(inputValueObj))
}

function editSaveClickHandle() {
    busy.activate();

    _WebService.request("community/edit/", inputValueObj).then(function (result) {
        gotUpdateprofileResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}


function gotUpdateprofileResult(result) {
    console.log("gotUpdateprofileResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;

        if (inputValueObj.community_name) {
            console.log("community_name change")
            _model.communityprofileDetails.value.community_name = inputValueObj.community_name;
        }

        if (inputValueObj.community_mobile) {
            console.log("community_mobile change")
            _model.communityprofileDetails.value.community_mobile = inputValueObj.community_mobile;
        }

        if (inputValueObj.country_phonecode) {
            console.log("country_phonecode change")
            _model.communityprofileDetails.value.country_phonecode = inputValueObj.country_phonecode;
        }

        inputValueObj = {};
        saveButtonVisible.value = false;

        tagEvents.emit("toastShow", { message: loc.value.profile_updated_successfully });
        tagEvents.emit("profileRefresh");

    }
}



function onUserImagegChangeClick() {
    imageSelectUserPopupShow.value = true;
}

function onUserImagegSelectionComplete(args) {
    imageSelectUserPopupShow.value = false;

    //uploadImageData = args.imageData;
    // slipUploadImageUrl.value = args.imagePath;

    busy.activate();

    var imageEncoded = encodeURIComponent(args.imageData);
    
    var apiBodyObj = {};
    apiBodyObj.image = imageEncoded;

    _WebService.request("community/uploadavatar", apiBodyObj).then(function (result) {
        gotUploadProfileImageResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotUploadProfileImageResult(result) {
    console.log("gotUploadProfileImageResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.data;

        // inputValueObj = {};
        // saveButtonVisible.value = true;

        // tagEvents.emit("toastShow", { message: "Profile Updated Successfully" });
        tagEvents.emit("profileRefresh");
        profileImage.value = _model.communityImageUrl + _model.communityprofileDetails.value.id+"?"+Math.random();;
    
    }
}





function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    onViewActivate: onViewActivate,
    editingMode: editingMode,
    editModeToggleClicked: editModeToggleClicked,
    editSaveClickHandle: editSaveClickHandle,
    profileImage: profileImage,
    profileMemberCount: profileMemberCount,
    profileStaffCount: profileStaffCount,
    profileName: profileName,
    profileID: profileID,
    profileEmail: profileEmail,
    profileMobile: profileMobile,
    profileCity: profileCity,
    profileDetails: profileDetails,
    profileCountry: profileCountry,
    profileCountryId: profileCountryId,
    onCountryChangeEvent: onCountryChangeEvent,
    onEditTextChangeEvent: onEditTextChangeEvent,

    onUserImagegChangeClick: onUserImagegChangeClick,
    imageSelectUserPopupShow: imageSelectUserPopupShow,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,

    saveButtonVisible: saveButtonVisible,

    onCountryCodeChangeEvent: onCountryCodeChangeEvent,
    profileCountryCode: profileCountryCode,
}