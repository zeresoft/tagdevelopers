var Observable = require("FuseJS/Observable");

var _model = require("Models/GlobalModel");
var _WebService = require("Models/WebService");

var communityArr = Observable();
var communityListData = Observable();

var communitySelectedIndex = Observable();

var createMerchantPopupShow = Observable(false);
var merchantNameInput = Observable("");
var categoryId;
var countryId = Observable();
var countryName = Observable();
var categoryListItems = Observable();
var categoryselectIndex = Observable();
var privateStatusInput = Observable(false);

var changeFefreshNeded = true;

tagEvents.on("profileRefresh", function (arg) {
  if (changeFefreshNeded) {
    console.log("profileRefresh : community list")
    var communityListUpdate = communityListData.toArray();

    if (_model.activePerspectiveType == "community") {
      for (var i = 0; i < communityListUpdate.length; i++) {
        if (communityListUpdate[i].iconType == "staff") {
          if (communityListUpdate[i].community_id == _model.nowCommunityID) {
            communityListUpdate[i].community_name = _model.communityprofileDetails.value.community_name;
          }
        }
      }
      
    } else {
        if (communityListUpdate[0].iconType == "me") {
            communityListUpdate[0].community_name = _model.userprofileDetails.value.user_firstname + " " + _model.userprofileDetails.value.user_lastname;
        }
    }

    communityListData.clear();
    communityListData.addAll(communityListUpdate);
  }

  changeFefreshNeded = true;
});

function menuDataLoadStart() {
  console.log("menuDataLoadStart");
  checkActivePerspective();
}

function checkActivePerspective() {
  console.log("checkActivePerspective");

  _WebService.request("perspective").then(function (result) {
    gotActivePerspectiveResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotActivePerspectiveResult(result) {
  console.log("gotActivePerspectiveResult");

  var arr = result;

  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultObj = arr.result;

    menuButtonsShow.raise();
    loadingStatusChange.raise();

    _model.activePerspectiveType = resultObj.type;
    console.log("activePerspectiveType", _model.activePerspectiveType);

    if (resultObj.charge_token_creation == 1) {
      _model.chargeTokenCreation = "yes";
    } else {
      _model.chargeTokenCreation = "no";
    }

    if (_model.activePerspectiveType == "community") {
      _model.nowCommunityID = resultObj.id;
    } else {
      _model.nowCommunityID = 0;
      updateSideHeader();
    }

    loadStaffCommunities();
  }
}

function updateSideHeader() {
  if (_model.activePerspectiveType == "community") {
    _model.perspectiveIsUser.value = false;
  } else {
    _model.perspectiveIsUser.value = true;
  }

  changeFefreshNeded = false;
  tagEvents.emit("profileRefresh");
}

function loadStaffCommunities() {
  console.log("loadStaffCommunities");

  communityArr.clear();

  _WebService.request("user/staffof").then(function (result) {
    gotStaffCommunitiesResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotStaffCommunitiesResult(result) {
  var arr = result;

  if (arr.status == "success") {
    var resultArr = arr.result;

    for (var i = 0; i < resultArr.length; i++) {
      resultArr[i].iconType = "staff";
    }
    communityArr.addAll(resultArr);

    //loadMemberCommunities();
    listComminity();
  }
}

function loadMemberCommunities() {
  console.log("loadMemberCommunities");

  _WebService.request("user/memberof").then(function (result) {
    gotMemberCommunitiesResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotMemberCommunitiesResult(result) {
  var arr = result;

  if (arr.status == "success") {
    var resultArr = arr.result;
    for (var i = 0; i < resultArr.length; i++) {
      resultArr[i].iconType = "member";
    }
    communityArr.addAll(resultArr);

    listComminity();
  }
}

function listComminity() {
  if (communityArr.length > 0) {
    var meUserName =
      _model.userprofileDetails.value.user_firstname +
      " " +
      _model.userprofileDetails.value.user_lastname;
    var itemObj = { community_name: meUserName, iconType: "me" };
    communityArr.insertAt(0, itemObj);
  }
  //to Create a new Community
  // Only an email verified user can create a community add checking

  if (_model.activePerspectiveType == "user") {
    navigationStatus(false);

    communityArr.add({
      community_name: loc.value.create_company,
      iconType: "createnew"
    });
    tagEvents.emit("dashboardShowEvent");
  } else if (_model.activePerspectiveType == "community") {
    communityDetailsLoad();
  }

  communityListData.clear();

  communityArr.forEach(function (item, index) {
    item.index = index;
    communityListData.add(item);
  });

  communityActiveHeighlight();
}

function communityDetailsLoad() {
  console.log("communityDetailsLoad");

  _WebService.request("community/details/" + _model.nowCommunityID).then(function (result) {
    gotCommunityDetailsLoadResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });

}

function gotCommunityDetailsLoadResult(result) {
  var arr = result;
  console.log(JSON.stringify(arr));
  if (arr.status == "success") {
    var resultObj = arr.result;
    _model.communityprofileDetails.value = resultObj;

    _model.nowCommunityID = resultObj.id;
    _model.nowCommunityRoleType = resultObj.role.role_type;
    _model.nowCommunityRoleName = resultObj.role.role_name;

    updateSideHeader();

    //navigationStatus(true);
    menuButtonsShow.raise();
    tagEvents.emit("dashboardShowEvent");
  }
}

function switchPerspective(switchTo, communitySwitchID) {
  console.log("switchPerspective");
  tagEvents.emit("menuCloseEvent");
  router.goto("blankPage");
  _model.phpWalletStatus = null;

  var urlPath;
  if (switchTo == "me") {
    urlPath = "perspective/switch";
  } else {
    urlPath = "perspective/switch/" + communitySwitchID;
  }

  _WebService.request(urlPath).then(function (result) {
    gotSwitchPerspectiveResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotSwitchPerspectiveResult(result) {
  var arr = result;
  console.log("gotSwitchPerspectiveResult");
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultObj = arr.result;

    _model.activePerspectiveType = resultObj.type;

    if (resultObj.charge_token_creation == 1) {
      _model.chargeTokenCreation = "yes";
    } else {
      _model.chargeTokenCreation = "no";
    }
    if (_model.activePerspectiveType == "user") {
      _model.nowCommunityID = 0;
      menuButtonsShow.raise();

      navigationStatus(false);
      tagEvents.emit("dashboardShowEvent");

      var lastCommunityAdd = communityListData.getAt(
        communityListData.length - 1
      );
      if (lastCommunityAdd.iconType != "createnew") {
        communityListData.add({
          community_name: loc.value.create_company,
          iconType: "createnew",
          index: communityListData.length
        });
        communityActiveHeighlight();
      }
      updateSideHeader();
    } else {
      var lastCommunityRemove = communityListData.getAt(
        communityListData.length - 1
      );

      if (lastCommunityRemove.iconType == "createnew") {
        communityListData.removeAt(communityListData.length - 1);
        communityActiveHeighlight();
      }
      _model.nowCommunityID = resultObj.id;
      communityDetailsLoad();
    }

    if (resultObj.crypto_addresses) {
      if (resultObj.crypto_addresses.single_address) {
        console.log("address available ----------------");
        _model.singleCryptoAddresses =
          resultObj.crypto_addresses.single_address;
      } else {
        getMyAddressCall();
      }
    } else {
      getMyAddressCall();
    }
  } else {
    switchPerspective("me");
  }
}

function communityActiveHeighlight() {
  console.log("***** ---------------- ------------------ **********");
  console.log("communityActiveHeighlight  : " + _model.nowCommunityID);
  console.log("***** ---------------- ------------------ **********");

  communitySelectedIndex.value = "-1";

  communityListData.forEach(function (item, index) {
    if (_model.nowCommunityID == 0 && item.iconType == "me") {
      communitySelectedIndex.value = index;
    } else {
      if (item.community_id == _model.nowCommunityID) {
        communitySelectedIndex.value = index;
      }
    }
  });
}

function communityList_changeHandler(args) {
  var selectedItem = args.data;
  console.log("*************************");
  console.log(JSON.stringify(selectedItem));

  if (selectedItem.iconType == "createnew") {
    communitySelectedIndex.value = "0";
    createNewCompanyClick();
  } else if (selectedItem.iconType == "me") {
    switchPerspective("me");
  } else {
    if (_model.nowCommunityID != selectedItem.community_id) {
      console.log("--------------------");
      console.log(_model.nowCommunityID, selectedItem.community_id);

      switchPerspective("community", selectedItem.community_id);
    }
  }
}

function navigationStatus(stat) {
  if (stat) {
    // menuArea.alpha = 1;
    // _model.nfcReturnTarget = "search";
  } else {
    //menuArea.alpha = 0.2;
    // _model.nfcReturnTarget = "";
  }

  // menu.enabled = stat;
}

function createNewCompanyClick() {
  createMerchantPopupShow.value = true;

  countryId.value = _model.userprofileDetails.value.user_country.country_id;
  countryName.value = _model.userprofileDetails.value.user_country.country_name;

  // loadCategoryListCall();
}

function loadCategoryListCall() {
  console.log("loadCategoryListCall");
  categoryListItems.clear();
  busy.activate();

  _WebService.request("category/list").then(function (result) {
    gotCategoryListResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotCategoryListResult(result) {
  console.log("gotCategoryListResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultArr = arr.result;
    if (resultArr !== null) {
      categoryListItems.addAll(resultArr);

      categoryId = resultArr[0].id;
      categoryselectIndex.value = 0;
    }
  }
}

function onCategorySelectionChange(args) {
  var selectedItem = JSON.parse(args.selectedItem);
  console.log(JSON.stringify(selectedItem));

  categoryId = selectedItem.id;
}

function onCountrySelectMerchantEvent(args) {
  countryId.value = args.countryID;
  countryName.value = args.countryName;
}

function popupCloseClicked() {
  createMerchantPopupShow.value = false;
}

function merchantCreate_clickHandler() {
  if (merchantNameInput.value !== "") {
    busy.activate();

    var apiBodyObj = {};

    apiBodyObj.community_name = merchantNameInput.value;
    apiBodyObj.country_id = countryId.value;
    if (privateStatusInput) {
      apiBodyObj.community_type = "private";
    }
    // apiBodyObj.category_id = categoryId;
    //other
    apiBodyObj.category_id = "26";

    _WebService.request("community/create", apiBodyObj).then(function (result) {
      gotCommunityCreateResult(result);
    }).catch(function (error) {
      console.log("Couldn't get data: " + error);
      showNetworkErrorMessage();
    });
  }
}

function gotCommunityCreateResult(result) {
  console.log("gotCommunityCreateResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    createMerchantPopupShow.value = false;

    tagEvents.emit("toastShow", {
      message: loc.value.merchant_creation_success
    });

    switchToNewCommunity(arr.community_id);
  } else {
    if (arr.error == "community_name_already_used") {
      tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.merchant_name_inuse });
    }
  }
}

function switchToNewCommunity(communitySwitchID) {
  console.log("switchToNewCommunity");

  tagEvents.emit("menuCloseEvent");

  router.goto("blankPage");

  _WebService.request("perspective/switch/" + communitySwitchID).then(function (result) {
    gotSwitchToNewCommunityResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotSwitchToNewCommunityResult(result) {
  var arr = result;
  console.log("gotSwitchToNewCommunityResult");
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultObj = arr.result;
    _model.activePerspectiveType = resultObj.type;
    _model.nowCommunityID = resultObj.id;
    getMyAddressCall();

    loadStaffCommunities();
  } else {
    switchPerspective("me");
  }
}

function getMyAddressCall() {
  _model.singleCryptoAddresses = "";

  _WebService.request("wallet/GetMyAddress").then(function (result) {
    gotGetMyAddressResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotGetMyAddressResult(result) {
  console.log("gotGetMyAddressResult");
  busy.deactivate();

  var arr = result;
  console.log(JSON.stringify(arr));

  if (arr.status == "success") {
    var resultObj = arr.result;
    _model.singleCryptoAddresses = resultObj.address;
  }
}

function showNetworkErrorMessage() {
  busy.deactivate();
  tagEvents.emit("alertEvent", {
    type: "info",
    title: loc.value.error,
    message: loc.value.network_error_message
  });
}

function communityUserRollCheck() {
  console.log("communityUserRollCheck");

  _WebService.request("community/getpermissions").then(function (result) {
    gotCommunityUserRollResult(result);
  }).catch(function (error) {
    console.log("Couldn't get data: " + error);
    showNetworkErrorMessage();
  });
}

function gotCommunityUserRollResult(result) {
  var arr = result;

  if (arr.status == "success") {
    var resultObj = arr.result;

    _model.nowCommunityRoleType = resultObj.role_type;
    _model.nowCommunityRoleName = resultObj.role_name;

    loadStaffCommunities();
    navigationStatus(true);

    router.goto("productsListPage");
  }
}

module.exports = {
  menuDataLoadStart: menuDataLoadStart,

  communityListData: communityListData,
  communitySelectedIndex: communitySelectedIndex,
  communityList_changeHandler: communityList_changeHandler,

  createMerchantPopupShow: createMerchantPopupShow,
  merchantNameInput: merchantNameInput,
  countryId: countryId,
  countryName: countryName,
  onCountrySelectMerchantEvent: onCountrySelectMerchantEvent,
  createNewCompanyClick: createNewCompanyClick,
  popupCloseClicked: popupCloseClicked,
  onCategorySelectionChange: onCategorySelectionChange,
  merchantCreate_clickHandler: merchantCreate_clickHandler,

  categoryListItems: categoryListItems,
  categoryselectIndex: categoryselectIndex,
  privateStatusInput: privateStatusInput
};
