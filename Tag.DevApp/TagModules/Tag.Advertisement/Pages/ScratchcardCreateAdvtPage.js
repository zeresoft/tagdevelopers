var Observable = require('FuseJS/Observable');
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var editingMode = Observable(false);
var editAdvId = "";
var editUrlType = "";

var titleInput = Observable("");

var adImageData = "";
var displayAdImage = Observable();
var displayAdImageLocal = Observable(true);
var imageSelectPopupShow = Observable(false);

var adUrlInput = Observable("");
var adVideoLinkInput = Observable("");

var imageAdpossible = Observable(true);
var videoAdpossible = Observable(true);


this.Parameter.onValueChanged(module, function (param) {
    console.log(JSON.stringify(param.advertisement))
    editingMode.value = false;

    editAdvId = "";
    editUrlType = "";

    titleInput.value = "";
    adImageData = ""
    displayAdImage.value = "";
    displayAdImageLocal.value = true;

    adUrlInput.value = "";
    adVideoLinkInput.value = "";

    imageAdpossible.value = true;
    videoAdpossible.value = true;

    if (param.advertisement != null) {
        editingMode.value = true;

        editAdvId = param.advertisement.id;
        titleInput.value = param.advertisement.title;

        editUrlType = param.advertisement.url_type;

        if (editUrlType == 1) {
            displayAdImageLocal.value = false;
            displayAdImage.value = param.advertisement.url_data;

            imageAdpossible.value = true;
            videoAdpossible.value = false;
        } else if (editUrlType == 3) {
            adVideoLinkInput.value = param.advertisement.url_data;

            imageAdpossible.value = false;
            videoAdpossible.value = true;
        }

    }

})

function adImageSelectClick() {
    imageSelectPopupShow.value = true;
}

function adImageRemoveClick() {
    adImageData = ""
    displayAdImage.value = "";
    displayAdImageLocal.value = true;
}

function onImageSelectionComplete(args) {
    imageSelectPopupShow.value = false;
    adImageData = encodeURIComponent(args.imageData);
    displayAdImageLocal.value = true;
    displayAdImage.value = args.imagePath;
}

function createCardClicked() {
    console.log("createCardClicked");

    if (titleInput.value == "") {
        tagEvents.emit("toastShow", { message: "Title should not be empty" });
        return;
    }

    if (adImageData == "" && adVideoLinkInput.value == "") {
        tagEvents.emit("toastShow", { message: "Please add an advertisement" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.title = titleInput.value;

    //url_type 1 = Image (Base64), 2= Web page url, 3= Video Url
    if (adImageData != "") {
        apiBodyObj.url_type = 1;
        apiBodyObj.url_data = adImageData;
    } else {
        apiBodyObj.url_type = 3;
        apiBodyObj.url_data = adVideoLinkInput.value;
    }

    _WebService.request("ScratchAds/CreateAd", apiBodyObj).then(function (result) {
        gotScratchCardAdCreateResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotScratchCardAdCreateResult(result) {
    console.log("gotScratchCardAdCreateResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        router.goBack();
        tagEvents.emit("toastShow", { message: "Advertisement created successfully" });
    } else {
        tagEvents.emit("toastShow", { message: "Unable to create advertisement" });
    }
}

function updateCardClicked() {
    console.log("updateCardClicked");

    if (titleInput.value == "") {
        tagEvents.emit("toastShow", { message: "Title should not be empty" });
        return;
    }

    if (editUrlType == 3 && adVideoLinkInput.value == "") {
        tagEvents.emit("toastShow", { message: "Please add an advertisement" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.update_id = editAdvId;
    apiBodyObj.title = titleInput.value;
    apiBodyObj.url_type = editUrlType;

    //url_type 1 = Image (Base64), 2= Web page url, 3= Video Url
    if (editUrlType == 1) {
        if (adImageData != "") {
            apiBodyObj.url_data = adImageData;
        }
    } else if (editUrlType == 3) {
        apiBodyObj.url_data = adVideoLinkInput.value;
    }

    _WebService.request("ScratchAds/UpdateScratchCardAd", apiBodyObj).then(function (result) {
        gotScratchCardAdUpdateResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotScratchCardAdUpdateResult(result) {
    console.log("gotScratchCardAdUpdateResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;
        // router.goBack();
        tagEvents.emit("toastShow", { message: "Advertisement updated successfully" });
    } else {
        tagEvents.emit("toastShow", { message: "Unable to update Advertisement" });
    }
}

function deleteCardClicked() {
    console.log("deleteCardClicked");
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.id = editAdvId;

    _WebService.request("ScratchAds/DeleteScratchCardAd", apiBodyObj).then(function (result) {
        gotDeleteCardResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotDeleteCardResult(result) {
    console.log("gotDeleteCardResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultObj = arr.result;

        router.goBack();
        tagEvents.emit("toastShow", { message: "Advertisement deleted" });

    } else {
        tagEvents.emit("toastShow", { message: loc.value.error_occurred });
    }
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    editingMode: editingMode,
    titleInput: titleInput,

    displayAdImage: displayAdImage,
    displayAdImageLocal: displayAdImageLocal,
    imageSelectPopupShow: imageSelectPopupShow,
    onImageSelectionComplete: onImageSelectionComplete,
    adImageSelectClick: adImageSelectClick,
    adImageRemoveClick: adImageRemoveClick,

    imageAdpossible: imageAdpossible,
    videoAdpossible: videoAdpossible,

    adUrlInput: adUrlInput,
    adVideoLinkInput: adVideoLinkInput,

    createCardClicked: createCardClicked,
    updateCardClicked: updateCardClicked,
    deleteCardClicked: deleteCardClicked,

}
