var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');
var GeoLocation = require("FuseJS/GeoLocation");
var placeTagImage = Observable();
var titleText = Observable();
var advtUrlText = Observable();
var rad_km = Observable();
var limitTxt = Observable();
var uniqTxt = Observable();
var imageSelectPopupShow = Observable(false);
var infoImage = Observable();
var description = Observable();
var createAdvrtBo = Observable(false);
var advtStatus = Observable();
var advtId = Observable();
var imagePathChangeBo = Observable(false);
var b64Image;
var str;
var locationData;
var timeoutLocation = Observable("");
var timeoutMs = 1000;

this.Parameter.onValueChanged(module, function (param) {

    _model.eventLocationSelected = null;
    if (param) {
        console.log("1")
        console.log(JSON.stringify(param.args.data))
        createAdvrtBo.value = false;
        advtId.value = param.args.data._id;
        titleText.value = param.args.data.title;
        limitTxt.value = param.args.data.limits;
        uniqTxt.value = param.args.data.uniques;
        rad_km.value = param.args.data.view_range_km;
        advtUrlText.value = param.args.data.type_data.url;
        placeTagImage.value = param.args.data.type_data.image;
        advtStatus.value = param.args.data.status;
        locationData = param.args.data.location;
        imagePathChangeBo.value = false;
        b64Image = "";

    } else {
        console.log("2")
        locationData = "";
        titleText.value = "";
        limitTxt.value = "";
        uniqTxt.value = "";
        rad_km.value = "";
        advtUrlText.value = "";
        placeTagImage.value = "";
        createAdvrtBo.value = true;
        getCurrentLocation()

    }

});


function getCurrentLocation() {

    GeoLocation.getLocation(timeoutMs).then(function (location) {
        timeoutLocation.value = JSON.stringify(location);
        var latitude = JSON.parse(timeoutLocation.value).latitude;
        var longitude = JSON.parse(timeoutLocation.value).longitude;
        var newLocationData = [longitude, latitude];
        locationData = newLocationData;
        console.log("got geolocationnnnnnnnnn " + locationData);
    }).catch(function (fail) {
        console.log("getLocation fail " + fail);
        timeoutMs = 500;
        getCurrentLocation();
    });

}

function onViewActivateCreate() {

    console.log(_model.eventLocationSelected);
    if (_model.eventLocationSelected) {

        var lat = JSON.parse(_model.eventLocationSelected).lat;
        var lng = JSON.parse(_model.eventLocationSelected).lng;
        var newLocationData = [lng, lat];
        locationData = newLocationData;
        console.log(locationData);
    }

}

function onImageUpload() {
    console.log("werwe");
    imageSelectPopupShow.value = true;
}

function onUserImagegSelectionComplete(args) {
    imagePathChangeBo.value = true;
    imageSelectPopupShow.value = false;
    placeTagImage.value = args.imagePath;
    console.log(args.imagePath);
    b64Image = encodeURIComponent(args.imageData);
}

function loadLocationMap() {
    router.push("tagWildMapPage", { locationData });
}

function advtSaveClick() {

    if (titleText.value == "") {
        tagEvents.emit("toastShow", { message: "Title should not be empty" });
        return;
    }
    if (limitTxt.value == "") {
        tagEvents.emit("toastShow", { message: "Limit should not be empty" });
        return;
    }
    if (uniqTxt.value == "") {
        tagEvents.emit("toastShow", { message: "Uniques should not be empty" });
        return;
    }

    if (limitTxt.value <= 0 || uniqTxt.value <= 0) {
        tagEvents.emit("toastShow", { message: "limits & Uniques should not be zero" });
        return;
    }

    if (placeTagImage.value == "") {
        tagEvents.emit("toastShow", { message: "Please upload image" });
        return;
    }

    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.title = titleText.value;
    apiBodyObj.type = "Advertisement";

    var jsonObt = {};
    if (advtUrlText.value != "") {
        jsonObt.url = advtUrlText.value;
    }
    //jsonObt.image = "";
    str = JSON.stringify(jsonObt);
    apiBodyObj.location = JSON.stringify(locationData);
    apiBodyObj.view_range_km = rad_km.value;
    apiBodyObj.uniques = uniqTxt.value;
    apiBodyObj.limits = limitTxt.value;
    apiBodyObj.type_data = str;

    if (createAdvrtBo.value == true) {
        apiBodyObj.image = b64Image;
        _WebService.request("tagwild/CreateNewAd", apiBodyObj).then(function (result) {
            gotAdvtsSaveResultData(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    } else {
        if (b64Image != "") {
            apiBodyObj.image = b64Image;
        }

        _WebService.request("tagwild/UpdateNewAd/" + advtId.value, apiBodyObj).then(function (result) {
            gotAdvtsUpdateResultData(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    }

}

function gotAdvtsSaveResultData(result) {
    console.log("gotAdvtsSaveResultData")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        router.goBack();
    } else {
        tagEvents.emit("toastShow", { message: "Insufficient balance to create advertisement" });
    }
}

function gotAdvtsUpdateResultData(result) {
    console.log("gotAdvtsUpdateResultData")
    busy.deactivate();
    var arr = result;
    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Successfully Updated" });
        router.goBack();
    } else {
        //tagEvents.emit("toastShow", { message: "Insufficient balance to create prize" });

    }
}

function onInfoImgUpload() {
    imageSelectPopupShow.value = true;
}

function advtDeleteClick() {
    tagEvents.emit("alertEvent", { type: "info", title: "Delete Advertisement", message: "Do you want to delete this advertisement?", callback: alertCallback });
}

function alertCallback() {
    busy.activate();
    var apiBodyObj = {};
    _WebService.request("tagwild/deleteAd/" + advtId.value, apiBodyObj).then(function (result) {
        gotAdvtsDeleteResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotAdvtsDeleteResult(result) {
    console.log("gotAdvtsDeleteResult")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Successfully Deleted" });
        router.goBack();
    } else {

    }
}

function cancelAdvrt() {
    router.goBack();

}

function activateAdvtClick() {
    busy.activate();
    var apiBodyObj = {};

    if (advtStatus.value == "active") {
        apiBodyObj.id = advtId.value;
        apiBodyObj.status = "deactive"
    } else {
        apiBodyObj.id = advtId.value;
        apiBodyObj.status = "active"
    }

    _WebService.request("tagwild/UpdateNewAd", apiBodyObj).then(function (result) {
        gotActivateAdvtResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotActivateAdvtResult(result) {
    console.log("gotActivateAdvtResult")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        router.goBack();
    } else {

    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    advtStatus: advtStatus,
    loadLocationMap: loadLocationMap,
    rad_km: rad_km,
    advtId: advtId,
    limitTxt: limitTxt,
    uniqTxt: uniqTxt,
    infoImage: infoImage,
    advtUrlText: advtUrlText,
    cancelAdvrt: cancelAdvrt,
    titleText: titleText,
    description: description,
    imagePathChangeBo: imagePathChangeBo,
    createAdvrtBo: createAdvrtBo,
    onImageUpload: onImageUpload,
    placeTagImage: placeTagImage,
    advtSaveClick: advtSaveClick,
    onInfoImgUpload: onInfoImgUpload,
    advtDeleteClick: advtDeleteClick,
    activateAdvtClick: activateAdvtClick,
    imageSelectPopupShow: imageSelectPopupShow,
    onViewActivateCreate: onViewActivateCreate,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete,
}

