var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');

var advrtListData = Observable();

function onViewActivateCreate() {
    advrtListData.clear();
    getAdvertListData();
}

function pagingLoadMore() {
    if (advrtListData.length != 0) {
        getAdvertListData();
    }
}

function getAdvertListData() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.count = 20;
    apiBodyObj.offset = advrtListData.length;

    _WebService.request("ScratchAds/ListScratchAds", apiBodyObj).then(function (result) {
        gotCreatedAdvrtListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCreatedAdvrtListResult(result) {
    console.log("gotCreatedAdvrtListResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        advrtListData.addAll(resultArr);
    }

}

function editScratchcardAdClicked(args) {
    router.push("scratchcardCreateAdvtPage", { advertisement: args.data, datId: Math.random() });
}

function createScratchcardAdClicked() {
    router.push("scratchcardCreateAdvtPage", { datId: Math.random() });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    pagingLoadMore: pagingLoadMore,
    advrtListData: advrtListData,
    editScratchcardAdClicked: editScratchcardAdClicked,
    onViewActivateCreate: onViewActivateCreate,
    createScratchcardAdClicked: createScratchcardAdClicked,
}