var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var _model = require('Models/GlobalModel');
var GeoLocation = require("FuseJS/GeoLocation");
var levelListIndex = Observable(-1);
var difficultyListIndex = Observable(-1);
var levelList = Observable({ name: "1", value: "1" }, { name: "2", value: "2" }, { name: "3", value: "3" }, { name: "4", value: "4" }, { name: "5", value: "5" });
var difficultyList = Observable({ name: "1", value: "1" }, { name: "2", value: "2" }, { name: "3", value: "3" }, { name: "4", value: "4" }, { name: "5", value: "5" });
var createPriceBo = Observable(false);
var loadType = Observable({ name: "Coupon", value: "coupon_prize" }, { name: "Wallet", value: "wallet_prize" });
var loadTypeIndex = Observable(-1);
var couponType = Observable();
var couponTypeIndex = Observable(-1);
var imageSelectPopupShow = Observable(false);
var placeTagImage = Observable();
var descriptionText = Observable();
var rad_km = Observable();
var limitTxt = Observable();
var totalAvbleTxt = Observable();
var titleTxt = Observable();
var couponLoadBo = Observable(false);
var walletTranBo = Observable(false);
var priceStatus = Observable();
var currencyCode = Observable();
var walletAmout = Observable();
var imgUpload = Observable(false);
var deal_Id = Observable();
var levelValue;
var difficultValue;
var categoryType;
var walletId;
var b64Image;
var coupon_Id;
var locationData;
var timeoutLocation = Observable("");
var timeoutMs = 1000;

this.Parameter.onValueChanged(module, function (param) {

    _model.eventLocationSelected = null;
    couponType.clear();
    levelListIndex.value = -1;
    difficultyListIndex.value = -1;
    loadTypeIndex.value = -1;
    couponTypeIndex.value = -1;
    currencyCode.value = loc.value.select;
    walletId = "";
    coupon_Id = "";
    levelValue = "";
    difficultValue = "";
    placeTagImage.value = "";
    createPriceBo.value = false;
    walletAmout.value = "";
    deal_Id.value = "";

    if (param) {

        deal_Id.value = param.args.data.id;
        createPriceBo.value = false;
        titleTxt.value = param.args.data.title;
        descriptionText.value = param.args.data.description;
        rad_km.value = param.args.data.view_range_km;
        totalAvbleTxt.value = param.args.data.total_available;
        limitTxt.value = param.args.data.limits.max_limit_per_user;
        levelValue = param.args.data.level;
        difficultValue = param.args.data.probability;
        categoryType = param.args.data.type;
        priceStatus.value = param.args.data.status;
        placeTagImage.value = param.args.data.gift_image;
        locationData = param.args.data.location;
        getLevel();
        getDifficulty();
        getCategoryType(param);
        imgUpload.value = false;


    } else {

        locationData = "";
        titleTxt.value = "";
        walletId = "";
        deal_Id.value = "";
        levelValue = "";
        difficultValue = "";
        descriptionText.value = "";
        rad_km.value = "";
        limitTxt.value = "";
        walletAmout.value = "";
        totalAvbleTxt.value = "";
        placeTagImage.value = "";
        couponLoadBo.value = true;
        walletTranBo.value = false;
        loadTypeIndex.value = 0;
        createPriceBo.value = true;
        categoryType = "coupon_prize";
        levelListIndex.value = "";
        difficultyListIndex.value = "";
        levelListIndex.value = -1;
        difficultyListIndex.value = -1;
        getCouponList();
        getCurrentLocation();

    }


});

function getCurrentLocation() {

    GeoLocation.getLocation(timeoutMs).then(function (location) {
        timeoutLocation.value = JSON.stringify(location);
        var latitude = JSON.parse(timeoutLocation.value).latitude;
        var longitude = JSON.parse(timeoutLocation.value).longitude;
        var newLocationData = [longitude, latitude];
        locationData = newLocationData;
        console.log("got geolocationnnnnnnnnn " + locationData);
    }).catch(function (fail) {
        console.log("getLocation fail " + fail);
        timeoutMs = 500;
        getCurrentLocation();
    });

}
function onViewActivateCreate() {

    console.log(_model.eventLocationSelected);
    if (_model.eventLocationSelected) {


        var lat = JSON.parse(_model.eventLocationSelected).lat;
        var lng = JSON.parse(_model.eventLocationSelected).lng;
        var newLocationData = [lng, lat];
        locationData = newLocationData;
        console.log(locationData);


    }

}
function loadLocationMap() {

    router.push("tagWildMapPage", { locationData });

}

function getCategoryType(param) {

    var obsArray = loadType.toArray();
    console.log(JSON.stringify(obsArray))
    console.log(categoryType);
    for (var i = 0; i < obsArray.length; i++) {

        if (categoryType == obsArray[i].value) {
            loadTypeIndex.value = i;
            if (categoryType == "coupon_prize") {
                couponLoadBo.value = true;
                walletTranBo.value = false;
                coupon_Id = param.args.data.coupen_info.coupon_id;
                getCouponList();
            } else if (categoryType == "wallet_prize") {
                walletTranBo.value = true;
                couponLoadBo.value = false;
                currencyCode.value = param.args.data.wallet_info.wallet_code;
                walletId = param.args.data.wallet_info.wallet_code;
                walletAmout.value = param.args.data.wallet_info.amount;


            }

        }
    }


}

function getLevel() {
    var obsArray = levelList.toArray();
    console.log(JSON.stringify(obsArray))
    console.log(levelValue);
    for (var i = 0; i < obsArray.length; i++) {

        if (levelValue == obsArray[i].value) {
            levelListIndex.value = i;

        }
    }

}

function getDifficulty() {
    var obsArray = difficultyList.toArray();
    console.log(JSON.stringify(obsArray))
    console.log(difficultValue);
    for (var i = 0; i < obsArray.length; i++) {

        if (difficultValue == obsArray[i].value) {
            difficultyListIndex.value = i;

        }
    }

}

function onImageUpload() {

    console.log("imagee");
    imageSelectPopupShow.value = true;

}

function onUserImagegSelectionComplete(args) {

    imageSelectPopupShow.value = false;
    placeTagImage.value = args.imagePath;
    b64Image = encodeURIComponent(args.imageData);
    imgUpload.value = true;
    console.log(placeTagImage.value)


}

function levelCategoryChange(args) {
    console.log(args.selectedItem);
    levelValue = JSON.parse(args.selectedItem).value;
    console.log(levelValue);

}
function difficultyCategoryChange(args) {
    console.log(args.selectedItem);
    difficultValue = JSON.parse(args.selectedItem).value;
    console.log(difficultValue);

}

function CategoryLoadChange(args) {

    console.log(args.selectedItem);
    categoryType = JSON.parse(args.selectedItem).value;
    console.log(categoryType);
    if (categoryType == "coupon_prize") {

        couponLoadBo.value = true;
        walletTranBo.value = false;
        // couponTypeIndex.value = "";
        var obsArray = couponType.toArray();
        if (obsArray.length == 0) {
            getCouponList();
        }

    } else {
        walletTranBo.value = true;
        couponLoadBo.value = false;


    }

}

function getCouponList() {

    busy.activate();
    var apiBodyObj = {};
    _WebService.request("Coupon/GetAllPrizeCoupons", apiBodyObj).then(function (result) {
        gotListCouponResultData(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotListCouponResultData(result) {
    console.log("gotListCouponResultData")
    busy.deactivate();
    couponType.clear();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            couponType.addAll(resultArr);
            if (resultArr.length == 1) {

                couponTypeIndex.value = 0;
                var obsArray = couponType.toArray();
                coupon_Id = obsArray[0].coupon_id;
                // tagEvents.emit("toastShow", { message: "Found only one coupon" });

            }
            if (createPriceBo.value == false) {
                getCouponId();

            }

        } else {
            loadTypeIndex.value = -1;
            loadTypeIndex.value = 1;
            tagEvents.emit("alertEvent", { type: "info", title: "Not Found", message: "Coupons are not found" });
            categoryType = "wallet_prize";
            walletTranBo.value = true;
            couponLoadBo.value = false;
        }

    } else {


    }
}

function getCouponId() {
    var obsArray = couponType.toArray();
    console.log(JSON.stringify(obsArray))
    console.log("check" + coupon_Id);
    for (var i = 0; i < obsArray.length; i++) {

        if (coupon_Id == obsArray[i].coupon_id) {
            couponTypeIndex.value = i;
            console.log("got" + couponTypeIndex.value);


        }
    }

}
function CouponLoadChange(args) {
    // coupenId =JSON.parse(args.selectedItem).id;
    console.log(JSON.parse(args.selectedItem).coupon_id);
    coupon_Id = JSON.parse(args.selectedItem).coupon_id;

}
function walletOptionChangedEvent(args) {

    console.log(JSON.stringify(args.currencyCode))
    currencyCode.value = args.currencyCode;
    walletId = args.currencyCode;

}

function createPrizeFunction() {
    console.log(difficultValue);
    if (titleTxt.value == "") {
        tagEvents.emit("toastShow", { message: "Please add title of app" });
        return;
    }
    if (descriptionText.value == "") {
        tagEvents.emit("toastShow", { message: "Please add description of app" });
        return;
    }
    if (rad_km.value == "") {
        tagEvents.emit("toastShow", { message: "Please add radious from location" });
        return;
    }
    if (levelValue == "") {
        tagEvents.emit("toastShow", { message: "Please select level" });
        return;
    }
    if (difficultValue == "") {
        tagEvents.emit("toastShow", { message: "Please select difficulty" });
        return;
    }
    if (limitTxt.value == "") {
        tagEvents.emit("toastShow", { message: "Please add user limit" });
        return;
    }
    if (totalAvbleTxt.value == "") {
        tagEvents.emit("toastShow", { message: "Please add total available" });
        return;
    }

    console.log(categoryType);
    busy.activate();
    var apiBodyObj = {};
    if (categoryType == "coupon_prize") {
        apiBodyObj.type = "coupon_prize";

        if (coupon_Id == -1) {
            busy.deactivate();
            tagEvents.emit("toastShow", { message: "Please select Coupon" });
            return;

        }
        apiBodyObj.coupon_id = coupon_Id;
    } else {
        apiBodyObj.type = "wallet_prize";
        console.log(walletId);
        if (walletAmout.value == "") {
            busy.deactivate();
            tagEvents.emit("toastShow", { message: "Please add wallet amout" });
            return;

        }
        if (walletId == "") {
            busy.deactivate();
            tagEvents.emit("toastShow", { message: "Please select currency code" });
            return;

        }
        apiBodyObj.wallet_code = walletId;
        apiBodyObj.amount = walletAmout.value;

    }
    apiBodyObj.title = titleTxt.value;
    apiBodyObj.community_id = _model.nowCommunityID;
    apiBodyObj.description = descriptionText.value;
    apiBodyObj.location = JSON.stringify(locationData);
    apiBodyObj.view_range_km = rad_km.value;
    apiBodyObj.probability = difficultValue;
    apiBodyObj.level = levelValue;
    apiBodyObj.limits = limitTxt.value;
    apiBodyObj.total_available = totalAvbleTxt.value;
    if (createPriceBo.value == false) {
        apiBodyObj.deal_id = deal_Id.value;
        _WebService.request("tagchaser/UpdateGift", apiBodyObj).then(function (result) {
            gotUpdateRewardResultData(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    } else {

        _WebService.request("tagchaser/createGift ", apiBodyObj).then(function (result) {
            gotRewardSaveResultData(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }

}

function gotUpdateRewardResultData(result) {
    console.log("gotUpdateRewardResultData")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {

        //  console.log(b64Image)

        if (b64Image != null) {

            uploadAvatharUpdate();
        } else {

            router.goBack();

        }

    } else {

    }
}

function gotRewardSaveResultData(result) {
    console.log("gotRewardSaveResultData")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        deal_Id.value = arr.result.id;

        if (b64Image != null) {
            uploadAvatharUpdate();
        } else {
            router.goBack();

        }

    } else {
        tagEvents.emit("toastShow", { message: "Insufficient balance to create prize" });



    }
}

function uploadAvatharUpdate() {
    // update_DealID = deal_id;
    // console.log("deeeeeeel"+update_DealID)
    var apiBodyObj = {};
    apiBodyObj.image = b64Image;
    apiBodyObj.community_id = _model.nowCommunityID;
    _WebService.request("tagchaser/uploadGiftAvatar/" + deal_Id.value, apiBodyObj).then(function (result) {
        gotRewardImageUpdateResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}
function gotRewardImageUpdateResult(result) {

    console.log("gotRewardImageUpdateResult")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        router.goBack();

    } else {


    }
}
function activatePriceClick() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.deal_id = deal_Id.value;

    if (priceStatus.value == "active") {
        _WebService.request("deal/deactivate", apiBodyObj).then(function (result) {
            gotActDeactAdvtResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    } else {
        _WebService.request("deal/activate", apiBodyObj).then(function (result) {
            gotActDeactAdvtResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });

    }




}

function gotActDeactAdvtResult(result) {
    console.log("gotActDeactAdvtResult")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {

        router.goBack();

    } else {


    }
}
function giftDeleteClick() {
    tagEvents.emit("alertEvent", { type: "info", title: "Delete Reward", message: "Do you want to delete this Reward?", callback: alertCallback });


}
function alertCallback() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.deal_id = deal_Id.value;
    apiBodyObj.community_id = _model.nowCommunityID;
    _WebService.request("tagchaser/deleteGift", apiBodyObj).then(function (result) {
        gotRewardsDeleteResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotRewardsDeleteResult(result) {
    console.log("gotAdvtsDeleteResult")
    busy.deactivate();
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        tagEvents.emit("toastShow", { message: "Successfully Deleted" });
        router.goBack();

    } else {


    }
}

function cancelPriceFuction() {

    router.goBack();

}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    limitTxt: limitTxt,
    rad_km: rad_km,
    titleTxt: titleTxt,
    loadType: loadType,
    deal_Id: deal_Id,
    imgUpload: imgUpload,
    loadTypeIndex: loadTypeIndex,
    levelList: levelList,
    couponType: couponType,
    walletAmout: walletAmout,
    onViewActivateCreate: onViewActivateCreate,
    priceStatus: priceStatus,
    currencyCode: currencyCode,
    couponLoadBo: couponLoadBo,
    walletTranBo: walletTranBo,
    loadLocationMap: loadLocationMap,
    onImageUpload: onImageUpload,
    couponTypeIndex: couponTypeIndex,
    difficultyList: difficultyList,
    levelListIndex: levelListIndex,
    createPriceBo: createPriceBo,
    placeTagImage: placeTagImage,
    totalAvbleTxt: totalAvbleTxt,
    CouponLoadChange: CouponLoadChange,
    descriptionText: descriptionText,
    activatePriceClick: activatePriceClick,
    giftDeleteClick: giftDeleteClick,
    CategoryLoadChange: CategoryLoadChange,
    createPrizeFunction: createPrizeFunction,
    cancelPriceFuction: cancelPriceFuction,
    levelCategoryChange: levelCategoryChange,
    difficultyCategoryChange: difficultyCategoryChange,
    difficultyListIndex: difficultyListIndex,
    imageSelectPopupShow: imageSelectPopupShow,
    walletOptionChangedEvent: walletOptionChangedEvent,
    onUserImagegSelectionComplete: onUserImagegSelectionComplete

}