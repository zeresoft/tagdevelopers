var Observable = require('FuseJS/Observable');
var _WebService = require('Models/WebService');
var advrtListData = Observable();
var randomValue;

function onViewActivateCreate() {
    randomValue = Math.random();
    advrtListData.clear();
    getAdvertListData();
}

function pagingLoadMore() {
    if (advrtListData.length != 0) {
        getAdvertListData();
    }
}

function getAdvertListData() {
    busy.activate();
    var apiBodyObj = {};
    apiBodyObj.count = 20;
    apiBodyObj.offset = advrtListData.length;

    _WebService.request("tagwild/MyNewAdsList", apiBodyObj).then(function (result) {
        gotCreatedAdvrtListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotCreatedAdvrtListResult(result) {
    console.log("gotCreatedAdvrtListResult");

    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            if (resultArr[0].type == "Advertisement") {
                advrtListData.addAll(resultArr);
            }

        }
    }

}

function showDetailview(args) {
    args.randomvalue = randomValue;
    router.push("tagWildCreateAdvtPage", { args });
}

function createRedPacketClicked() {
    router.push("tagWildCreateAdvtPage");
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}


module.exports = {
    pagingLoadMore: pagingLoadMore,
    advrtListData: advrtListData,
    showDetailview: showDetailview,
    onViewActivateCreate: onViewActivateCreate,
    createRedPacketClicked: createRedPacketClicked,
}