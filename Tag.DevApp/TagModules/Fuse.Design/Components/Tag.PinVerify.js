var Observable = require('FuseJS/Observable');

var numberarray = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'clear', '0', 'back'];
var pinEntered = "";
var pinDisplayArr = Observable();

this.ShowPopup.onValueChanged(module, function (item) {
    console.log("---------------------------------");

})

function pinnumberClickHandler(args) {
    if (args.data == "clear") {
        pinEntered = "";
    } else if (args.data == "back") {
        console.log(pinEntered);
        pinEntered = pinEntered.slice(0, pinEntered.length - 1);
        console.log(pinEntered);
    } else {
        if (pinEntered.length < 4) {
            pinEntered = pinEntered + args.data;

            if (pinEntered.length == 4) {
                pinComplete();
            }
        }
    }
    pinDisplayArr.clear();
    var pinEnteredArr = pinEntered.split("");
    pinDisplayArr.addAll(pinEnteredArr);
}

function pinComplete() {
    pinEnteredEvent.raise({ pin: pinEntered });
}

module.exports = {
    numberarray: numberarray,
    pinDisplayArr: pinDisplayArr,
    pinnumberClickHandler: pinnumberClickHandler,
}