var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var recipientTypeItems = Observable({ index: 0, name: loc.value.user, value: "user" }, { index: 1, name: loc.value.merchant, value: "merchant" });
var recipientTypeIndex = Observable(0);

var userNameInput = Observable("");
var usersListResult = Observable();


function onRecipientTypeChange() {
    usersListResult.clear();
}

function searchUsePopup_clickHandler() {
    if (userNameInput.value !== "") {
        console.log("searchUsePopup_clickHandler");

        usersListResult.clear();
        busy.activate();
        var apiBodyObj = {};

        if (recipientTypeIndex.value == 0) {

            var checkIdInt = userNameInput.value;
            if (isNaN(checkIdInt)) {
                if (Validator.email(checkIdInt)) {
                    apiBodyObj.email = checkIdInt;
                } else {
                    apiBodyObj.name = checkIdInt;
                }
            } else {
                apiBodyObj.id = checkIdInt;
            }

            _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
                gotSearchUserResult(result);
            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });
        } else {
            apiBodyObj.name = userNameInput.value;
            _WebService.request("community/searchNew", apiBodyObj).then(function (result) {
                gotSearchCommunityResult(result);
            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });
        }
    }
}

function gotSearchUserResult(result) {
    console.log("gotSearchUserResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        usersListResultProcess(resultArr);
    }
}

function gotSearchCommunityResult(result) {
    console.log("gotSearchCommunityResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            usersListResultProcess(resultArr);
        }

    }
}

function usersListResultProcess(resultArr) {
    var resultListArr = [];
    for (var i = 0; i < resultArr.length; i++) {
        var addObj = {};

        addObj.id = resultArr[i].id;

        if (resultArr[i].community_name) {
            addObj.urlimage = _model.communityImageUrl + resultArr[i].id;
            addObj.name = resultArr[i].community_name;
        } else {
            addObj.urlimage = _model.userImageUrl + resultArr[i].id;
            addObj.name = resultArr[i].name;
        }
        resultListArr.push(addObj);
    }

    usersListResult.addAll(resultListArr);
}

function usersList_changeHandler(args) {
    if (recipientTypeIndex.value == 0) {
        userSelctedEvent.raise({selectedType: "user", selectedId: args.data.id, selectedName: args.data.name });
    }else{
        userSelctedEvent.raise({selectedType: "community", selectedId: args.data.id, selectedName: args.data.name });
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    userNameInput: userNameInput,
    searchUsePopup_clickHandler: searchUsePopup_clickHandler,
    usersListResult: usersListResult,
    usersList_changeHandler: usersList_changeHandler,

    recipientTypeItems: recipientTypeItems,
    recipientTypeIndex: recipientTypeIndex,
    onRecipientTypeChange:onRecipientTypeChange,
}