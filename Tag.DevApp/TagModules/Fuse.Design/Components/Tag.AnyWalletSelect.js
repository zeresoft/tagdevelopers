
var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletOptionsPopupShow = Observable(false);
var walletCatagoryItems = Observable();
var selectedWalletCatagoryIndex = Observable();
var walletTypeItems = Observable();
var selectedWalletTypeIndex = Observable();

var walletSelectID = "";
var walletSelectCode = "";

function wallerSelectButtonClicked() {
    walletSelectID = "";
    walletSelectCode = "";

    walletCategoryLoad();
    walletOptionsPopupShow.value = true;
}


function walletCategoryLoad() {
    if (_model.walletCatagoryList.value) {
        console.log("_model.walletCatagoryList.value " + _model.walletCatagoryList.value)

        selectedWalletCatagoryIndex.value = 0;
        walletCatagoryItems = _model.walletCatagoryList;

        walletTypeLoad(walletCatagoryItems.getAt(0).catagory);

    } else {
        walletCatagoryItems.clear();

        _WebService.request("wallet/category").then(function (result) {
            gotWalletCategoryResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }

}

function gotWalletCategoryResult(result) {
    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;

        for (var i = 0; i < resultArr.length; i++) {
            var itemObj = {};
            var keyName = resultArr[i] + "_title";
            itemObj.name = loc.value[keyName];
            itemObj.catagory = resultArr[i];
            walletCatagoryItems.add(itemObj);
        }

        selectedWalletCatagoryIndex.value = 0;
        _model.walletCatagoryList = walletCatagoryItems;

        walletTypeLoad(resultArr[0]);
    }
}


function onWalletCatagorySelectionChange(args) {
    console.log("onWalletCatagorySelectionChange")

    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))
    walletTypeLoad(selectedItem.catagory);
}

function walletTypeLoad(typeValue) {
    console.log("typeValue " + typeValue)
    walletTypeItems.clear();
    selectedWalletTypeIndex.value = -1;

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;
    apiBodyObj.wallet_type = typeValue;

    _WebService.request("wallet/types", apiBodyObj).then(function (result) {
        gotWalletTypesListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotWalletTypesListResult(result) {
    console.log("gotWalletTypesListResult")

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null && resultArr.length > 0) {
            walletSelectID = resultArr[0].wallet_id;
            walletSelectCode = resultArr[0].currency_code;

            selectedWalletTypeIndex.value = 0;
            walletTypeItems.addAll(resultArr);
        }
    }
}

function onWalletTypeSelectionChange(args) {
    console.log("onWalletTypeSelectionChange")

    var selectedItem = JSON.parse(args.selectedItem);
    console.log(JSON.stringify(selectedItem))
    walletSelectID = selectedItem.wallet_id;
    walletSelectCode = selectedItem.currency_code;
}

function walletOptionSelect() {
    if (walletSelectID != "") {
        walletOptionsPopupShow.value = false;
        walletSelctedEvent.raise({ walletId: walletSelectID, currencyCode: walletSelectCode });
    }
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    wallerSelectButtonClicked: wallerSelectButtonClicked,
    walletOptionsPopupShow: walletOptionsPopupShow,
    walletOptionSelect: walletOptionSelect,

    walletCatagoryItems: walletCatagoryItems,
    selectedWalletCatagoryIndex: selectedWalletCatagoryIndex,
    walletTypeItems: walletTypeItems,
    selectedWalletTypeIndex: selectedWalletTypeIndex,
    onWalletCatagorySelectionChange: onWalletCatagorySelectionChange,
    onWalletTypeSelectionChange: onWalletTypeSelectionChange,
}