var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var countryListPopupShow = Observable(false);
var countryListResult = Observable();

function showCountryList() {
    countryListPopupShow.value = true;
    console.log(JSON.stringify("countryListResult"))
    console.log(JSON.stringify(_model.countryList))
    countryListResult.clear();

    if (_model.countryList) {

        countryListResult.addAll(_model.countryList);
    } else {
        countryListResult.clear();
        busy.activate();

        _WebService.request("country/getcountries").then(function (result) {
            gotCountryListResult(result);
        }).catch(function (error) {
            console.log("Couldn't get data: " + error);
            showNetworkErrorMessage();
        });
    }

}

function gotCountryListResult(result) {
    console.log("gotCountryListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;

        countryListResult.addAll(resultArr);
        _model.countryList = resultArr;
    }
}

function onCountrySelectionChange(args) {
    console.log(JSON.stringify(args.data))

    countryChangeEvent.raise({ countryID: args.data.id, countryCode: args.data.country_code, countryName: args.data.country_name });
    countryListPopupShow.value = false;
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    showCountryList: showCountryList,
    countryListPopupShow: countryListPopupShow,
    countryListResult: countryListResult,
    onCountrySelectionChange: onCountrySelectionChange,

}