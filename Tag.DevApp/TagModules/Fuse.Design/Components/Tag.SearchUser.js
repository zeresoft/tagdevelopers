var Observable = require('FuseJS/Observable');
var Validator = require('Utils/Validator');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var userNameInput = Observable("");
var usersListResult = Observable();

var searchMerchant = false;

this.SearchMerchant.onValueChanged(module, function (item) {
    console.log("searchMerchant 1  " + item);
    // if (item) {
        console.log("searchMerchant 2  " + item);
        if (item == true) {
            console.log("item True");
            searchMerchant = true;
        }else{
            console.log("item false");
            searchMerchant = false;
        }

        usersListResult.clear();
        userNameInput.value = "";
    // }
})

function searchUsePopup_clickHandler() {
    if (userNameInput.value !== "") {

        usersListResult.clear();
        busy.activate();

        console.log("searchUsePopup_clickHandler");
        console.log(searchMerchant);
        var apiBodyObj = {};
        
        if (!searchMerchant) {

            var checkIdInt = userNameInput.value;
            if (isNaN(checkIdInt)) {
                if (Validator.email(checkIdInt)) {
                    apiBodyObj.email = checkIdInt;
                } else {
                    apiBodyObj.name = checkIdInt;
                }
            } else {
                apiBodyObj.id = checkIdInt;
            }

            _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
                gotSearchUserResult(result);
            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });
        } else {
            apiBodyObj.name = userNameInput.value;
            _WebService.request("community/searchNew", apiBodyObj).then(function (result) {
                gotSearchCommunityResult(result);
            }).catch(function (error) {
                console.log("Couldn't get data: " + error);
                showNetworkErrorMessage();
            });
        }
    }
}

function gotSearchUserResult(result) {
    console.log("gotSearchUserResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        usersListResultProcess(resultArr);
    }
}

function gotSearchCommunityResult(result) {
    console.log("gotSearchCommunityResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            usersListResultProcess(resultArr);
        }

    }
}

function usersListResultProcess(resultArr) {
    var resultListArr = [];
    for (var i = 0; i < resultArr.length; i++) {
        var addObj = {};

        addObj.id = resultArr[i].id;

        if (resultArr[i].community_name) {
            addObj.urlimage = _model.communityImageUrl + resultArr[i].id;
            addObj.name = resultArr[i].community_name;
        } else {
            if (_model.activePerspectiveType == "user") {
                addObj.urlimage = _model.userImageUrl + resultArr[i].id+"?kycImage=0";
            } else {
                addObj.urlimage = _model.userImageUrl + resultArr[i].id;
            }
    
            addObj.name = resultArr[i].name;
        }
        resultListArr.push(addObj);
    }

    usersListResult.addAll(resultListArr);
}

function usersList_changeHandler(args) {
    userSelctedEvent.raise({ selectedId: args.data.id, selectedName: args.data.name });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    userNameInput: userNameInput,
    searchUsePopup_clickHandler: searchUsePopup_clickHandler,
    usersListResult: usersListResult,
    usersList_changeHandler: usersList_changeHandler,
}