
var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletOptionsPopupShow = Observable(false);
var walletsListData = Observable();

function wallerSelectButtonClicked() {
    walletOptionsPopupShow.value = true;
    if (walletsListData.length == 0) {
        walletsListLoad();
    }
}

function walletsListLoad() {
    console.log("walletsListLoad")
    walletsListData.clear();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;

    _WebService.request("wallet/list", apiBodyObj).then(function (result) {
        gotWalletListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotWalletListResult(result) {
    console.log("gotWalletListResult")

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            walletsListData.addAll(resultArr);
        }

    }
}

function walletOptionSelect(args) {
    console.log(JSON.stringify(args.data))
    walletOptionsPopupShow.value = false;
    
    walletSelctedEvent.raise({ walletId: args.data.wallet_id, currencyCode: args.data.currency_code });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

function createTokenClickHandler() {
    walletOptionsPopupShow.value = false;
    tokenCreateClickEvent.raise();
}

module.exports = {
    wallerSelectButtonClicked: wallerSelectButtonClicked,
    walletsListData: walletsListData,
    walletOptionsPopupShow: walletOptionsPopupShow,
    walletOptionSelect: walletOptionSelect,
    createTokenClickHandler: createTokenClickHandler,
}