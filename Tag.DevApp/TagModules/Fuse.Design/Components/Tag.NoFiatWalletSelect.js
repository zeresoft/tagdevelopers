var Observable = require('FuseJS/Observable');

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var walletOptionsPopupShow = Observable(false);
var walletsListData = Observable();
var searchWalletkeyword = Observable("");



function wallerSelectButtonClicked() {
    walletOptionsPopupShow.value = true;
}

function searchWalletClickHandler() {
    if (searchWalletkeyword.value != "") {
        walletsListData.clear();
        walletTypeLoad(searchWalletkeyword.value);
    }
}

function walletTypeLoad(keySerach) {
    console.log("typeValue " + keySerach)
    busy.activate();

    var apiBodyObj = {};
    apiBodyObj.new_call = 1;
    apiBodyObj.currency_code = keySerach;

    _WebService.request("wallet/types", apiBodyObj).then(function (result) {
        gotWalletTypesListResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });
}

function gotWalletTypesListResult(result) {
    console.log("gotWalletTypesListResult")
    busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null && resultArr.length > 0) {

            var walletsFilterArr = [];
            for (var i = 0; i < resultArr.length; i++) {
                if (resultArr[i].wallet_type_numeric == 1 || resultArr[i].wallet_type_numeric == 3) {
                    walletsFilterArr.push(resultArr[i]);
                }
            }

            walletsListData.addAll(walletsFilterArr);
        }
    }
}

function walletOptionSelect(args) {
    console.log(JSON.stringify(args.data))
    walletOptionsPopupShow.value = false;

    walletSearchSelctedEvent.raise({ walletId: args.data.wallet_id, currencyCode: args.data.currency_code });
}

function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    wallerSelectButtonClicked: wallerSelectButtonClicked,
    walletsListData: walletsListData,
    walletOptionsPopupShow: walletOptionsPopupShow,
    walletOptionSelect: walletOptionSelect,

    searchWalletkeyword: searchWalletkeyword,
    searchWalletClickHandler: searchWalletClickHandler,
}