var Observable = require('FuseJS/Observable');
var Preferences = require('Utils/Preferences');

var unoproj = require("AppVersion/UnoProject");

var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');


var requestsListAvailable = Observable(false);
var requestsListLength = Observable(0);


var menuAreaDisplay = Observable(true);
var merchantMenuShow = Observable(false);
var userMenuShow = Observable(false);

var demoTtletextShow = Observable(false);
var versionString = unoproj.version;

_model.logedinToDemoMode.onValueChanged(module, function (item) {
    console.log("------------------------logedinToDemoMode changed " + item);
    demoTtletextShow.value = item;
});

function userDisplayArea_clickHandler() {
    // if (_model.activePerspectiveType != "user") {
    //     if (_model.nowCommunityRoleType != "member") {
    menuAreaDisplay.value = !menuAreaDisplay.value;
    //     }
    // }
}

function onMenuButtonsShowEvent() {
    menuAreaDisplay.value = true;
}

tagEvents.on("profileRefresh", function (arg) {
    if (_model.activePerspectiveType == "user") {
        userMenuShow.value = true;
        merchantMenuShow.value = false;
    } else {
        userMenuShow.value = false;
        merchantMenuShow.value = true;
    }
});

function walletButtonClicked() {
    tagEvents.emit("menuCloseEvent");
    router.goto("walletListPage");
}

function verificationClicked() {
    tagEvents.emit("menuCloseEvent");

    if (_model.activePerspectiveType == "user") {
        router.push("kycVerificationUserPage");
    } else {
        router.push("kycVerifyMerchantPage");
    }
}

function memberListClicked() {
    tagEvents.emit("menuCloseEvent");
    router.push("merchantSearchPage");
}

function rolesClicked() {
    tagEvents.emit("menuCloseEvent");
    router.push("rolesListPage");
}

function advertisingClicked() {
    tagEvents.emit("menuCloseEvent");
    router.push("adTypesPage");
}

function settingsClicked() {
    tagEvents.emit("menuCloseEvent");
    router.push("settingsPage");
}

function identifiersButtonClicked() {
    tagEvents.emit("menuCloseEvent");
    router.push("createIdentifierPage");
}

function logoutClicked() {
    tagEvents.emit("menuCloseEvent");



    Preferences.write("email", null);
    Preferences.write("access_token", null);
    Preferences.write("refresh_token", null);
    Preferences.write("token_type", null);
    Preferences.write("expires_time", null);

    router.goto("loginPage");
}

function merchantsListClicked() {
    tagEvents.emit("menuCloseEvent");
    router.push("merchantsListPage");
}



var backColorShowUser = Observable(true);
_model.perspectiveIsUser.onValueChanged(module, function (item) {
    console.log("perspectiveIsUser changed " + item)
    backColorShowUser.value = item;

    requestsListAvailable.value = false;

    if (item) {
        checkContactsPending();
    }
});


function checkContactsPending() {
    requestsListAvailable.value = false;

    if (_model.accessToken != undefined) {
        _WebService.request("contact/getcontactrequests").then(function (result) {
            gotRequestsListResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    }
}

function gotRequestsListResult(result) {
    console.log("gotRequestsListResult");

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr !== null) {
            requestsListLength.value = resultArr.length;
            requestsListAvailable.value = true;
        } else {
            requestsListLength.value = 0;
            requestsListAvailable.value = false;
        }
    }
}


tagEvents.on("contactNoRefresh", function (arg) {
    console.log("contactNoRefresh", arg.number)
    if (arg.number == 0) {
        requestsListLength.value = 0;
        requestsListAvailable.value = false;
    } else {
        requestsListLength.value = arg.number;
        requestsListAvailable.value = true;
    }

});

function contactListClicked() {
    tagEvents.emit("menuCloseEvent");
    var requestStatus = "no";
    if (requestsListAvailable.value == true) {
        requestStatus = "yes";
    }
    router.push("contactsPage", { requestStatus: requestStatus, datId: Math.random() });
}

module.exports = {
    versionString: versionString,
    menuAreaDisplay: menuAreaDisplay,
    merchantMenuShow: merchantMenuShow,
    userMenuShow: userMenuShow,
    onMenuButtonsShowEvent: onMenuButtonsShowEvent,
    userDisplayArea_clickHandler: userDisplayArea_clickHandler,
    walletButtonClicked: walletButtonClicked,
    verificationClicked: verificationClicked,
    memberListClicked: memberListClicked,
    rolesClicked: rolesClicked,
    advertisingClicked: advertisingClicked,
    identifiersButtonClicked: identifiersButtonClicked,
    settingsClicked: settingsClicked,
    logoutClicked: logoutClicked,

    backColorShowUser: backColorShowUser,
    requestsListAvailable: requestsListAvailable,
    requestsListLength: requestsListLength,

    merchantsListClicked: merchantsListClicked,
    contactListClicked: contactListClicked,
    demoTtletextShow: demoTtletextShow,


}


