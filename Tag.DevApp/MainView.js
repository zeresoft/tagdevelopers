var Observable = require('FuseJS/Observable');
var Localization = require("Locales/Localization");
var EventEmitter = require("FuseJS/EventEmitter");

var QrScanner = require("QrScanner");
var nfcReader = require("NfcScannerManager");
var _model = require('Models/GlobalModel');
var _WebService = require('Models/WebService');

var logomdpi = Observable("Logo/logomdpi.png");
var logohdpi = Observable("Logo/logohdpi.png");
var logoxhdpi = Observable("Logo/logoxhdpi.png");
var logoxxhdpi = Observable("Logo/logoxxhdpi.png");
var logoxxxhdpi = Observable("Logo/logoxxxhdpi.png");

var nfcScanPossible = false;

var topBackColorShowUser = Observable(true);
var navigatorOpenStatus = Observable(false);

var activeShopId = "";
var activeShopName = "";
var cartItemsCount = Observable(0);
var identifierValue = null;

tagEvents = new EventEmitter("toastShow", "alertEvent", "menuCloseEvent", "dashboardShowEvent", "profileRefresh", "qrScanEvent", "nfcAddScanedEvent", "nfcChargeScanedEvent", "statusSearchEvent", "contactNoRefresh", "cartItemsCountEmit", "scratchPlayEvent", "transactionFiletrEvent", "shopFeedbackEvent");

tagEvents.on("dashboardShowEvent", function (arg) {
    // _model.merchantLinked = "";

    loadDashboard();
});

function loadDashboard() {
    router.goto("dataListingPage");

    // if (_model.activePerspectiveType == "user") {
    //     router.goto("userShoppingPage", { showTitle: true, datId: Math.random() });
    // } else {
    //     router.goto("merchantShopPage", { showTitle: true, datId: Math.random() });
    // }
}

tagEvents.on("menuCloseEvent", function (arg) {
    navigatorOpenStatus.value = false;
});

_model.perspectiveIsUser.onValueChanged(module, function (item) {
    console.log("perspectiveIsUser changed " + item);
    topBackColorShowUser.value = item;
});

function enableNfcScan() {
    console.log("---**** enableNfcScan")
    nfcScanPossible = true;
    nfcReader.nfcScannInit("start");
}

function disableNfcScan() {
    console.log("---**** disableNfcScan")
    nfcScanPossible = false;
}

function loadQRPage() {
    tagEvents.emit("qrScanEvent", { callback: qrReceivedHandler });
}

function qrReceivedHandler(scanData) {
    console.log("scanData qr manage" + scanData)
    tagEvents.emit("alertEvent", { type: "info", title: "QR Code", message: scanData });

}

var qrCallbackFunction = null;

tagEvents.on("qrScanEvent", function (arg) {
    qrCallbackFunction = arg.callback;

    QrScanner.Init("QRCODE", false, true);
    QrScanner.Launch();
});

//QReceived event returns Object containing
//"format" - data format
//"result" - data received when scanned
QrScanner.on("QRECEIVED", function (message) {
    var scanData = message.result;
    console.log("scanData scanner" + scanData)
    qrCallbackFunction(scanData);
});


nfcReader.onNfcReceived = function (message) {
    console.log("onNfcReceived " + message);
    console.log("nfcScanPossible " + nfcScanPossible);
    console.log("nfcScanPossible " + _model.nfcReturnTarget);
    identifierValue = null;

    if (nfcScanPossible) {
        var scanData = message;

        router.getRoute(function (route) {
            console.log(JSON.stringify(route))

            if (route[0] === "walletChargePage") {
                tagEvents.emit("nfcChargeScanedEvent", { scanData: scanData });
            } else {
                if (_model.nfcReturnTarget == "search") {
                    searchIdentifierList(scanData);
                } else if (_model.nfcReturnTarget == "add") {
                    tagEvents.emit("nfcAddScanedEvent", { scanData: scanData });
                }
            }
        })
    }
};

function searchIdentifierList(searchValue) {
    // busy.activate();

    identifierValue = searchValue;

    var apiBodyObj = {};
    apiBodyObj.search = searchValue;

    _WebService.request("Identifiers/", apiBodyObj).then(function (result) {
        gotIdentifiersSearchResult(result);
    }).catch(function (error) {
        console.log("Couldn't get data: " + error);
        showNetworkErrorMessage();
    });

}

function gotIdentifiersSearchResult(result) {
    console.log("gotIdentifiersSearchResult")
    // busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length != 0) {
            if (resultArr[0].linked_to == "user") {
                searchUsersHandler(1, resultArr[0].user_id);
            } else {
                searchUsersHandler(2, resultArr[0].merchant_id);
            }
        }
    } else {
        //not valid
        identifierValue = null;
    }
}



function searchUsersHandler(userType, id) {
    // busy.activate();

    console.log("searchUsePopup_clickHandler");
    var apiBodyObj = {};

    if (userType == 1) {
        apiBodyObj.id = id;

        _WebService.request("user/searchuser", apiBodyObj).then(function (result) {
            gotSearchUserResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    } else {
        apiBodyObj.name = id;
        _WebService.request("community/searchNew", apiBodyObj).then(function (result) {
            gotSearchCommunityResult(result);
        }).catch(function (error) {
            showNetworkErrorMessage();
        });
    }
}

function gotSearchUserResult(result) {
    console.log("gotSearchUserResult")
    // busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            usersListResultProcess(resultArr[0], "user");
        }
    }
}

function gotSearchCommunityResult(result) {
    console.log("gotSearchCommunityResult")
    // busy.deactivate();

    var arr = result;
    console.log(JSON.stringify(arr))

    if (arr.status == "success") {
        var resultArr = arr.result;
        if (resultArr.length !== 0) {
            usersListResultProcess(resultArr[0], "community");
        }
    }
}

var nfcResultUser;
var nfcUserType;

function usersListResultProcess(resultUser, userType) {
    nfcResultUser = resultUser;
    nfcUserType = userType;

    // tagEvents.emit("alertEvent", { type: "confirm", title: "NFC tag", message: "NFC tag detected, do you want to continue?", callback: confirmNfctagClosedHandler });

    //confirm removed based on task list
    usersListResultProcessConfirmed();
}

function confirmNfctagClosedHandler(args) {
    if (args === "yes") {
        usersListResultProcessConfirmed();
    }
}

function usersListResultProcessConfirmed() {
    resultUser = nfcResultUser;
    userType = nfcUserType;

    console.log("usersListResultProcess")
    console.log("userType " + userType)

    if (_model.activePerspectiveType == "user") {
        if (userType == "community") {
            router.push("merchantScanMenuPage", { resultUser: resultUser, perspective: "user", identifier: identifierValue, datId: Math.random() });
        } else {
            router.push("userScanMenuPage", { resultUser: resultUser, identifier: identifierValue, datId: Math.random() });
        }
    } else {
        if (userType == "community") {
            router.push("merchantScanMenuPage", { resultUser: resultUser, perspective: "community", identifier: identifierValue, datId: Math.random() });
        } else {

            var roleStatusScaned = "notapproved";
            var roleTypeScaned = 0;
            var roleNameScaned = "Non Member";

            if (resultUser.role) {
                if (resultUser.role.role_status == "approved") {
                    roleStatusScaned = "approved";
                    roleTypeScaned = resultUser.role.role_type;
                    roleNameScaned = resultUser.role.role_name;
                }
            }

            router.push("merchantSearchResultPage", { username: resultUser.name, rating: resultUser.rating, role: roleNameScaned, role_status: roleStatusScaned, role_type: roleTypeScaned, userID: resultUser.id, identifier: identifierValue, datId: Math.random() });
        }
    }


}

function loadFavoriteProducts() {
    router.push("favoriteListPage", { datId: Math.random() });
}

tagEvents.on("cartItemsCountEmit", function (arg) {
    console.log("cartItemsCountEmit", JSON.stringify(arg));
    activeShopId = arg.shop_id;
    activeShopName = arg.shop_name;
    getShoppingCartItems();
});

function getShoppingCartItems() {
    console.log("getShoppingCartItems");
    cartItemsCount.value = 0;

    _WebService.request("shop/Cart", { shop_id: activeShopId }).then(function (result) {
        gotShoppingCartItems(result);
    }).catch(function (error) {
        showNetworkErrorMessage();
    });
}

function gotShoppingCartItems(result) {
    console.log("gotShoppingCartItems");

    var arr = result;
    console.log(JSON.stringify(arr));

    if (arr.status == "success") {
        var resultArr = arr.list;
        var totalqty = 0;

        for (var i = 0; i < resultArr.length; i++) {
            totalqty = totalqty + resultArr[i].qty;
        }

        cartItemsCount.value = totalqty;
    }
}

function checkOutPageClickHandler() {
    router.push("checkOutPage", { shop_id: activeShopId, shop_title: activeShopName, datId: Math.random() });
}


function chatsListClicked() {
    if (_model.logedinToDemoMode.value == true) {
        router.goto("chatTabDiscover");
    } else {
        tagEvents.emit("alertEvent", { type: "info", title: "CHAT", message: "Blog and chat coming soon" });
    }
}

function filterButtonClicked() {
    tagEvents.emit("transactionFiletrEvent");
}

function feedbackButtonClicked() {
    tagEvents.emit("shopFeedbackEvent");
}


function showNetworkErrorMessage() {
    busy.deactivate();
    tagEvents.emit("alertEvent", { type: "info", title: loc.value.error, message: loc.value.network_error_message });
}

module.exports = {
    loc: Localization.loc,
    tagEvents: tagEvents,
    topBackColorShowUser: topBackColorShowUser,
    navigatorOpenStatus: navigatorOpenStatus,

    loadDashboard: loadDashboard,
    loadQRPage: loadQRPage,

    enableNfcScan: enableNfcScan,
    disableNfcScan: disableNfcScan,
    loadFavoriteProducts: loadFavoriteProducts,
    cartItemsCount: cartItemsCount,
    chatsListClicked: chatsListClicked,
    checkOutPageClickHandler: checkOutPageClickHandler,
    filterButtonClicked: filterButtonClicked,
    feedbackButtonClicked: feedbackButtonClicked,

    logomdpi: logomdpi,
    logohdpi: logohdpi,
    logoxhdpi: logoxhdpi,
    logoxxhdpi: logoxxhdpi,
    logoxxxhdpi: logoxxxhdpi,

};

